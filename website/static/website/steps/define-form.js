$(function() {
    var $activity = $('[name="activity"]');
    var mapping = $activity.data('mapping');
    var iovFields = ['Conditional Cash Transfer', 'Providing Business Grants', 'Unconditional Cash Transfer']

    $activity 
        .on('change', activityChanged)
        .trigger('change');

    function hideAllParameters() {
        $('[name^="parameter"]')
            .parents('.form-group')
            .addClass('hidden');
    }

    function hideOutputCountSource() {
        $('[name="output_count_source"]')
            .parents('.form-group')
            .addClass('hidden');
    }

    function activityChanged() {
        var activity = $activity.val();
        hideAllParameters();
        hideOutputCountSource()
        if (activity) {
            var parameters = mapping[activity];
            var parameterSelectors = parameters.map(function(parameterName) {
                return '[name="parameter__' + parameterName + '"]';
            });
            var $inputs = $(parameterSelectors.join(', '));
            $inputs.parents('.form-group').removeClass('hidden');

            // show output count source field if activity is not IOV
            if ($.inArray($('[name="activity"] option:selected').text(), iovFields) === -1) {
                $('[name="output_count_source"]')
                .parents('.form-group')
                .removeClass('hidden');
            }
        }
    }
});

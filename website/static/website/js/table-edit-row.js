$(function() {

    function closeEditRow(rowElement) {
        rowElement.closest('.analysis-table__tbody').removeClass('analysis-table__tbody--edit-active')
          .find('.analysis-table__edit-row--active').removeClass('analysis-table__edit-row--active');
        rowElement.removeClass('analysis-table__edit-row--active');
    }

    $('.analysis-table__edit-row').each(function () {
        var $editRow = $(this)
        var $cancelButton = $editRow.find('button[value="cancel"]')
        var $saveButton = $editRow.find('button[value="save_cost_line_item_config"]')


        if ($editRow.hasClass('allocate-costs')) {
            var $noteInput = $editRow.find('textarea[name="note"]');
            $saveButton.on('click', function (evt) {
                $saveButton.attr('disabled', true)
                evt.preventDefault();

                var endpoint = $saveButton.data('endpoint')
                var noteContent = $noteInput.val()
                var pk = $saveButton.data('object-id')
                var crsf = $editRow.find('input[name="csrfmiddlewaretoken"]').val()
                var data = {
                        id: pk,
                        note: noteContent,
                }

                $.ajax({
                    type: "POST",
                    url: endpoint,
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    headers: {
                        'X-CSRFToken': crsf
                    }
                }).then(function (r) {
                    $saveButton.attr('disabled', false)
                    closeEditRow($editRow)
                    var button = $editRow.closest('tbody ').find('.analysis-table__edit-cell button');
                    if (noteContent) {
                        button.html('View note').addClass('analysis-table__category-edit--note-active');
                    } else {
                        button.html('Add note').removeClass('analysis-table__category-edit--note-active')
                    }

                }).fail(function (e) {
                    console.log(e)
                    $saveButton.attr('disabled', false)
                });
                return false
            })
        }

        if ($editRow.hasClass('confirm-categories')) {
            var $sectorInput = $editRow.find('select[name="sector_id"]');
            var $categoryInput = $editRow.find('select[name="category_id"]');
            $saveButton.on('click', function (evt) {
                evt.preventDefault();
                $saveButton.attr('disabled', true)

                var endpoint = $saveButton.data('endpoint')
                var initialSector = parseInt($sectorInput.closest('form').data('initial-sector_id'))
                var initialCategory = parseInt($sectorInput.closest('form').data('initial-category_id'))
                var sector = parseInt($sectorInput.val())
                var category = parseInt($categoryInput.val())
                if (initialCategory === category && sector === initialSector) {
                    $saveButton.attr('disabled', false)
                    closeEditRow($editRow)
                } else {
                    var pk = $saveButton.data('object-id');
                    var crsf = $editRow.find('input[name="csrfmiddlewaretoken"]').val()
                    var data = {
                        id: pk,
                        sector_id: sector,
                        category_id: category,
                    }
                    $.ajax({
                        type: "POST",
                        url: endpoint,
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        headers: {
                            'X-CSRFToken': crsf
                        }
                    }).then(function (r) {
                        $saveButton.attr('disabled', false)
                        closeEditRow($editRow)
                        $editRow.closest('tbody').remove()
                    }).fail(function (e) {
                        console.log(e)
                        $saveButton.attr('disabled', false)
                    });
                }

                return false
            })
        }


        $cancelButton.on('click', function (evt) {
            evt.preventDefault();
            closeEditRow($editRow)
            return false;
        })


    });

});

$(function() {

    function initializeInsightsChart(canvas) {
        var ctx = canvas.getContext('2d');
        var $canvas = $(canvas);
        var data = $canvas.data('insights-chart-data');
        var numItems = data.length;
        var maxValue = data.reduce(function(highest, dataPoint) {
            return Math.max(highest, dataPoint.output_cost_all, dataPoint.output_cost_direct_only);
        }, 0);
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: data.map(pick('label')),
                datasets: [{
                    label: $canvas.data('direct-only-text'),
                    data: data.map(pick('output_cost_direct_only')),
                    backgroundColor: data.map(function(item) {
                        return item.highlight ? '#1b77d4' : '#9CC6F2';
                    }),
                    borderColor: new Array(numItems).fill('#FFFFFF'),
                    borderWidth:2,
                },
                {
                    label: $canvas.data('all-text'),
                    data: data.map(pick('output_cost_all')),
                    backgroundColor: data.map(function(item) {
                        return item.highlight ? '#46a458' : '#C1E8C8';
                    }),
                    borderColor: new Array(numItems).fill('#FFFFFF'),
                    borderWidth:2,
                }]
            },
            options: {
                indexAxis: 'y',
                maintainAspectRatio: false,
                defaultFontFamily: 'Arial',
                defaultFontSize: '14',
                plugins: {
                    legend: {
                        display: false
                    }
                },
                tooltips: {
                    enabled: false
                },
                scales: {
                    x: [{
                        stacked: false,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                        // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return getDioptraCurrencySymbol() + value;
                                // return '$' + value;
                            },
                            maxTicksLimit: 2,
                            beginAtZero: true,
                            max: maxValue,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: $canvas.data('x-axis-label')
                        }
                    }],
                    y: [{
                        barPercentage: 1.0,
                        categoryPercentage: 1.0,
                        ticks: {
                            beginAtZero: true
                        },
                        stacked: true,
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });
    }

    $('.insights-chart').each(function(i, canvas) {
        initializeInsightsChart(canvas);
    });
});

$(function() {
    if ($('#insightsPie').length) {
        var ctx = document.getElementById('insightsPie').getContext('2d');
        var data = $('#insightsPie').data('pie-chart-data');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [{
                    data: data.map(pick('percent_of_total')),
                    backgroundColor: [
                        '#144678',
                        '#2379d1',
                        '#5ed2d9',
                        '#4aa35b',
                        '#ede46d'
                    ].slice(0, data.length),
                }],
                labels: data.map(pick('label')),
            },
            options: {
                indexAxis: 'y',
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        beginAtZero: false
                    }
                },
                maintainAspectRatio: false,
                defaultFontFamily: 'Arial',
                defaultFontSize: '14',
            }
        });
        myChart.canvas.parentNode.style.height = (120 * data.length) + 'px';
    }
});


if (!Array.prototype.fill) {
  Object.defineProperty(Array.prototype, 'fill', {
    value: function(value) {

      // Steps 1-2.
      if (this == null) {
        throw new TypeError('this is null or not defined');
      }

      var O = Object(this);

      // Steps 3-5.
      var len = O.length >>> 0;

      // Steps 6-7.
      var start = arguments[1];
      var relativeStart = start >> 0;

      // Step 8.
      var k = relativeStart < 0 ?
        Math.max(len + relativeStart, 0) :
        Math.min(relativeStart, len);

      // Steps 9-10.
      var end = arguments[2];
      var relativeEnd = end === undefined ?
        len : end >> 0;

      // Step 11.
      var final = relativeEnd < 0 ?
        Math.max(len + relativeEnd, 0) :
        Math.min(relativeEnd, len);

      // Step 12.
      while (k < final) {
        O[k] = value;
        k++;
      }

      // Step 13.
      return O;
    }
  });
}

function pick(key) {
  return function(obj) {
    if (obj.hasOwnProperty(key)) {
      return obj[key];
    }
    return undefined;
  }
}
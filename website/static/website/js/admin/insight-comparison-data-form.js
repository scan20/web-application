$(function() {
    var $activity = $('[name="activity"]');
    var activityParameterMapping = $activity.data('activity-parameter-mapping');
    var activityOutputMetricMapping = $activity.data('activity-output-metric-mapping');

    $activity 
        .on('change', activityChanged)
        .trigger('change');

    function hideAllParameters() {
        $('[name^="parameter"]')
            .parents('.form-group')
            .addClass('hidden');
    }

    function hideOutputMetricCosts() {
        $('[name^="output_cost"]')
            .parents('.form-group')
            .addClass('hidden');
    }

    function activityChanged() {
        var activity = $activity.val();
        hideAllParameters();
        hideOutputMetricCosts();
        if (activity) {
            var parameters = activityParameterMapping[activity];
            var outputMetricCosts = activityOutputMetricMapping[activity];
            var selectors = [];

            selectors = parameters.reduce(function(acc, parameterName) {
                acc.push('[name="parameter__' + parameterName + '"]');
                return acc;
            }, selectors);

            selectors = outputMetricCosts.reduce(function(acc, outputMetricName) {
                acc.push('[name="output_cost__all__' + outputMetricName + '"]');
                acc.push('[name="output_cost__direct_only__' + outputMetricName + '"]');
                return acc
            }, selectors);

            var $inputs = $(selectors.join(', '));
            $inputs.parents('.form-group').removeClass('hidden');
        }
    }
});

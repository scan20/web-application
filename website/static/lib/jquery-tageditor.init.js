$(function() {
  $(':input[data-tageditor]').each(function(i, input) {
    var $input = $(input);
    var options = $input.data('tageditor');
    if (options == undefined || options == '') {
        options = {};
    }

    options['onChange'] = function(field, editor, tags) {
        $(field).trigger('change');
    }

    $input.tagEditor(options);
    if ($input.attr('disabled') == "disabled") {
        $input.next('.tag-editor').addClass('disabled');
    }
  });
});

/**
 * Plugin to interface with ombumess video asset files.
 *
 * Problems:
 *
 * The default object selector uses `showRelatedObjectLookup()` (in
 * RelatedObjectLookups.js) to show the pop-up.  But the pop-up links are
 * hardcoded with `dismissRelatedLookupPopup(win, "chosenId")` so it's not
 * possible to have custom handlers or to return extra info about the object.
 */
(function() {

  CKEDITOR.plugins.add('ombuvideo', {
    requires: 'widget,ombuutil',
    icons: 'ombuvideo',
    init: function(editor) {

      var util = CKEDITOR.ombuutil;

      // Register Dialog.
      CKEDITOR.dialog.add('ombuvideo', this.path + 'dialogs/dialog.js')

      // Add the stylesheet to the editor.
      var pluginDirectory = this.path;
      editor.addContentsCss(pluginDirectory + 'styles/ombuvideo.css');

      // Add the stylesheet to the base page for dialog styles.
      util.addPageStylesheet(pluginDirectory + 'styles/ombuvideo.css');

      editor.widgets.add('ombuvideo', {
        button: 'Video',
        template: '<div data-ombuvideo=""></div>',
        allowedContent: 'div[data-ombuvideo]',
        requiredContent: 'div[data-ombuvideo]',
        dialog: 'ombuvideo',
        upcast: function(element) {
          return element && element.name == 'div' && element.attributes['data-ombuvideo'];
        },
        init: function() {
          // Populate widget data from DOM.
          var widget = this;
          var widget = this;
          var settings = util.unpackWidgetSettings(widget, 'data-ombuvideo');
        },
        data: function() {
          // Move widget data into DOM.
          var widget = this;
          var settings = util.packWidgetSettings(widget, 'data-ombuvideo');
          if (settings.objInfo) {
            var previewHtml = settings.objInfo.image_url ? '<img src="' + settings.objInfo.image_url + '" />' : '';
            widget.element.setHtml(previewHtml);
          }
        }
      });

    }
  });

})();

from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from website.app_log.loggers import log_help_item_created
from website.help.fields import HelpItemType, HELP_CATEGORIES, HELP_SECTORS, format_identifier, format_title
from website.help.models import HelpItem
from website.models import Category, Sector

User = get_user_model()


@receiver(post_save, sender=Category)
def save_category_help_items(sender, instance, created, **kwargs):
    for definition in HELP_CATEGORIES:
        identifier = format_identifier(definition.identifier, category=instance.name)
        try:
            HelpItem.objects.get(identifier=identifier)
        except HelpItem.DoesNotExist:
            hi = HelpItem.objects.create(
                type=definition.type,
                title=format_title(definition.title, category=instance.name),
                identifier=identifier
            )
            log_help_item_created(hi)


@receiver(post_save, sender=Sector)
def save_sector_help_items(sender, instance, created, **kwargs):
    for definition in HELP_SECTORS:
        identifier = format_identifier(definition.identifier, sector=instance.name)
        try:
            HelpItem.objects.get(identifier=identifier)
        except HelpItem.DoesNotExist:
            hi = HelpItem.objects.create(
                type=definition.type,
                title=format_title(definition.title, sector=instance.name),
                identifier=identifier
            )
            log_help_item_created(hi)


@receiver(post_delete, sender=Category)
def delete_category_help_items(sender, instance, *args, **kwargs):
    HelpItem.objects.filter(identifier__in=[format_identifier(category.identifier, category=instance.name)
                                            for category in HELP_CATEGORIES]).delete()

@receiver(post_delete, sender=Sector)
def delete_sector_help_items(sender, instance, *args, **kwargs):
    HelpItem.objects.filter(identifier__in=[format_identifier(sector.identifier, sector=instance.name)
                                            for sector in HELP_SECTORS]).delete()


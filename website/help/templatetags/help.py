from django import template
from django.template.loader import render_to_string
from functools import lru_cache
from django.utils.safestring import mark_safe

from website.help.models import HelpItem

register = template.Library()


def user_can_edit(user):
    return user.has_perm('website.change_helpitem')

@lru_cache(maxsize=128)
def render_help(user_can_edit_help, identifier=None):

    if identifier:
        item = HelpItem.objects.filter(identifier=identifier).first()
        if item and item.help_text:
            context = {
                    "identifier": identifier,
                    "help_text": item.help_text,
                    "title": item.title,
                    "link": item.link,
                    "item": item,
                    "user_can_edit_help": user_can_edit_help,
                }
            markup = render_to_string(
                "help/_help-item.html",
                context
            )
            return mark_safe(markup)  # nosec
    return str()


@register.simple_tag(takes_context=True, name="help")
def help_item(context, identifier=None):
    return render_help(user_can_edit(context.get('user')), identifier)


@register.simple_tag(takes_context=True, name="help_category")
def help_category(context, category, identifier):
    identifier = f"{identifier.strip('_')}__{category.lower().replace(' ', '_')}_category"
    return render_help(user_can_edit(context.get('user')), identifier)


@register.simple_tag(takes_context=True, name="help_sector")
def help_sector(context, sector, identifier):
    identifier = f"{identifier.strip('_')}__{sector.lower().replace(' ', '_')}_sector"
    return render_help(user_can_edit(context.get('user')), identifier)


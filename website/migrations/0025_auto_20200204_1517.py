# Generated by Django 2.2.3 on 2020-02-04 23:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0024_auto_20200124_0859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='ci_grant_code',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Grant Code'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='ci_sector_code',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Sector Code'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='ci_site_code',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Site Code'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='ci_total_cost',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Total Cost'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='tr_amount',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Amount'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='tr_date',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='fieldlabeloverrides',
            name='tr_site_code',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Site Code'),
        ),
    ]

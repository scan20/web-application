from django.db import migrations, models


# Data migration for making some string columns non-nullable,
# by setting an empty string in the place of null.
# See 0030 for more details.
#
# Ideally we'd have these combined, but combing the data and schema migration results in a
# 'pending trigger events' error during the migration due to the way Django migrations work
# (this is not the case in many other ORMs).
#
# Because we can't apply the migrations together, we have a ton of duplication between:
# - the model changes
# - data migration
# - column changes
# There's nothing easy to do about it and it's not worth it since once the migration runs
# it doesn't need to be maintained.
class Migration(migrations.Migration):
    dependencies = [
        ('website', '0028_costlineitem_note'),
    ]

    operations = [
        migrations.RunSQL("UPDATE website_costlineitem SET account_code='' WHERE account_code IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET budget_line_code='' WHERE budget_line_code IS NULL"),
        migrations.RunSQL(
            "UPDATE website_costlineitem SET budget_line_description='' WHERE budget_line_description IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET country_code='' WHERE country_code IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET dummy_field_1='' WHERE dummy_field_1 IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET dummy_field_2='' WHERE dummy_field_2 IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET grant_code='' WHERE grant_code IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET sector_code='' WHERE sector_code IS NULL"),
        migrations.RunSQL("UPDATE website_costlineitem SET site_code='' WHERE site_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET account_code='' WHERE account_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET budget_line_code='' WHERE budget_line_code IS NULL"),
        migrations.RunSQL(
            "UPDATE website_transaction SET budget_line_description='' WHERE budget_line_description IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET country_code='' WHERE country_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET currency_code='' WHERE currency_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET dummy_field_1='' WHERE dummy_field_1 IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET dummy_field_2='' WHERE dummy_field_2 IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET dummy_field_3='' WHERE dummy_field_3 IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET dummy_field_4='' WHERE dummy_field_4 IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET dummy_field_5='' WHERE dummy_field_5 IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET grant_code='' WHERE grant_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET sector_code='' WHERE sector_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET site_code='' WHERE site_code IS NULL"),
        migrations.RunSQL("UPDATE website_transaction SET transaction_code='' WHERE transaction_code IS NULL"),
        migrations.RunSQL(
            "UPDATE website_transaction SET transaction_description='' WHERE transaction_description IS NULL"),

    ]

# Generated by Django 2.2.3 on 2020-06-09 01:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('website', '0032_settings_transaction_country_filter'),
    ]

    operations = [
        migrations.AddField(
                model_name='analysis',
                name='currency_code',
                field=models.CharField(blank=True, choices=[('USD', 'USD'), ('GBP', 'GBP'), ('EUR', 'EUR')], default='', max_length=255, verbose_name='Currency code'),
        )

    ]

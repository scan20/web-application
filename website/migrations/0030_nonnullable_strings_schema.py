from django.db import migrations, models


# Sets database-level DEFAULT '' and NOT NULL for text columns on transactions and costlineitem,
# along with Django-level for the same.
#
# Because we bypass the ORM for bulk inserts (via COPY FROM),
# we need database constraints set up.
# Django's migrations, however, don't handle this- we need to go the long way around.
# But we also need to do the AlterField so Django treats the fields appropriate (with the default and non-null).
class Migration(migrations.Migration):
    dependencies = [
        ('website', '0029_nonnullable_strings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='costlineitem',
            name='account_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Account code'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='budget_line_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Budget line code'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='budget_line_description',
            field=models.CharField(blank=True, default='', max_length=1000, verbose_name='Budget line description'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='country_code',
            field=models.CharField(blank=True, default='', max_length=10, verbose_name='Country code'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='dummy_field_1',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 1'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='dummy_field_2',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 2'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='grant_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Grant code'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='sector_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Sector code'),
        ),
        migrations.AlterField(
            model_name='costlineitem',
            name='site_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Site code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='account_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Account code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='budget_line_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Budget line code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='budget_line_description',
            field=models.CharField(blank=True, default='', max_length=1000, verbose_name='Budget line description'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='country_code',
            field=models.CharField(blank=True, default='', max_length=10, verbose_name='Country code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='currency_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Currency code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='dummy_field_1',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 1'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='dummy_field_2',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 2'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='dummy_field_3',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 3'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='dummy_field_4',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 4'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='dummy_field_5',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Dummy field 5'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='grant_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Grant code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='sector_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Sector code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='site_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Site code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_code',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Transaction code'),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_description',
            field=models.TextField(blank=True, default='', verbose_name='Transaction description'),
        ),
        migrations.AlterIndexTogether(
            name='transaction',
            index_together={
                ('country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code')},
        ),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN account_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN account_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN budget_line_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN budget_line_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN budget_line_description SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN budget_line_description SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN country_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN country_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN dummy_field_1 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN dummy_field_1 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN dummy_field_2 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN dummy_field_2 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN grant_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN grant_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN sector_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN sector_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_costlineitem ALTER COLUMN site_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_costlineitem ALTER COLUMN site_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN account_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN account_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN budget_line_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN budget_line_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN budget_line_description SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN budget_line_description SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN country_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN country_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN currency_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN currency_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN dummy_field_1 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN dummy_field_1 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN dummy_field_2 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN dummy_field_2 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN dummy_field_3 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN dummy_field_3 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN dummy_field_4 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN dummy_field_4 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN dummy_field_5 SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN dummy_field_5 SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN grant_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN grant_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN sector_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN sector_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN site_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN site_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN transaction_code SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN transaction_code SET DEFAULT ''"),
        migrations.RunSQL('ALTER TABLE website_transaction ALTER COLUMN transaction_description SET NOT NULL'),
        migrations.RunSQL("ALTER TABLE website_transaction ALTER COLUMN transaction_description SET DEFAULT ''"),
    ]

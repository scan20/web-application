# Generated by Django 2.2.3 on 2021-09-27 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0038_auto_20210927_1330'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysis',
            name='output_count_source',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Output count data source'),
        ),
    ]

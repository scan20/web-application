# Generated by Django 2.2.3 on 2019-09-18 06:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_auto_20190917_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountCodeDescription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_code', models.CharField(max_length=255, verbose_name='Account Code')),
                ('account_description', models.CharField(max_length=255, verbose_name='Account Description')),
                ('sensitive_data', models.BooleanField(default=False, help_text='When checked, costs and transactions will not be shown for this account code.', verbose_name='Sensitive Data')),
            ],
            options={
                'verbose_name': 'Account Code Description',
                'verbose_name_plural': 'Account Code Description',
            },
        ),
        migrations.AlterModelOptions(
            name='activity',
            options={'ordering': ['name'], 'verbose_name': 'Activity', 'verbose_name_plural': 'Activities'},
        ),
        migrations.AlterModelOptions(
            name='costefficiencystrategy',
            options={'verbose_name': 'Cost Efficiency Strategy', 'verbose_name_plural': 'Cost Efficiency Strategies'},
        ),
    ]

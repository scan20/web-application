from django.db import migrations, models

class Migration(migrations.Migration):

    dependencies = [
        ('website', '0026_auto_20200315_1731'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='costlineitem',
            index_together={('country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code')},
        ),
    ]

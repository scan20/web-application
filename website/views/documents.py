from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import F, Sum, Value
from django.http import HttpResponse
from openpyxl import Workbook
from openpyxl.styles import Alignment, Border, Font, PatternFill, Side
from openpyxl.styles.colors import WHITE
from openpyxl.utils import get_column_letter
from openpyxl.worksheet import worksheet
from openpyxl.writer.excel import save_virtual_workbook
from website.currency import currency_name, currency_symbol
from website.models import Analysis, FieldLabelOverrides
from website.models.activity import OutputMetric
from website.app_log import loggers as app_loggers

_gray_fill = PatternFill(start_color='00DADADA',
                         end_color='00DADADA',
                         fill_type='solid')

_dark_red_fill = PatternFill(start_color='00530000',
                             end_color='00530000',
                             fill_type='solid')

_black_side = Side(style='thin', color="000000")
_black_border = Border(left=_black_side, top=_black_side, right=_black_side, bottom=_black_side)


def _get_cost_line_items(an_analysis: Analysis):
    return an_analysis.contributing_cost_line_items \
        .annotate(allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))) \
        .order_by('-allocated_cost')


def _get_cost_breakdown_categories_and_sectors(an_analysis: Analysis):
    return an_analysis.contributing_cost_line_items \
        .values('config__sector__name', 'config__category__name') \
        .annotate(allocated_cost_sum=Sum(F('total_cost') * (F('config__allocation') / Value(100)))) \
        .order_by('-allocated_cost_sum')


def _write_metadata_table(ws: worksheet, an_analysis: Analysis, author: str, starting_row: int = 1) -> int:
    """
    Write the metadata table to the provided worksheet in the upper left corner.

    Returns the last row with data on it to position other things on the page.
    """
    row = starting_row

    metadata = [
        ("Analysis Title", an_analysis.title),
        ("Analysis Type", an_analysis.analysis_type.title),
        ("Analysis Description", an_analysis.description),
        ("Analysis Start Date", an_analysis.start_date),
        ("Analysis End Date", an_analysis.end_date),
        ("Country", an_analysis.country.name),
        (FieldLabelOverrides.label_for("ci_grant_code", "Grants"), ", ".join(an_analysis.grants_list())),
        ("Activity Being Analyzed", an_analysis.activity.name),
    ]

    for each_parameter in an_analysis.parameters.items():
        for each_output_metric in an_analysis.activity.output_metric_objects():
            if each_parameter[0] not in each_output_metric.parameters:
                continue
            label = str(each_output_metric.parameters[each_parameter[0]].label)
            value = each_parameter[1]
            metadata.append((label, value))

    if not an_analysis.activity.is_iov():
        metadata += [
            ("Output count data source", an_analysis.output_count_source),
        ]

    metadata += [
        ("Currency", f"{currency_name(analysis=an_analysis, plural=True)}"),
        ("Author", author),
    ]

    for item in metadata:
        ws[f"A{row}"] = item[0]
        ws[f"A{row}"].fill = _gray_fill
        ws[f"A{row}"].font = Font(bold=True)
        ws[f"A{row}"].border = _black_border

        ws[f"B{row}"] = item[1]
        ws[f"B{row}"].fill = _gray_fill
        ws[f"B{row}"].border = _black_border
        ws[f"B{row}"].alignment = Alignment(horizontal='left')

        row += 1

    return row


def _write_cost_efficiency_table(ws: worksheet, an_analysis: Analysis, starting_row: int) -> int:
    """
    Write the cost efficiency table to the provided worksheet

    Returns the last row with data on it to position other things on the page.
    """
    row = starting_row

    ws[f"A{row}"] = "Cost Efficiency"
    ws[f"A{row}"].font = Font(bold=True)

    row += 1
    each_metric: OutputMetric
    for each_metric in an_analysis.activity.output_metric_objects():
        ws[f"A{row}"] = f"{each_metric.metric_name} Direct Project Costs only"
        ws[f"A{row}"].border = _black_border

        ws[f"B{row}"] = an_analysis.output_costs[each_metric.id]['direct_only']
        ws[f"B{row}"].border = _black_border

        row += 1

        ws[f"A{row}"] = f"{each_metric.metric_name} including Direct Project Costs, Direct Shared Costs, Indirect Costs"
        ws[f"A{row}"].border = _black_border

        ws[f"B{row}"] = an_analysis.output_costs[each_metric.id]['all']
        ws[f"B{row}"].border = _black_border

        row += 1

    return row


def _write_cost_breakdown_table(ws: worksheet, an_analysis: Analysis, starting_row: int) -> int:
    """
    Write the cost breakdown table to the provided worksheet.  The data in this table has
      functions that are dependent on other tables so this fills in blanks and then another
      function goes back through and fills them in.

    Returns the last row with data on it to position other things on the page.
    """
    row = starting_row

    # Cost Breakdown Section
    ws[f"A{row}"] = "Cost Breakdown"
    ws[f"A{row}"].font = Font(bold=True)
    row += 1

    ws[f"A{row}"] = FieldLabelOverrides.label_for("ci_sector_code", "Sector")
    ws[f"A{row}"].fill = _dark_red_fill
    ws[f"A{row}"].font = Font(color=WHITE)
    ws[f"A{row}"].border = _black_border

    ws[f"B{row}"] = "Category"
    ws[f"B{row}"].fill = _dark_red_fill
    ws[f"B{row}"].font = Font(color=WHITE)
    ws[f"B{row}"].border = _black_border

    ws[f"C{row}"] = "Amount"
    ws[f"C{row}"].fill = _dark_red_fill
    ws[f"C{row}"].font = Font(color=WHITE)
    ws[f"C{row}"].border = _black_border

    ws[f"D{row}"] = "% Of Total Amount"
    ws[f"D{row}"].fill = _dark_red_fill
    ws[f"D{row}"].font = Font(color=WHITE)
    ws[f"D{row}"].border = _black_border

    row += 1

    first_data_row = row
    breakdown_data = _get_cost_breakdown_categories_and_sectors(an_analysis)
    for each_breakdown in breakdown_data:
        ws[f"A{row}"] = each_breakdown['config__sector__name']
        ws[f"A{row}"].border = _black_border

        ws[f"B{row}"] = each_breakdown['config__category__name']
        ws[f"B{row}"].border = _black_border

        ws[f"C{row}"] = 0
        ws[f"C{row}"].number_format = '#,##0.00'
        ws[f"C{row}"].border = _black_border

        ws[f"D{row}"] = 0
        ws[f"D{row}"].number_format = '#,##0.00'
        ws[f"D{row}"].border = _black_border

        row += 1
    ws[f"C{row}"] = f"=SUM(C{first_data_row}:C{row - 1})"
    ws[f"C{row}"].number_format = '#,##0.00'
    ws[f"C{row}"].border = _black_border

    ws[f"D{row}"] = f"=SUM(D{first_data_row}:D{row - 1})"
    ws[f"D{row}"].number_format = '#,##0.00'
    ws[f"D{row}"].border = _black_border

    return row


def _fill_in_cost_breakdown_functions(ws: worksheet,
                                      first_data_row: int,
                                      last_data_row: int,
                                      full_cost_model_first_data_row: int,
                                      full_cost_model_last_data_row: int):
    row = first_data_row

    while row < last_data_row:
        ws[f"C{row}"] = (f"=SUMIFS("
                         f"I{full_cost_model_first_data_row}:I{full_cost_model_last_data_row}, "
                         f"A{full_cost_model_first_data_row}:A{full_cost_model_last_data_row}, "
                         f"A{row}, "
                         f"B{full_cost_model_first_data_row}:B{full_cost_model_last_data_row}, "
                         f"B{row}"
                         f")")
        ws[f"D{row}"] = f"=C{row}/SUM(I{full_cost_model_first_data_row}:I{full_cost_model_last_data_row}) * 100"
        row += 1


def _write_full_cost_model_table(ws: worksheet, an_analysis: Analysis, starting_row: int) -> int:
    """
    Write the full cost model table to the provided worksheet

    Returns the last row with data on it to position other things on the page.
    """
    row = starting_row

    ws[f"A{row}"] = "Cost Model"
    ws[f"A{row}"].font = Font(bold=True)
    row += 1

    ws[f"A{row}"] = FieldLabelOverrides.label_for("ci_sector_code", "Sector")
    ws[f"A{row}"].fill = _dark_red_fill
    ws[f"A{row}"].font = Font(color=WHITE)
    ws[f"A{row}"].border = _black_border

    ws[f"B{row}"] = "Category"
    ws[f"B{row}"].fill = _dark_red_fill
    ws[f"B{row}"].font = Font(color=WHITE)
    ws[f"B{row}"].border = _black_border

    ws[f"C{row}"] = "Cost Item"
    ws[f"C{row}"].fill = _dark_red_fill
    ws[f"C{row}"].font = Font(color=WHITE)
    ws[f"C{row}"].border = _black_border

    ws[f"D{row}"] = FieldLabelOverrides.label_for("ci_grant_code", "Grant")
    ws[f"D{row}"].fill = _dark_red_fill
    ws[f"D{row}"].font = Font(color=WHITE)
    ws[f"D{row}"].border = _black_border

    ws[f"E{row}"] = FieldLabelOverrides.label_for("tr_site_code", "Site")
    ws[f"E{row}"].fill = _dark_red_fill
    ws[f"E{row}"].font = Font(color=WHITE)
    ws[f"E{row}"].border = _black_border

    ws[f"F{row}"] = FieldLabelOverrides.label_for("ci_total_cost", "Total Cost")
    ws[f"F{row}"].fill = _dark_red_fill
    ws[f"F{row}"].font = Font(color=WHITE)
    ws[f"F{row}"].border = _black_border

    ws[f"G{row}"] = "% of Activity"
    ws[f"G{row}"].fill = _dark_red_fill
    ws[f"G{row}"].font = Font(color=WHITE)
    ws[f"G{row}"].border = _black_border

    ws[f"H{row}"] = "Notes"
    ws[f"H{row}"].fill = _dark_red_fill
    ws[f"H{row}"].font = Font(color=WHITE)
    ws[f"H{row}"].border = _black_border

    ws[f"I{row}"] = "Item Total"
    ws[f"I{row}"].fill = _dark_red_fill
    ws[f"I{row}"].font = Font(color=WHITE)
    ws[f"I{row}"].border = _black_border
    row += 1

    for each_cost_line_item in _get_cost_line_items(an_analysis):
        ws[f"A{row}"] = each_cost_line_item.config.sector.name
        ws[f"B{row}"] = each_cost_line_item.config.category.name
        ws[f"C{row}"] = each_cost_line_item.budget_line_description
        ws[f"D{row}"] = each_cost_line_item.grant_code
        ws[f"E{row}"] = each_cost_line_item.site_code
        ws[f"F{row}"] = each_cost_line_item.total_cost
        ws[f"F{row}"].number_format = '#,##0.00'
        ws[f"G{row}"] = each_cost_line_item.config.allocation
        ws[f"G{row}"].number_format = '#,##0.00'
        ws[f"H{row}"] = each_cost_line_item.note
        ws[f"I{row}"] = f"=(F{row} * G{row})/100"
        ws[f"I{row}"].number_format = '#,##0.00'
        row += 1

    return row


def _formatting_pass(ws: worksheet):
    for i, each_col in enumerate(ws.columns, 1):
        max_length = 0
        for each_cell in each_col:
            if each_cell.value:
                width = len(str(each_cell.value))
                if width > max_length:
                    max_length = width
        ws.column_dimensions[get_column_letter(i)].width = max_length + 2


@login_required
def full_cost_model_spreadsheet(request, pk):
    analysis = Analysis.objects.get(pk=pk)

    app_loggers.log_analysis_cost_model_download(analysis, analysis.contributing_cost_line_items.count(), request.user)

    workbook = Workbook()
    worksheet = workbook.active

    # Metadata Section
    last_metadata_row = _write_metadata_table(worksheet, analysis, author=request.user.get_full_name())

    row = last_metadata_row
    row += 2

    # Cost Efficiency Section
    first_cost_efficiency_row = row
    last_cost_efficiency_row = _write_cost_efficiency_table(worksheet, analysis, first_cost_efficiency_row)

    row = last_cost_efficiency_row
    row += 2

    first_cost_breakdown_row = row
    last_cost_breakdown_row = _write_cost_breakdown_table(worksheet, analysis, first_cost_breakdown_row)

    row = last_cost_breakdown_row
    row += 2

    first_full_cost_model_row = row
    last_full_cost_model_row = _write_full_cost_model_table(worksheet, analysis, first_full_cost_model_row)

    row = last_full_cost_model_row

    _fill_in_cost_breakdown_functions(worksheet,
                                      first_cost_breakdown_row + 2,
                                      last_cost_breakdown_row,
                                      first_full_cost_model_row + 2,
                                      last_full_cost_model_row)

    _formatting_pass(worksheet)

    response = HttpResponse(content=save_virtual_workbook(workbook), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = f'attachment; filename=\"{analysis.title.lower().replace(" ", "")}-insights-{datetime.now():%Y%m%d-%H%M}.xlsx\"'
    return response

from logging import getLogger

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied, ValidationError
from django.db import connection, transaction as db_transaction
from django import forms
from django.forms.models import model_to_dict
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.html import strip_tags
from django.views.generic.detail import DetailView, SingleObjectMixin
from ombucore.admin import panel_commands as commands
from ombucore.admin.sites import site
from ombucore.admin.views import FormView
from ombucore.admin.views.mixins import PanelUIMixin
from ombucore.admin.widgets import FlatpickrDateWidget
from ombucore.logger import log

from website.data_loading import BulkInserter
from website.models import (
    Analysis,
    AnalysisSectorCategory,
    AnalysisSectorCategoryGrant,
    CostLineItem,
    CostLineItemConfig,
    Transaction
)
from website.workflow import Workflow

logger = getLogger(__name__)


class DuplicateBudgetUploadAnalysisForm(forms.Form):
    pass


class DuplicateTransactionStoreAnalysisForm(forms.Form):


    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    change_analysis_date_range = forms.ChoiceField(
        choices=(
            ('use_existing', 'Duplicate using the current analysis date range'),
            ('change', 'Duplicate using a new analysis date range'),
        ),
        widget=forms.RadioSelect(),
    )
    start_date = forms.DateField(
        required=False,
        widget=FlatpickrDateWidget(options={'dateFormat': settings.DATE_FORMAT}),
    )
    end_date = forms.DateField(
        required=False,
        widget=FlatpickrDateWidget(options={'dateFormat': settings.DATE_FORMAT}),
    )

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('start_date') > cleaned_data.get('end_date'):
            raise ValidationError("Start date must be before end date.")
        return cleaned_data

    class Media:
        js = ('website/js/duplicate.js', )


class DuplicateView(SingleObjectMixin, FormView):
    queryset = Analysis.objects
    title = None
    supertitle = 'Duplicate'
    success_message = '<strong>%(title)s</strong> was successfully duplicated.'
    log_action = 'Duplicated'
    duplicated = False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object_id = self.object.id
        if 'confirmed' in request.GET:
            self.duplicate()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_initial(self):
        if self.object.source == Analysis.DATA_STORE_NAME:
            return {
                'start_date': self.object.start_date,
                'end_date': self.object.end_date,
            }
        return {}

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def get_form_class(self):
        if self.object.source == Analysis.DATA_STORE_NAME:
            return DuplicateTransactionStoreAnalysisForm
        return DuplicateBudgetUploadAnalysisForm

    def get_template_names(self):
        if self.object.source == Analysis.DATA_STORE_NAME:
            return 'panel-form-duplicate-transaction-store-analysis.html'
        return 'panel-form-duplicate-budget-upload-analysis.html'

    def form_valid(self, form):
        response = super().form_valid(form)
        obj_dict = model_to_dict(self.object)
        success_message = self.get_success_message(obj_dict)
        values = {}
        if form.cleaned_data.get('change_analysis_date_range') == 'change':
            if form.cleaned_data.get('start_date'):
                values['start_date'] = form.cleaned_data.get('start_date')
            if form.cleaned_data.get('end_date'):
                values['end_date'] = form.cleaned_data.get('end_date')
        new_analysis = clone_analysis(self.object.id, self.request.user, **values)
        if values:
            new_analysis.needs_transaction_resync = True
            workflow = Workflow(new_analysis, None)
            workflow.invalidate_step('insights')
            new_analysis.save()
        self.duplicated = True
        if success_message:
            messages.success(self.request, success_message)
        log(
            actor=self.request.user,
            action=self.log_action,
            obj=new_analysis,
            message=self.get_log_message(obj_dict),
        )
        self.panel_commands.append(commands.Resolve({'operation': 'duplicated'}))
        # return redirect(reverse('analysis-define-update', kwargs={'pk': new_analysis.pk}))
        return response

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        if self.object and not self.duplicated:
            obj_info = site.related_info_for(self.object)
            kwargs['title'] = obj_info['title']
        kwargs['duplicated'] = getattr(self, 'duplicated', False)
        kwargs['object_id'] = self.object.id
        return kwargs

    def get_success_message(self, data):
        if 'title' not in data:
            data['title'] = str(self.object)
        return self.success_message % data

    def get_log_message(self, cleaned_data):
        return strip_tags(self.get_success_message(cleaned_data))

    def dispatch(self, request, *args, **kwargs):
        analysis = self.get_object()
        if not request.user.has_perm('website.duplicate_analysis', analysis):
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)



@db_transaction.atomic
def clone_analysis(analysis_id, owner=None, **values):
    """Clone an analysis, and all of its:

    - transactions
    - cost line items
    - cost line item configs
    - sector categories
    - sector category grants

    All cloning is done through the bulk CSV loader, so it's fast.

    Each clone has a 'cloned_from_id' set to the model it was cloned from.
    It is used by this cloning method (other parts of the app can also use it if desired).

    When we clone a nested model, like CostLineItemConfig <- CostLineItem <- Analysis,
    we build a map of "old id to new id" for the intermediate model (CostLineItem).
    Then when we build up the rows of Configs to insert, we can grab the new 'parent' ID from that map.
    """
    og_analysis = Analysis.objects.get(pk=analysis_id)

    values = {field: value for field, value
              in values.items()
              if field in ('start_date', 'end_date',)}

    new_title = og_analysis.title + " (DUPLICATE, NEW DATES)" if values else og_analysis.title + " (DUPLICATE)"

    new_analysis = simple_clone(
        og_analysis,
        title=new_title,
        owner_id=owner.id,
        **values,
    )

    def cloneable_row(row, **attrs):
        row['cloned_from_id'] = row.pop('id')
        row.update(attrs)
        for k, v in row.items():
            if v is None:
                row[k] = '\u0001'
        return row

    def old_to_new_id_map(model, **filters):
        return {
            old: new
            for (old, new) in
            model.objects.filter(**filters).values_list('cloned_from_id', 'id').all()
        }

    with BulkInserter(connection, CostLineItem._meta.db_table) as inserter:
        for cli in CostLineItem.objects.filter(analysis=og_analysis).values().all():
            inserter.add_row(cloneable_row(cli, analysis_id=new_analysis.pk))

    old_to_new_clis = old_to_new_id_map(CostLineItem, analysis=new_analysis)
    with BulkInserter(connection, CostLineItemConfig._meta.db_table) as inserter:
        for clic in CostLineItemConfig.objects.filter(cost_line_item__in=old_to_new_clis.keys()).values().all():
            inserter.add_row(cloneable_row(
                clic,
                cost_line_item_id=old_to_new_clis[clic['cost_line_item_id']]
            ))

    # We must import transactions once we have the new cost line item they point to.
    # Updating after-the-fact is extremely slow (or would require some indices on cloned_from_id
    # which we don't want, as this is a large table that receives bulk imports).
    with BulkInserter(connection, Transaction._meta.db_table) as inserter:
        for txn in Transaction.objects.filter(analysis=og_analysis).values().all():
            inserter.add_row(cloneable_row(
                txn,
                analysis_id=new_analysis.pk,
                cost_line_item_id=old_to_new_clis[txn['cost_line_item_id']]
            ))

    with BulkInserter(connection, AnalysisSectorCategory._meta.db_table) as inserter:
        for asc in AnalysisSectorCategory.objects.filter(analysis=og_analysis).values().all():
            inserter.add_row(cloneable_row(asc, analysis_id=new_analysis.pk))

    old_to_new_ascs = old_to_new_id_map(AnalysisSectorCategory, analysis=new_analysis)
    with BulkInserter(connection, AnalysisSectorCategoryGrant._meta.db_table) as inserter:
        grants = AnalysisSectorCategoryGrant.objects.filter(sector_category__in=old_to_new_ascs.keys())
        for ascg in grants.values().all():
            inserter.add_row(cloneable_row(
                ascg,
                sector_category_id=old_to_new_ascs[ascg['sector_category_id']]
            ))
    return new_analysis


def simple_clone(o, **attrs):
    model = type(o)
    values = model.objects.filter(pk=o.pk).values().first()
    values.pop(model._meta.pk.name)
    values['cloned_from_id'] = o.pk
    values.update(**attrs)
    clone = model(**values)
    clone.save()
    return clone
import logging
from decimal import Decimal, DecimalException

import django_filters
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import ImproperlyConfigured
from django.core.paginator import Paginator
from django.db import models
from django.db.models import F, Q, Sum, Value
from django.forms import Form
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext as _, gettext_lazy as _l
from django.views.generic import CreateView, DetailView, UpdateView
from django.views.generic.base import ContextMixin, View, TemplateView
from django.views.generic.detail import SingleObjectMixin
from django_filters import filters
from django_filters.views import FilterView

from ombucore.admin import panel_commands
from ombucore.admin.buttons import CancelButton, SubmitButton
from ombucore.admin.filterset import FilterSet
from ombucore.admin.panel_commands import Resolve
from ombucore.admin.views import FilterMixin
from ombucore.admin.views.base import FormView as PanelsFormView, search_field_for_model, FormView

from website.data_loading import get_transactions_data_store_count
from website.forms.analysis import DefineForm, FixMissingDataBulkForm, CategorizeSectorBulkForm
from website.models import Analysis, Category, CostLineItemConfig, Sector
from website.models import AnalysisSectorCategoryGrant, InsightComparisonData
from website.models import CostEfficiencyStrategy, CostLineItem
from website.models.activity import OutputMetric
from website.models.core import IndirectCostRecovery
from website.models.settings import Settings
from website.utils import group_by
from website.workflow import Workflow
from website.app_log import loggers as app_loggers

logger = logging.getLogger(__name__)


class AllocateSectorGrantSiteFilterSet(FilterSet):
    search = django_filters.CharFilter(
        label='Search',
        method='keyword_search',
        widget=forms.TextInput(attrs={
            'placeholder': 'Enter keyword',
            'class': 'filters__search-input',
        }),
    )

    order_by = django_filters.OrderingFilter(
        fields=(
            ('site_code', 'site_code'),
            ('budget_line_description', 'budget_line_description'),
            ('grant_code', 'grant_code'),
            ('account_code', 'account_code'),
            ('total_cost', 'total_cost'),
        )
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
            Q(site_code__icontains=value) |
            Q(sector_code__icontains=value) |
            Q(grant_code__icontains=value) |
            Q(account_code__icontains=value) |
            Q(budget_line_description__icontains=value)
        )

    site_code = django_filters.ChoiceFilter(
        label='Site Code',
        empty_label="Any",
        choices=CostLineItem.site_code_filter_choices,
        widget=forms.Select,
    )

    class Meta:
        model = CostLineItem
        fields = ['search', 'site_code', ]

class FixMissingDataFilterSet(FilterSet):
    search = django_filters.CharFilter(
            label='Search',
            method='keyword_search',
            widget=forms.TextInput(attrs={
                'placeholder': 'Enter keyword',
                'class': 'filters__search-input',
            }),
    )
    site_code = django_filters.ChoiceFilter(
            label='Site Code',
            empty_label="Any",
            choices=CostLineItem.site_code_filter_choices,
            widget=forms.Select,

    )

    grant_code = django_filters.ChoiceFilter(
            label='Grant Code',
            empty_label="Any",
            choices=CostLineItem.grant_code_filter_choices,
            widget=forms.Select,

    )

    order_by = django_filters.OrderingFilter(
            fields=(
                ('site_code', 'site_code'),
                ('budget_line_description', 'budget_line_description'),
                ('grant_code', 'grant_code'),
                ('account_code', 'account_code'),
                ('total_cost', 'total_cost'),
            )
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
                Q(site_code__icontains=value) |
                Q(grant_code__icontains=value) |
                Q(account_code__icontains=value) |
                Q(budget_line_description__icontains=value)
        )

    class Meta:
        model = CostLineItem
        fields = ['search', 'site_code', 'grant_code', ]


class AnalysisPermissionRequiredMixin(PermissionRequiredMixin):

    def has_permission(self):
        perms = self.get_permission_required()
        return self.request.user.has_perms(perms, self.analysis)


class AnalysisStepMixin:
    step_name = None  # The name of the step in the workflow.
    title = None
    help_text = None
    model = Analysis

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.dioptra_settings = Settings.objects.first()
        self.setup_step()

    def setup_step(self):
        self.object = self.get_object()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.step = self.workflow.active_step

    def dispatch(self, request, *args, **kwargs):
        if not self.step or not self.step.dependencies_met:
            return redirect(reverse('analysis', kwargs={'pk': self.analysis.pk}))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.title:
            context['title'] = self.title
        if self.help_text:
            context['help_text'] = self.help_text

        context['dioptra_settings'] = self.dioptra_settings
        context['analysis'] = self.analysis
        context['workflow'] = self.workflow
        context['step'] = self.step

        return context


class AnalysisObjectMixin(ContextMixin):
    context_object_name = None

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = Analysis.objects.all()
        pk = self.kwargs.get('pk')
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        if pk is None:
            raise AttributeError(
                "Generic detail view %s must be called with either an object "
                "pk or a slug in the URLconf." % self.__class__.__name__
            )

        try:
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get_context_object_name(self, obj):
        if self.context_object_name:
            return self.context_object_name
        elif isinstance(obj, models.Model):
            return obj._meta.model_name
        else:
            return None


class AnalysisStepDetailMixin(AnalysisStepMixin, AnalysisObjectMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)



class AnalysisStepFiltersetMixin(AnalysisStepMixin, AnalysisObjectMixin, TemplateView):
    def get_filterset_class(self):
        if self.filterset_class:
            return self.filterset_class
        elif self.model:
            meta_dict = {'model': self.model}
            filterset_class_dict = {}
            search_field = search_field_for_model(self.model)
            if search_field:
                meta_dict['fields'] = ['search']
                filterset_class_dict['search'] = filters.CharFilter(field_name=search_field,
                                lookup_expr='icontains',
                                help_text='',)
            filterset_class_dict['Meta'] = type(str('Meta'), (object,), meta_dict)
            filterset_class = type(str('%sFilterSet' % self.model._meta.object_name),
                                                 (FilterSet,), filterset_class_dict)
            return filterset_class
        else:
            msg = "'%s' must define 'filterset_class' or 'model'"
            raise ImproperlyConfigured(msg % self.__class__.__name__)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.modify_queryset(self.filterset.qs)
        context = self.get_context_data(
            object=self.object,
            filter=self.filterset,
            object_list=self.object_list
        )
        return self.render_to_response(context)

    def modify_queryset(self, queryset):
        return queryset

class PostActionHandlerMixin:

    def post(self, request, *args, **kwargs):
        if 'action' in request.POST and request.POST['action'] in self.actions:
            handler = 'handle_{}'.format(request.POST['action'])
            if hasattr(self, handler):
                response = getattr(self, handler)(request, *args, **kwargs)
                if response:
                    return response
        query = f"?{request.GET.urlencode()}" if request.GET else ''
        return redirect(self.request.path + query)


class AnalysisDetailView(LoginRequiredMixin, DetailView):
    """
    Serves as the main entrypoint for linking to an analysis.

    Redirects to the latest-incomplete step in the analysis workflow.
    """
    model = Analysis

    def get(self, request, *args, **kwargs):
        self.analysis = self.get_object()
        workflow = Workflow(self.analysis, None)
        step = workflow.get_last_incomplete_or_last()
        return redirect(step.get_href())


class DefineCreate(PermissionRequiredMixin, AnalysisStepMixin, CreateView):
    step_name = 'define'
    form_class = DefineForm
    title = _l('Define the details of this analysis')
    help_text = _l('Enter the details of the analysis you wish to create. Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/define.html'
    permission_required = 'website.add_analysis'

    def setup_step(self):
        self.object = AnalysisDetailView()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.step = self.workflow.active_step

    def get_success_url(self):
        return reverse('analysis-define-update', kwargs={'pk': self.object.pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        app_loggers.log_analysis_created(self.object, self.request.user)
        return response


class DefineUpdate(AnalysisPermissionRequiredMixin, AnalysisStepMixin, UpdateView):
    step_name = 'define'
    form_class = DefineForm
    title = _l('Define the details of this analysis')
    help_text = _l('Enter the details of the analysis you wish to create. Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/define.html'
    permission_required = 'website.change_analysis'

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['data_loaded'] = self.workflow.get_step('load-data').is_complete
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        # Save the form model.
        self.object = form.save()
        app_loggers.log_analysis_updated(self.object, self.request.user)

        # Re-initialize the workflow so the references to the analysis are correct.
        self.setup_step()

        # Invalidate the insights, try to recalculate.
        self.workflow.invalidate_step('insights')
        self.workflow.calculate_if_possible()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.request.path


class LoadData(AnalysisPermissionRequiredMixin, PostActionHandlerMixin, AnalysisStepMixin, DetailView):
    step_name = 'load-data'
    title = _l('Load cost data')
    help_text = _('Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/load-data.html'
    actions = ['import_data', 'upload', 'reset_data', 'transaction_resync']
    permission_required = 'website.change_analysis'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.settings = Settings.objects.first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['import_transaction_limit'] = settings.IMPORTED_TRANSACTION_LIMIT
        context['transaction_country_filter'] = self.settings.transaction_country_filter
        analysis = self.analysis
        if not self.step.is_complete:
            context['transactions_count'] = None
            country_code = None
            if self.settings.transaction_country_filter:
                country_code = analysis.country.code
            try:
                context['transactions_count'] = get_transactions_data_store_count(analysis.grants, analysis.start_date, analysis.end_date, country_code=country_code)
            except Exception as e:
                logger.exception(e)
                context['import_errors'] = [_('There was an error querying transactions.')]
        return context

    def handle_import_data(self, request, *args, **kwargs):
        succeeded, result = self.step.load_transactions_from_data_store(filter_by_country=self.settings.transaction_country_filter)
        if (not succeeded) and ('errors' in result):
            context = self.get_context_data(object=self.object)
            context['import_errors'] = result['errors']
            return self.render_to_response(context)
        else:
            try:
                t_count = get_transactions_data_store_count(self.analysis.grants, self.analysis.start_date, self.analysis.end_date)
            except Exception as e:
                logger.exception(e)
                t_count = 'n/a'
            app_loggers.log_analysis_transactions_imported(self.analysis, t_count, result.get('imported_count'), self.request.user)
            messages.success(request, _("%(imported_count)s transactions imported successfully.") % result)

        return None  # Normal routing.

    def handle_upload(self, request, *args, **kwargs):
        f = request.FILES.get('file', None)
        errors = []
        if not f:
            errors.append(_("Please select a file."))

        succeeded, result = self.step.load_cost_line_items_from_file(f)
        if (not succeeded) and ('errors' in result):
            errors += result['errors']

        if len(errors) > 0:
            context = self.get_context_data(object=self.object)
            context['upload_errors'] = errors
            return self.render_to_response(context)
        else:
            app_loggers.log_analysis_budget_uploaded(self.analysis, result.get('imported_count'), self.request.user)
            messages.success(request, _("%(imported_count)s cost line items loaded successfully.") % result)

        if request.POST.get('currency_code'):
            self.object.currency_code = request.POST.get('currency_code')
            self.object.save()

        return None  # Normal routing.

    def handle_reset_data(self, request, *args, **kwargs):
        # Clear the currency code if it was set by a file upload.
        self.object.currency_code = ''
        self.object.save()

        self.step.invalidate()
        return None  # Normal routing.

    def handle_transaction_resync(self, request, *args, **kwargs):
        # Clear the currency code if it was set by a file upload.
        succeeded, result = self.step.resync_transactions_from_data_store(filter_by_country=self.settings.transaction_country_filter)
        if (not succeeded) and ('errors' in result):
            context = self.get_context_data(object=self.object)
            context['import_errors'] = result['errors']
            return self.render_to_response(context)
        self.object.output_costs = {}
        self.object.needs_transaction_resync = False
        self.object.save()
        messages.success(self.request, _("Transactions have been synced successfully."))


class FixMissingData(FilterMixin, AnalysisPermissionRequiredMixin, PostActionHandlerMixin, AnalysisStepFiltersetMixin, FilterView):
    step_name = 'fix-missing-data'
    title = _l('Assign Sector and Category to Cost Data')
    help_text = _('Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/fix-missing-data.html'
    model = CostLineItem
    actions = ['save_cost_line_item_configs']
    permission_required = 'website.change_analysis'
    filterset_class = FixMissingDataFilterSet


    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.modify_queryset(self.filterset.qs)
        self.object_list = self.object_list.filter(Q(config__sector__isnull=True) | Q(config__category__isnull=True))

        context = self.get_context_data(
            object=self.object,
            filter=self.filterset,
            object_list=self.object_list
        )
        return self.render_to_response(context)

    def modify_queryset(self, queryset):
        return queryset\
            .filter(analysis_id=self.object.id,)\
            .select_related('config') \
            .prefetch_related('transactions')

    def setup_step(self):
        super().setup_step()
        self.sector_choices = [(sector.id, sector.name) for sector in Sector.objects.all()]
        self.category_choices = [(category.id, category.name) for category in Category.objects.all()]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ordering = [
            'grant_code',
            'budget_line_description',
            'site_code',
            'sector_code',
            'account_code',
            'total_cost',
            # FIXME this is not ideal.   We should be sorting by the account_code_description but that isn't accessible at the moment
            F('config__sector__name').asc(nulls_first=True),
            F('config__category__name').asc(nulls_first=True),
        ]
        self.filterset.form.fields['site_code'].choices = self.object.site_codes_choices_from_cost_line_items()
        self.filterset.form.fields['grant_code'].choices = self.object.grant_codes_choices_from_cost_line_items()

        if self.object_list.query.order_by:
            for field in self.object_list.query.order_by:
                if field.lstrip('-') in ordering:
                    ordering.remove(field.lstrip('-'))
                    ordering = [field, ] + ordering

        cost_line_items = self.object_list.order_by(*ordering)

        paginator = Paginator(cost_line_items, self.dioptra_settings.paginate_by)
        page = paginator.get_page(self.request.GET.get('page', 1))

        context['paginator'] = paginator
        context['paginator_page'] = page
        context['cost_line_items'] = page.object_list
        context['cost_line_item_count'] = cost_line_items.count()
        context['total_cost_line_item_count'] = self.object.cost_line_items.filter(Q(config__sector__isnull=True) | Q(config__category__isnull=True)).count()
        context['cost_line_items_sum'] = cost_line_items.aggregate(Sum('total_cost'))['total_cost__sum']
        context['sector_choices'] = self.sector_choices
        context['category_choices'] = self.category_choices
        return context

    def handle_save_cost_line_item_configs(self, request, *args, **kwargs):
        config_data = self._configs_data_for_post(request.POST)
        for config_id, attrs in config_data.items():
            CostLineItemConfig.objects.filter(
                    cost_line_item__analysis_id=self.analysis.id
            ).filter(id=config_id).update(**attrs)
        self.analysis.ensure_sector_category_objects()
        return None

    def _configs_data_for_post(self, post):
        data = {}
        for field_name, value in post.items():
            if field_name.startswith('config_'):
                if 'sector_id' in field_name:
                    config_id = int(field_name.replace('config_sector_id_', ''))
                    if not config_id in data:
                        data[config_id] = {}
                    if value and value != '':
                        data[config_id]['sector_id'] = int(value)
                elif 'category_id' in field_name:
                    config_id = int(field_name.replace('config_category_id_', ''))
                    if not config_id in data:
                        data[config_id] = {}
                    if value and value != '':
                        data[config_id]['category_id'] = int(value)
        return data


class FixMissingDataBulk(AnalysisPermissionRequiredMixin, LoginRequiredMixin, PanelsFormView, SingleObjectMixin):
    model = Analysis
    form_class = FixMissingDataBulkForm
    supertitle = _l('Selected Cost Items')
    title = _l('Assign Sector/Category')
    permission_required = 'website.change_analysis'
    buttons = [
        SubmitButton(text=_('Assign Selected Items')),
        CancelButton(),
    ]

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.object = self.get_object()
        self.analysis = self.object
        self.config_ids = self._get_config_ids(request)

    def _get_config_ids(self, request):
        if request.method == 'POST':
            return request.POST.getlist('config_ids')
        else:
            # config_ids are a comma-separated string (e.g. `12,44,2,34`).
            return request.GET.get('config_ids', '').split(',')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial']['config_ids'] = self.config_ids
        return kwargs

    def form_valid(self, form):
        sector = form.cleaned_data['sector']
        category = form.cleaned_data['category']
        config_ids = form.cleaned_data['config_ids']
        self._assign_configs(sector, category, config_ids)
        return super().form_valid(form)

    def _assign_configs(self, sector, category, config_ids):
        update_kwargs = {}
        if sector:
            update_kwargs['sector'] = sector
        if category:
            update_kwargs['category'] = category
        CostLineItemConfig.objects.filter(
                cost_line_item__analysis_id=self.analysis.id
        ).filter(
                id__in=config_ids
        ).update(**update_kwargs)
        self.analysis.ensure_sector_category_objects()

    def get_success_message(self, cleaned_data):
        return _('%(count)s cost items updated') % {'count': len(self.config_ids)}

    def get_success_commands(self):
        return [
            panel_commands.Resolve()
        ]


class Categorize(AnalysisPermissionRequiredMixin, AnalysisStepMixin, DetailView):
    """
    Redirects to the first sector to confirm categories.
    """
    step_name = 'categorize'
    permission_required = 'website.change_analysis'

    def get(self, request, *args, **kwargs):
        """
        Return redirect to first Sector to categorize.
        """
        return redirect(self.step.steps[0].get_href())


class CategorizeSectorFilterSet(FilterSet):
    search = django_filters.CharFilter(
            label='Search',
            method='keyword_search',
            widget=forms.TextInput(attrs={
                'placeholder': 'Enter keyword',
                'class': 'filters__search-input',
            }),
    )

    order_by = django_filters.OrderingFilter(
            fields=(
                ('site_code', 'site_code'),
                ('budget_line_description', 'budget_line_description'),
                ('grant_code', 'grant_code'),
                ('account_code', 'account_code'),
                ('total_cost', 'total_cost'),
            )
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
                Q(site_code__icontains=value) |
                Q(sector_code__icontains=value) |
                Q(grant_code__icontains=value) |
                Q(account_code__icontains=value) |
                Q(budget_line_description__icontains=value)
        )

    class Meta:
        model = CostLineItem
        fields = ['search', ]


# class CategorizeSector(AnalysisPermissionRequiredMixin, PostActionHandlerMixin, AnalysisStepMixin, DetailView):
class CategorizeSector(FilterMixin, AnalysisPermissionRequiredMixin, PostActionHandlerMixin, AnalysisStepFiltersetMixin, FilterView):
    step_name = 'categorize'
    title = _l('Review & confirm all cost items in each category')
    help_text = _('Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/categorize-sector.html'
    actions = ['save_cost_line_item_config', 'confirm_sector_category', 'confirm_sector_category_all']
    permission_required = 'website.change_analysis'
    filterset_class = CategorizeSectorFilterSet
    model = CostLineItem

    def setup_step(self):
        self.object = self.get_object()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.parent_step = self.workflow.active_step

        self.sector_choices = [(sector.id, sector.name) for sector in Sector.objects.all()]
        self.category_choices = [(category.id, category.name) for category in Category.objects.all()]

        self.step = None
        for substep in self.parent_step.steps:
            if substep.get_href() == self.request.path:
                self.step = substep
                break

    def modify_queryset(self, queryset):
        return (
            queryset.filter(analysis_id=self.object.id,)
            .select_related('config')
            # Do NOT prefetch transactions here, we need to load them on-demand or it's very slow.
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cost_line_items = list(self.object_list)
        grouped_cost_line_items = group_by(cost_line_items, lambda c: (c.config.sector_id, c.config.category_id))
        sector_categories = self.analysis.sector_categories.filter(sector=self.step.sector).select_related('sector', 'category')
        sc_cost_line_items = list()
        for sc in sector_categories:
            sc_cost_line_items.append(
                (sc, grouped_cost_line_items.get((sc.sector_id, sc.category_id), []))
            )
        unconfirmed_count = sector_categories.filter(confirmed=False).count()

        context.update({
            'sector_categories': sector_categories,
            'sector_category_items': sc_cost_line_items,
            'unconfirmed_count': unconfirmed_count,
            'sector_choices': self.sector_choices,
            'category_choices': self.category_choices,
        })

        return context

    def get_context_data_old(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sector_categories = self.analysis.sector_categories.filter(sector=self.step.sector).select_related('sector', 'category')
        unconfirmed_count = sector_categories.filter(confirmed=False).count()
        context.update({
            'sector_categories': sector_categories,
            'unconfirmed_count': unconfirmed_count,
            'sector_choices': self.sector_choices,
            'category_choices': self.category_choices,
        })
        if len(self.parent_step.steps) and self.step == self.parent_step.steps[-1]:
            if not all([step.is_complete for step in self.parent_step.steps]):
                context.update({
                    'next_message': _('You must finish confirming all sectors/categories before moving on.'),
                })
        return context

    def handle_save_cost_line_item_config(self, request, *args, **kwargs):
        config = CostLineItemConfig.objects.get(pk=int(request.POST.get('config_id')))
        sector = Sector.objects.get(pk=int(request.POST.get('sector_id')))
        category = Category.objects.get(pk=int(request.POST.get('category_id')))

        # Verify config's cost linei tem is part of this analysis.
        if config.cost_line_item.analysis_id == self.analysis.id:
            previous_sector_id = config.sector.id
            config.sector = sector
            config.category = category
            config.save()

            # Add any missing sector category objects.
            sector_category_ids_added = self.analysis.ensure_sector_category_objects()

            # Mark any newly added sector category objects as "confirmed".
            self.analysis.sector_categories.filter(id__in=sector_category_ids_added).update(confirmed=True)

            self.workflow.invalidate_step('insights')
            self.workflow.calculate_if_possible()

            # If the previous sector is no longer used, return a redirect to
            # the newly assigned sector.
            if self.analysis.sector_categories.filter(sector__id=previous_sector_id).count() == 0:
                next_step = self.parent_step.get_step_by_sector(sector)
                if next_step:
                    return redirect(next_step.get_href())
                else:
                    return redirect(self.parent_step.get_href())
            return None

    def handle_confirm_sector_category(self, request, *args, **kwargs):
        sector_category = self.analysis.sector_categories.get(pk=int(request.POST.get('sector_category_id')))
        sector_category.confirmed = True
        sector_category.save()
        self.workflow.calculate_if_possible()

    def handle_confirm_sector_category_all(self, request, *args, **kwargs):
        sector_categories = self.analysis.sector_categories.filter(sector=self.step.sector).select_related('sector', 'category').all()
        for sector_category in sector_categories:
            sector_category.confirmed = True
            sector_category.save()
        self.workflow.calculate_if_possible()


class CategorizeSectorBulk(AnalysisPermissionRequiredMixin, LoginRequiredMixin, PanelsFormView, SingleObjectMixin):
    model = Analysis
    form_class = CategorizeSectorBulkForm
    supertitle = _l('Selected Cost Items')
    title = _l('Change Sector/Category')
    permission_required = 'website.change_analysis'
    buttons = [
        SubmitButton(text=_('Assign Selected Items')),
        CancelButton(),
    ]

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.object = self.get_object()
        self.analysis = self.object
        self.config_ids = self._get_config_ids(request)

    def _get_config_ids(self, request):
        if request.method == 'POST':
            return request.POST.getlist('config_ids')
        else:
            # config_ids are a comma-separated string (e.g. `12,44,2,34`).
            return request.GET.get('config_ids', '').split(',')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial']['config_ids'] = self.config_ids
        return kwargs

    def form_valid(self, form):
        sector = form.cleaned_data['sector']
        category = form.cleaned_data['category']
        config_ids = form.cleaned_data['config_ids']
        self._assign_configs(sector, category, config_ids)
        return super().form_valid(form)

    def _assign_configs(self, sector, category, config_ids):
        update_kwargs = {}
        if sector:
            update_kwargs['sector'] = sector
        if category:
            update_kwargs['category'] = category
        CostLineItemConfig.objects.filter(
                cost_line_item__analysis_id=self.analysis.id
        ).filter(
                id__in=config_ids
        ).update(**update_kwargs)
        self.analysis.ensure_sector_category_objects()
        self.analysis.sector_categories.filter(sector=sector.id).update(confirmed=False)

    def get_success_message(self, cleaned_data):
        return _('%(count)s cost items updated') % {'count': len(self.config_ids)}

    def get_success_commands(self):
        return [
            panel_commands.Resolve()
        ]




class IdentifyOutputValue(AnalysisPermissionRequiredMixin, AnalysisStepMixin, DetailView):
    step_name = 'identify-output-value'
    permission_required = 'website.change_analysis'

    def get(self, request, *args, **kwargs):
        """
        Return redirect to first Sector to categorize.
        """
        if len(self.step.steps):
            return redirect(self.step.steps[0].get_href())
        else:
            return redirect(self.workflow.get_next(self.step).get_href())


class AssignOutputValue(AnalysisPermissionRequiredMixin, AnalysisStepMixin, PostActionHandlerMixin, DetailView):
    step_name = 'identify-output-value'
    template_name = 'analysis/assign-output-value.html'
    permission_required = 'website.change_analysis'
    title = _l('Which cost items correspond to the value of cash transferred?')
    actions = ['save_output_value', ]

    def setup_step(self):
        self.object = self.get_object()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.parent_step = self.workflow.active_step
        self.step = None
        for substep in self.parent_step.steps:
            if substep.get_href() == self.request.path:
                self.step = substep
                break

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'sector_categories': self.analysis.sector_categories.filter(
                sector=self.step.sector,
                sector_category_grants__contributes_to_activity=True
            ).distinct().select_related('sector', 'category'),
        })
        if len(self.parent_step.steps) and self.step == self.parent_step.steps[-1]:
            if not all([step.is_complete for step in self.parent_step.steps]):
                context.update({
                    'next_message': _('You must designate at least one item as value of cash transferred before moving on.'),
                })

        return context

    def handle_save_output_value(self, request, *args, **kwargs):
        ids = list(map(int, request.POST.getlist('assign_output_value')))
        selected = CostLineItemConfig.objects.filter(sector_id=self.step.sector.id, cost_line_item__id__in=ids)
        unselected = CostLineItemConfig.objects.filter(
            output_value=True, sector_id=self.step.sector.id,
            cost_line_item__analysis_id=self.analysis.id).exclude(cost_line_item__id__in=ids)
        selected.update(output_value=True)
        unselected.update(output_value=False)

        self.parent_step.update_parameter_values()


class SetContribution(AnalysisPermissionRequiredMixin, LoginRequiredMixin, AnalysisStepMixin, DetailView):
    """
    Redirects to the first sector/grant to set contributions.
    """
    step_name = 'set-contribution'
    permission_required = 'website.change_analysis'

    def get(self, request, *args, **kwargs):
        """
        Return redirect to first Sector to categorize.
        """
        if len(self.step.steps):
            url = self.step.steps[0].get_href()
            for step in self.step.steps:
                if not step.is_complete:
                    url = step.get_href()
                    break

            return redirect(url)
        else:
            return redirect(self.workflow.get_next(self.step).get_href())


class SetContributionSectorGrant(AnalysisPermissionRequiredMixin, AnalysisStepMixin, DetailView):
    step_name = 'set-contribution'
    help_text = _('Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/set-contribution-sector-grant.html'
    permission_required = 'website.change_analysis'

    def setup_step(self):
        self.object = self.get_object()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.parent_step = self.workflow.active_step

        self.step = None
        for substep in self.parent_step.steps:
            if substep.get_href() == self.request.path:
                self.step = substep
                break

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sector_category_grants = self.analysis.sector_category_grants.filter(
                sector_category__sector=self.step.sector,
                grant=self.step.grant,
        ).select_related('sector_category')
        unset_count = sector_category_grants.filter(contributes_to_activity=None).count()
        context.update({
            'sector_category_grants': sector_category_grants,
            'unset_count': unset_count,
            'title': _('Did these categories contribute to {activity_name}?').format(
                    activity_name=self.analysis.activity_name(),
            ),
        })
        next_step = self.step.get_next()
        if next_step and not next_step.dependencies_met:
            context.update({
                'next_message': "You must finish setting the contribution for all categories before moving on."
            })
        return context

    def post(self, request, *args, **kwargs):
        yes_ids = []
        no_ids = []
        for input_name, value in request.POST.items():
            if input_name.startswith('sector_category_grant_'):
                sector_category_grant_id = int(input_name.replace('sector_category_grant_', ''))
                if value == 'yes':
                    yes_ids.append(sector_category_grant_id)
                else:
                    no_ids.append(sector_category_grant_id)

        if len(yes_ids):
            AnalysisSectorCategoryGrant.objects.filter(
                    id__in=yes_ids,
                    sector_category__sector=self.step.sector,
                    grant=self.step.grant,
            ).update(contributes_to_activity=True)

        if len(no_ids):
            AnalysisSectorCategoryGrant.objects.filter(
                    id__in=no_ids,
                    sector_category__sector=self.step.sector,
                    grant=self.step.grant,
            ).update(contributes_to_activity=False)

        if len(yes_ids) or len(no_ids):
            self.workflow.invalidate_step('insights')

        self.workflow.calculate_if_possible()

        return redirect(self.request.path)


class Allocate(AnalysisPermissionRequiredMixin, AnalysisStepDetailMixin):
    """
    Redirects to the first sector/grant to allocate.
    """
    step_name = 'allocate'
    permission_required = 'website.change_analysis'

    def get(self, request, *args, **kwargs):
        """
        Return redirect to first Sector to categorize.
        """
        if len(self.step.steps):
            return redirect(self.step.steps[0].get_href())
        else:
            return redirect(self.workflow.get_next(self.step).get_href())




class AllocateSectorGrant(FilterMixin, AnalysisPermissionRequiredMixin, AnalysisStepFiltersetMixin, FilterView):
    step_name = 'allocate'
    help_text = _('Lorem ipsum dolor site amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.')
    template_name = 'analysis/allocate-sector-grant.html'
    permission_required = 'website.change_analysis'
    model = CostLineItem
    filterset_class = AllocateSectorGrantSiteFilterSet

    def setup_step(self):
        self.object = self.get_object()
        self.analysis = self.object
        self.workflow = Workflow(self.analysis, self.step_name)
        self.parent_step = self.workflow.active_step

        self.step = None
        for substep in self.parent_step.steps:
            if substep.get_href() == self.request.path:
                self.step = substep
                break

        self.sector_category_grants = self.analysis.sector_category_grants.filter(
                sector_category__sector=self.step.sector,
                grant=self.step.grant,
                contributes_to_activity=True,
        ).select_related('sector_category', 'sector_category__category')

    def modify_queryset(self, queryset):
        return queryset.filter(analysis_id=self.object.id,
            config__sector=self.step.sector,
            grant_code=self.step.grant,
        ).select_related('config', 'config__sector') \
            .prefetch_related('transactions') \
            .order_by(
                'grant_code',
                'budget_line_description',
                'site_code',
                'sector_code',
                'account_code',
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        cost_line_items_count = 0
        for sector_category_grant in self.sector_category_grants:
            cost_line_items_count += sector_category_grant.get_cost_line_items().count()

        context.update({
            'cost_line_items_count': cost_line_items_count,
            'sector_category_grants': self.sector_category_grants,
            'title': _('How much did each cost item contribute to {activity_name}?').format(
                    activity_name=self.analysis.activity_name(),
            ),
        })

        self.filterset.form.fields['site_code'].choices = self.object.site_codes_choices_from_cost_line_items()

        if self.step.sector.type_obj().shared:
            numerator, denominator, suggested_allocation = self.step.sector.type_obj().get_suggested_allocation(self.analysis, self.step.grant)
            context['suggested_allocation_numerator'] = numerator
            context['suggested_allocation_denominator'] = denominator
            context['suggested_allocation'] = suggested_allocation

            existing_allocation = self.sector_category_grants.first().get_cost_line_items().first().config.allocation
            if (suggested_allocation
                    and existing_allocation
                    and f"{suggested_allocation:.2f}" != f"{existing_allocation:.2f}"
                    and isinstance(self.step.sector.type_obj(), IndirectCostRecovery)):
                context.update({
                    'next_message': _("The suggested indirect costs allocation has changed since this step was last completed. "
                                      "Click \"Save Suggested %\" to update this analysis\' calculations with the updated "
                                      "suggested percentage."),
                })
        if ('next_message' not in context
                and self.step.get_next()
                and not self.step.get_next().dependencies_met):
            context.update({
                'next_message': _("You must finish setting the allocation for all cost items before moving on."),
            })

        return context

    def post(self, request, *args, **kwargs):
        data = self._get_cost_line_item_ids_and_allocations_from_post(request)
        data, errors = self._validate_data(data)
        if len(errors):
            context = self.get_context_data(errors=errors)
            return self.render_to_response(context)
        else:
            self._save_data(data)
            self.workflow.invalidate_step('insights')
            self.workflow.calculate_if_possible()
        query = f"?{request.GET.urlencode()}" if request.GET else ''
        return redirect(self.request.path + query)

    def _get_cost_line_item_ids_and_allocations_from_post(self, request):
        data = {}
        for input_name, allocation in request.POST.items():
            if input_name.startswith('cost_line_item_allocation_'):
                cost_line_item_id = int(input_name.replace('cost_line_item_allocation_', ''))
                if allocation != '':
                    data[cost_line_item_id] = allocation
                elif allocation == '':
                    data[cost_line_item_id] = None
        return data

    def _validate_data(self, data):
        errors = {}
        for cost_line_item_id, allocation in data.items():
            if allocation != None:
                try:
                    allocation = allocation.strip().replace('%', '')
                    allocation = Decimal(allocation)
                    data[cost_line_item_id] = allocation
                    if allocation > 100:
                        errors[cost_line_item_id] = _('Invalid')
                    elif allocation < 0:
                        errors[cost_line_item_id] = _('Invalid')
                except DecimalException:
                    errors[cost_line_item_id] = _('Not a number')
        return data, errors

    def _save_data(self, data):
        for cost_line_item_id, allocation in data.items():
            cost_line_item = self.analysis.cost_line_items.get(pk=cost_line_item_id)
            cost_line_item.config.allocation = allocation
            cost_line_item.config.save()



class SaveSuggestedToAllConfirmForm(Form):
    pass


class SaveSuggestedToAllConfirmView(AnalysisObjectMixin, FormView):
    title = None
    supertitle = 'Save Suggested % to All Items'
    template_name = 'panel-form-save-suggested.html'
    success_message = 'Suggested &#37; updated successfully.'
    form_class = SaveSuggestedToAllConfirmForm

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.analysis = self.get_object()
        self.workflow = Workflow(self.analysis, 'allocate')
        self.parent_step = self.workflow.active_step
        self.step = None
        for substep in self.parent_step.steps:
            if substep.get_href() == reverse('analysis-allocate-sector-grant', kwargs=self.kwargs):
                self.step = substep
                break

        self.sector_category_grants = self.analysis.sector_category_grants.filter(
            sector_category__sector=self.step.sector,
            grant=self.step.grant,
            contributes_to_activity=True,
        ).select_related('sector_category')

    def form_valid(self, form):
        response = super().form_valid(form)
        self._apply_suggested_value_to_all_cost_line_items()
        self.workflow.invalidate_step('insights')
        self.workflow.calculate_if_possible()
        return response

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['analysis'] = self.analysis
        ctx['workflow'] = self.workflow
        ctx['step'] = self.step
        return ctx

    def get_success_commands(self):
        return [
            Resolve({
                'operation': 'saved',
            })
        ]

    def _apply_suggested_value_to_all_cost_line_items(self):
        data = {}
        for sector_category_grant in self.sector_category_grants:
            for cost_line_item in sector_category_grant.get_cost_line_items():
                data[cost_line_item.id] = str(self.step.sector.type_obj().get_suggested_allocation(self.analysis, self.step.grant)[2])
        for cost_line_item_id, allocation in data.items():
            cost_line_item = self.analysis.cost_line_items.get(pk=cost_line_item_id)
            cost_line_item.config.allocation = allocation
            cost_line_item.config.save()



class Insights(AnalysisStepMixin, AnalysisPermissionRequiredMixin, DetailView):
    step_name = 'insights'
    template_name = 'analysis/insights.html'
    permission_required = 'website.view_analysis'

    def dispatch(self, request, *args, **kwargs):
        if not self.step.calculations_done():
            self.step.calculate_if_possible()
        if not (self.step.dependencies_met and self.step.is_complete):
            return redirect(reverse('analysis', kwargs={'pk': self.analysis.pk}))
        if not self.has_permission():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['output_metrics'] = []
        for output_metric in self.analysis.activity.output_metric_objects():
            context['output_metrics'].append({
                'output_metric': output_metric,
                'output_cost_all': self.analysis.output_costs[output_metric.id]['all'],
                'output_cost_direct_only': self.analysis.output_costs[output_metric.id]['direct_only'],
                'insights_chart_data': self._get_insights_chart_data(output_metric),
            })
        context['cost_efficiency_strategies'] = CostEfficiencyStrategy.objects.filter(activities=self.analysis.activity)
        context['cost_breakdown'] = self._get_cost_breakdown_data()
        context['cost_model_table_paginator'] = self._get_cost_model_table_paginator()
        context['cost_model_table_page'] = context['cost_model_table_paginator'].get_page(self.request.GET.get('page', 1))
        if self.analysis.currency_code and self.analysis.currency_code != settings.ISO_CURRENCY_CODE:
            context['data_excluded_footnote'] = "Since the analysis results are not in the default currency, " \
                                                "they are not directly comparable with the comparison data points, " \
                                                "therefore you should convert your results to the default currency " \
                                                "before comparing with these other data points."
        return context

    def _get_insights_chart_data(self, output_metric: OutputMetric):
        comparison_data_points = InsightComparisonData.objects.filter(activity=self.analysis.activity)
        if len(comparison_data_points) == 0:
            return None
        if not self.analysis.currency_code or self.analysis.currency_code == settings.ISO_CURRENCY_CODE:
            # This program is excluded from comparisons if the currency don't match.  This avoids currency conversion confusion.
            data = [
                {
                    'label': _('This Program'),
                    'grants': ', '.join(self.analysis.grants_list()),
                    'description': f'{output_metric.output_unit}: {output_metric.total_output(**self.analysis.parameters):,.2f}',
                    'tooltip': self.analysis.title,
                    'output_cost_all': self.analysis.output_costs[output_metric.id]['all'],
                    'output_cost_direct_only': self.analysis.output_costs[output_metric.id]['direct_only'],
                    'highlight': True,
                },
            ]
        else:
            data = []
        for comparison_data_point in comparison_data_points:
            data.append({
                'label': self._make_insights_chart_label(comparison_data_point.country.name),
                'grants': ', '.join(comparison_data_point.grants_list()),
                'description': f'{output_metric.output_unit}: {output_metric.total_output(**comparison_data_point.parameters):,.2f}',
                'tooltip': comparison_data_point.name,
                'output_cost_all': comparison_data_point.output_costs[output_metric.id]['all'],
                'output_cost_direct_only': comparison_data_point.output_costs[output_metric.id]['direct_only'],
                'highlight': False,
            })

        data.sort(key=lambda x: x['output_cost_all'])
        return data

    def _make_insights_chart_label(self, string):
        if len(string) > 25:
            break_index = self._insights_chart_label_find_break(string)
            if break_index is not None:
                return [
                    string[:break_index],
                    string[break_index:],
                ]
        return string

    def _insights_chart_label_find_break(self, string, start_index=0):
        index_of_space = string.find(' ', start_index)
        if index_of_space < 0:
            return None
        if index_of_space > 20:
            return index_of_space
        return self._insights_chart_label_find_break(string, index_of_space + 1)

    def _get_cost_breakdown_data(self):
        data = []
        query_result = self.analysis.contributing_cost_line_items.values(
                'config__sector__name', 'config__category__name'
        ).annotate(
                allocated_cost_sum=Sum(F('total_cost') * (F('config__allocation') / Value(100)))
        )
        for item in query_result:
            data.append({
                'sector_name': item['config__sector__name'],
                'category_name': item['config__category__name'],
                'amount': item['allocated_cost_sum'],
            })

        # Sort by amount but we aren't done...
        data.sort(key=lambda x: x['amount'], reverse=True)

        total_cost = sum([d['amount'] for d in data])

        for d in data:
            if total_cost > 0:
                d['percent_of_total'] = (d['amount'] / total_cost) * 100
                d['percent_of_total_formatted'] = float(format(d['percent_of_total'], '.2f'))
            else:
                d['percent_of_total'] = 0
                d['percent_of_total_formatted'] = 0.0

        # ICR should never be in the top 5 for this pie chart so we'll remove it and
        #   make sure it ends up in the All Other Costs section
        icr_index = next((index for (index, d) in enumerate(data) if d["category_name"] == "ICR"), None)
        if icr_index is not None:
            icr_data = [data.pop(icr_index)]
        else:
            icr_data = []

        chart_data = []
        for d in data[:5]:
            chart_data.append({
                'label': f'{d["category_name"]} ({d["sector_name"]}) {d["percent_of_total_formatted"]}%',
                'percent_of_total': float(format(d['percent_of_total'], '.2f')),
            })

        remaining_percent = sum(map(lambda d: d['percent_of_total'], data[5:] + icr_data))
        remaining_percent_formatted = float(format(remaining_percent, '.2f'))
        if remaining_percent > 0:
            chart_data.append({
                'label': f'{"All other costs"} {remaining_percent_formatted}%',
                'percent_of_total': remaining_percent,
            })

        # Move ICR back to where it was.
        data += icr_data
        data.sort(key=lambda x: x['amount'], reverse=True)

        return {
            'total_cost': total_cost,
            'costs_by_sector_category': data,
            'chart_data': chart_data,
        }

    def _get_cost_model_table_paginator(self):
        order_by = self.request.GET.get('order_by', None)
        if order_by not in ['total_cost', '-total_cost', 'config__allocation', '-config__allocation', 'allocated_cost', '-allocated_cost']:
            order_by = '-allocated_cost'

        cost_line_items = self.analysis.contributing_cost_line_items.annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).order_by(order_by)

        paginator = Paginator(cost_line_items, self.dioptra_settings.paginate_by)
        return paginator


class CostLineItemTransactions(PermissionRequiredMixin, DetailView):
    template_name = 'analysis/analysis-table/_analysis-table-transaction-rows-content.html'
    permission_required = 'website.change_analysis'
    model = CostLineItem

    def has_permission(self):
        perms = self.get_permission_required()
        analysis = self.get_object().analysis
        return self.request.user.has_perms(perms, analysis)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transactions'] = self.object.transactions.all()
        context['type'] = self.request.GET.get('type')
        return context

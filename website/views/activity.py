from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from website.models import Activity, InsightComparisonData, CostEfficiencyStrategy
from website.models.activity import OutputMetric


class ActivityInsights(LoginRequiredMixin, DetailView):
    model = Activity
    template_name = 'insights/insight.html'
    context_object_name = 'activity'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['output_metrics'] = []
        for output_metric in self.object.output_metric_objects():
            context['output_metrics'].append({
                'output_metric': output_metric,
                'insights_chart_data': self._get_insights_chart_data(output_metric),
            })
        context['cost_efficiency_strategies'] = CostEfficiencyStrategy.objects.filter(activities=self.object)
        context['show_cost_efficiency_comparison'] = any([
            output_metric['insights_chart_data']
            for output_metric in context['output_metrics']
        ])
        return context

    def _get_insights_chart_data(self, output_metric: OutputMetric):
        comparison_data_points = InsightComparisonData.objects.filter(activity=self.object)
        if len(comparison_data_points) == 0:
            return None

        data = []

        for comparison_data_point in comparison_data_points:
            data.append({
                'label': self._make_insights_chart_label(comparison_data_point.country.name),
                'grants': ', '.join(comparison_data_point.grants_list()),
                'description': f'{output_metric.output_unit}: {output_metric.total_output(**comparison_data_point.parameters):,.2f}',
                'tooltip': comparison_data_point.name,
                'output_cost_all': comparison_data_point.output_costs[output_metric.id]['all'],
                'output_cost_direct_only': comparison_data_point.output_costs[output_metric.id]['direct_only'],
            })

        data.sort(key=lambda x: x['output_cost_all'])
        return data

    def _make_insights_chart_label(self, string):
        if len(string) > 25:
            break_index = self._insights_chart_label_find_break(string)
            if break_index is not None:
                return [
                    string[:break_index],
                    string[break_index:],
                ]
        return string

    def _insights_chart_label_find_break(self, string, start_index=0):
        index_of_space = string.find(' ', start_index)
        if index_of_space < 0:
            return None
        if index_of_space > 20:
            return index_of_space
        return self._insights_chart_label_find_break(string, index_of_space + 1)


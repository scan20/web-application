from django.apps import AppConfig


class WebsiteConfig(AppConfig):

    name = 'website'
    label = 'website'
    verbose_name = 'Website'

    def ready(self):
        import website.app_log.signals

import requests
from allauth.socialaccount import app_settings
from allauth.socialaccount.providers.oauth2.views import OAuth2Adapter, OAuth2CallbackView, OAuth2LoginView
from django.conf import settings as django_settings
from .provider import OneLoginProvider


_root_url = f'https://{django_settings.ONELOGIN_APP}.onelogin.com'


class OneLoginOAuth2Adapter(OAuth2Adapter):
    provider_id = OneLoginProvider.id
    settings = app_settings.PROVIDERS.get(provider_id, {})

    authorize_url = f'{_root_url}/oidc/2/auth'
    access_token_url = f'{_root_url}/oidc/2/token'
    profile_url = f'{_root_url}/oidc/2/me'

    redirect_uri_protocol = settings.get('PROTOCOL', 'https')

    def complete_login(self, request, app, access_token, **kwargs):
        headers = {"Authorization": f"Bearer {access_token.token}"}
        profile_data = requests.get(self.profile_url, headers=headers)
        return self.get_provider().sociallogin_from_response(request, profile_data.json())


oauth2_login = OAuth2LoginView.adapter_view(OneLoginOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(OneLoginOAuth2Adapter)

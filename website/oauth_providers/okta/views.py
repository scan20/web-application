import requests
from allauth.socialaccount import app_settings
from allauth.socialaccount.providers.oauth2.views import OAuth2Adapter, OAuth2CallbackView, OAuth2LoginView
from django.conf import settings as django_settings
from .provider import OktaProvider


class OktaOAuth2Adapter(OAuth2Adapter):
    provider_id = OktaProvider.id
    settings = app_settings.PROVIDERS.get(provider_id, {})

    provider_default_url = settings.get('DEFAULT_URL')

    authorize_url = f'{provider_default_url}{django_settings.OKTA_OAUTH2_PATH}/authorize'
    access_token_url = f'{provider_default_url}{django_settings.OKTA_OAUTH2_PATH}/token'
    profile_url = f'{provider_default_url}{django_settings.OKTA_OAUTH2_PATH}/userinfo'

    redirect_uri_protocol = settings.get('PROTOCOL', 'https')

    def complete_login(self, request, app, access_token, **kwargs):
        headers = {"Authorization": f"Bearer {access_token.token}"}
        profile_data = requests.get(self.profile_url, headers=headers)
        return self.get_provider().sociallogin_from_response(request, profile_data.json())


oauth2_login = OAuth2LoginView.adapter_view(OktaOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(OktaOAuth2Adapter)

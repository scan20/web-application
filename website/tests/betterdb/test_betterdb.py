import unittest
from collections import namedtuple

from django import db
from django.db import connection
from django.test import TestCase
from expects import *

from website.betterdb import (
    bulk_delete,
    delete,
)
from website.betterdb.bulk_create_manytomany import bulk_create_manytomany
from website.betterdb.bulk_update_dicts import build_update_sql
from website.betterdb.bulk_upsert import build_upsert_sql
from website.betterdb.transactions import Rollback, transaction
from website.tests.betterdb.models import TestM2M, TestTree


def dbcalls():
    return len(connection.queries)


class FakeBase(object):
    pk = None

    _meta = namedtuple("fakemeta", ["concrete_fields"])([])

    def save(self, *args, **kwargs):
        self.pk = 1

    def delete(self, *args, **kwargs):
        self.pk = None

    def refresh_from_db(self, *args, **kwargs):
        pass


class UpsertTests(TestCase):
    def setUp(self):
        self.cursor = db.connection.cursor().__enter__()

    def build(self, *args, **kwargs):
        safe, rows = build_upsert_sql(self.cursor, *args, **kwargs)
        return safe.as_string(self.cursor.cursor)

    def test_generates_sql(self):
        expect(self.build("tbl", [{"x": 1, "y": 2}, {"x": 3, "y": 4}])).to(
            equal('INSERT INTO "tbl" ("x", "y") VALUES %s ON CONFLICT DO NOTHING')
        )
        expect(self.build("tbl", [{"x": 1}, {"x": 3}])).to(
            equal('INSERT INTO "tbl" ("x") VALUES %s ON CONFLICT DO NOTHING')
        )
        expect(self.build("tbl", [{"x": 1}, {"x": 3}], constraint="x")).to(
            equal('INSERT INTO "tbl" ("x") VALUES %s ON CONFLICT ("x") DO NOTHING')
        )
        expect(self.build("tbl", [{"x": 1}, {"x": 3}], constraint="uniq")).to(
            equal(
                'INSERT INTO "tbl" ("x") VALUES %s ON CONFLICT ON CONSTRAINT "uniq" DO NOTHING'
            )
        )
        expect(self.build("tbl", [{"x": 1, "y": 2}, {"x": 3, "y": 4}], update=True)).to(
            equal(
                'INSERT INTO "tbl" ("x", "y") VALUES %s ON CONFLICT DO UPDATE SET ("x", "y") = (EXCLUDED."x", EXCLUDED."y")'
            )
        )
        expect(
            self.build(
                "tbl", [{"x": 1, "y": 2}, {"x": 3, "y": 4}], constraint="x", update=True
            )
        ).to(
            equal(
                'INSERT INTO "tbl" ("x", "y") VALUES %s ON CONFLICT ("x") DO UPDATE SET ("y") = (EXCLUDED."y")'
            )
        )
        expect(
            self.build("tbl", [{"x": 1}, {"x": 3}], constraint="uniq", update=True)
        ).to(
            equal(
                'INSERT INTO "tbl" ("x") VALUES %s ON CONFLICT ON CONSTRAINT "uniq" DO UPDATE SET ("x") = (EXCLUDED."x")'
            )
        )
        expect(
            self.build("tbl", [{"x": 1, "y": 2}, {"x": 3, "y": 4}], update=["x"])
        ).to(
            equal(
                'INSERT INTO "tbl" ("x", "y") VALUES %s ON CONFLICT DO UPDATE SET ("x") = (EXCLUDED."x")'
            )
        )

    def test_errors_for_mismatched_keys(self):
        with self.assertRaises(KeyError):
            self.build("tbl", [{"x": 1}, {"y": 2}])

    def test_orders_rows(self):
        _, rows = build_upsert_sql(
            self.cursor, "tbl", [{"z": 1, "x": 2}, {"x": 3, "z": 4}]
        )
        expect(rows).to(equal([[2, 1], [3, 4]]))


class BulkCreateManyToManyTests(TestCase):
    def test_creates(self):
        a, b, c, d = [TestM2M.objects.create() for _ in range(4)]
        bulk_create_manytomany(
            TestM2M,
            "others",
            "from_testm2m",
            "to_testm2m",
            [(a, b), (a, c), (b, c), (b, d)],
        )
        expect(list(a.others.all())).to(have_len(2))
        expect(list(b.others.all())).to(have_len(2))
        expect(list(c.others.all())).to(be_empty)


class BulkUpdateDictsTests(TestCase):
    def setUp(self):
        self.cursor = db.connection.cursor().__enter__()

    def build(self, *args, **kwargs):
        safe, rows = build_update_sql(self.cursor, *args, **kwargs)
        return safe.as_string(self.cursor.cursor)

    def test_generates_sql(self):
        expect(
            self.build(
                "tbl", [{"x": 1, "y": 2, "z": 3}, {"x": 3, "y": 4, "z": 5}], ["x"]
            )
        ).to(
            equal(
                'UPDATE "tbl" AS t1 SET "y" = t2."y", "z" = t2."z" FROM (VALUES %s) AS t2("x", "y", "z") WHERE t1."x" = t2."x"'
            )
        )
        expect(
            self.build(
                "tbl", [{"x": 1, "y": 2, "z": 3}, {"x": 3, "y": 4, "z": 5}], ["x", "y"]
            )
        ).to(
            equal(
                'UPDATE "tbl" AS t1 SET "z" = t2."z" FROM (VALUES %s) AS t2("x", "y", "z") WHERE t1."x" = t2."x" AND t1."y" = t2."y"'
            )
        )
        expect(self.build("tbl", [{"x": 1, "y": 2}, {"x": 3, "y": 4}], "x")).to(
            equal(
                'UPDATE "tbl" AS t1 SET "y" = t2."y" FROM (VALUES %s) AS t2("x", "y") WHERE t1."x" = t2."x"'
            )
        )
        expect(
            self.build(
                "tbl",
                [{"x": 1, "y": 2}, {"x": 3, "y": 4}],
                ["x"],
                expressions={"y": lambda lhs, rhs: f"{lhs} || {rhs}"},
            )
        ).to(
            equal(
                'UPDATE "tbl" AS t1 SET "y" = t1."y" || t2."y" FROM (VALUES %s) AS t2("x", "y") WHERE t1."x" = t2."x"'
            )
        )

    def test_errors_on_only_pk(self):
        with self.assertRaises(ValueError):
            self.build("tbl", [{"x": 1}], "x")

    def test_errors_on_missing_pk(self):
        with self.assertRaises(ValueError):
            self.build("tbl", [{"y": 2}], "x")

    def test_errors_for_mismatched_keys(self):
        with self.assertRaises(KeyError):
            self.build("tbl", [{"x": 1, "z": 2}, {"x": 1, "y": 2}], "x")

    def test_orders_rows(self):
        _, rows = build_update_sql(
            self.cursor, "tbl", [{"z": 1, "x": 2}, {"x": 3, "z": 4}], "x"
        )
        expect(rows).to(equal([[2, 1], [3, 4]]))


class DeleteTests(TestCase):
    def test_bulk_deletes_multiple_querysets(self):
        t1, t2, t3, t4 = [TestTree.objects.create(name=str(i)) for i in range(4)]
        q1, q2, q3, q4 = [TestTree.objects.filter(name=str(i)) for i in range(4)]
        expect(bulk_delete([q1, q3])).to(equal(2))
        expect(list(TestTree.objects.all())).to(contain_exactly(t2, t4))
        expect(delete(q2)).to(equal(1))
        expect(list(TestTree.objects.all())).to(contain_exactly(t4))

    def test_works_with_empty_set(self):
        expect(bulk_delete([])).to(equal(0))
        expect(delete(TestTree.objects.none())).to(equal(0))


class TransactionTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        delete(TestTree.objects.all())

    @classmethod
    def tearDownClass(cls):
        delete(TestTree.objects.all())

    def test_transaction_propogates_error(self):
        expect(TestTree.objects.all().count()).to(equal(0))
        with self.assertRaises(ZeroDivisionError):
            with transaction():
                TestTree.objects.create(name="x")
                _ = 1 / 0
        expect(TestTree.objects.all().count()).to(equal(0))

    def test_transaction_propogates_rollbacks(self):
        expect(TestTree.objects.all().count()).to(equal(0))
        with self.assertRaises(Rollback):
            with transaction():
                TestTree.objects.create(name="x")
                raise Rollback()
        expect(TestTree.objects.all().count()).to(equal(0))

    def test_transaction_can_ignore_rollback(self):
        expect(TestTree.objects.all().count()).to(equal(0))
        with transaction(reraise_rollback=False):
            TestTree.objects.create(name="x")
            raise Rollback()
        expect(TestTree.objects.all().count()).to(equal(0))

    def test_invokes_callback_on_rollback(self):
        expect(TestTree.objects.all().count()).to(equal(0))
        x = []
        with transaction(reraise_rollback=False, on_rollback=lambda: x.append(1)):
            TestTree.objects.create(name="x")
            raise Rollback()
        expect(TestTree.objects.all().count()).to(equal(0))
        expect(x).to(equal([1]))

    def test_can_use_savepoint(self):
        with transaction():
            TestTree.objects.create(name="x")
            with self.assertRaises(ZeroDivisionError):
                with transaction(savepoint=True):
                    TestTree.objects.create(name="y")
                    _ = 1 / 0
        expect(TestTree.objects.all().count()).to(equal(1))
        delete(TestTree.objects.all())

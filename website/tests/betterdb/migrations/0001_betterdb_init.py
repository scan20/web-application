from django.db import migrations, models
import django.db.models.deletion

from website import betterdb


class Migration(migrations.Migration):

    dependencies = [("website", "0001_initial")]

    operations = [
        migrations.CreateModel(
            name="TestTree",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField()),
            ],
            bases=(
                models.Model,
                betterdb.ReprMixin,
            ),
        ),
        migrations.AddField(
            model_name="testtree",
            name="parent",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="children_set",
                to="betterdb.TestTree",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="TestM2M",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField()),
                (
                    "others",
                    models.ManyToManyField(
                        related_name="other_set", to="betterdb.TestM2M"
                    ),
                ),
            ],
            bases=(
                betterdb.ReprMixin,
                models.Model,
            ),
        ),
    ]

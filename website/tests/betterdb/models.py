from django.db import models

from website.betterdb import ReprMixin


class TestTree(ReprMixin, models.Model):
    name = models.TextField()

    parent = models.ForeignKey(
        "TestTree", null=True, on_delete=models.SET_NULL, related_name="children_set"
    )


class TestM2M(ReprMixin, models.Model):
    name = models.TextField()

    others = models.ManyToManyField("TestM2M", related_name="others_reverse")

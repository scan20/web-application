import datetime
import os
import unittest

from django.test import TestCase

from website.data_loading import get_transactions_data_store_count
from website.data_loading import load_cost_line_items_from_file
from website.data_loading import load_transactions_from_data_store
from website.models.activity import Activity, ActivityGroup
from website.models.analysis import Analysis, AnalysisType
from website.models.core import Country
from website.tests import import_test_transaction_store
from website.views.duplicator import clone_analysis

test_data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_data')


def make_analysis(**kwargs):
    activity_group, created = ActivityGroup.objects.get_or_create(name="Test Group")
    activity, created = Activity.objects.get_or_create(name='Treatment For Severe Acute Malnutrition', group=activity_group)
    defaults = {
        'title': 'SAM Treatment Gabon: Grants DF119, OD061',
        'activity': activity,
        'parameters': {
            'number_of_children_treated_for_sam': 1600,
        },
        'description': 'Analysis description',
        'start_date': datetime.date(2017, 6, 12),
        'end_date': datetime.date(2018, 11, 19),
        'grants': 'DF119,OD061',
    }
    defaults.update(kwargs)
    return Analysis.objects.create(**defaults)

@unittest.skip("These tests are broken but should be repaired")
class BudgetUploadTestCase(TestCase):

    def setUp(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
        }
        self.countries = {
            'Gabon': Country.objects.create(name='Gabon', code='GA'),
        }

    def test_load_data_from_xlsx_file_succeeds(self):
        file_path = os.path.join(test_data_dir, 'MC - YDP Budget - Clean Year 1.xlsx')
        analysis = make_analysis(
                analysis_type=self.analysis_types['Ex-ante analysis'],
                country=self.countries['Gabon'],
        )
        with open(file_path, 'rb') as f:
            succeeded, result = load_cost_line_items_from_file(analysis, f)
            self.assertEqual(succeeded, True)

    def test_load_data_from_csv_file_succeeds(self):
        file_path = os.path.join(test_data_dir, 'MC - YDP Budget - Clean Year 1.xlsx - Sheet1.csv')
        analysis = make_analysis(
                analysis_type=self.analysis_types['Ex-ante analysis'],
                country=self.countries['Gabon'],
        )
        with open(file_path, 'rb') as f:
            succeeded, result = load_cost_line_items_from_file(analysis, f)
            self.assertEqual(succeeded, True)

    def test_load_data_from_csv_file_with_windows_encoding(self):
        file_path = os.path.join(test_data_dir, 'Budget - Shared Costs, ICR Calculators_LL.csv')
        analysis = make_analysis(
                analysis_type=self.analysis_types['Ex-ante analysis'],
                country=self.countries['Gabon'],
        )
        with open(file_path, 'rb') as f:
            succeeded, result = load_cost_line_items_from_file(analysis, f)
            self.assertEqual(succeeded, True)


@unittest.skip("These tests are broken but should be repaired")
class TransactionsImportTestCast(TestCase):
    databases = '__all__'
    def setUp(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
        }
        self.countries = {
            'Gabon': Country.objects.create(name='Gabon', code='5JO'),
        }

    def test_get_transactions_data_store_count(self):
        count = get_transactions_data_store_count(['DF119'], '2015-01-31', '2015-06-31')
        self.assertTrue(isinstance(count, int))
        self.assertTrue(count > 0)

        count = get_transactions_data_store_count(['fakegrantcode'], '2015-01-31', '2015-06-31')
        self.assertEqual(count, 0)

    def test_load_transactions_from_data_store(self):
        analysis = make_analysis(
                analysis_type=self.analysis_types['Ex-ante analysis'],
                country=self.countries['Gabon'],
                start_date=datetime.date(2000, 1, 31),
                end_date=datetime.date(2019, 6, 30),
                grants='DF119',
        )
        transaction_count = get_transactions_data_store_count(['DF119'], '2000-01-31', '2019-06-30')
        analysis = load_transactions_from_data_store(analysis)
        self.assertEqual(transaction_count, analysis.transactions.all().count())



class FilterZeroCostLineItemsTestCase(TestCase):
    """
    Given Budget spreadsheet with 0 dollar cost items.xlsx Excel file,
    I can create an analysis: 01-Apr-2000 to 01-Apr-2020, Afghanistan, grant = Unknown
    """
    def setUp(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
        }
        self.countries = {
            'Afghanistan': Country.objects.create(name='Afghanistan', code='AF'),
        }

    def test_load_data_from_xlsx_file(self):
        file_path = os.path.join(test_data_dir, 'Budget spreadsheet with 0 dollar cost items.xlsx')
        analysis = make_analysis(
                analysis_type=self.analysis_types['Ex-ante analysis'],
                country=self.countries['Afghanistan'],
                grants='Unknown',
                start_date=datetime.date(2000, 4, 1),
                end_date=datetime.date(2020, 4, 1),
        )
        with open(file_path, 'rb') as f:
            succeeded, result = load_cost_line_items_from_file(analysis, f)
            self.assertEqual(succeeded, True)
            self.assertEqual(result['imported_count'], 183)


@unittest.skip("These tests are WIP")
class TestSetupTestCase(TestCase):
    transactions_sql_dump = 'ClonedAnalysisTest-transaction-store-test-data.sql'

    def setUp(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
        }
        self.countries = {
            'Afghanistan': Country.objects.create(name='Afghanistan', code='AF'),
        }
        self.analysis = make_analysis(
            analysis_type=self.analysis_types['Ex-ante analysis'],
            country=self.countries['Afghanistan'],
            grants='Unknown',
            start_date=datetime.date(2000, 4, 1),
            end_date=datetime.date(2020, 4, 1),
        )
        import_test_transaction_store(self.transactions_sql_dump)

    def test_cloning_analysis(self):
        self.assertGreater(self.analysis.transactions.count(), 0)


@unittest.skip("These tests are WIP")
class ClonedAnalysisTestCase(TestCase):
    transactions_sql_dump = 'ClonedAnalysisTest-transaction-store-test-data.sql'

    def setUp(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
        }
        self.countries = {
            'Afghanistan': Country.objects.create(name='Afghanistan', code='AF'),
        }

    def _setup_test_analysis(self):
        import_test_transaction_store(self.transactions_sql_dump)
        self.analysis = make_analysis(
            analysis_type=self.analysis_types['Ex-ante analysis'],
            country=self.countries['Afghanistan'],
            grants='DF119',
            start_date=datetime.date(2000, 4, 1),
            end_date=datetime.date(2020, 4, 1),
        )

        load_transactions_from_data_store(self.analysis)

    def test_cloning_analysis(self):
        source_transaction_count = get_transactions_data_store_count(
            self.analysis.grants, self.analysis.start_date, self.analysis.end_date)

        load_transactions_from_data_store(self.analysis)

        self.assertEqual(source_transaction_count, )
        self.assertEqual(source_transaction_count, self.analysis.transactions.all().count())

        cloned_analysis = clone_analysis(self.analysis)
        self.assertEqual(self.analysis.start_date, cloned_analysis.start_date)
        self.assertEqual(self.analysis.end_date, cloned_analysis.end_date)


    def test_cloning_analysis_with_different_date_range(self):
        source_transaction_count = get_transactions_data_store_count(
            self.analysis.grants_list(), self.analysis.start_date, self.analysis.end_date)

        load_transactions_from_data_store(self.analysis)
        start_date, end_date, owner = None, None, None
        cloned_analysis = clone_analysis(self.analysis, start_date=start_date, end_date=end_date, owner=owner)
        self.assertEqual(self.analysis.transactions.count(), cloned_analysis.transactions.count())
        self.assertNotEqual(start_date, cloned_analysis.start_date)
        self.assertNotEqual(end_date, cloned_analysis.end_date)
        self.assertTrue(cloned_analysis.needs_transaction_store_sync)

        cloned_analysis_transaction_count = get_transactions_data_store_count(
            cloned_analysis, cloned_analysis.start_date, cloned_analysis.end_date)

        self.assertNotEqual(source_transaction_count, cloned_analysis_transaction_count)

        self.assertNotEqual(self.analysis.transactions.count(), cloned_analysis.transactions.count())
        self.assertFalse(cloned_analysis.needs_transaction_resync)
        self.assertEqual(cloned_analysis_transaction_count, cloned_analysis.transactions.count())

        self.assertFalse(cloned_analysis.needs_transaction_resync)

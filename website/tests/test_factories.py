import pytest

from website.models import Activity, ActivityGroup, Analysis, AnalysisType, Category, CostLineItem, Country, Sector, Transaction
from .factories import ActivityFactory, ActivityGroupFactory, AnalysisFactory, AnalysisTypeFactory, CategoryFactory, CostLineItemFactory, CountryFactory, SectorFactory, TransactionFactory


@pytest.mark.django_db
def test_country_factory():
    m = CountryFactory(name="Test Country Name")
    assert m.name == "Test Country Name"
    assert Country.objects.count() == 1


@pytest.mark.django_db
def test_activity_group_factory():
    m = ActivityGroupFactory(name="Test Activity Group Name")
    assert m.name == "Test Activity Group Name"
    assert ActivityGroup.objects.count() == 1


@pytest.mark.django_db
def test_analysis_type_factory():
    m = AnalysisTypeFactory(title="Test Analysis Type Title")
    assert m.title == "Test Analysis Type Title"
    assert AnalysisType.objects.count() == 1


@pytest.mark.django_db
def test_activity_factory():
    m = ActivityFactory(name="Test Activity Name")
    assert m.name == "Test Activity Name"
    assert Activity.objects.count() == 1
    assert ActivityGroup.objects.count() == 1


@pytest.mark.django_db
def test_analysis_factory():
    m = AnalysisFactory(title="Test Analysis Title")
    assert m.title == "Test Analysis Title"
    assert Analysis.objects.count() == 1
    assert AnalysisType.objects.count() == 1
    assert Activity.objects.count() == 1
    assert Country.objects.count() == 1


@pytest.mark.django_db
def test_sector_factory():
    m = SectorFactory(name="Test Sector Name")
    assert m.name == "Test Sector Name"
    assert Sector.objects.count() == 1


@pytest.mark.django_db
def test_category_factory():
    m = CategoryFactory(name="Test Category Name")
    assert m.name == "Test Category Name"
    assert Category.objects.count() == 1


@pytest.mark.django_db
def test_costlineitem_factory():
    m = CostLineItemFactory(grant_code="Test Cost Line Item Grant Code")
    assert m.grant_code == "Test Cost Line Item Grant Code"
    assert CostLineItem.objects.count() == 1
    assert Analysis.objects.count() == 1

@pytest.mark.django_db
def test_transaction_factory():
    m = TransactionFactory()
    assert Transaction.objects.count() == 1

import datetime
from decimal import Decimal

import factory
from factory.fuzzy import FuzzyDate

from website.models import Activity, ActivityGroup, Analysis, AnalysisType, Category, CostLineItem, Country, Sector, Transaction


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country


class ActivityGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ActivityGroup


class AnalysisTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AnalysisType


class ActivityFactory(factory.django.DjangoModelFactory):
    group = factory.SubFactory(ActivityGroupFactory)

    class Meta:
        model = Activity


class AnalysisFactory(factory.django.DjangoModelFactory):
    analysis_type = factory.SubFactory(AnalysisTypeFactory)
    activity = factory.SubFactory(ActivityFactory)
    country = factory.SubFactory(CountryFactory)
    start_date = FuzzyDate(datetime.date(2010, 1, 1), datetime.date(2018, 12, 31))
    end_date = FuzzyDate(datetime.date(2019, 1, 1))

    class Meta:
        model = Analysis


class SectorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Sector


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category


class CostLineItemFactory(factory.django.DjangoModelFactory):
    total_cost = Decimal("100.01")
    analysis = factory.SubFactory(AnalysisFactory)

    class Meta:
        model = CostLineItem

class TransactionFactory(factory.django.DjangoModelFactory):
    date = FuzzyDate(datetime.date(2010,1,1))
    amount_in_instance_currency = Decimal("100.01")
    amount_in_source_currency = Decimal("100.01")
    analysis = factory.SubFactory(AnalysisFactory)
    class Meta:
        model = Transaction
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transactions (
    transaction_date date,
    country_code text,
    grant_code text,
    budget_line_code text,
    account_code text,
    site_code text,
    sector_code text,
    transaction_code text,
    transaction_description text,
    currency_code character varying(4),
    budget_line_description text,
    amount numeric,
    dummy_field_1 text,
    dummy_field_2 text,
    dummy_field_3 text,
    dummy_field_4 text,
    dummy_field_5 text
);


--
-- Name: TABLE transactions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.transactions IS 'Created 2020-03-30T23:25:13.043842';



--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transactions (transaction_date, country_code, grant_code, budget_line_code, account_code, site_code, sector_code, transaction_code, transaction_description, currency_code, budget_line_description, amount, dummy_field_1, dummy_field_2, dummy_field_3, dummy_field_4, dummy_field_5) FROM stdin;
2015-05-09	2LR	GA298	SAL01	70050	MO	OADM	2015007 payroll	Dianah Majekodunmi	USD	DCOP/ Snr. TA (Health)-SA	2014	\N	\N	\N	\N	\N
2015-05-09	2LR	GA298	SAL02	70050	MO	HEAL	2015007 payroll	Alfred J. Drobia	USD	Senior Advisor (M&E)-SAL	2050	\N	\N	\N	\N	\N
2015-05-09	2LR	GA298	SUP06	75000	MO	HEAL	MUBV 12808	Breakfast + Conf room	USD	Central-level Self-Assess	700	\N	\N	\N	\N	\N
2015-05-10	5IQ	DF119	E8222	71600	\N	HEAL	6523	Passport copy Fayrouz Sh.	USD	Housing - CO Support	2.16	\N	\N	\N	\N	\N
2015-05-10	5JO	DF119	BE01	71301	AMM	OADM	JV1530/1507	SCHOOL TUITION/FC KIDS	JOD	Finance Controller	162.6	\N	\N	\N	\N	\N
2015-05-10	5JO	DF119	BPH01	71311	AMM	OADM	APCV/1549	HEATING MAINT KHURRAM	JOD	Deputy Director of Operat	2.47	\N	\N	\N	\N	\N
2015-05-10	5JO	DF119	BPH02	71311	AMM	OADF	OPC/1607	FIRE EXTNGSHER/HUSSAM.G	JOD	Field Coordinator Mafraq	5.76	\N	\N	\N	\N	\N
\.



--
-- PostgreSQL database dump complete
--


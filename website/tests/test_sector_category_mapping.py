import datetime
import unittest

from django.test import TestCase

from website.models import Activity, ActivityGroup, Analysis, AnalysisType, Category, CostLineItem, Sector, SectorCategoryMapping
from website.models import Country


@unittest.skip("These tests are broken but should be repaired")
class SectorCategoryMappingTestCase(TestCase):

    def setUp(self):
        self.country = Country.objects.create(name='Mock Country', code='MC')
        self.activity_group = ActivityGroup.objects.create(name="Test Group")

        self.analysis_type = AnalysisType.objects.create(title='Mock Analysis Type')
        self.activity = Activity.objects.create(
                name='Mock Activity',
                group=self.activity_group,
        )
        self.analysis = Analysis.objects.create(**{
            'title': 'SAM Treatment Gabon: Grants DF119, OD061',
            'analysis_type': self.analysis_type,
            'activity': self.activity,
            'country': self.country,
            'parameters': {
                'number_of_children_treated_for_sam': 1600,
            },
            'description': 'Analysis description',
            'start_date': datetime.date(2017, 6, 12),
            'end_date': datetime.date(2018, 11, 19),
            'grants': 'DF119,OD061',
        })
        self.sector1 = Sector.objects.create(name='Sector 1')
        self.sector2 = Sector.objects.create(name='Sector 2')
        self.category1 = Category.objects.create(name='Category 1')
        self.category2 = Category.objects.create(name='Category 2')

    def test_map_by_country_code(self):
        cost_line_item = self._create_mock_cost_line_item(
                country_code='5JO',
        )
        SectorCategoryMapping.objects.create(
                country_code='5JO',
                sector=self.sector1,
        )
        SectorCategoryMapping.auto_categorize_cost_line_items(CostLineItem.objects.all())

        cost_line_item = CostLineItem.objects.get(pk=cost_line_item.pk)

        self.assertEqual(cost_line_item.config.sector, self.sector1)
        self.assertEqual(cost_line_item.config.category, None)

    def test_map_separate_sector_and_category(self):
        cost_line_item = self._create_mock_cost_line_item(
                country_code='5JO',
        )
        SectorCategoryMapping.objects.create(
                country_code='5JO',
                sector=self.sector1,
        )
        SectorCategoryMapping.objects.create(
                country_code='5JO',
                category=self.category1,
        )
        SectorCategoryMapping.auto_categorize_cost_line_items(CostLineItem.objects.all())

        cost_line_item = CostLineItem.objects.get(pk=cost_line_item.pk)

        self.assertEqual(cost_line_item.config.sector, self.sector1)
        self.assertEqual(cost_line_item.config.category, self.category1)

    def test_map_generic_to_specific(self):
        cost_line_item = self._create_mock_cost_line_item(
                country_code='5JO',
                grant_code='DF119',
        )
        SectorCategoryMapping.objects.create(
                country_code='5JO',
                sector=self.sector1,
        )
        SectorCategoryMapping.objects.create(
                country_code='5JO',
                grant_code='DF119',
                sector=self.sector2,
        )
        SectorCategoryMapping.auto_categorize_cost_line_items(CostLineItem.objects.all())

        cost_line_item = CostLineItem.objects.get(pk=cost_line_item.pk)

        self.assertEqual(cost_line_item.config.sector, self.sector2)
        self.assertEqual(cost_line_item.config.category, None)

    def _create_mock_cost_line_item(self, **kwargs):
        defaults = {
            'analysis': self.analysis,
            'country_code': 'AAAAAAAAAAA',
            'grant_code': 'AAAAAAAAA',
            'budget_line_code': 'AAAAAAAAAAA',
            'account_code': 'AAAAAAAAAAA',
            'site_code': 'AAAAAAAAAAA',
            'sector_code': 'AAAAAAAAAAA',
            'budget_line_description': 'AAAAAAAAAAA',
            'total_cost': 100,
        }
        defaults.update(**kwargs)
        return CostLineItem.objects.create(**defaults)

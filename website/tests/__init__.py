import os
import subprocess

from django.conf import settings


def import_test_transaction_store(dump):
    db = settings.DATABASES.get("transaction_store")
    user, name, host, pw, port = (
        db.get("USER"),
        db.get("NAME"),
        db.get("HOST"),
        db.get("PASSWORD"),
        db.get("PORT"),
    )
    # nosec: We need to use subprocess with shell to run these tests since they use psql against
    # another running container. This is only used in tests anyway.
    subprocess.run(
        f"PGPASSWORD='{pw}' psql -q -h {host} -p {port} -U {user} {name} -c 'DROP TABLE IF EXISTS transactions; DROP TABLE IF EXISTS transactions_meta'",
        shell=True,  # nosec
    )
    subprocess.run(
        f"PGPASSWORD='{pw}' psql -q -h {host} -p {port} -U {user} {name} < {os.path.join(settings.PROJECT_DIR, 'website/tests/test_data', dump)}",
        shell=True,  # nosec
    )

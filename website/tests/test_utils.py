from django.test import SimpleTestCase
from expects import *

from website.utils import (
    StableSet,
    columnify,
    count,
    flatlist,
    flatten,
    flow,
    group_and_count,
    group_by,
    invert,
    iremoveprefix,
    iremovesuffix,
    minmax,
    pull_if,
    remove_if,
    removeprefix,
    removesuffix,
    unique,
    immutablemap,
)


class TestUnique(SimpleTestCase):
    def test_unique_preserves_order(self):
        expect(str(unique([]))).to(contain("generator"))
        expect(list(unique([4, 2, 1, 4, 2, 3]))).to(equal([4, 2, 1, 3]))
        expect(list(unique([3, 2, 4, 1, 2, 4]))).to(equal([3, 2, 4, 1]))
        expect(list(unique([]))).to(equal([]))

    def test_unique_with_predicate(self):
        expect(list(unique([4, 2, 1, 4, 2, 3], lambda x: x % 2))).to(equal([4, 1]))


class FlattenTests(SimpleTestCase):
    def test_flattens_lists(self):
        expect(flatten([[1, 2], [3, 4]])).to(equal([1, 2, 3, 4]))

    def test_flattens_generators(self):
        g1 = (i for i in [1, 2])
        g2 = (i for i in [3, 4])
        expect(flatten(g for g in [g1, g2])).to(equal([1, 2, 3, 4]))


class FlowTests(SimpleTestCase):
    def test_flows(self):
        f = flow([lambda x: x + 1, lambda x: x * 10])
        y = f(1)
        expect(y).to(equal(20))


class GroupByTests(SimpleTestCase):
    def test_group_by(self):
        f = group_by([1, 2, 3, 4, 5, 6, 7], lambda x: x % 2 == 0)
        expect(f).to(equal({True: [2, 4, 6], False: [1, 3, 5, 7]}))


class GroupAndCountTests(SimpleTestCase):
    def test_group_and_count(self):
        f = group_and_count([1, 2, 3, 4, 5, 6, 7], lambda x: x % 2 == 0)
        expect(f).to(equal({True: 3, False: 4}))


class ColumnifyTests(SimpleTestCase):
    def test_columnify(self):
        expect(columnify("(AB)")).to(equal("a_b"))
        expect(columnify(" ABCD ")).to(equal("a_b_c_d"))
        expect(columnify("A (b1) C2 3 4d")).to(equal("a_b_1_c_2_3_4d"))
        expect(columnify("A (b) Cdd")).to(equal("a_b_cdd"))
        expect(columnify("A_(Ab) Cdd_)")).to(equal("a_ab_cdd"))
        expect(columnify("BaseCase1st")).to(equal("base_case_1st"))
        expect(columnify("OfferingID")).to(equal("offering_id"))
        expect(columnify("GSI ID")).to(equal("g_s_i_id"))


class StableSetTests(SimpleTestCase):
    def test_stable_ordering_and_sorting_behavior(self):
        ss = StableSet([3, 1, 2, 1])
        expect(ss.to_list()).to(equal([3, 1, 2]))
        ss.add(0)
        ss.add(4)
        ss.add(0)
        expect(ss.to_list()).to(equal([3, 1, 2, 0, 4]))
        ss.remove(2)
        ss.remove(2)
        ss.remove(4)
        expect(ss.to_list()).to(equal([3, 1, 0]))
        expect(ss.to_list(sort=True)).to(equal([0, 1, 3]))
        ss.union([1, 0, 5])
        expect(ss.to_list()).to(equal([3, 1, 0, 5]))
        ss.difference([0, 1, 2])
        expect(ss.to_list()).to(equal([3, 5]))


class FilteringTests(SimpleTestCase):
    def test_remove_if_removes_values_matching_predicate(self):
        values = [1, 2, 3, 4, 5, 4]
        expect(list(remove_if(values, lambda x: x % 2))).to(equal([2, 4, 4]))
        expect(list(remove_if(values, invert(lambda x: x % 2)))).to(equal([1, 3, 5]))
        expect(values).to(have_len(6))

    def test_pull_mutates_list(self):
        values = [1, 2, 3, 4, 5, 4]
        pull_if(values, lambda x: x % 2)
        expect(values).to(equal([2, 4, 4]))
        pull_if(values, invert(lambda x: x % 2))
        expect(values).to(equal([]))


class FlatlistTests(SimpleTestCase):
    def test_works(self):
        e = flatlist([], 1, [2, 3, 4], *[[5, 6], [7, 8]], 9, *[[0]])
        expect(e).to(equal([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))


class ImmutablemapTests(SimpleTestCase):
    def test_immutability(self):
        d = {"x": 1, "y": 2, "z": 3}
        x = immutablemap(d)

        with self.assertRaises(TypeError):
            x["foo"] = 1

        with self.assertRaises(TypeError):
            x["x"] = 1

        with self.assertRaises(TypeError):
            del x["x"]

        with self.assertRaises(TypeError):
            x.clear()

        with self.assertRaises(TypeError):
            x.update({"z": 2})

        with self.assertRaises(TypeError):
            x.setdefault("v", 4)

        with self.assertRaises(TypeError):
            x.popitem()

        expect(x.items()).to(equal(d.items()))


class CountTests(SimpleTestCase):
    def test_count(self):
        x = [1, 2, 3, 4, 5]
        expect(count(x)).to(equal(5))
        expect(count(o for o in x)).to(equal(5))
        expect(count(x, lambda o: o >= 3)).to(equal(3))


class MinMaxTests(SimpleTestCase):
    def test_minmax(self):
        x = [5, 2, 3, 7, 1]
        expect(minmax([])).to(equal((None, None)))
        expect(minmax([1])).to(equal((1, 1)))
        expect(minmax(x)).to(equal((1, 7)))
        expect(minmax(x, key=lambda i: -1 * i)).to(equal((7, 1)))

        noncomp_elements = [[i] for i in x]
        expect(minmax(noncomp_elements, key=lambda i: i[0])).to(equal(([1], [7])))


class PrefixSuffixTests(SimpleTestCase):
    def test_prefix_suffix(self):
        s = "ABC def"
        expect(removeprefix(s, "ABC")).to(equal(" def"))
        expect(removeprefix(s, "abc")).to(equal("ABC def"))
        expect(removeprefix(s, "X")).to(equal("ABC def"))

        expect(iremoveprefix(s, "ABC")).to(equal(" def"))
        expect(iremoveprefix(s, "abc")).to(equal(" def"))
        expect(iremoveprefix(s, "x")).to(equal("ABC def"))

        expect(removesuffix(s, "def")).to(equal("ABC "))
        expect(removesuffix(s, "DEF")).to(equal("ABC def"))
        expect(removesuffix(s, "X")).to(equal("ABC def"))

        expect(iremovesuffix(s, "def")).to(equal("ABC "))
        expect(iremovesuffix(s, "DEF")).to(equal("ABC "))
        expect(iremovesuffix(s, "X")).to(equal("ABC def"))

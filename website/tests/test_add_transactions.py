import datetime
import subprocess

from django.conf import settings
from django.test import TestCase

from website.data_loading import load_transactions_from_data_store
from website.models import Analysis
from website.tests import import_test_transaction_store
from website.tests.factories import ActivityFactory, AnalysisFactory, CountryFactory


class TestCostLineItemsFromTransactions(TestCase):
    databases = '__all__'

    def test_transaction_loading(self):
        import_test_transaction_store('dioptra__testing-transaction-store__20200331-1656PM_test_transaction_loading.sql')
        activity = ActivityFactory(
                name="Supporting GBV Survivors at Women's Centers",
                output_metrics="{NumberOfWomenServed}"
        )
        country = CountryFactory(name="Jordan",
                                 code="JO")
        analysis: Analysis = AnalysisFactory(
                title="DF119 WPE CM Jordan Ex-Post Analysis (April 2015) – OMBU #39422 test",
                activity=activity,
                parameters='{"number_of_women_served": 2992}',
                start_date=datetime.date(2015, 5, 1),
                end_date=datetime.date(2016, 4, 30),
                country=country,
                grants="DF119"
        )

        success, messages = load_transactions_from_data_store(analysis)
        assert success, messages['errors']
        assert messages['imported_count'] == 4

        analysis.create_cost_line_items_from_transactions()

        assert analysis.cost_line_items.count() == 4

        for each_cost_line_item in analysis.cost_line_items.all():
            assert each_cost_line_item.transactions.count() == 1, f"{each_cost_line_item.budget_line_description} doesn't have any transactions associated with it"

from django.core.management import call_command
from django.test import TestCase


class DashboardTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command('build', '--soft', '-y')

    def setUp(self):
        self.client.login(
            username='dioptra_default+administrator@ombuweb.com', password='password'
        )

    def test_can_view_dashboard(self):
        response = self.client.get('/', follow=True)
        self.assertContains(response, 'Legal Aid Case Management', 3, 200)

    def test_can_view_analysis(self):
        response = self.client.get('/analysis/5/assign-sector-category/')
        self.assertContains(
            response,
            'DFID CCI IRC Cash Transfer Program Ex-Ante Analysis (September 2017)',
            1,
            200,
        )

    def test_can_view_lesson(self):
        response = self.client.get('/activity/1/')
        self.assertContains(
            response,
            'Provision of sufficient quantity of safe water to meet the drinking and domestic needs of people in need.',
            1,
            200,
        )

    def test_can_define_analysis(self):
        response = self.client.get('/analysis/define/')
        self.assertContains(response, 'Define the details of this analysis', 2, 200)

    def test_can_view_help_page(self):
        response = self.client.get('/help/')
        self.assertContains(response, 'Using Dioptra results', 1, 200)

    def test_can_view_help_article(self):
        response = self.client.get('/help/scale-of-training/')
        self.assertContains(response, 'Scale of Training', 1, 200)

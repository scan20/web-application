from django.test import TestCase


class LoginPageTest(TestCase):
    def test_can_view_login_page(self):
        response = self.client.get('/', follow=True)
        self.assertContains(response, 'Log In', 1, 200)

    def test_can_view_password_reset_page(self):
        response = self.client.get('/accounts/password/reset/')
        self.assertContains(response, 'Reset My Password', 1, 200)

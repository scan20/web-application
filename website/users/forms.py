from allauth.account.forms import LoginForm
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from ombucore.admin.buttons import CancelButton, LinkButton, SubmitButton
from ombucore.admin.forms.base import ModelFormBase

User = get_user_model()


class SelfEditUserForm(ModelFormBase):
    buttons = [
        SubmitButton(
                text='Save',
                style='primary',
                disable_when_form_unchanged=True,
                align='left',
        ),
        CancelButton(
                align='left',
        ),

        LinkButton(
                text='Change Password',
                href=reverse_lazy('account_change_password'),
                style='primary',
                align='right',
        ),

    ]

    class Meta:
        model = get_user_model()
        fields = [
            'name',
        ]


class AdminEditUserForm(ModelFormBase):
    class Meta:
        model = get_user_model()
        fields = [
            'name',
            'email',
            'role',
            'primary_country',
            'secondary_countries',
            'is_active',
        ]
        help_texts = {
            'is_active': _("Inactive users cannot log in."),
        }


class AdminSelfEditUserForm(ModelFormBase):
    buttons = [
        SubmitButton(
                text='Save',
                style='primary',
                disable_when_form_unchanged=True,
                align='left',
        ),
        CancelButton(
                align='left',
        ),

        LinkButton(
                text='Change Password',
                href=reverse_lazy('account_change_password'),
                style='success',
                align='right',
        ),

    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['role'].disabled = True

    class Meta:
        model = get_user_model()
        fields = [
            'name',
            'email',
            'role',
            'primary_country',
            'secondary_countries',
        ]


class AdminOnlyLoginForm(LoginForm):
    def clean(self):
        super().clean()
        if self.user.role != User.ADMIN:
            raise ValidationError(self.error_messages['account_inactive'])

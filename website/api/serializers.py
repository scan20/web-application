from rest_framework import serializers

from website.models import Category, CostLineItem, CostLineItemConfig, Sector


class CostLineItemNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostLineItem
        fields = ['id', 'note']


class CostLineItemSectorCategorySerializer(serializers.ModelSerializer):
    sector_id = serializers.IntegerField(source='config.sector_id')
    category_id = serializers.IntegerField(source='config.category_id')

    class Meta:
        model = CostLineItem
        fields = ('id', 'sector_id', 'category_id')

    def validate_sector_id(self, value):
        try:
            Sector.objects.get(pk=value)
        except Sector.DoesNotExist:
            raise serializers.ValidationError("No matching Sector found")
        return value

    def validate_category_id(self, value):
        try:
            Category.objects.get(pk=value)
        except Category.DoesNotExist:
            raise serializers.ValidationError("No matching Category found")
        return value

    def update(self, instance, validated_data):
        config_data = validated_data.pop('config', None)
        self.update_config(instance, config_data)
        return super().update(instance, validated_data)

    def update_config(self, cost_line_item, config_data):
        CostLineItemConfig.objects.filter(cost_line_item=cost_line_item).update(**config_data)
        cost_line_item.analysis.ensure_sector_category_objects()
        # Because changing the config of a cost item can affect the analysis
        # cost outputs, unconfirm the target sector category to force a
        # recalculation
        cost_line_item.analysis.sector_categories.filter(sector=config_data["sector_id"]).update(confirmed=False)

from django.urls import path
from . import views

urlpatterns = [
    path('cost_line_item/<int:pk>/add_note/', views.CostLineItemNotesAPI.as_view(), name='api--costlineitem--add-note'),
    path('cost_line_item/<int:pk>/update_sector_and_category/', views.CostLineItemSectorCategoryAPI.as_view(), name='api--costlineitem--edit-sector-category')
]
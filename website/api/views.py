from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from website.api.serializers import CostLineItemNoteSerializer, CostLineItemSectorCategorySerializer
from website.models import CostLineItem


class CostLineItemNotesAPI(APIView):
    permission_required = 'website.change_analysis'

    def post(self, request, pk, format=None):
        try:
            cost_line_item = CostLineItem.objects.get(pk=pk)
        except CostLineItem.DoesNotExist:
            return HttpResponse(status=404)

        # Check permissions
        if not self.request.user.has_perm(self.permission_required, cost_line_item.analysis):
            return HttpResponse(status=404)

        if request.method == 'POST':
            serializer = CostLineItemNoteSerializer(cost_line_item, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data)
            return JsonResponse(serializer.errors, status=400)

        else:
            return HttpResponse(status=404)


class CostLineItemSectorCategoryAPI(APIView):
    permission_required = 'website.change_analysis'

    def post(self, request, pk, format=None):
        try:
            cost_line_item = CostLineItem.objects.get(pk=pk)
        except CostLineItem.DoesNotExist:
            return HttpResponse(status=404)

        # Check permissions
        if not self.request.user.has_perm(self.permission_required, cost_line_item.analysis):
            return HttpResponse(status=404)

        if request.method == 'POST':
            data = request.data
            serializer = CostLineItemSectorCategorySerializer(cost_line_item, data=data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data)
            return JsonResponse(serializer.errors, status=400)
        else:
            return HttpResponse(status=404)

import contextlib
import csv
import dataclasses
import datetime
import io
import logging
from decimal import Decimal
from typing import List, Union, Tuple, Dict, Optional

import django
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import connections, connection
from django.db.models import CharField
from django.utils.translation import gettext_lazy as _
from django_excel import ExcelInMemoryUploadedFile, ExcelMixin
from psycopg2 import sql
from psycopg2.extras import DictCursor
from pyexcel import exceptions

from website import stopwatch
from website.models import Analysis
from website.models.cost_line_item import CostLineItem
from website.models.transaction import Transaction, TransactionLike

logger = logging.getLogger(__name__)


################################################################################
# Cost line items from upload.
################################################################################

def load_cost_line_items_from_file(analysis, f):
    """
    Loads cost line items from an Excel or CSV file.

    f should be a readable file object.
    """
    if not isinstance(f, ExcelMixin):
        f = ExcelInMemoryUploadedFile(f, 'file', name=None, content_type=None, size=None, charset=None)

    try:
        try:
            data = f.get_array()
        except UnicodeDecodeError:
            # This is a very likely encoding for CSV data from outdated windows machines.  If UTF-8 fails we should check for this before giving up
            try:
                data = f.get_array(encoding="cp1252")
            except:
                raise
        except:
            raise
    except exceptions.FileTypeNotSupported as e:
        return False, {'errors': [ERROR_MESSAGES['file_type_not_supported']]}
    except Exception as e:
        logger.exception(e)
        return False, {'errors': [ERROR_MESSAGES['error_reading_file']]}

    if len(data) == 0 or len(data) == 1:
        return False, {'errors': [ERROR_MESSAGES['file_empty']]}

    if len(data) > 5000:
        return False, {'errors': [ERROR_MESSAGES['file_too_large']]}

    # Column headers in first row, skip.
    data = data[1:]

    country_code = analysis.country.code

    imported_count = 0
    errors = []
    for row_num, row_data in enumerate(data):
        cost_line_data, row_errors = _parse_upload_row_to_dict(row_num, row_data)
        if len(row_errors):
            errors += row_errors
        else:
            cost_line_data.update({
                'analysis': analysis,
                'country_code': country_code,
            })

            if cost_line_data.get('total_cost') != 0:
                cost_line_item = CostLineItem(**cost_line_data)
                try:
                    cost_line_item.full_clean()
                    cost_line_item.save()
                    imported_count += 1
                except ValidationError as e:
                    if hasattr(e, 'message_dict'):
                        for field_name, messages in e.message_dict.items():
                            for message in messages:
                                errors.append(ERROR_MESSAGES['invalid_model_field'] % {
                                    'row': row_num,
                                    'column': _human_field_name(field_name),
                                    'error_message': message,
                                })
                    else:
                        errors.append(
                                ERROR_MESSAGES['invalid_row_generic'] % {'row': row_num}
                        )
                except Exception as e:
                    errors.append(
                            ERROR_MESSAGES['invalid_row_generic'] % {'row': row_num}
                    )

    if len(errors):
        # Clean up if there were any errors.
        analysis.cost_line_items.all().delete()
        return (False, {'errors': errors})

    analysis.source = f.name
    analysis.save()

    return (True, {'imported_count': imported_count})


def cast_to_decimal_two_decimal_places(value):
    return round(Decimal(value), 2)


ERROR_MESSAGES = {
    'file_type_not_supported': _("This file type is not supported. Allowed file types are csv, tsv, xls, xlsx."),
    'error_reading_file': _("There was an error reading data from the file."),
    'file_empty': _("There is no data in this file."),
    'invalid_row_column': _("Row %(row)s, column %(column)s contains invalid data \"%(value)s\"."),
    'required_row_column': _("Row %(row)s, column %(column)s: This field cannot be null."),
    'invalid_model_field': _("Row %(row)s, column %(column)s: %(error_message)s"),
    'invalid_row_generic': _("Row %(row)s could not be imported due to errors."),
    'error_importing_from_transaction_store': _("An error was encountered while importing transactions"),
    'file_too_large': _(f"The number of cost line items in the file submitted exceeds Dioptra’s data limit of {settings.COST_LINE_ITEMS_ROW_LIMIT:,} rows. Please double check the file and remove any cost line items that are $0. If this error still persists, please contact the Dioptra administrator."),
}

COST_LINE_ITEM_IMPORT_HEADERS = [
    {'name': 'grant_code', 'cast': None, 'required': True},
    {'name': 'budget_line_code', 'cast': None, 'required': True},
    {'name': 'account_code', 'cast': None, 'required': True},
    {'name': 'site_code', 'cast': None},
    {'name': 'sector_code', 'cast': None},
    {'name': 'budget_line_description', 'cast': None, 'required': True},
    {'name': 'total_cost', 'cast': cast_to_decimal_two_decimal_places},
    {'name': 'loe_or_unit', 'cast': cast_to_decimal_two_decimal_places},
    {'name': 'months_or_unit', 'cast': cast_to_decimal_two_decimal_places},
    {'name': 'unit_cost', 'cast': cast_to_decimal_two_decimal_places},
    {'name': 'dummy_field_1', 'cast': None},
    {'name': 'dummy_field_2', 'cast': None},
]


def _parse_upload_row_to_dict(row_num, row_data):
    d = {}
    row_errors = []
    for i, header in enumerate(COST_LINE_ITEM_IMPORT_HEADERS):
        try:
            value = row_data[i]
        except IndexError as e:
            continue

        if header.get('required', False) is False and (value is None or value == ''):
            field = CostLineItem._meta.get_field(header.get('name'))
            if isinstance(field, CharField) and field.null is False and field.default == '':
                value = ''
            else:
                value = None

        if header.get('required', False) and value is None:
            row_errors.append(ERROR_MESSAGES['required_row_column'] % {
                'row': row_num + 1,
                'column': _human_field_name(header['name']),
            })
        if value and header['cast']:
            try:
                value = header['cast'](value)
            except Exception as e:
                row_errors.append(ERROR_MESSAGES['invalid_row_column'] % {
                    'row': row_num + 1,
                    'column': _human_field_name(header['name']),
                    'value': value,
                })
        d[header['name']] = value
    return (d, row_errors)


def _human_field_name(field_name):
    if not hasattr(_human_field_name, '_name_map'):
        _human_field_name._name_map = {field.name: field.verbose_name for field in CostLineItem._meta.fields}
    return _human_field_name._name_map.get(field_name, field_name)


################################################################################
# Transactions from data store.
################################################################################

@stopwatch.trace()
def load_transactions_from_data_store(analysis: Analysis, filter_by_country: bool = False) -> Tuple[bool, Dict]:
    # Use bulk inserting, it's quite a bit faster: http://stefano.dissegna.me/django-pg-bulk-insert.html
    # Using django models was about 4.5s for 15k rows, this is 1.3s.
    # It should be refactored into something more reusable as we need to build more bulk inserts.

    country_code = analysis.country.code if filter_by_country else None
    total_count = 0
    xactions = []
    try:
        with _transactions_cursor(analysis.grants, analysis.start_date, analysis.end_date, country_code) as transactions_cur:
            with BulkInserter(connection, Transaction._meta.db_table) as inserter:
                while True:
                    rows = transactions_cur.fetchmany(500)
                    if len(rows) == 0:
                        break
                    total_count += len(rows)
                    for row in rows:
                        row = dict(row)
                        # Remove extra whitespace. There's a couple micro-optimization here:
                        # - Use try/except since most values are strings,
                        #   and it's much faster than a type/hasattr check.
                        # - Compare the stripped and original values,
                        #   and only update the dict if they are not equal.
                        #   This is much faster than unconditionally setting a dict,
                        #   which requires both a hash lookup and a memory mutation.
                        for field, value in row.items():
                            try:
                                new_value = value.strip()
                                if value != new_value:
                                    row[field] = value.strip()
                            except AttributeError:
                                pass
                        amount = row.pop('amount', 0)
                        row['amount_in_source_currency'] = Decimal(amount)
                        row['amount_in_instance_currency'] = Decimal(amount)
                        row['date'] = row.pop('transaction_date')
                        row['analysis_id'] = analysis.id
                        xactions.append(TransactionLike(**row))
                        inserter.add_row(row)
        analysis.source = Analysis.DATA_STORE_NAME
        analysis.save()
    except Exception as e:
        logger.exception(e)
        return False, {'errors': [ERROR_MESSAGES['error_importing_from_transaction_store']]}
    return True, {'imported_count': total_count, 'imported_transactions': xactions}



class BulkInserter(contextlib.AbstractContextManager):
    """The character used to represent a null value. If we use the default,
    we get empty strings converted to NULLs, which we don't want.
    But in some cases, we do want NULLs- we can replace their None value in a CSV or whatever string
    with this control character."""
    null = '\u0001'

    def __init__(self, conn, table, debug=False):
        self.debug = debug
        self.rows = 0
        self.conn = conn
        self.table = table
        self.stream = io.StringIO()
        self.writer = None

    def add_row(self, row: Dict):
        self.rows += 1
        if self.writer is None:
            self.writer = csv.DictWriter(self.stream, delimiter='\t', fieldnames=row.keys())
            self.writer.writeheader()
        self.writer.writerow(row)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        if self.writer is not None:
            self.stream.flush()
            self.stream.seek(0)
            if self.debug:
                print(f'Inserting {self.rows} rows')
                print(self.stream.getvalue())
                self.stream.seek(0)
            with self.conn.cursor() as cursor:
                fieldnames = [f'"{f}"' for f in self.writer.fieldnames]
                fieldnames = ", ".join(fieldnames)
                cursor.copy_expert(
                    f"COPY {self.table} ({fieldnames}) FROM STDIN "
                    # We do not want nulls, so instead of an empty string being turned to NULL,
                    # use something that won't match anything.
                    f"WITH HEADER DELIMITER '\t' NULL '{self.null}' CSV",
                    self.stream,
                )
        return super().__exit__(*args, **kwargs)


def get_transactions_data_store_count(grant_codes: Union[List[str], str], date_start: Union[str, datetime.date], date_end: Union[str, datetime.date], country_code: Optional[str] = None) -> int:
    with connections['transaction_store'].cursor() as cursor:
        if isinstance(date_start, datetime.date):
            date_start = date_start.strftime('%Y-%m-%d')
        if isinstance(date_end, datetime.date):
            date_end = date_end.strftime('%Y-%m-%d')
        if isinstance(grant_codes, str):
            grant_codes = [grant_code.strip().upper() for grant_code in grant_codes.split(',')]
        try:
            if country_code:
                cursor.execute(
                    """
               SELECT
                   count(*) 
                FROM
                   transactions 
                WHERE
                   upper(grant_code) = ANY (%s) 
                   AND upper(country_code) = %s 
                   AND transaction_date BETWEEN SYMMETRIC %s AND %s;
                    """, (grant_codes, country_code, date_start, date_end,)

                )
            else:
                cursor.execute(
                        """
                   SELECT
                       count(*) 
                    FROM
                       transactions 
                    WHERE
                       upper(grant_code) = ANY (%s) 
                       AND transaction_date BETWEEN SYMMETRIC %s AND %s;
                        """, (grant_codes, date_start, date_end,)

                )
        except django.db.utils.DataError as e:
            # logger.info(f"Invalid query while getting count: {e}")
            return 0

        return cursor.fetchone()[0]


@contextlib.contextmanager
def _transactions_cursor(grant_codes: Union[List[str], str], date_start: Union[str, datetime.date], date_end: Union[str, datetime.date], country_code: Optional[str] = None):
    if isinstance(date_start, datetime.date):
        date_start = date_start.strftime('%Y-%m-%d')
    if isinstance(date_end, datetime.date):
        date_end = date_end.strftime('%Y-%m-%d')
    if isinstance(grant_codes, str):
        grant_codes = [grant_code.strip().upper() for grant_code in grant_codes.split(',')]

    conn = connections['transaction_store']
    conn.ensure_connection()

    with conn.connection.cursor(cursor_factory=DictCursor) as cursor:
        fields = [
            "account_code",
            "amount",
            "budget_line_code",
            "budget_line_description",
            "country_code",
            "currency_code",
            "dummy_field_1",
            "dummy_field_2",
            "dummy_field_3",
            "dummy_field_4",
            "dummy_field_5",
            "grant_code",
            "sector_code",
            "site_code",
            "transaction_code",
            "transaction_date",
            "transaction_description",
        ]

        if country_code is not None:
            query = sql.SQL("""SELECT 
                                 {} 
                               FROM 
                                 transactions 
                               WHERE
                                 upper(grant_code) = ANY (%s)
                                 AND upper(country_code) = %s 
                                 AND transaction_date BETWEEN SYMMETRIC %s AND %s;""") \
                .format(sql.SQL(', ').join(map(sql.Identifier, fields)))
            cursor.execute(query, (grant_codes, country_code, date_start, date_end,))

        else:
            query = sql.SQL("""SELECT 
                                 {} 
                               FROM 
                                 transactions 
                               WHERE
                                 upper(grant_code) = ANY (%s)
                                 AND transaction_date BETWEEN SYMMETRIC %s AND %s;""") \
                .format(sql.SQL(', ').join(map(sql.Identifier, fields)))
            cursor.execute(query, (grant_codes, date_start, date_end,))

        yield cursor

import django_filters
from django import forms
from django.contrib.auth import get_user_model
from django.db.models import Q

from website.filterset import FilterSet
from .models import Activity, Analysis, Country

User = get_user_model()


class AnalysisFilterSet(FilterSet):
    class Meta:
        model = Analysis
        fields = ['country',
                  'activity',
                  'created_by']

    search = django_filters.CharFilter(
            label='Search',
            method='keyword_search',
            widget=forms.TextInput(attrs={
                'placeholder': 'Enter keyword',
                'class': 'filters__search-input',
            }),
    )

    country = django_filters.ModelChoiceFilter(
            label='Country',
            empty_label="Any",
            queryset=Country.objects.all()

    )

    activity = django_filters.ModelChoiceFilter(
            label='Activity',
            empty_label="Any",
            queryset=Activity.objects.all()

    )

    created_by = django_filters.ModelChoiceFilter(
            label='Owner',
            field_name='owner',
            empty_label="Any",
            queryset=User.objects.all()

    )
    created_by.field.label_from_instance = lambda user:  user.get_full_name()

    order_by = django_filters.OrderingFilter(
            fields=(
                ('title', 'title'),
                ('updated', 'updated'),
                ('grants', 'grants'),
                ('country', 'country'),
                ('activity', 'activity'),
                ('owner', 'owner'),
                ('output_costs', 'output_costs'),
            )
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
                Q(title__icontains=value) |
                Q(grants__icontains=value)
                # Q(country__name__icontains=value) |
                # Q(activity__name__icontains=value) |
                # Q(owner__name__icontains=value)
        )

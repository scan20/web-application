from django import forms
from ombucore.admin.templatetags.panels_extras import jsonattr


class TagEditorWidget(forms.TextInput):

    def __init__(self, options=None, attrs=None):
        attrs = attrs if attrs else {}
        options = options if options else {}
        attrs['data-tageditor'] = jsonattr(options)
        super().__init__(attrs=attrs)

    class Media:
        js = (
            'lib/jquery-tageditor/jquery.caret.min.js',
            'lib/jquery-tageditor/jquery.tag-editor.min.js',
            'lib/jquery-tageditor.init.js',
        )
        css = {
            'all': ('lib/jquery-tageditor/jquery.tag-editor.css',)
        }


class ArraySelectMultiple(forms.SelectMultiple):
    def value_omitted_from_data(self, data, files, name):
            return False

class ArrayCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    def value_omitted_from_data(self, data, files, name):
            return False

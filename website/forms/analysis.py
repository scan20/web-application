import re
from decimal import Decimal

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.forms import HiddenInput
from django.utils.translation import gettext as _, gettext_lazy as _l
from ombucore.admin.templatetags.panels_extras import jsonattr
from ombucore.admin.widgets import FlatpickrDateWidget
from website.forms.fields import CalculatedFixedDecimalField

from website.forms.widgets import TagEditorWidget
from website.models import Analysis, Sector, Category, FieldLabelOverrides, Settings
from website.models.activity import get_activity_parameter_mapping, get_all_activity_parameter_fields, get_activity_output_metric_mapping, OUTPUT_METRICS_BY_ID
from ombucore.admin.forms.base import ModelFormBase


class DefineForm(forms.ModelForm):
    class Meta:
        model = Analysis
        fields = [
            'title',
            'description',
            'analysis_type',
            'start_date',
            'end_date',
            'country',
            'grants',
            'activity',
            'output_count_source'
        ]
        widgets = {
            'start_date': FlatpickrDateWidget(options={'dateFormat': settings.DATE_FORMAT}),
            'end_date': FlatpickrDateWidget(options={'dateFormat': settings.DATE_FORMAT}),
            'grants': TagEditorWidget(options={'forceLowercase': False}),
        }

    class Media:
        js = (
            'website/steps/define-form.js',
        )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.settings = Settings.objects.first()
        self.data_loaded = kwargs.pop('data_loaded', False)

        super().__init__(*args, **kwargs)
        self.fields['analysis_type'].empty_label = settings.EMPTY_LABEL
        self.fields['activity'].empty_label = settings.EMPTY_LABEL
        self.fields['country'].empty_label = settings.EMPTY_LABEL
        self.fields['grants'].label = FieldLabelOverrides.label_for("ci_grant_code", "Grants")

        # Disable fields in data is already loaded.
        if self.data_loaded:
            self.fields['start_date'].disabled = True
            self.fields['end_date'].disabled = True
            self.fields['grants'].disabled = True
            if self.settings.transaction_country_filter:
                self.fields['country'].disabled = True


        # Inject activity->parameters mapping into activity field.
        self.mapping = get_activity_parameter_mapping()
        self.fields['activity'].widget.attrs.update({
            'data-mapping': jsonattr(self.mapping),
        })

        # Only allow countries that the User is associated with to be selectable
        self.fields['country'].queryset = self.user.associated_countries

        # Add fields for all possible parameters.
        for parameter_name, field in get_all_activity_parameter_fields().items():
            field_name = self._parameter_field_name(parameter_name)
            self.fields[field_name] = field

            if not isinstance(field, CalculatedFixedDecimalField):
                # Update parameter requirement based on selected activity.
                # Only runs when the form is being submitted so all fields appear
                # required otherwise.
                self.fields[field_name].required = True
                if self.data and self.data.get('activity'):
                    activity = int(self.data.get('activity'))
                    if parameter_name not in self.mapping[activity]:
                        self.fields[field_name].required = False

        # Unpack the `analysis.parameters` dict into initial values for the form.
        for parameter_name, value in self.instance.parameters.items():
            self.initial[self._parameter_field_name(parameter_name)] = value

    def save(self, commit=True):
        self.instance.parameters = self._collect_parameter_data()
        return super().save(commit=commit)

    def clean(self):
        if self.instance.pk is None:
            self.instance.owner = self.user
        cleaned_data = super().clean()
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        if start_date and end_date and start_date > end_date:
            self.add_error('start_date', ValidationError("Start date must be before end date."))
        return cleaned_data

    def _collect_parameter_data(self):
        parameter_data = {}
        activity = self.cleaned_data['activity']
        activity_parameters = self.mapping[activity.id]
        for parameter_name in activity_parameters:
            field_name = self._parameter_field_name(parameter_name)
            if field_name in self.cleaned_data:
                if isinstance(self.fields[field_name], CalculatedFixedDecimalField):
                    value = self.cleaned_data[field_name]
                    if isinstance(value, Decimal):
                        value = float(value)
                    if value == 0:
                        value = None
                    parameter_data[parameter_name] = value
                else:
                    parameter_data[parameter_name] = float(self.cleaned_data[field_name])
        return parameter_data

    def _parameter_field_name(self, field_name):
        return f'parameter__{field_name}'

    def parameter_fields(self):
        """
        Serves the parameter fields up dynamically for the template to render.
        """
        for field_name in self.fields:
            if 'parameter__' in field_name:
                yield self[field_name]

    def clean_grants(self):
        grants = self.cleaned_data['grants']
        if grants:
            grants = list(map(str.strip, grants.split(',')))
            for grant in grants:
                if not self._grant_is_valid(grant):
                    raise forms.ValidationError(f'Invalid grant format: "{grant}"')
            grants = ','.join(grants)
        return grants

    def _grant_is_valid(self, grant):
        return True if re.match(r'^\S+$', grant) else False


class FixMissingDataBulkForm(forms.Form):
    sector = forms.ModelChoiceField(Sector.objects.all(), required=False)
    category = forms.ModelChoiceField(Category.objects.all(), required=False)
    config_ids = forms.TypedMultipleChoiceField(coerce=int, widget=forms.MultipleHiddenInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['config_ids'].choices = ((config_id, config_id) for config_id in kwargs['initial']['config_ids'])

    def clean(self):
        cleaned_data = super().clean()

        if not cleaned_data['sector'] and not cleaned_data['category']:
            self.add_error('sector', _('Please select a sector or a category.'))
            self.add_error('category', _('Please select a sector or a category.'))

        return cleaned_data


class CategorizeSectorBulkForm(forms.Form):
    sector = forms.ModelChoiceField(Sector.objects.all(), required=False)
    category = forms.ModelChoiceField(Category.objects.all(), required=False)
    config_ids = forms.TypedMultipleChoiceField(coerce=int, widget=forms.MultipleHiddenInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['config_ids'].choices = ((config_id, config_id) for config_id in kwargs['initial']['config_ids'])

    def clean(self):
        cleaned_data = super().clean()

        if not cleaned_data['sector'] and not cleaned_data['category']:
            self.add_error('sector', _('Please select a sector or a category.'))
            self.add_error('category', _('Please select a sector or a category.'))

        return cleaned_data



class ReassignOwnerForm(ModelFormBase):
    class Meta:
        model = Analysis
        fields = [
            'owner',
        ]
        help_texts = {
            'owner': _l('Select the user who owns this analysis'),
        }

import re

from django import forms
from django.utils.translation import gettext as _
from ombucore.admin.forms.base import ModelFormBase
from ombucore.admin.templatetags.panels_extras import jsonattr

from website.forms.fields import PositiveFixedDecimalField, CalculatedFixedDecimalField
from website.forms.widgets import TagEditorWidget
from website.models.activity import Activity
from website.models.activity import OUTPUT_METRICS, get_activity_output_metric_mapping
from website.models.activity import get_activity_parameter_mapping, get_all_activity_parameter_fields
from website.models.insight_comparison_data import InsightComparisonData


class InsightComparisonDataForm(ModelFormBase):
    class Meta:
        model = InsightComparisonData
        fields = [
            'name',
            'country',
            'grants',
            'activity',
        ]
        widgets = {
            'grants': TagEditorWidget(options={'forceLowercase': False}),
        }

    class Media:
        js = (
            'website/js/admin/insight-comparison-data-form.js',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.activity_parameter_mapping = get_activity_parameter_mapping()
        self.activity_output_metric_mapping = get_activity_output_metric_mapping()

        # Inject activity->parameters and activity->output_metrics mappings
        # into activity field.
        self.fields['activity'].widget.attrs.update({
            'data-activity-parameter-mapping': jsonattr(self.activity_parameter_mapping),
            'data-activity-output-metric-mapping': jsonattr(self.activity_output_metric_mapping),
        })

        # Add fields for all possible parameters.
        for parameter_name, field in get_all_activity_parameter_fields().items():
            field_name = self._parameter_field_name(parameter_name)
            if isinstance(field, CalculatedFixedDecimalField):
                field = PositiveFixedDecimalField(label=field.label)
            self.fields[field_name] = field


            # Update parameter requirement based on selected activity.
            # Only runs when the form is being submitted so all fields appear
            # required otherwise.
            self.fields[field_name].required = True
            if self.data and self.data.get('activity'):
                activity_id = int(self.data.get('activity'))
                if parameter_name not in self.activity_parameter_mapping[activity_id]:
                    self.fields[field_name].required = False

        # Add fields for all possible output metric costs.
        for output_metric in OUTPUT_METRICS:
            base_label = output_metric.metric_name
            output_metric_id = output_metric.id
            field_name_all = self._output_metric_cost_all_field_name(output_metric_id)
            field_name_direct_only = self._output_metric_cost_direct_only_field_name(output_metric_id)
            self.fields[field_name_direct_only] = PositiveFixedDecimalField(
                    label='{label} {type}'.format(label=base_label, type=_('(Direct Project Costs only)')),
            )
            self.fields[field_name_all] = PositiveFixedDecimalField(
                    label='{label} {type}'.format(label=base_label, type=_('(Including Direct Project Costs, Direct Shared Costs, and Indirect Costs)')),
            )

            # Update output metric cost requirement based on selected activity.
            # Only runs when the form is being submitted so all fields appear
            # required otherwise.
            if self.data and self.data.get('activity'):
                activity_id = int(self.data.get('activity'))
                activity = Activity.objects.get(pk=activity_id)
                if output_metric_id not in self.activity_output_metric_mapping[activity_id]:
                    self.fields[field_name_all].required = False
                    self.fields[field_name_direct_only].required = False

        # Unpack the `parameters` dict into initial values for the form.
        for parameter_name, value in self.instance.parameters.items():
            self.initial[self._parameter_field_name(parameter_name)] = value

        # Unpack the `costs` dict into initial values for the form.
        for output_metric_id, values in self.instance.output_costs.items():
            self.initial[self._output_metric_cost_all_field_name(output_metric_id)] = values.get('all', None)
            self.initial[self._output_metric_cost_direct_only_field_name(output_metric_id)] = values.get('direct_only', None)

    def save(self, commit=True):
        self.instance.parameters = self._collect_parameters_data()
        self.instance.output_costs = self._collect_costs_data()
        return super().save(commit=commit)

    def _collect_parameters_data(self):
        data = {}
        activity = self.cleaned_data['activity']

        activity_parameters = self.activity_parameter_mapping[activity.id]
        for parameter_name in activity_parameters:
            field_name = self._parameter_field_name(parameter_name)
            if field_name in self.cleaned_data:
                data[parameter_name] = float(self.cleaned_data[field_name])
        return data

    def _collect_costs_data(self):
        data = {}
        activity = self.cleaned_data['activity']

        activity_output_metrics = self.activity_output_metric_mapping[activity.id]
        for output_metric_id in activity_output_metrics:
            data[output_metric_id] = {}
            field_name_all = self._output_metric_cost_all_field_name(output_metric_id)
            if field_name_all in self.cleaned_data:
                data[output_metric_id]['all'] = float(self.cleaned_data[field_name_all])
            field_name_direct_only = self._output_metric_cost_direct_only_field_name(output_metric_id)
            if field_name_direct_only in self.cleaned_data:
                data[output_metric_id]['direct_only'] = float(self.cleaned_data[field_name_direct_only])

        return data

    def _parameter_field_name(self, parameter_name):
        return f'parameter__{parameter_name}'

    def _output_metric_cost_all_field_name(self, output_metric_id):
        return f'output_cost__all__{output_metric_id}'

    def _output_metric_cost_direct_only_field_name(self, output_metric_id):
        return f'output_cost__direct_only__{output_metric_id}'

    def parameter_fields(self):
        """
        Serves the parameter fields up dynamically for the template to render.
        """
        for field_name in self.fields:
            if 'parameter__' in field_name:
                yield self[field_name]

    def clean_grants(self):
        grants = self.cleaned_data['grants']
        if grants:
            grants = list(map(str.strip, grants.split(',')))
            for grant in grants:
                if not self._grant_is_valid(grant):
                    raise forms.ValidationError(f'Invalid grant format: "{grant}"')
            grants = ','.join(grants)
        return grants

    def _grant_is_valid(self, grant):
        return True if re.match(r'^\S+$', grant) else False

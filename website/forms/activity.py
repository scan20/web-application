from django import forms
from django.utils.translation import gettext_lazy as _
from ombucore.admin.forms.base import ModelFormBase
from website.models.activity import OUTPUT_METRIC_CHOICES


class ActivityForm(ModelFormBase):
    output_metric_1 = forms.ChoiceField(
        label=_('Output Metric 1'),
        choices=OUTPUT_METRIC_CHOICES,
    )
    output_metric_2 = forms.ChoiceField(
        label=_('Output Metric 2'),
        choices=[('', _('(Choose)'))] + OUTPUT_METRIC_CHOICES,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs and kwargs['instance']:
            initial = kwargs.get('initial', {})
            instance = kwargs['instance']
            if len(instance.output_metrics):
                if len(instance.output_metrics) >= 1:
                    initial['output_metric_1'] = instance.output_metrics[0]
                if len(instance.output_metrics) >= 2:
                    initial['output_metric_2'] = instance.output_metrics[1]
            kwargs.update({'initial': initial})
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        output_metrics = []
        output_metric_1 = cleaned_data.pop('output_metric_1')
        if output_metric_1:
            output_metrics.append(output_metric_1)
        output_metric_2 = cleaned_data.pop('output_metric_2')
        if output_metric_2:
            output_metrics.append(output_metric_2)
        self.instance.output_metrics = output_metrics
        return cleaned_data

    class Meta:
        fields = [
            'name',
            'description',
            'icon',
            'group',
            'output_metric_1',
            'output_metric_2',
            'show_in_menu',
        ]
        fieldsets = (
            (_('Analysis'), {
                'fields': (
                    'name',
                    'description',
                    'icon',
                    'output_metric_1',
                    'output_metric_2',
                ),
            }),
            (_('Program Design Lessons'), {
                'fields': (
                    'group',
                    'show_in_menu',
                ),
            }),
        )
        help_texts = {
            'group': _("Select the group for this activity in the Program Design Lessons menu."),
        }


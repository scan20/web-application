from django.utils.html import mark_safe
from django.utils.translation import gettext as _
from ombucore.htmloutput_field import HtmlOutputField
from ombucore.admin.forms.base import ModelFormBase


class SectorCategoryMappingForm(ModelFormBase):
    criteria_text = HtmlOutputField(
        label='',
        render_fn=lambda *ars, **kwargs: mark_safe('<h2>{}</h2>'.format(_('Criteria'))),
        required=False,
    )
    result_text = HtmlOutputField(
        label='',
        render_fn=lambda *ars, **kwargs: mark_safe('<h2>{}</h2>'.format(_('Result'))),
        required=False,
    )

    def clean(self):
        cleaned_data = self.cleaned_data
        if not cleaned_data['sector'] and not cleaned_data['category']:
            self.add_error('category', _('Please select a sector, category, or both'))
        return cleaned_data

    class Meta:
        fields = [
            'criteria_text',
            'country_code',
            'grant_code',
            'budget_line_code',
            'account_code',
            'site_code',
            'sector_code',
            'budget_line_description',
            'result_text',
            'sector',
            'category',
        ]


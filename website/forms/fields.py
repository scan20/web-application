from decimal import Decimal

from django import forms
from django.core.exceptions import ValidationError


class FixedDecimalField(forms.DecimalField):

    def __init__(self, *args, **kwargs):
        self.quantize = kwargs.pop('quantize', '0.01')
        if not 'decimal_places' in kwargs:
            kwargs['decimal_places'] = 2
        super().__init__(*args, **kwargs)

    def prepare_value(self, value):
        if value:
            if not isinstance(value, Decimal):
                value = Decimal(value)
            if self.quantize:
                value = value.quantize(Decimal(self.quantize))
        return value

    def to_python(self, value):
        super().to_python(value)
        if value:
            if not isinstance(value, Decimal):
                value = Decimal(value)
            if self.quantize:
                value = value.quantize(Decimal(self.quantize))
        return value


class PositiveFixedDecimalField(FixedDecimalField):
    def validate(self, value):
        super().validate(value)
        if isinstance(value, Decimal) and value <= 0:
            raise ValidationError("This field is required and must be greater than zero.", code='invalid')


class CalculatedFixedDecimalField(FixedDecimalField):
    def __init__(self, *args, **kwargs):
        kwargs['required'] = False
        kwargs['disabled'] = True
        super().__init__(*args, **kwargs)

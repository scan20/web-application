import django_filters
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from ombucore.admin.filterset import FilterSet
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.sites import site
from ombucore.admin.views import AddView, ChangeView, DeleteView

from website import models as website_models
from website.forms.core import SectorCategoryMappingForm


class ProtectedDeleteView(DeleteView):
    """
    Displays a custom protected error message when used with category or sector admins.
    """

    def get_protected_error_message(self, _unused):
        """
        Override method from DeleteView.
        """
        field = self.object.__class__.__name__.lower()  # category or sector
        titles_in_use = self.titles_in_use(field)
        protected_error_message = self.custom_protected_error_message(titles_in_use, field)
        return protected_error_message

    def titles_in_use(self, field):
        query = Q(**{field: self.object})
        related_analyses = website_models.AnalysisSectorCategory.objects.filter(query).all()
        unique_analyses = set(
            [r.analysis for r in related_analyses]
        )  # remove duplicate analyses
        titles_in_use = [u.title for u in list(unique_analyses)]
        return titles_in_use

    def custom_protected_error_message(self, titles_in_use, field):
        num_titles = len(titles_in_use)
        num_titles_to_display = 10
        message = f"Unable to delete {field} {self.object.name}. It's being used by the following analyses:</br>"

        for title in titles_in_use[:num_titles_to_display]:
            message += f"- {title}</br>"

        if num_titles > num_titles_to_display:
            message += f"... and {num_titles - num_titles_to_display} more analyses"
        return message


class CategoryFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='name',
            lookup_expr='icontains',
    )

    class Meta:
        fields = ['search', ]


class CategoryAdmin(ModelAdmin):
    filterset_class = CategoryFilterSet
    form_config = {
        'fields': ['name', 'description'],
    }

    list_display = (
        ('name', _('Name')),
    )

    delete_view = ProtectedDeleteView


site.register(website_models.Category, CategoryAdmin)


class SectorFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='name',
            lookup_expr='icontains',
    )
    direct_program_cost = django_filters.ChoiceFilter(
            label=_('Direct Program Cost?'),
            field_name='direct_program_cost',
            choices=(
                (True, _('Yes')),
                (False, _('No')),
            ),
    )

    class Meta:
        fields = ['search', 'direct_program_cost']


class SectorAdmin(ModelAdmin):
    filterset_class = SectorFilterSet
    form_config = {
        'fields': ['name', 'type'],
    }

    delete_view = ProtectedDeleteView

    list_display = (
        ('name', _('Name')),
        ('display_type', _('Type')),
    )

    def display_type(self, obj):
        return dict(obj.TYPE_CHOICES).get(obj.type)


site.register(website_models.Sector, SectorAdmin)


class RegionFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='search',
            method='filter_search'
    )
    order_by = django_filters.OrderingFilter(
            choices=(
                ('name', _('Name (A-Z)')),
                ('-name', _('Name (Z-A)')),
                ('region_code', _('Code (A-Z)')),
                ('-region_code', _('Code (Z-A)')),
            ),
            empty_label=None,
    )

    class Meta:
        fields = ['search', 'order_by']

    def filter_search(self, queryset, name, value):
        return queryset.filter(
                Q(name__icontains=value) | Q(region_code__icontains=value)
        )


class RegionAdmin(ModelAdmin):
    filterset_class = RegionFilterSet
    form_config = {
        'fields': ['name', 'region_code'],
    }

    list_display = (
        ('name', _('Name')),
        ('region_code', _('Code')),
    )


site.register(website_models.Region, RegionAdmin)


class CountryFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='search',
            method='filter_search'
    )
    order_by = django_filters.OrderingFilter(
            choices=(
                ('name', _('Name (A-Z)')),
                ('-name', _('Name (Z-A)')),
                ('code', _('Code (A-Z)')),
                ('-code', _('Code (Z-A)')),
                ('region__name', _('Region (A-Z)')),
                ('-region__name', _('Region (Z-A)')),
            ),
            empty_label=None,
    )

    class Meta:
        fields = ['search', 'region', 'order_by']

    def filter_search(self, queryset, name, value):
        return queryset.filter(
                Q(name__icontains=value) | Q(code__icontains=value)
        )


class CountryAdmin(ModelAdmin):
    filterset_class = CountryFilterSet
    form_config = {
        'fields': ['name', 'code', 'region'],
    }

    list_display = (
        ('name', _('Name')),
        ('code', _('Code')),
        ('region', _('Region')),
    )


site.register(website_models.Country, CountryAdmin)


class SectorCategoryMappingDeleteView(DeleteView):

    def get_success_message(self, data):
        msg = f"{getattr(self.object.sector, 'name', '<None>')} - {getattr(self.object.category, 'name', '<None>')} mapping was successfully deleted."
        return msg


class SectorCategoryMappingAddView(AddView):
    def get_success_message(self, data):
        msg = f"{getattr(self.object.sector, 'name', '<None>')} - {getattr(self.object.category, 'name', '<None>')} mapping was successfully created."
        return msg


class SectorCategoryMappingChangeView(ChangeView):
    def get_success_message(self, data):
        msg = f"{getattr(self.object.sector, 'name', '<None>')} - {getattr(self.object.category, 'name', '<None>')} mapping was successfully updated."
        return msg



class SectorCategoryMappingFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='search',
            method='filter_search'
    )
    sector = django_filters.ModelChoiceFilter(
            label='Result Sector',
            queryset=website_models.Sector.objects.all(),
    )
    category = django_filters.ModelChoiceFilter(
            label='Result Category',
            queryset=website_models.Category.objects.all(),
    )

    class Meta:
        fields = ['search', 'sector', 'category']

    def filter_search(self, queryset, name, value):
        return queryset.filter(
                Q(country_code__icontains=value) |
                Q(grant_code__icontains=value) |
                Q(budget_line_code__icontains=value) |
                Q(account_code__icontains=value) |
                Q(site_code__icontains=value) |
                Q(sector_code__icontains=value) |
                Q(budget_line_description__icontains=value) |
                Q(sector__name__icontains=value) |
                Q(category__name__icontains=value)
        )


class SectorCategoryMappingAdmin(ModelAdmin):
    filterset_class = SectorCategoryMappingFilterSet
    form_class = SectorCategoryMappingForm

    add_view = SectorCategoryMappingAddView
    delete_view = SectorCategoryMappingDeleteView
    change_view = SectorCategoryMappingChangeView

    list_display = (
        # ('display_criteria', _('Criteria')),
        ('country_code', _('Country')),
        ('grant_code', _('Grant')),
        ('budget_line_code', _('Budget line code')),
        ('account_code', _('Account')),
        ('site_code', _('Site')),
        ('sector_code', _('Sector')),
        ('budget_line_description', _('Budget line description')),
        # ('sector', _('Result Sector')),
        # ('category', _('Result Category')),
        ('display_result', _('Result')),
    )

    def display_criteria(self, obj):
        fields = [
            'country_code',
            'grant_code',
            'budget_line_code',
            'account_code',
            'site_code',
            'sector_code',
            'budget_line_description',
        ]
        field_names = {field.name: field.verbose_name for field in obj._meta.fields}
        criteria = []
        for field in fields:
            if getattr(obj, field, None):
                field_name = field_names[field]
                field_value = getattr(obj, field)
                criteria.append(f'{field_name}: {field_value}')
        return ', '.join(criteria)

    def display_result(self, obj):
        items = []
        if obj.sector:
            items.append(obj.sector.name)
        if obj.category:
            items.append(obj.category.name)
        return ' - '.join(items)


site.register(website_models.SectorCategoryMapping, SectorCategoryMappingAdmin)

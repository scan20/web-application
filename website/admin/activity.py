import django_filters
from django.utils.translation import gettext_lazy as _
from ombucore.admin.filterset import FilterSet
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.sites import site

from website.models import Activity, ActivityGroup
from website.forms.activity import ActivityForm


class ActivityFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='name',
            lookup_expr='icontains',
    )

    class Meta:
        fields = ['search', ]


class ActivityAdmin(ModelAdmin):
    filterset_class = ActivityFilterSet
    form_class = ActivityForm
    list_display = (
        ('name', _('Name')),
    )


site.register(Activity, ActivityAdmin)



class ActivityGroupFilterSet(FilterSet):
    search = django_filters.CharFilter(
            field_name='name',
            lookup_expr='icontains',
    )

    class Meta:
        fields = ['search', ]


class ActivityGroupAdmin(ModelAdmin):
    filterset_class = ActivityGroupFilterSet
    list_display = (
        ('name', _('Name')),
    )


site.register(ActivityGroup, ActivityGroupAdmin)

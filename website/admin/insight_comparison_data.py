import django_filters
from django import forms
from django.db.models import Q
from django.urls import reverse
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _
from ombucore.admin.actionlink import ActionLink
from ombucore.admin.filterset import FilterSet
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.sites import site

from website import models as website_models
from website.forms.insight_comparison_data import InsightComparisonDataForm


class InsightComparisonDataFilterSet(FilterSet):
    search = django_filters.CharFilter(
            method='keyword_search',
    )
    country = django_filters.ModelChoiceFilter(
            label=_('Country'),
            field_name='country',
            queryset=website_models.Country.objects.all(),
            widget=forms.Select,
    )
    activity = django_filters.ModelChoiceFilter(
            label=_('Activity'),
            field_name='activity',
            queryset=website_models.Activity.objects.all(),
            widget=forms.Select,
    )
    order_by = django_filters.OrderingFilter(
            choices=(
                ('name', _('Name (A-Z)')),
                ('-name', _('Name (Z-A)')),
            ),
            empty_label=None,
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(grants__icontains=value)
        )

    class Meta:
        fields = ['search', 'country', 'activity']


class InsightComparisonDataAdmin(ModelAdmin):
    filterset_class = InsightComparisonDataFilterSet
    form_class = InsightComparisonDataForm

    list_display = (
        ('name', _('Name')),
        ('country', _('Country')),
        ('display_grants', _('Grants')),
        ('activity', _('Activity')),
    )

    def display_grants(self, instance):
        return ', '.join(instance.grants_list())

site.register(website_models.InsightComparisonData, InsightComparisonDataAdmin)

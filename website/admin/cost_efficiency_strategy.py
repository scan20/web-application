import django_filters
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from ombucore.admin.filterset import FilterSet
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.sites import site

from website.models import CostEfficiencyStrategy, Activity


class CostEfficiencyStrategyFilterSet(FilterSet):
    search = django_filters.CharFilter(
        method='keyword_search'
    )

    activity = django_filters.ModelChoiceFilter(
        label=_('Activity'),
        field_name='activities',
        queryset=Activity.objects.all(),
    )

    order_by = django_filters.OrderingFilter(
            choices=(
                ('title', _('Title (A-Z)')),
                ('-title', _('Title (Z-A)')),
            ),
            empty_label=None,
    )

    def keyword_search(self, queryset, name, value):
        return queryset.filter(
            Q(title__icontains=value) |
            Q(efficiency_driver_description__icontains=value) |
            Q(strategy_to_improve_description__icontains=value)
        )

    class Meta:
        fields = ['search', ]


class CostEfficiencyStrategyAdmin(ModelAdmin):
    filterset_class = CostEfficiencyStrategyFilterSet
    list_display = (
        ('title', _('Title')),
        ('display_activities', _('Activity being analyzed')),
    )

    def display_activities(self, strategy):
        return ', '.join([
            activity.name
            for activity in strategy.activities.all()
        ])


site.register(CostEfficiencyStrategy, CostEfficiencyStrategyAdmin)

from typing import Dict, Optional

from django.db import models
from django.utils.translation import gettext_lazy as _
from ombucore.admin.fields import ForeignKey

from website.forms.fields import CalculatedFixedDecimalField, PositiveFixedDecimalField
from website.models.fields import ChoiceArrayField


class OutputMetric:
    output_name: Optional[str] = None
    output_unit: Optional[str] = None
    metric_name: Optional[str] = None
    metric_equation: Optional[str] = None
    parameters: Dict[str, PositiveFixedDecimalField] = {}

    @property
    def id(self) -> str:
        return self.__class__.__name__

    def __str__(self) -> str:
        return str(self.output_name)

    def calculate(self, *args, **kwargs):
        pass

    def total_output(self, **kwargs):
        """
        Override this method for any OutputMetric with more than one parameter!

        When we are dealing with a single parameter it is fine to return it unchanged
          but if we have more than one in the OutputMetric.parameters some math should
          be done to make sure we are returning the actual total output.
        """
        for k, v in kwargs.items():
            if k in self.parameters.keys():
                return v
        return ValueError(f"Unknown parameters found for this OutputMetric: {list(kwargs.keys())}")


class NumberOfPersonYearsOfWaterAccess(OutputMetric):
    output_name = _('Number of Person-Years of Water Access')
    output_unit = _('Person-Years of Water Access')
    metric_name = _('Cost per Person per Year of Water Access')
    metric_equation = 'cost_output_sum / (number of people served * number of years water was provided)'
    parameters = {
        'number_of_people_served': PositiveFixedDecimalField(label=_('Number of people served')),
        'number_of_years_water_was_provided': PositiveFixedDecimalField(label=_('Number of years water was provided')),
    }

    def calculate(self, cost_output_sum, number_of_people_served, number_of_years_water_was_provided, **kwargs):
        return cost_output_sum / (number_of_people_served * number_of_years_water_was_provided)

    def total_output(self, number_of_people_served=0, number_of_years_water_was_provided=0):
        return number_of_people_served * number_of_years_water_was_provided


class NumberOfPersonYearsOfLatrineAccess(OutputMetric):
    output_name = _('Number of Person-Years of Latrine Access')
    output_unit = _('Person-Years of Latrine Access')
    metric_name = _('Cost per Person per Year of Latrine Access')
    metric_equation = 'cost_output_sum / (number of people * number of years a latrine can last)'
    parameters = {
        'number_of_people': PositiveFixedDecimalField(label=_('Number of people served')),
        'number_of_years_a_latrine_can_last': PositiveFixedDecimalField(label=_('Number of years a latrine can last')),
    }

    def calculate(self, cost_output_sum, number_of_people, number_of_years_a_latrine_can_last, **kwargs):
        return cost_output_sum / (number_of_people * number_of_years_a_latrine_can_last)

    def total_output(self, number_of_people=0, number_of_years_a_latrine_can_last=0):
        return number_of_people * number_of_years_a_latrine_can_last


class NumberOfHouseholdMonths(OutputMetric):
    output_name = _('Number of Household-Months')
    output_unit = _('Household-Months')
    metric_name = _('Cost per Household Served per Month')
    metric_equation = 'cost_output_sum / (number of households served * number of months served)'
    parameters = {
        'number_of_households_served': PositiveFixedDecimalField(label=_('Number of households served')),
        'number_of_months_served': PositiveFixedDecimalField(label=_('Number of months served')),
    }

    def calculate(self, cost_output_sum, number_of_households_served, number_of_months_served, **kwargs):
        return cost_output_sum / (number_of_households_served * number_of_months_served)

    def total_output(self, number_of_households_served=0, number_of_months_served=0):
        return number_of_households_served * number_of_months_served


class NumberOfPeopleReceivingTraining(OutputMetric):
    output_name = _('Number of People Receiving Training')
    output_unit = _('People Receiving Training')
    metric_name = _('Cost per Person Receiving Training')
    metric_equation = 'cost_output_sum / number of people receiving training'
    parameters = {
        'number_of_people_receiving_training': PositiveFixedDecimalField(label=_('Number of people receiving training')),
    }

    def calculate(self, cost_output_sum, number_of_people_receiving_training, **kwargs):
        return cost_output_sum / number_of_people_receiving_training


class NumberOfChildrenServed(OutputMetric):
    output_name = _('Number of Children Served')
    output_unit = _('Children Served')
    metric_name = _('Cost per Child Served')
    metric_equation = 'cost_output_sum / number of children served'
    parameters = {
        'number_of_children_served': PositiveFixedDecimalField(label=_('Number of children served')),
    }

    def calculate(self, cost_output_sum, number_of_children_served, **kwargs):
        return cost_output_sum / number_of_children_served


class NumberOfCommunitiesWithAccountabilityMechanismsCreated(OutputMetric):
    output_name = _('Number of Communities with Accountability Mechanisms Created')
    output_unit = _('Communities with Accountability Mechanisms Created')
    metric_name = _('Cost per Community with Accountability Mechanisms Created')
    metric_equation = 'cost_output_sum / number of communities with accountability mechanisms created'
    parameters = {
        'number_of_communities_with_accountability_mechanisms_created': PositiveFixedDecimalField(label=_('Number of Communities with Accountability Mechanisms Created')),
    }

    def calculate(self, cost_output_sum, number_of_communities_with_accountability_mechanisms_created, **kwargs):
        return cost_output_sum / number_of_communities_with_accountability_mechanisms_created


class NumberOfCoupleYearsOfProtectionProvided(OutputMetric):
    output_name = _('Number of Couple-Years of Protection (CYPs) Provided')
    output_unit = _('Couple-Years of Protection (CYPs) Provided')
    metric_name = _('Cost per Couple per Year of Protection')
    metric_equation = 'cost_output_sum / CYPs provided from FP methods distributed'
    parameters = {
        'cyps_provided_from_fp_methods_distributed': PositiveFixedDecimalField(label=_('Number of CYPs provided from FP methods distributed')),
    }

    def calculate(self, cost_output_sum, cyps_provided_from_fp_methods_distributed, **kwargs):
        return cost_output_sum / cyps_provided_from_fp_methods_distributed


class NumberOfWomenDeliveringWithASkilledAttendant(OutputMetric):
    output_name = _('Number of Women Delivering with a Skilled Attendant')
    output_unit = _('Women Delivering with a Skilled Attendant')
    metric_name = _('Cost per Institutional Birth')
    metric_equation = 'cost_output_sum / number of women delivering with a skilled attendant'
    parameters = {
        'number_of_women_delivering_with_a_skilled_attendant': PositiveFixedDecimalField(label=_('Number of women delivering with a skilled attendant')),
    }

    def calculate(self, cost_output_sum, number_of_women_delivering_with_a_skilled_attendant, **kwargs):
        return cost_output_sum / number_of_women_delivering_with_a_skilled_attendant


class EquivalentDollarValueOfItemsDistributed(OutputMetric):
    output_name = _(f'Equivalent Cash Value of Items Distributed')
    output_unit = _(f'Cash Value of Items Distributed')
    metric_name = _(f'Cost per Cash Value Distributed')
    metric_equation = '(cost_output_sum - value_items_distributed) / value_items_distributed'
    parameters = {
        'value_items_distributed': PositiveFixedDecimalField(label=_(f'Value of items distributed')),
    }

    def calculate(self, cost_output_sum, value_items_distributed, **kwargs):
        return (cost_output_sum - value_items_distributed) / value_items_distributed


class NumberOfUnits(OutputMetric):
    output_name = _('Number Of Units')
    output_unit = _('Units')
    metric_name = _('Cost per Unit')
    metric_equation = 'cost_output_sum / number of units'
    parameters = {
        'number_of_units': PositiveFixedDecimalField(label=_('Number of Units')),
    }

    def calculate(self, cost_output_sum, number_of_units, **kwargs):
        return cost_output_sum / number_of_units


class NumberOfCasesManaged(OutputMetric):
    output_name = _('Number of Cases Managed')
    output_unit = _('Cases Managed')
    metric_name = _('Cost per Case')
    metric_equation = 'cost_output_sum / number of cases managed'
    parameters = {
        'number_of_cases_managed': PositiveFixedDecimalField(label=_('Number of Cases Managed')),
    }

    def calculate(self, cost_output_sum, number_of_cases_managed, **kwargs):
        return cost_output_sum / number_of_cases_managed


class NumberOfMentalHealthConsultationsProvided(OutputMetric):
    output_name = _('Number of Mental Health Consultations Provided')
    output_unit = _('Mental Health Consultations Provided')
    metric_name = _('Cost per Mental Health Consultation')
    metric_equation = 'cost_output_sum / number of mental health consultations provided'
    parameters = {
        'number_of_mental_health_consultations_provided': PositiveFixedDecimalField(label=_('Number of mental health consultations provided')),
    }

    def calculate(self, cost_output_sum, number_of_mental_health_consultations_provided, **kwargs):
        return cost_output_sum / number_of_mental_health_consultations_provided


class NumberOfHouseholdsReachedWithCoaching(OutputMetric):
    output_name = _('Number of Households Reached with Coaching')
    output_unit = _('Households Reached with Coaching')
    metric_name = _('Cost per Household')
    metric_equation = 'cost_output_sum / number of households reached with coaching'
    parameters = {
        'number_of_households_reached_with_coaching': PositiveFixedDecimalField(label=_('Number of households reached with coaching')),
    }

    def calculate(self, cost_output_sum, number_of_households_reached_with_coaching, **kwargs):
        return cost_output_sum / number_of_households_reached_with_coaching


class NumberOfPeopleReceivingABusinessGrant(OutputMetric):
    output_name = _('Number of People Receiving a Business Grant')
    output_unit = _('People Receiving a Business Grant')
    metric_name = _('Cost per Person Receiving a Business Grant')
    metric_equation = 'cost_output_sum /  number of people receiving a business grant'
    parameters = {
        'number_of_people_receiving_a_business_grant': PositiveFixedDecimalField(label=_('Number of people receiving a business grant')),
    }

    def calculate(self, cost_output_sum, number_of_people_receiving_a_business_grant, **kwargs):
        return cost_output_sum / number_of_people_receiving_a_business_grant


class NumberOfPrimaryHealthConsultationsProvided(OutputMetric):
    output_name = _('Number of Primary Health Consultations Provided')
    output_unit = _('Primary Health Consultations Provided')
    metric_name = _('Cost per Consultation')
    metric_equation = 'cost_output_sum / number of primary health consultations provided'
    parameters = {
        'number_of_primary_health_consultations_provided': PositiveFixedDecimalField(label=_('Number of primary health consultations provided')),
    }

    def calculate(self, cost_output_sum, number_of_primary_health_consultations_provided, **kwargs):
        return cost_output_sum / number_of_primary_health_consultations_provided


class NumberOfVaccineDosesDistributed(OutputMetric):
    output_name = _('Number of Vaccine Doses Distributed')
    output_unit = _('Vaccine Doses Distributed')
    metric_name = _('Cost per Vaccine Dose Distributed')
    metric_equation = 'cost_output_sum / number of vaccine doses distributed'
    parameters = {
        'number_of_vaccine_doses_distributed': PositiveFixedDecimalField(label=_('Number of vaccine doses distributed')),
    }

    def calculate(self, cost_output_sum, number_of_vaccine_doses_distributed, **kwargs):
        return cost_output_sum / number_of_vaccine_doses_distributed


class NumberOfPeopleReceivingVaccines(OutputMetric):
    output_name = _('Number of People Receiving Vaccines')
    output_unit = _('People Receiving Vaccines')
    metric_name = _('Cost per Person Receiving Vaccines')
    metric_equation = 'cost_output_sum / number of people receiving vaccines'
    parameters = {
        'number_of_people_receiving_vaccines': PositiveFixedDecimalField(label=_('Number of people receiving vaccines')),
    }

    def calculate(self, cost_output_sum, number_of_people_receiving_vaccines, **kwargs):
        return cost_output_sum / number_of_people_receiving_vaccines


class NumberOfWomenServed(OutputMetric):
    output_name = _('Number of Women Served')
    output_unit = _('Women Served')
    metric_name = _('Cost per Woman Served')
    metric_equation = 'cost_output_sum / number of women served'
    parameters = {
        'number_of_women_served': PositiveFixedDecimalField(label=_('Number of women served')),
    }

    def calculate(self, cost_output_sum, number_of_women_served, **kwargs):
        return cost_output_sum / number_of_women_served


class NumberOfTeacherDaysOfTraining(OutputMetric):
    output_name = _('Number of Teacher-Days of Training')
    output_unit = _('Teacher-Days of Training')
    metric_name = _('Cost per Teacher per Day of Training')
    metric_equation = 'cost_output_sum / (number of teachers * number of days of training)'
    parameters = {
        'number_of_teachers': PositiveFixedDecimalField(label=_('Number of teachers')),
        'number_of_days_of_training': PositiveFixedDecimalField(label=_('Number of days of training')),
    }

    def calculate(self, cost_output_sum, number_of_teachers, number_of_days_of_training, **kwargs):
        return cost_output_sum / (number_of_teachers * number_of_days_of_training)

    def total_output(self, number_of_teachers=0, number_of_days_of_training=0):
        return number_of_teachers * number_of_days_of_training


class NumberOfTeacherYearsOfSupport(OutputMetric):
    output_name = _('Number of Teacher-Years of Support')
    output_unit = _('Teacher-Years of Support')
    metric_name = _('Cost per Teacher per Year  of Support')
    metric_equation = 'cost_output_sum / (number of teachers * number of years of support)'
    parameters = {
        'number_of_teachers': PositiveFixedDecimalField(label=_('Number of teachers')),
        'number_of_years_of_support': PositiveFixedDecimalField(label=_('Number of years of support'))

    }

    def calculate(self, cost_output_sum, number_of_teachers, number_of_years_of_support, **kwargs):
        return cost_output_sum / (number_of_teachers / number_of_years_of_support)

    def total_output(self, number_of_teachers=0, number_of_years_of_support=0):
        return number_of_teachers * number_of_years_of_support


class NumberOfChildrenTreated(OutputMetric):
    output_name = _('Number of Children Treated (Excluding Defaulters)')
    output_unit = _('Children Treated (Excluding Defaulters)')
    metric_name = _('Cost per Child Treated')
    metric_equation = 'cost_output_sum / number of children treated'
    parameters = {
        'number_of_children_treated': PositiveFixedDecimalField(label=_('Number of Children Treated (Excluding Defaulters)')),
    }

    def calculate(self, cost_output_sum, number_of_children_treated, **kwargs):
        return cost_output_sum / number_of_children_treated


class ValueOfCashTransferred(OutputMetric):
    output_name = _(f'Value of Cash Transferred')
    output_unit = _(f'Cash Transferred')
    metric_name = _(f'Cost per Cash Transferred')
    metric_equation = '(cost_output_sum - value of cash transferred) / value of cash transferred'
    parameters = {
        'value_of_cash_transferred': CalculatedFixedDecimalField(label=_(f'Value of Cash Transferred'),
                                                                 help_text="You will flag this value on the Identify Output Value step."),
    }

    def calculate(self, cost_output_sum, value_of_cash_transferred, **kwargs):
        return (cost_output_sum - value_of_cash_transferred) / value_of_cash_transferred


class ValueOfBusinessGrantAmount(OutputMetric):
    output_name = _(f'Value of Business Grant Amount')
    output_unit = _(f'Amount Provided')
    metric_name = _(f'Cost per Grant Cash Provided')
    metric_equation = '(cost_output_sum - value of business grant) / value of business grant'
    parameters = {
        'value_of_business_grant_amount': CalculatedFixedDecimalField(label=_('Value of Business Grant Amount'),
                                                                      help_text="You will flag this value on the Identify Output Value step."),
    }

    def calculate(self, cost_output_sum, value_of_business_grant_amount, **kwargs):
        return (cost_output_sum - value_of_business_grant_amount) / value_of_business_grant_amount


OUTPUT_METRICS = [
    NumberOfPersonYearsOfWaterAccess(),
    NumberOfPersonYearsOfLatrineAccess(),
    NumberOfHouseholdMonths(),
    NumberOfPeopleReceivingTraining(),
    NumberOfChildrenServed(),
    NumberOfCommunitiesWithAccountabilityMechanismsCreated(),
    NumberOfCoupleYearsOfProtectionProvided(),
    NumberOfWomenDeliveringWithASkilledAttendant(),
    EquivalentDollarValueOfItemsDistributed(),
    NumberOfUnits(),
    NumberOfCasesManaged(),
    NumberOfMentalHealthConsultationsProvided(),
    NumberOfHouseholdsReachedWithCoaching(),
    NumberOfPeopleReceivingABusinessGrant(),
    NumberOfPrimaryHealthConsultationsProvided(),
    NumberOfVaccineDosesDistributed(),
    NumberOfPeopleReceivingVaccines(),
    NumberOfWomenServed(),
    NumberOfTeacherDaysOfTraining(),
    NumberOfTeacherYearsOfSupport(),
    NumberOfChildrenTreated(),
    ValueOfCashTransferred(),
    ValueOfBusinessGrantAmount(),
]
OUTPUT_METRIC_CHOICES = [
    (output_metric.id, output_metric.output_name)
    for output_metric in OUTPUT_METRICS
]
OUTPUT_METRICS_BY_ID = {
    output_metric.id: output_metric
    for output_metric in OUTPUT_METRICS
}


class ActivityGroup(models.Model):
    app_log_entry_link_name = "ombucore.admin:website_activitygroup_change"

    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
    )

    class Meta:
        verbose_name = _('Activity Group')
        verbose_name_plural = _('Activity Groups')
        ordering = ['name']

    def __str__(self):
        return self.name

    @property
    def activities_for_menu(self):
        return self.activities.filter(show_in_menu=True)


class Activity(models.Model):
    app_log_entry_link_name = "ombucore.admin:website_activity_change"
    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
    )
    description = models.TextField(
            verbose_name=_('Description'),
            blank=True,
            null=True,
    )
    icon = models.CharField(
            choices=(
                ('water', _('Access to Clean Water')),
                ('agriculture', _('Agricultural Extension Support')),
                ('business_training', _('Business Skills Training')),
                ('at_risk_children', _('Case Mangement for At-Risk Children')),
                ('child_friendly_spaces', _('Child Friendly Spaces')),
                ('family_planning', _('Distribution of Family Planning Supplies')),
                ('delivery', _('Encouraging Women to Deliver at Health Institutions')),
                ('food_assistance', _('Food Assistance')),
                ('latrine', _('Latrine-building Program')),
                ('legal_aid', _('Legal Aid')),
                ('nonfood_distribution', _('Non-food-item Distribution')),
                ('business_grants', _('Providing Business Grants')),
                ('primary_healthcare_services', _('Provision of Primary Healthcare Services')),
                ('vaccines', _('Provision of Vaccines')),
                ('gbv_survivors', _('Supporting Gender-based Violence Survivors at Women\'s Centers')),
                ('teacher_development', _('Teacher Development: Face-to-face Training')),
                ('teacher_development_ongoing', _('Teacher Development: Ongoing Professional Support')),
                ('malnutrition', _('Treatment for Severe Acute Malnutrition')),
                ('cash', _('Unconditional Cash Distribution')),
                ('villages', _('Villages')),
            ),
            default='cash',
            max_length=255,
    )
    group = ForeignKey(
            ActivityGroup,
            on_delete=models.PROTECT,
            related_name='activities',
    )
    output_metrics = ChoiceArrayField(
            models.CharField(
                    max_length=255,
                    blank=True,
                    choices=OUTPUT_METRIC_CHOICES,
            ),
            verbose_name=_('Output Metrics'),
            default=list,
    )
    show_in_menu = models.BooleanField(
            verbose_name=_('Show this activity in the Program Design Lessons menu'),
            help_text=_('When checked, this activity insights page will be accessible from the Program Design Lessons menu.'),
            default=True,
    )

    class Meta:
        verbose_name = _('Activity')
        verbose_name_plural = _('Activities')
        ordering = ['name']

    def __str__(self):
        return self.name

    def output_metric_objects(self):
        return [
            OUTPUT_METRICS_BY_ID.get(output_metric_id)
            for output_metric_id in self.output_metrics
        ]

    def is_iov(self):
        """Activities classified as Identify Output Value (IOV)."""
        iov_activities = ['Conditional Cash Transfer', 'Providing Business Grants', 'Unconditional Cash Transfer']
        is_iov = self.name in iov_activities
        return is_iov


def get_all_activity_parameter_fields():
    parameters = {}
    for output_metric in OUTPUT_METRICS:
        parameters.update(output_metric.parameters)
    return parameters


def get_activity_parameter_mapping():
    mapping = {}
    for activity in Activity.objects.all():
        activity_fields = []
        for output_metric in activity.output_metric_objects():
            activity_fields += output_metric.parameters.keys()
        mapping[activity.pk] = activity_fields
    return mapping


def get_activity_output_metric_mapping():
    mapping = {}
    for activity in Activity.objects.all():
        mapping[activity.pk] = activity.output_metrics
    return mapping

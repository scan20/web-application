from django.db import models
from django.utils.translation import gettext_lazy as _


class CostLineItem(models.Model):
    analysis = models.ForeignKey(
            'website.Analysis',
            verbose_name=_('Analysis'),
            on_delete=models.CASCADE,
            related_name='cost_line_items',
    )
    country_code = models.CharField(
            verbose_name=_('Country code'),
            max_length=10,
            null=False,
            blank=True,
            default='',
    )
    grant_code = models.CharField(
            verbose_name=_('Grant code'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    budget_line_code = models.CharField(
            verbose_name=_('Budget line code'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    account_code = models.CharField(
            verbose_name=_('Account code'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    site_code = models.CharField(
            verbose_name=_('Site code'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    sector_code = models.CharField(
            verbose_name=_('Sector code'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    budget_line_description = models.CharField(
            verbose_name=_('Budget line description'),
            max_length=1000,
            null=False,
            blank=True,
            default='',
    )
    total_cost = models.DecimalField(
            verbose_name=_('Total cost'),
            max_digits=12,
            decimal_places=2,
    )
    loe_or_unit = models.DecimalField(
            verbose_name=_('LOE or Unit'),
            max_digits=12,
            decimal_places=2,
            null=True,
            blank=True,
    )
    months_or_unit = models.DecimalField(
            verbose_name=_('Months or Unit'),
            max_digits=12,
            decimal_places=2,
            null=True,
            blank=True,
    )
    unit_cost = models.DecimalField(
            verbose_name=_('Unit cost'),
            max_digits=12,
            decimal_places=2,
            null=True,
            blank=True,
    )
    dummy_field_1 = models.CharField(
            verbose_name=_('Dummy field 1'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )
    dummy_field_2 = models.CharField(
            verbose_name=_('Dummy field 2'),
            max_length=255,
            null=False,
            blank=True,
            default='',
    )

    note = models.CharField(
            verbose_name=_("Note"),
            max_length=2048,
            null=False,
            blank=True,
            default='',
    )

    cloned_from = models.ForeignKey(
            'website.CostLineItem',
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )

    class Meta:
        verbose_name = _('Cost Line Item')
        verbose_name_plural = _('Cost Line Items')
        index_together = [
            ['country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code']
        ]

    @classmethod
    def site_code_filter_choices(cls):
        return [(p, p) for p in cls.objects.order_by('site_code').values_list('site_code', flat=True).distinct() if p]

    @classmethod
    def grant_code_filter_choices(cls):
        return [(p, p) for p in cls.objects.order_by('grant_code').values_list('grant_code', flat=True).distinct() if p]


class CostLineItemConfig(models.Model):
    cost_line_item = models.OneToOneField(
            CostLineItem,
            verbose_name=_('Cost Line Item'),
            on_delete=models.CASCADE,
            related_name='config',
    )
    sector = models.ForeignKey(
            'website.Sector',
            verbose_name=_('Sector'),
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )
    category = models.ForeignKey(
            'website.Category',
            verbose_name=_('Category'),
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )
    allocation = models.DecimalField(
            verbose_name=_('% allocation to activity'),
            max_digits=5,
            decimal_places=2,
            null=True,
            blank=True,
    )

    output_value = models.BooleanField(
            verbose_name=_('output value'),
            default=False
    )
    cloned_from = models.ForeignKey(
            'website.CostLineItemConfig',
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )

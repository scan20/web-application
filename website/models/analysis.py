import logging
from decimal import Decimal
from typing import List, Optional

from django.conf import settings
from django.db import connection, models
from django.db.models import F, JSONField, Q, Sum, Value
from django.utils.translation import gettext_lazy as _
from ombucore.admin.fields import ForeignKey

from website import betterdb, stopwatch
from website.models.core import Sector, SectorCategoryMapping
from website.models.cost_line_item import CostLineItem
from website.models.transaction import TransactionLike

logger = logging.getLogger(__name__)


class AnalysisType(models.Model):
    title = models.CharField(
            verbose_name=_('Title'),
            max_length=255,
    )

    class Meta:
        verbose_name = _('Analysis Type')
        verbose_name_plural = _('Analyses Types')

    def __str__(self) -> str:
        return self.title


class Analysis(models.Model):
    CURRENCY_CHOICES = (
        ("USD", "US dollars"),
        ("GBP", "British Pound sterling"),
        ("EUR", "Euros"),
    )

    DATA_STORE_NAME = 'Data store'

    app_log_entry_link_name = 'analysis'
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    owner = ForeignKey(
            settings.AUTH_USER_MODEL,  # see here: https://docs.djangoproject.com/en/dev/topics/auth/customizing/#django.contrib.auth.get_user_model
            null=True,
            on_delete=models.SET_NULL,
            related_name='analyses',
    )
    source = models.CharField(
            verbose_name=_('Source'),
            max_length=255,
            blank=True,
            null=True,
    )
    analysis_type = models.ForeignKey(
            AnalysisType,
            verbose_name=_('Analysis Type'),
            on_delete=models.PROTECT,
            related_name='+',
    )
    activity = models.ForeignKey(
            'website.Activity',
            verbose_name=_('Activity Being Analyzed'),
            on_delete=models.PROTECT,
    )
    title = models.CharField(
            verbose_name=_('Title'),
            max_length=255,
    )
    description = models.TextField(
            verbose_name=_('Description'),
            blank=True,
            null=True,
    )
    start_date = models.DateField(
            verbose_name=_('Start Date'),
    )
    end_date = models.DateField(
            verbose_name=_('End Date'),
    )
    country = models.ForeignKey(
            'website.Country',
            verbose_name=_('Country'),
            on_delete=models.PROTECT,
            related_name='+',
    )
    grants = models.CharField(max_length=255)
    parameters = JSONField(default=dict)
    output_costs = JSONField(default=dict)
    cloned_from = models.ForeignKey(
            'website.Analysis',
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )
    currency_code = models.CharField(
            verbose_name=_('Currency code'),
            max_length=255,
            choices=CURRENCY_CHOICES,
            null=False,
            blank=True,
            default='',
    )
    output_count_source = models.CharField(
        verbose_name=_('Output count data source'),
        max_length=255,
        null=True,
        blank=True,
    )
    needs_transaction_resync = models.BooleanField(default=False, editable=False)

    class Meta:
        verbose_name = _('Analysis')
        verbose_name_plural = _('Analyses')

    def __str__(self) -> str:
        return self.title

    def activity_name(self) -> str:
        return self.activity.name

    def query_grants(self) -> List[str]:
        """
        The list of grants used to query transactions.
        """
        return [grant.strip() for grant in self.grants.split(',')]

    def grants_list(self) -> List[str]:
        """
        The list of grants in the analysis' cost line items.
        """
        return list(self.cost_line_items.order_by('grant_code').values_list('grant_code', flat=True).distinct())

    def is_complete(self) -> bool:
        first_metric_id = next(iter(self.activity.output_metrics), None)  # Get first output metric or None.
        if first_metric_id:
            return first_metric_id in self.output_costs
        else:
            return False

    def get_first_cost_per_output_all(self) -> Decimal:
        """
        Returns the first output cost including shared.
        """
        first_metric_id = next(iter(self.activity.output_metrics), None)  # Get first output metric or None.
        if first_metric_id and first_metric_id in self.output_costs:
            metric_costs = self.output_costs.get(first_metric_id, {})
            return metric_costs.get('all', None)
        return None

    def has_transactions(self) -> bool:
        return self.transactions.count() > 0

    @stopwatch.trace()
    def create_cost_line_items_from_transactions(self, transactions=None) -> None:
        self._create_cost_line_items_fast(transactions)

    def _create_cost_line_items_fast(self, transactions: Optional[List[TransactionLike]]):
        cli_key_fields = ['country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code']

        def cli_key(tr):
            return tuple(getattr(tr, f) for f in cli_key_fields)

        # Transactions can be 'grouped' by some fields, and each of them in the group point to the same CostLineItem.
        # We increment the total cost of the Transactions the CostLineItem points to,
        # and keep track of the CostLineItem a Transaction points to.
        #
        # Then at the end, we do a bulk insert of our CostLineItems (to save the new amount, and get an ID)
        # and update the Transactions (to save the CostLineItem FK).
        #
        # We need to use an UPDATE with a JOIN, so we can update all the transactions with a single query.
        # Otherwise we'd need to make very many queries, to update all the transactions/groups.
        # The alternative would be to create Transactions and CostLineItems in memory,
        # then insert CostLineItems and only then Transactions, but that is a pretty big change.
        # (note that Transactions have a FK to CostLineItem, but CostLineItem then depends on Transaction via its sum,
        # and bi-direction coupling leads to issues like this).
        if transactions is None:
            transactions = self.transactions.all()

        cost_line_items_by_key = {}

        for t in transactions:
            key = cli_key(t)
            cli = cost_line_items_by_key.get(key)
            if cli is None:
                cli = dict(
                    analysis_id=self.id,
                    country_code=t.country_code,
                    grant_code=t.grant_code,
                    budget_line_code=t.budget_line_code,
                    account_code=t.account_code,
                    site_code=t.site_code,
                    sector_code=t.sector_code,
                    budget_line_description=t.budget_line_description,
                    total_cost=0,
                    dummy_field_1='',
                    dummy_field_2='',
                    note=''
                )
                cost_line_items_by_key[key] = cli
            cli['total_cost'] += t.amount_in_instance_currency

        cli_dicts = [li for li in cost_line_items_by_key.values() if not -0.01 < li['total_cost'] < 0.01]

        betterdb.bulk_insert(CostLineItem, cli_dicts)

        # See above for why we do this
        # (match CLIs and Transactions on the common fields, as defined by `cli_key` above).
        #
        # We need a line like this for each field:
        #   (cli.account_code = website_transaction.account_code)
        # These fields cannot be null, so we do not need to worry about null comparisons.
        # Also, because we aren't interpolating any actual row values, this should be safe without escaping.
        filters = [f"cli.{f} = website_transaction.{f}" for f in cli_key_fields]
        # nosec: None of the interpolated values are user-provided,
        # and assembling this statement through psycopg2 is sufficiently inconvenient.
        q = f"""
UPDATE website_transaction
SET cost_line_item_id = (
    SELECT cli.id
    FROM website_costlineitem cli
    WHERE analysis_id = {self.id}
    AND ({' AND '.join(filters)})
    LIMIT 1)
WHERE analysis_id = {self.id}
"""  # nosec
        with connection.cursor() as cursor:
            cursor.execute(q)

        # We have a lot of items in this set, so this is MUCH faster.
        betterdb.delete(self.transactions.filter(cost_line_item_id__isnull=True))

    @stopwatch.trace()
    def sync_cost_line_items(self, transactions=None):
        if transactions is None:
            transactions = self.transactions.all()

        cli_key_fields = ['country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code']

        def cli_key(tr):
            return tuple(getattr(tr, f) for f in cli_key_fields)

        cost_line_items_by_key = {}
        for cli in self.cost_line_items.all():
            cli.total_cost = 0
            cost_line_items_by_key[cli_key(cli)] = cli

        for t in transactions:
            key = cli_key(t)
            cli = cost_line_items_by_key.get(key)
            if cli is None:
                cli = CostLineItem(
                    analysis=self,
                    country_code=t.country_code,
                    grant_code=t.grant_code,
                    budget_line_code=t.budget_line_code,
                    account_code=t.account_code,
                    site_code=t.site_code,
                    sector_code=t.sector_code,
                    budget_line_description=t.budget_line_description,
                    total_cost=0,
                )
                cost_line_items_by_key[key] = cli
            cli.total_cost += t.amount_in_instance_currency

        clis = []
        for cli in cost_line_items_by_key.values():
            if not -0.01 < cli.total_cost < 0.01:
                clis.append(cli)
            else:
                if cli.id:
                    cli.delete()

        betterdb.bulk_insert(CostLineItem, [cli for cli in clis if not cli.id])
        betterdb.bulk_update_dicts(CostLineItem, [{'id': cli.id, 'total_cost': cli.total_cost} for cli in clis if cli.id])

        # See above for why we do this
        # (match CLIs and Transactions on the common fields, as defined by `cli_key` above).
        #
        # We need a line like this for each field:
        #   (cli.account_code = website_transaction.account_code)
        # These fields cannot be null, so we do not need to worry about null comparisons.
        # Also, because we aren't interpolating any actual row values, this should be safe without escaping.
        filters = [f"cli.{f} = website_transaction.{f}" for f in cli_key_fields]
        # nosec: None of the interpolated values are user-provided,
        # and assembling this statement through psycopg2 is sufficiently inconvenient.
        q = f"""
UPDATE website_transaction
SET cost_line_item_id = (
    SELECT cli.id
    FROM website_costlineitem cli
    WHERE analysis_id = {self.id}
    AND ({' AND '.join(filters)})
    LIMIT 1)
WHERE analysis_id = {self.id}
"""  # nosec
        with connection.cursor() as cursor:
            cursor.execute(q)

        betterdb.delete(self.transactions.filter(cost_line_item_id__isnull=True))

    def _create_cost_line_items_slow(self):
        def cost_line_item_from_transactions(tqs):
            first_transaction = tqs.first()
            if not first_transaction:
                return None
            total_cost = tqs.aggregate(models.Sum('amount_in_instance_currency'))[
                'amount_in_instance_currency__sum']
            if not -0.01 < total_cost < 0.01:
                return CostLineItem.objects.create(
                    analysis=self,
                    country_code=first_transaction.country_code,
                    grant_code=first_transaction.grant_code,
                    budget_line_code=first_transaction.budget_line_code,
                    account_code=first_transaction.account_code,
                    site_code=first_transaction.site_code,
                    sector_code=first_transaction.sector_code,
                    budget_line_description=first_transaction.budget_line_description,
                    total_cost=total_cost,
                )

        fields = ['country_code', 'grant_code', 'budget_line_code', 'account_code', 'site_code', 'sector_code']
        combinations = self.transactions.values_list(*fields).distinct()
        cost_line_items = []
        for combination_list in combinations:
            filter_kwargs = {
                field: combination_list[i]
                for i, field in enumerate(fields)
            }
            transactions_qs = self.transactions.filter(**filter_kwargs)
            cost_line_item = cost_line_item_from_transactions(transactions_qs)
            if cost_line_item:
                cost_line_items.append(cost_line_item)
                transactions_qs.update(cost_line_item_id=cost_line_item.id)
        self.transactions.filter(cost_line_item_id__isnull=True).delete()

    def auto_categorize_cost_line_items(self) -> None:
        cost_line_items = self.cost_line_items.prefetch_related(
            'config',
            'config__category',
            'config__sector'
        ).all()
        SectorCategoryMapping.auto_categorize_cost_line_items(cost_line_items)

    @stopwatch.trace()
    def ensure_sector_category_objects(self) -> List[int]:
        """
        Creates AnalysisSectorCategory objects for each combination of
        Sector and Category used by cost line items if they don't already exist.
        Removes AnalysisSectorCategory objects if they're no longer used by
        cost line items.
        """
        # Get sector_id, category_id tuples from the cost line items.
        sectors_categories = list(self.cost_line_items.exclude(
                Q(config__sector__isnull=True) | Q(config__category__isnull=True)
        ).order_by(
                'config__sector__order', 'config__category__order'
        ).values_list(
                'config__sector', 'config__category'
        ).distinct())

        # If we have no sector categories, there's nothing to create or delete.
        # Return an empty list (created ids).
        if len(sectors_categories) == 0:
            return []

        sector_ids, category_ids = zip(*sectors_categories)
        sectors_categories_set = set(sectors_categories)

        used_ids = []
        existing = AnalysisSectorCategory.objects.filter(
            analysis=self,
            sector_id__in=sector_ids,
            category_id__in=category_ids,
        )
        for o in existing:
            sector_category_id = (o.sector_id, o.category_id)
            if sector_category_id in sectors_categories_set:
                sectors_categories_set.remove(sector_category_id)
                used_ids.append(o.id)
        to_create = [
            AnalysisSectorCategory(
                    analysis=self,
                    sector_id=sector_id,
                    category_id=category_id,
            ) for (sector_id, category_id) in sectors_categories_set]
        AnalysisSectorCategory.objects.bulk_create(to_create)

        created_ids = [o.id for o in to_create]
        used_ids.extend(created_ids)

        # Delete any sector categories that are no longer used by cost line items.
        self.sector_categories.exclude(id__in=used_ids).delete()

        self.ensure_sector_category_grant_objects()

        return created_ids

    def ensure_sector_category_grant_objects(self) -> List[int]:
        to_create = []
        to_update = []

        grants_list = self.grants_list()
        sector_categories = list(self.sector_categories.prefetch_related('sector').all())

        # If we have no sector categories, we have no grants to create.
        # Return an empty list (created ids).
        if len(sector_categories) == 0:
            return []

        sector_ids, category_ids = zip(*[(sc.sector_id, sc.category_id) for sc in sector_categories])

        sector_category_grants_with_cost_line_items = {
            (o.config.sector_id, o.config.category_id, o.grant_code)
            for o in
            self.cost_line_items.prefetch_related('config').filter(
                config__sector_id__in=sector_ids,
                config__category_id__in=category_ids,
                grant_code__in=grants_list,
            ).all()
        }
        analysis_scgrants_by_key = {
            (o.sector_category_id, o.grant): o
            for o in
            AnalysisSectorCategoryGrant.objects.filter(
                sector_category__in=sector_categories, grant__in=grants_list
            ).all()
        }
        used_ids = []
        for sector_category in sector_categories:
            for grant_code in grants_list:
                used_key = (sector_category.sector_id, sector_category.category_id, grant_code)
                if used_key not in sector_category_grants_with_cost_line_items:
                    continue

                existing_scg = analysis_scgrants_by_key.get((sector_category.id, grant_code))
                if existing_scg is None:
                    to_create.append(AnalysisSectorCategoryGrant(
                            sector_category=sector_category,
                            grant=grant_code,
                            contributes_to_activity=not sector_category.sector.direct_program_cost or None,
                    ))
                elif not sector_category.sector.direct_program_cost:
                    existing_scg.contributes_to_activity = True
                    to_update.append(existing_scg)
                else:
                    used_ids.append(existing_scg.id)

        AnalysisSectorCategoryGrant.objects.bulk_create(to_create)
        AnalysisSectorCategoryGrant.objects.bulk_update(to_update, ['contributes_to_activity'])

        created_ids = [o.id for o in to_create]
        used_ids.extend([o.id for o in to_update])
        used_ids.extend(created_ids)
        AnalysisSectorCategoryGrant.objects.filter(
                sector_category_id__in=self.sector_categories.values_list('id', flat=True),
        ).exclude(
                id__in=used_ids,
        ).delete()
        return created_ids

    def get_sectors_used(self):
        return Sector.objects.filter(
                id__in=self.sector_categories.values_list('sector', flat=True).distinct()
        )

    @property
    def sector_category_grants(self):
        return AnalysisSectorCategoryGrant.objects.filter(
                sector_category__analysis_id=self.id,
        )

    def has_parameters(self) -> bool:
        parameter_keys = self.parameters.keys()
        for output_metric in self.activity.output_metric_objects():
            for output_metric_param in output_metric.parameters.keys():
                if output_metric_param not in parameter_keys:
                    return False
        return True

    def has_uncategorized_cost_line_items(self) -> bool:
        return self.get_uncategorized_cost_line_items().count() > 0

    def get_uncategorized_cost_line_items(self):
        return self.cost_line_items.filter(
                Q(config__sector__isnull=True) | Q(config__category__isnull=True)
        ).order_by('config__sector__order', 'config__category__order')

    def _get_contributing_sector_category_grant_combos(self):
        """
        Returns tuples of (sector_id, category_id, grant) that are marked as
        contributing to the activity.
        """
        return self.sector_category_grants.filter(
                contributes_to_activity=True,
        ).values_list('sector_category__sector', 'sector_category__category', 'grant')

    def site_codes_choices_from_cost_line_items(self):
        return [(p, p) for p in self.cost_line_items.order_by('site_code').values_list('site_code', flat=True).distinct() if p]

    def grant_codes_choices_from_cost_line_items(self):
        return [(p, p) for p in self.cost_line_items.order_by('grant_code').values_list('grant_code', flat=True).distinct() if p]


    @property
    def contributing_cost_line_items(self):
        # Build a queryset filter that can be used to filter cost line items 
        # to only return those that have been marked as contributing to the activity.
        filters = Q()
        combos = self._get_contributing_sector_category_grant_combos()
        if len(combos):
            for sector_id, category_id, grant in combos:
                filters |= Q(config__sector_id=sector_id, config__category_id=category_id, grant_code=grant)
            return self.cost_line_items.filter(filters)
        return self.cost_line_items.none()

    def get_cost_output_sum_all(self) -> float:
        return self.contributing_cost_line_items.annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).aggregate(
                Sum('allocated_cost')
        )['allocated_cost__sum'] or 0

    def get_cost_output_sum_direct_only(self) -> float:
        return self.contributing_cost_line_items.filter(
                config__sector__direct_program_cost=True,
        ).annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).aggregate(
                Sum('allocated_cost')
        )['allocated_cost__sum'] or 0

    def calculate_output_costs(self) -> None:
        cost_output_sum_all = float(self.get_cost_output_sum_all())
        cost_output_sum_direct_only = float(self.get_cost_output_sum_direct_only())
        params = self.parameters.copy()
        self.output_costs = {}
        for output_metric in self.activity.output_metric_objects():
            try:
                params.update({'cost_output_sum': cost_output_sum_all})
                output_cost_all = output_metric.calculate(**params)

                params.update({'cost_output_sum': cost_output_sum_direct_only})
                output_cost_direct_only = output_metric.calculate(**params)

                self.output_costs[output_metric.id] = {
                    'all': float(round(output_cost_all, 2)),
                    'direct_only': float(round(output_cost_direct_only, 2)),
                }
            except Exception as e:
                logger.exception("Error calculating output cost")
        self.save()


class AnalysisSectorCategory(models.Model):
    analysis = models.ForeignKey(
            Analysis,
            verbose_name=_('Analysis'),
            on_delete=models.CASCADE,
            related_name='sector_categories',
    )
    sector = models.ForeignKey(
            'website.Sector',
            verbose_name=_('Sector'),
            on_delete=models.PROTECT,
            related_name='+',
            null=True,
    )
    category = models.ForeignKey(
            'website.Category',
            verbose_name=_('Category'),
            on_delete=models.PROTECT,
            related_name='+',
            null=True,
    )
    confirmed = models.BooleanField(
            verbose_name=_('Confirmed?'),
            default=False,
    )
    cloned_from = models.ForeignKey(
            'website.AnalysisSectorCategory',
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )

    class Meta:
        ordering = ['sector__order', 'category__order']

    def get_cost_line_items(self):
        return self.analysis.cost_line_items \
            .filter(
                config__sector=self.sector,
                config__category=self.category,
        ) \
            .select_related('config') \
            .prefetch_related('transactions') \
            .order_by(
                'grant_code',
                'budget_line_description',
                'site_code',
                'sector_code',
                'account_code',
        )

    def get_contributing_cost_line_items(self):
        # Build a queryset filter that can be used to filter cost line items 
        # to only return those that have been marked as contributing to the activity.
        filters = Q()
        for sector_id, category_id, grant in self.analysis._get_contributing_sector_category_grant_combos():
            filters |= Q(config__sector_id=sector_id, config__category_id=category_id, grant_code=grant)

        return self.get_cost_line_items().filter(filters)


class AnalysisSectorCategoryGrant(models.Model):
    sector_category = models.ForeignKey(
            AnalysisSectorCategory,
            verbose_name=_('Sector Category'),
            on_delete=models.CASCADE,
            related_name='sector_category_grants',
    )
    grant = models.CharField(
            verbose_name=_('Grant'),
            max_length=255,
    )
    contributes_to_activity = models.BooleanField(
            null=True,
            verbose_name=_('Contributes to activity'),
    )
    cloned_from = models.ForeignKey(
            'website.AnalysisSectorCategoryGrant',
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
            related_name='+',
    )

    def get_cost_line_items(self):
        return self.sector_category.analysis.cost_line_items \
            .filter(
                config__sector=self.sector_category.sector,
                config__category=self.sector_category.category,
                grant_code=self.grant,
        ) \
            .select_related('config') \
            .prefetch_related('transactions') \
            .order_by(
                'grant_code',
                'budget_line_description',
                'site_code',
                'sector_code',
                'account_code',
        )

    def allocation_complete(self):
        return self.sector_category.analysis.cost_line_items.filter(
                config__sector=self.sector_category.sector,
                config__category=self.sector_category.category,
                grant_code=self.grant,
                config__allocation__isnull=True,
        ).count() == 0

    class Meta:
        ordering = ['sector_category__sector__type',
                    'sector_category__sector__order',
                    'grant',
                    'sector_category__category__order']

from typing import Tuple

from django.db import models
from django.db.models import F, Q, Sum, Value
from django.utils.translation import gettext_lazy as _
from ombucore.admin.fields import ForeignKey
from website import betterdb, stopwatch

from website.models.cost_line_item import CostLineItemConfig
from website.models.mixins import OrderableMixin


class Category(OrderableMixin, models.Model):
    app_log_entry_link_name = "ombucore.admin:website_category_change"
    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
    )
    description = models.TextField(
            verbose_name=_('Description'),
    )
    order = models.PositiveIntegerField(
            verbose_name=_('Sort Order'),
    )

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['order']

    def __str__(self):
        return self.name


class SectorType(object):
    id = None
    name = None
    shared = False


class DirectProjectCost(SectorType):
    id = 10
    name = _('Direct Project Cost')
    shared = False
    allocation_editable = True


class DirectSharedProjectCost(SectorType):
    id = 20
    name = _('Direct Shared Project Cost')
    shared = True
    allocation_estimation_template = 'analysis/allocation-estimation-direct-shared-project-costs.html'
    allocation_editable = True

    def get_suggested_allocation(self, analysis, grant) -> Tuple[float, float, float]:
        # Get cost line items in the grant that are part of "Direct program cost"
        # sectors that have been marked as "contributing" to the activity.
        numerator = analysis.contributing_cost_line_items.filter(
                grant_code=grant,
                config__sector__direct_program_cost=True,
        ).annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).aggregate(
                Sum('allocated_cost')
        )['allocated_cost__sum']
        if numerator is None:
            numerator = 0

        # Sum all cost line items in the grant.
        denominator = analysis.cost_line_items.filter(
                grant_code=grant,
                config__sector__direct_program_cost=True,
        ).aggregate(
                Sum('total_cost')
        )['total_cost__sum']
        if denominator is None:
            denominator = 0

        if denominator == 0:
            percentage = 0
        else:
            percentage = (numerator / denominator) * 100

        return numerator, denominator, percentage


class IndirectCostRecovery(SectorType):
    id = 30
    name = _('Indirect Cost Recovery (ICR)')
    shared = True
    allocation_estimation_template = 'analysis/allocation-estimation-indirect-cost-recovery.html'
    allocation_editable = False

    def get_suggested_allocation(self, analysis, grant) -> Tuple[float, float, float]:
        # Get cost line items in the grant that are part of "Direct program cost"
        # sectors that have been marked as "contributing" to the activity.
        numerator = analysis.contributing_cost_line_items.filter(
                grant_code=grant,
        ).filter(
                Q(config__sector__direct_program_cost=True) | Q(config__sector__type=DirectSharedProjectCost.id)
        ).annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).aggregate(
                Sum('allocated_cost')
        )['allocated_cost__sum']
        if numerator is None:
            numerator = 0

        # Sum all cost line items in the grant.
        denominator = analysis.cost_line_items.filter(
                grant_code=grant,
        ).filter(
                Q(config__sector__direct_program_cost=True) | Q(config__sector__type=DirectSharedProjectCost.id)
        ).aggregate(
                Sum('total_cost')
        )['total_cost__sum']
        if denominator is None:
            denominator = 0

        if denominator == 0:
            percentage = 0
        else:
            percentage = (numerator / denominator) * 100

        return numerator, denominator, percentage


class Sector(OrderableMixin, models.Model):
    app_log_entry_link_name = "ombucore.admin:website_sector_change"
    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
    )
    TYPES = (
        DirectProjectCost(),
        DirectSharedProjectCost(),
        IndirectCostRecovery(),
    )
    TYPE_CHOICES = list((
        (sector_type.id, sector_type.name)
        for sector_type in TYPES
    ))
    type = models.IntegerField(
            verbose_name=_('Type'),
            choices=TYPE_CHOICES,
            default=TYPES[0].id,
    )
    direct_program_cost = models.BooleanField(
            verbose_name=_('Is this a Direct Program Cost?'),
            default=False,
    )
    order = models.PositiveIntegerField(
            verbose_name=_('Sort Order'),
    )

    class Meta:
        verbose_name = _('Sector')
        verbose_name_plural = _('Sectors')
        ordering = ['type', 'order']

    def __str__(self):
        return self.name

    def type_obj(self):
        for sector_type in self.TYPES:
            if sector_type.id == self.type:
                return sector_type
        return None

    def save(self, *args, **kwargs):
        if self.type_obj().shared:
            self.direct_program_cost = False
        else:
            self.direct_program_cost = True
        return super().save(*args, **kwargs)

    def get_previous_type(self):
        for i, sector_type in enumerate(self.TYPES):
            if sector_type.id == self.type:
                if i > 0:
                    return self.TYPES[i - 1]
        return None


class Region(models.Model):
    app_log_entry_link_name = "ombucore.admin:website_region_change"
    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
    )
    region_code = models.CharField(
            verbose_name=_('Region Code'),
            max_length=255,
            null=True,
            blank=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Region')
        verbose_name_plural = _('Regions')
        ordering = ['name']


class Country(models.Model):
    app_log_entry_link_name = "ombucore.admin:website_country_change"

    name = models.CharField(
            verbose_name=_('Name'),
            max_length=255,
            unique=True
    )
    code = models.CharField(
            verbose_name=_('Code'),
            max_length=10,
    )
    region = ForeignKey(Region,
                        verbose_name=_('Region'),
                        null=True,
                        blank=True,
                        on_delete=models.PROTECT,
                        )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ['name']


class SectorCategoryMapping(models.Model):
    app_log_entry_link_name = "ombucore.admin:website_sectorcategorymapping_change"

    country_code = models.CharField(
            verbose_name=_('Country code'),
            max_length=255,
            null=True,
            blank=True,
    )
    grant_code = models.CharField(
            verbose_name=_('Grant code'),
            max_length=255,
            null=True,
            blank=True,
    )
    budget_line_code = models.CharField(
            verbose_name=_('Budget line code'),
            max_length=255,
            null=True,
            blank=True,
    )
    account_code = models.CharField(
            verbose_name=_('Account code'),
            max_length=255,
            null=True,
            blank=True,
    )
    site_code = models.CharField(
            verbose_name=_('Site code'),
            max_length=255,
            null=True,
            blank=True,
    )
    sector_code = models.CharField(
            verbose_name=_('Sector code'),
            max_length=255,
            null=True,
            blank=True,
    )
    budget_line_description = models.CharField(
            verbose_name=_('Budget line description'),
            max_length=255,
            null=True,
            blank=True,
    )
    sector = ForeignKey(
            Sector,
            verbose_name=_('Sector'),
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
    )
    category = ForeignKey(
            Category,
            verbose_name=_('Category'),
            on_delete=models.SET_NULL,
            null=True,
            blank=True,
    )

    criteria_fields = [
        'country_code',
        'grant_code',
        'budget_line_code',
        'account_code',
        'site_code',
        'sector_code',
        'budget_line_description',
    ]

    class Meta:
        verbose_name = _('Sector / Category Mapping')
        verbose_name_plural = _('Sector / Category Mappings')

    def __str__(self):
        return str(self._meta.verbose_name)

    @classmethod
    @stopwatch.trace()
    def auto_categorize_cost_line_items(cls, cost_line_items):
        mappings = cls.generate_mappings()
        to_insert = []
        to_update = []
        for cost_line_item in cost_line_items:
            # All dicts must have the same keys for insert or update
            if hasattr(cost_line_item, 'config'):
                config = {'id': cost_line_item.config.id, 'sector_id': cost_line_item.config.sector_id, 'category_id': cost_line_item.config.category_id}
                is_new = False
            else:
                config = {'cost_line_item_id': cost_line_item.id, 'sector_id': None, 'category_id': None, 'output_value': False}
                is_new = True
            # We can have a LOT of line items.
            # We want to make sure we only update the ones that have changed.
            values_changed = False
            for mapping in mappings:
                if cls.cost_line_item_matches(mapping, cost_line_item):
                    mapped_sector = mapping['result']['sector']
                    if mapped_sector and config['sector_id'] != mapped_sector.id:
                        config['sector_id'] = mapped_sector.id
                        values_changed = True
                    mapped_category = mapping['result']['category']
                    if mapped_category and config['category_id'] != mapped_category.id:
                        config['category_id'] = mapped_category.id
                        values_changed = True
            if is_new:
                to_insert.append(config)
            elif values_changed:
                to_update.append(config)
        betterdb.bulk_insert(CostLineItemConfig, to_insert)
        betterdb.bulk_update_dicts(CostLineItemConfig, to_update)

    @classmethod
    def cost_line_item_matches(cls, mapping, cost_line_item):
        for field, value in mapping['criteria'].items():
            if not getattr(cost_line_item, field) == value:
                return False
        return True

    @classmethod
    def generate_mappings(cls):
        mappings = []
        for mapping in cls.objects.prefetch_related('sector', 'category').all():
            m = {
                'criteria': {},
                'result': {
                    'sector': getattr(mapping, 'sector', None),
                    'category': getattr(mapping, 'category', None),
                },
            }
            for field in cls.criteria_fields:
                value = getattr(mapping, field, None)
                if value:
                    m['criteria'][field] = value

            mappings.append(m)
        mappings.sort(key=lambda m: len(m['criteria'].keys()))
        return mappings

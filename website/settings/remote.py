import http.client

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from website.settings.base import *

ENVIRONMENT_TYPE = os.environ.get("ENVIRONMENT_TYPE", 'remote')

INSTANCE_NAME = os.environ.get("INSTANCE_NAME")


def get_local_ip():
    try:
        connection = http.client.HTTPConnection("169.254.169.254", timeout=2)
        connection.request('GET', '/latest/meta-data/local-ipv4')
        response = connection.getresponse()
        return response.read().decode('utf-8')
    except Exception:
        return None


# BASE_URL = os.environ.get('BASE_URL')
BASE_URL = f"https://{DOMAIN}"

ALLOWED_HOSTS = ['', get_local_ip(), DOMAIN]
STATIC_ROOT = '/var/www/static/'
STATIC_URL = '/static/'
MEDIA_ROOT = '/var/www/media/'
MEDIA_URL = '/media/'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_HSTS_SECONDS = 15768000  # Trigger Strict-Transport-Security header.
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True

BASICAUTH_DISABLE = False

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'DEBUG',
        'handlers': ['console'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': True,
        },
        'django.template': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': False,
        },
        'django.db': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}


def sentry_before_send(event, hint):
    if event.get('logger') == 'django.security.DisallowedHost':
        return None
    return event


if os.environ.get('SENTRY_DSN', None):
    sentry_sdk.init(
            dsn=os.environ.get('SENTRY_DSN', None),
            integrations=[DjangoIntegration()],
            before_send=sentry_before_send,
    )

# #### AWS SES Email #### #
EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_SES_REGION_NAME = os.environ.get('SES_REGION', AWS_DEFAULT_REGION)
AWS_SES_REGION_ENDPOINT = f'email.{AWS_SES_REGION_NAME}.amazonaws.com'

# Security settings
CSRF_COOKIE_SECURE = True  # Only allow cookies on HTTPS # https://docs.djangoproject.com/en/2.2/ref/settings/#csrf-cookie-secure
SESSION_COOKIE_SECURE = True


if "ISO_CURRENCY_CODE" not in os.environ:
    raise ImproperlyConfigured("The environment variable ISO_CURRENCY_CODE is not set")

ISO_CURRENCY_CODE = os.environ.get('ISO_CURRENCY_CODE')

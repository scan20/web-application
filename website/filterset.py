import django_filters


class FilterSet(django_filters.FilterSet):
    """
    Subclassed to provide extra functionality to support the designs.
    """

    def applied_filters_count(self):
        """
        Returns the number of filters that were actually applied.

        Excludes the `search` and `order_by` fields if present on the filterset.
        """
        self.is_valid()  # Triggers form cleaning.
        exclude = ['search', 'order_by']
        applied_count = 0
        if hasattr(self.form, 'cleaned_data'):
            for field_name, field_value in self.form.cleaned_data.items():
                if field_name not in exclude and field_value:
                    applied_count += 1
        return applied_count

    def filters_applied(self):
        """
        Returns True if the form was submitted and any filters (including
        `search` but excluding `order_by`) were applied.
        """
        self.is_valid()  # Triggers form cleaning.
        if hasattr(self.form, 'cleaned_data'):
            for field_name, field_value in self.form.cleaned_data.items():
                if field_name != 'order_by' and field_value:
                    return True
        return False

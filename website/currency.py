from typing import Optional, TYPE_CHECKING

import currency
from django.conf import settings

if TYPE_CHECKING:
    from website.models import Analysis

ISO_CURRENCY_CODE_NOT_SET = settings.ISO_CURRENCY_CODE == 'none'


def currency_code(analysis: "Analysis" = None) -> Optional[str]:
    if ISO_CURRENCY_CODE_NOT_SET:
        return None
    elif analysis is not None and analysis.currency_code:
        return analysis.currency_code
    else:
        return settings.ISO_CURRENCY_CODE


def currency_name(plural=False, analysis: "Analysis" = None):
    if ISO_CURRENCY_CODE_NOT_SET:
        return None
    elif analysis is not None and analysis.currency_code:
        return currency.name(analysis.currency_code, plural=plural)
    else:
        return currency.name(settings.ISO_CURRENCY_CODE, plural=plural)


def currency_name_plural(analysis: "Analysis" = None) -> Optional[str]:
    return currency_name(plural=True, analysis=analysis)


def currency_symbol(analysis: "Analysis" = None) -> Optional[str]:
    if ISO_CURRENCY_CODE_NOT_SET:
        return None
    elif analysis is not None and analysis.currency_code:
        return currency.symbol(analysis.currency_code)
    else:
        return currency.symbol(settings.ISO_CURRENCY_CODE)


def get_currency_locale(currency_code: str) -> str:
    """
    Currency is always formatted as if we are in America per the client
    """

    return 'en_US'

"""website URL Configuration"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from website.admin.app_log import SendPendingNotificationsView
from website.help import views as help_views
from website.users.views import AdminLoginView, CustomPasswordResetFromKeyView
from website.views import activity
from website.views import analysis
from website.views import documents
from website.views import styleguide
from website.views.dashboard import dashboard
from website.views.duplicator import DuplicateView

urlpatterns = [
    path('', dashboard, name='dashboard'),

    # Analysis.
    path('analysis/define/', analysis.DefineCreate.as_view(), name='analysis-define-create'),
    path('analysis/<int:pk>/', analysis.AnalysisDetailView.as_view(), name='analysis'),
    path('analysis/<int:pk>/define/', analysis.DefineUpdate.as_view(), name='analysis-define-update'),
    path('analysis/<int:pk>/load-data/', analysis.LoadData.as_view(), name='analysis-load-data'),
    path('analysis/<int:pk>/assign-sector-category/', analysis.FixMissingData.as_view(), name='analysis-fix-missing-data'),
    path('analysis/<int:pk>/assign-sector-category/bulk/', analysis.FixMissingDataBulk.as_view(), name='analysis-fix-missing-data-bulk'),
    path('analysis/<int:pk>/categorize/', analysis.Categorize.as_view(), name='analysis-categorize'),
    path('analysis/<int:pk>/categorize/<int:sector_pk>/', analysis.CategorizeSector.as_view(), name='analysis-categorize-sector'),
    path('analysis/<int:pk>/categorize/<int:sector_pk>/bulk/', analysis.CategorizeSectorBulk.as_view(), name='analysis-categorize-sector-bulk'),
    path('analysis/<int:pk>/set-contribution/', analysis.SetContribution.as_view(), name='analysis-set-contribution'),
    path('analysis/<int:pk>/set-contribution/<int:sector_pk>/<str:grant>/', analysis.SetContributionSectorGrant.as_view(), name='analysis-set-contribution-sector-grant'),
    path('analysis/<int:pk>/allocate/', analysis.Allocate.as_view(), name='analysis-allocate'),
    path('analysis/<int:pk>/allocate/<int:sector_pk>/<str:grant>/', analysis.AllocateSectorGrant.as_view(), name='analysis-allocate-sector-grant'),
    path('analysis/<int:pk>/allocate/<int:sector_pk>/<str:grant>/save-suggested/', analysis.SaveSuggestedToAllConfirmView.as_view(), name='analysis-allocate-sector-grant--save-suggested'),

    path('analysis/<int:pk>/identify-output-value/', analysis.IdentifyOutputValue.as_view(), name='analysis-identify-output-value'),
    path('analysis/<int:pk>/assign-output-value/<int:sector_pk>/', analysis.AssignOutputValue.as_view(), name='analysis-assign-output-value'),
    path('analysis/<int:pk>/insights/', analysis.Insights.as_view(), name='analysis-insights'),
    path('analysis/<int:pk>/download', documents.full_cost_model_spreadsheet, name='analysis-cost-model-spreadsheet'),
    path('analysis/<int:pk>/copy', DuplicateView.as_view(), name='analysis-create-copy'),

    path('cost-line-item/<int:pk>/transactions/', analysis.CostLineItemTransactions.as_view(), name='cost-line-item-transactions'),
    path('activity/<int:pk>/', activity.ActivityInsights.as_view(), name='activity-insights'),
    path('styleguide/', styleguide.styleguidetoc, name='styleguidetoc'),
    path('styleguide/colors/', styleguide.styleguidecolors, name='styleguidecolors'),
    path('styleguide/typography/', styleguide.styleguidetypography, name='styleguidetypography'),
    path('styleguide/rte/', styleguide.styleguiderte, name='styleguiderte'),
    path('styleguide/forms/', styleguide.styleguideforms, name='styleguideforms'),
    path('styleguide/buttons-links/', styleguide.styleguidebuttons, name='styleguidebuttons'),

    path('help/', help_views.help_menu, name='help-menu'),
    path('help/<path:path>/', help_views.help_page, name='help-page'),
    path('panels/help/menu/', help_views.help_page, name='help-menu-edit'),
    path('panels/app_log/send-notifications/', SendPendingNotificationsView.as_view(), name='send-notification-subscriptions'),

    # Authentication
    re_path(r"^accounts/password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
            CustomPasswordResetFromKeyView.as_view(),
            name="account_reset_password_from_key"),
    path('accounts/', include('website.email_2fa.urls')),
    path('accounts/', include('allauth.urls')),
    path('accounts/adminlogin/', AdminLoginView.as_view(), name="adminlogin"),

    # API
    path('api/', include('website.api.urls')),

    path('', include('ombucore.urls')),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
        path('admin/', admin.site.urls),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

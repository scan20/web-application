from functools import lru_cache

from babel.numbers import format_currency
from django import template
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from website.currency import currency_name as get_currency_name, currency_symbol as get_currency_symbol, get_currency_locale
from website.models import ActivityGroup, FieldLabelOverrides

register = template.Library()


@register.filter(name='chunks')
def chunks(iterable, chunk_size):
    iterable = iter(iterable)
    while True:
        chunk = []
        try:
            for _ in range(chunk_size):
                chunk.append(next(iterable))
            yield chunk
        except StopIteration:
            if chunk:
                yield chunk
            break


@register.filter
def exclude_field(form, exclude_field_name=None):
    """
    Provides an iterator over form fields that excludes the given field name.
    """
    for field in form:
        if field.name != exclude_field_name:
            yield field


@register.simple_tag
def get_activity_groups():
    return ActivityGroup.objects.all()


@lru_cache(maxsize=128)
def load_field_label_override(field_name, default=None):
    return FieldLabelOverrides.label_for(field_name, default)


@register.filter
def label_override(default_label, field_name):
    label = load_field_label_override(field_name, default_label)
    return _(label)


@register.filter(name='format_currency')
def format_currency_filter(value, currency_override=settings.ISO_CURRENCY_CODE):
    if not currency_override:
        currency_override = settings.ISO_CURRENCY_CODE
    if value is not None:
        return mark_safe(format_currency(value, currency_override, locale=get_currency_locale(currency_override)))  # nosec


@register.simple_tag
def currency_symbol():
    return get_currency_symbol()


@register.simple_tag
def currency_name(plural=False):
    return get_currency_name(plural=plural)


@register.simple_tag
def get_base_url():
    return settings.BASE_URL


@lru_cache(maxsize=128)
def do_render_icon(name):
    return mark_safe(render_to_string(template_name=f"icons/{name}.svg"))  # nosec


@register.simple_tag
def render_icon(name):
    return do_render_icon(name)

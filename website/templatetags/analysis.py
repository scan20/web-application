from django import forms
from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from ombucore.admin.widgets import FlatpickrDateWidget

from website.models import AccountCodeDescription

register = template.Library()


@register.filter
def sum_cost_line_items(cost_line_items):
    return sum(cost_line_item.total_cost for cost_line_item in cost_line_items)


@register.filter
def contains_sensitive_data(cost_line_items):
    return AccountCodeDescription.objects.filter(
            account_code__in=(
                cost_line_item.account_code for cost_line_item in cost_line_items
            ),
            sensitive_data=True
        ).exists()


@register.filter
def dict_get(d, key):
    if d:
        return d.get(key)
    return None


@register.filter
def attr(obj, attr_name):
    if obj:
        return getattr(obj, attr_name)
    return None


@register.filter
def is_flatpickr(field):
    return isinstance(field.field.widget, FlatpickrDateWidget)


@register.filter
def is_number_input(field):
    return isinstance(field.field.widget, forms.NumberInput)


@register.filter
def is_file_input(field):
    return isinstance(field.field.widget, forms.FileInput)


@register.filter
def is_hidden_input(field):
    return isinstance(field.field.widget, forms.HiddenInput)


@register.filter
def subtract(first, second):
    return first - second


@register.filter
def insights_chart_height(num_items):
    return (40 * num_items) + 52


@register.filter
def get_class_name(value):
    return type(value).__name__


@register.filter
def cost_line_items_by_category(qs, category):
    return qs.filter(config__category=category)


from contextlib import contextmanager
from functools import partial
from typing import (Any, ContextManager, Dict, Iterable, Iterator, List, Optional, Tuple, Union)

from psycopg2.extensions import adapt, register_adapter
from psycopg2.sql import Composable, Composed, Identifier, SQL

# See https://www.postgresql.org/docs/9.6/datatype-numeric.html
MAX_PG_INT = 2147483647
MIN_PG_INT = -2147483648


class Cursor(object):
    arraysize: int
    itersize: int  # pg extension

    def execute(self, statement, params: Union[Tuple, List] = None):
        ...

    def fetchone(self) -> List:
        ...

    def fetchall(self) -> List[List]:
        ...

    def fetchmany(self, chunk_size: int) -> List[List]:
        ...


def unwrap_cursor(cursor: Cursor) -> Cursor:
    if hasattr(cursor, "cursor"):
        return cursor.cursor
    return cursor


class Connection(object):
    def cursor(self) -> ContextManager[Cursor]:
        ...


class DjangoConnection(Connection):
    class features(object):
        empty_fetchmany_value: Any

    def chunked_cursor(self) -> ContextManager[Cursor]:
        ...


TServerSideCursorProvider = DjangoConnection


@contextmanager
def new_server_side_cursor(
    con: TServerSideCursorProvider
) -> ContextManager[Tuple[Cursor, Any]]:
    # Creating named cursors from psycopg Connection objects is not implemented
    with con.chunked_cursor() as cursor:
        yield cursor, con.features.empty_fetchmany_value


class SqlUnsafe(Composable):
    """There is no sql.Composable subclass for keywords,
    which are not identifiers or literals. So if you need ASC or whatever,
    you can use this. Note it is UNSAFE! Never pass in user-defined code
    (in the future, we can validate this to only Postgres keywords).
    """

    def as_string(self, context):
        return self._wrapped


@contextmanager
def set_search_path(cursor: Cursor, path):
    cursor.execute("select current_setting('search_path')")
    existing = cursor.fetchone()[0]
    try:
        cursor.execute("SET SEARCH_PATH TO " + path)
        yield
    finally:
        cursor.execute("SET SEARCH_PATH TO " + existing)


class Schema(Identifier):
    pass


class Table(Identifier):
    pass


_public_schema = Schema("public")
_temp_schema = Schema("pg_temp")


class FQN(Identifier):
    def __init__(self, schema: Union[Schema, str], table: Union[Table, str]):
        if not isinstance(schema, Schema):
            schema = Schema(schema)
        if not isinstance(table, Table):
            table = Table(table)
        self.schema = schema
        self.table = table
        super().__init__(schema.string, table.string)

    @classmethod
    def parse(cls, s: str):
        sch, tbl = s.split(".")
        return cls(Schema(sch), tbl)

    @classmethod
    def public(cls, t: Union[Table, str]):
        return cls(_public_schema, t)

    @classmethod
    def temp(cls, t: Union[Table, str]):
        return cls(_temp_schema, t)


class Column(Identifier):
    pass


class Keyword(SqlUnsafe):
    pass


class OrderByExpr(Composed):
    """Represents an ORDER BY column / direction pair expression

    `column` may either be a column name, or a Column instance.
    `direction` may either be a direction keyword ("ASC", "DESC"), or a Keyword instance.
    """

    def __init__(
        self,
        column: Union[Column, str],
        direction: Optional[Union[Keyword, str]] = None,
    ):
        if not isinstance(column, Column):
            column = Column(column)

        if not isinstance(direction, Keyword):
            direction = Keyword(direction if direction else "ASC")

        sql = SQL("{} {}").format(column, direction)
        super().__init__(sql)


class PGIntArray(object):
    def __init__(self, items=None):
        self.items = items or []

    @classmethod
    def adapt(cls, casted):
        return casted

    def getquoted(self) -> bytes:
        s = ",".join(f"{x}" for x in self.items)
        s = "{" + s + "}"
        val = adapt(s).getquoted()
        return val + b"::integer[]"


register_adapter(PGIntArray, PGIntArray.adapt)

def map_as_dict(
    row_it: Iterable[List[Any]], columns: Union[Iterable[str], Iterable[Identifier]]
) -> Iterator[Dict[str, Any]]:
    """Maps row tuples to a dictionary keyed by column name"""
    if isinstance(columns[0], Identifier):
        columns = [col.string for col in columns]

    yield from map(partial(_zip_row, columns=columns), row_it)


def _zip_row(cells, columns):
    assert len(cells) == len(columns)
    return {columns[i]: cell for i, cell in enumerate(cells)}

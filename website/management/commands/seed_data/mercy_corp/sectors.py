from website.models.core import DirectProjectCost, DirectSharedProjectCost, IndirectCostRecovery

SECTORS = [
    {'direct_program_cost': True,
     'name': 'Basic needs',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Conflict management',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Economic development',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Education supports',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Environment/Disaster Risk Reduction',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Food Security',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Governance',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Public health',
     'type': DirectProjectCost.id},

    {'direct_program_cost': False,
     'name': 'Direct Shared Cost',
     'type': DirectSharedProjectCost.id},

    {'direct_program_cost': False,
     'name': 'ICR',
     'type': IndirectCostRecovery.id}
]

from website.models.core import DirectProjectCost, DirectSharedProjectCost, IndirectCostRecovery

SECTORS = [
    {'direct_program_cost': True,
     'name': 'ERD',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Education',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Governance',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Health',
     'type': DirectProjectCost.id},

    {'direct_program_cost': True,
     'name': 'Protection',
     'type': DirectProjectCost.id},

    {'direct_program_cost': False,
     'name': 'Direct Shared Cost',
     'type': DirectSharedProjectCost.id},

    {'direct_program_cost': False,
     'name': 'ICR',
     'type': IndirectCostRecovery.id}
]

from website.models.core import DirectProjectCost, DirectSharedProjectCost, IndirectCostRecovery

SECTORS = [
    {
        'name': 'Economic Development',
        'direct_program_cost': True,
        'type': DirectProjectCost.id,
    },
    {
        'name': 'Education Support',
        'direct_program_cost': True,
        'type': DirectProjectCost.id,
    },
    {
        'name': 'Public Health',
        'direct_program_cost': True,
        'type': DirectProjectCost.id,
    },
    {
        'name': 'Direct Shared Cost',
        'direct_program_cost': False,
        'type': DirectSharedProjectCost.id,
    },
    {
        'name': 'ICR',
        'direct_program_cost': False,
        'type': IndirectCostRecovery.id,
    },
]

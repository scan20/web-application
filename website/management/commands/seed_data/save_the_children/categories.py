CATEGORIES = [
    {'description': 'All cost items associated with international staff members '
                    'that receive staff benefits, e.g. salary, benefits, housing, '
                    'home leave, professional development, recruitment.',
     'name': 'International Staff'},
    {'description': 'All cost items associated with national staff members that '
                    'receive staff benefits, e.g. salary, benefits, professional '
                    'development, recruitment.',
     'name': 'National Staff'},
    {'description': 'All cost items associated with personnel who are not '
                    'considered official staff members and who do not receive '
                    'fringe benefits, e.g. community health workers, teachers, '
                    'protection monitors, social workers, trainers, enumerators, '
                    'evaluation consultants, incentive workers.',
     'name': 'Non-Staff Personnel'},
    {'description': 'All cost items associated with work-related travel, e.g. '
                    'flights, taxi, accommodation, per diem, visa. This includes '
                    'vehicle pool expenses such as car rental, motorcycles, fuel, '
                    'and maintenance.',
     'name': 'Travel'},
    {'description': 'All non-personnel cost items specifically used or procured '
                    'specifically for the implementation of program activities, '
                    'e.g. NFI kits, food transportation, cold chain services, '
                    'warehousing, cash transfers, training materials, behavior '
                    'change incentives.',
     'name': 'Materials & Activities'},
    {'description': 'All capital assets and equipment costs.',
     'name': 'Assets & Equipment'},
    {'description': 'All non-personnel items associated with having offices, e.g. '
                    'rent, utilities, stationery, office supplies, generators, '
                    'legal fees, software licenses.',
     'name': 'Office Expenses'},
    {'description': 'All cost items for goods, services, or activities that are '
                    'outsourced to and delivered by a partner, civil society '
                    'organization, government agency, or contracting agency to '
                    'provide services to clients on behalf of the lead '
                    'organization.',
     'name': 'Sub-Grants'},
    {'description': 'Indirect costs which are not closely linked to program '
                    "activities, but are necessary  for the organization's "
                    'operations and overall management particularly in HQ.',
     'name': 'ICR'}
]

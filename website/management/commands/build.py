import csv
import datetime
import os
import sys
from decimal import Decimal
from pathlib import Path

from allauth.socialaccount.models import SocialApp
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.files import File
from django.core.validators import get_available_image_extensions
from ombucore.assets.models import ImageAsset
from ombucore.build import BuildCommand

from website.data_loading import load_cost_line_items_from_file
from website.help.build import help_build
from website.management.commands.utils import BulkCreateManager
from website.models import AccountCodeDescription, CostEfficiencyStrategy
from website.models import Activity, ActivityGroup
from website.models import Analysis, AnalysisType
from website.models import Category, Country, Region, Sector
from website.models import InsightComparisonData
from website.models import SectorCategoryMapping, Transaction
from website.models import Settings


class Command(BuildCommand):

    def handle(self, *args, **options):
        super().handle(*args, **options)
        self.add_settings()
        self.add_countries_and_regions()

        self.add_assets()

        ngo = options.get('ngo')

        print(f"Loading data for NGO: {ngo}")
        if ngo == "default":
            from .seed_data.default import SECTORS, MAPPINGS, CATEGORIES
        elif ngo == "mercy-corp":
            from .seed_data.mercy_corp import SECTORS, MAPPINGS, CATEGORIES
        elif ngo == "irc":
            from .seed_data.irc import SECTORS, MAPPINGS, CATEGORIES
        elif ngo == "save-the-children":
            from .seed_data.save_the_children import SECTORS, MAPPINGS, CATEGORIES
        else:
            raise ValueError(f"Invalid NGO selection: '{ngo}'")

        self.add_categories_and_sectors(sectors=SECTORS, categories=CATEGORIES)
        self.add_sector_category_mappings(mappings=MAPPINGS)
        self.add_activities()
        self.add_analysis_types()
        self.add_cost_efficiency_strategies()
        self.add_account_code_descriptions()
        self.add_users()
        self.add_analyses()
        self.add_insight_comparison_data()

        help_build()

    def add_arguments(self, parser):
        parser.add_argument(
                "--ngo",
                const="default",
                nargs="?",
                default="default",
                choices=['default', 'save-the-children', 'irc', 'mercy-corp'],
                help='Populate the database with data for this particular NGO'
        )

        super().add_arguments(parser)

    def add_users(self):
        User = get_user_model()

        users_to_create = [
            {'name': 'Administrator',
             'primary_country': 'DR Congo',
             'secondary_countries': [],
             'role': User.ADMIN},
            {'name': 'Caitlin Tulloch',
             'primary_country': 'Jordan',
             'secondary_countries': ['Niger', 'Yemen', 'Philippines', 'Iraq'],
             'role': User.BASIC_USER},
            {'name': 'Akmal Shah',
             'primary_country': 'Iraq',
             'secondary_countries': ['Jordan', 'Yemen'],
             'role': User.BASIC_USER},
            {'name': 'Takhani Kromah',
             'primary_country': 'Liberia',
             'secondary_countries': ['Jordan'],
             'role': User.BASIC_USER},
            {'name': 'Jane Doe',
             'primary_country': 'Philippines',
             'secondary_countries': [],
             'role': User.BASIC_USER},
            {'name': 'John Doe',
             'primary_country': 'Iraq',
             'secondary_countries': [],
             'role': User.BASIC_USER},
            {'name': 'OMBU Test',
             'primary_country': 'Iraq',
             'secondary_countries': [],
             'email': 'analytics@ombuweb.com',
             'role': User.ADMIN},
        ]

        for each_user in users_to_create:
            new_user = User.objects.create(name=each_user['name'],
                                           primary_country=self.countries[each_user['primary_country']],
                                           role=each_user['role'])
            for each_country in each_user['secondary_countries']:
                new_user.secondary_countries.add(self.countries[each_country])

            if 'email' in each_user:
                new_user.email = each_user['email']
            else:
                new_user.email = f"dioptra_{settings.INSTANCE_NAME}+{new_user.name.lower().replace(' ', '')}@ombuweb.com"
            new_user.set_password('password')
            new_user.save()

    def add_settings(self):
        site_settings = Settings.objects.create()

        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'Budget Upload Template.xls')
        with open(path, 'rb') as f:
            site_settings.budget_upload_template.save('Budget Upload Template.xls', File(f, name='Budget Upload Template.xls'), save=True)

        site_settings.save()

    def add_countries_and_regions(self):
        west_africa = Region.objects.create(name='West Africa', region_code='WA')
        east_africa = Region.objects.create(name='East Africa', region_code='EA')
        asia = Region.objects.create(name='Asia', region_code='AS')
        middle_east = Region.objects.create(name='Middle East', region_code='ME')
        self.countries = {
            'Bangladesh': Country.objects.create(name='Bangladesh', code='BD', region=asia),
            'Jordan': Country.objects.create(name='Jordan', code="JO", region=middle_east),
            'Niger': Country.objects.create(name='Niger', code='NE', region=west_africa),
            'Yemen': Country.objects.create(name='Yemen', code="YE", region=east_africa),
            'Iraq': Country.objects.create(name='Iraq', code="IQ", region=middle_east),
            'Lebanon': Country.objects.create(name='Lebanon', code='LB', region=middle_east),
            'Liberia': Country.objects.create(name='Liberia', code='LR', region=west_africa),
            'Somalia': Country.objects.create(name='Somalia', code='SO', region=east_africa),
            'Ethiopia': Country.objects.create(name='Ethiopia', code='ET', region=east_africa),
            'Mali': Country.objects.create(name='Mali', code='ML', region=west_africa),
            'Burundi': Country.objects.create(name='Burundi', code='BI', region=west_africa),
            'Tanzania': Country.objects.create(name='Tanzania', code='TZ', region=east_africa),
            'Kenya': Country.objects.create(name='Kenya', code='KE', region=east_africa),
            'Myanmar': Country.objects.create(name='Myanmar', code='MM', region=asia),
            'DR Congo': Country.objects.create(name='Democratic Republic of Congo', code="CD", region=west_africa),
            'Pakistan': Country.objects.create(name='Pakistan', code='PK', region=asia),
            'Afghanistan': Country.objects.create(name='Afghanistan', code='AF', region=asia),
            'Chad': Country.objects.create(name='Chad', code='TD', region=west_africa),
            'Philippines': Country.objects.create(name='Philippines', code="PH", region=asia),
        }

    def add_categories_and_sectors(self, sectors, categories):

        self.sectors = {}
        for i, sector in enumerate(sectors):
            self.sectors[sector['name']] = Sector.objects.create(order=i, **sector)

        self.categories = {}
        for i, category in enumerate(categories):
            self.categories[category['name']] = Category.objects.create(order=i, **category)

    def add_sector_category_mappings(self, mappings):

        for sector_code, account_code, category_name, sector_name in mappings:
            data = {}
            if sector_code:
                data['sector_code'] = sector_code
            if account_code:
                data['account_code'] = account_code
            if category_name:
                data['category'] = self.categories[category_name]
            if sector_name:
                data['sector'] = self.sectors[sector_name]

            SectorCategoryMapping.objects.create(**data)

    def add_analysis_types(self):
        self.analysis_types = {
            'Ex-ante analysis': AnalysisType.objects.create(title='Ex-ante analysis'),
            'Ex-post analysis': AnalysisType.objects.create(title='Ex-post analysis'),
        }

    def add_activities(self):
        groups = [
            'Cash',
            'Community',
            'Economic',
            'Education',
            'Environment',
            'Health',
            'Protection',
        ]
        self.groups = {}
        for group_name in groups:
            self.groups[group_name] = ActivityGroup.objects.create(name=group_name)

        access_to_clean_water = Activity.objects.create(
                name='Access to Clean Water',
                description='Provision of sufficient quantity of safe water to '
                            'meet the drinking and domestic needs of people in '
                            'need.',
                icon='water',
                group=self.groups['Environment'],
                output_metrics=['NumberOfPersonYearsOfWaterAccess'],
        )
        access_to_latrines = Activity.objects.create(
                name='Access to Latrines',
                description='Provision of adequate, appropriate and acceptable '
                            'latrines to allow rapid, safe and secure access at '
                            'all times.',
                icon='latrine',
                group=self.groups['Health'],
                output_metrics=['NumberOfPersonYearsOfLatrineAccess'],
        )
        agricultural_extension_support = Activity.objects.create(
                name='Agricultural Extension Support',
                description='Support to beneficiaries with crops, fertilizer, '
                            'livestock, tools; integrating them into efficient '
                            'supply value chains; providing training on '
                            'technology, irrigation, storage, processing, '
                            'financing, marketing, etc.',
                icon='agriculture',
                group=self.groups['Education'],
                output_metrics=['NumberOfHouseholdMonths'],
        )
        business_skills_training = Activity.objects.create(
                name='Business Skills Training',
                description='Provision of vocational and business skills '
                            'training in literacy, numeracy, marketing, '
                            'accounting, business plan development, etc.',
                icon='business_training',
                group=self.groups['Education'],
                output_metrics=['NumberOfPeopleReceivingTraining'],
        )
        case_management_for_at_risk_children = Activity.objects.create(
                name='Case Management for At-Risk Children',
                description='Provision of case management services for '
                            'children who are unaccompanied minors or '
                            'at risk of trafficking and exploitation.',
                icon='at_risk_children',
                group=self.groups['Community'],
                output_metrics=['NumberOfChildrenServed'],
        )
        child_friendly_spaces = Activity.objects.create(
                name='Child Friendly Spaces',
                description='Provision of safe spaces and schools where '
                            'communities create nurturing environments '
                            'for children to access free and structured '
                            'play, recreation, leisure and learning activities; '
                            'these spaces may provide health, nutrition and '
                            'psychosocial support and other activities that '
                            'restore a sense of normality and continuity. ',
                icon='child_friendly_spaces',
                group=self.groups['Community'],
                output_metrics=['NumberOfChildrenServed'],
        )
        conditional_cash_transfer = Activity.objects.create(
                name='Conditional Cash Transfer',
                description='Provision of cash to individuals, households, or '
                            'community recipients through electronic or direct '
                            'cash, or via paper or evouchers, with conditions '
                            'that must be met before receiving a transfer or '
                            'restrictions on what a transfer can be spent on '
                            'once received.',
                icon='cash',
                group=self.groups['Cash'],
                output_metrics=['ValueOfCashTransferred'],
        )
        creating_social_accountability_mechanisms = Activity.objects.create(
                name='Creating Social Accountability Mechanisms',
                description='Provision of social accountability programs.',
                icon='cash',
                group=self.groups['Community'],
                output_metrics=['NumberOfCommunitiesWithAccountabilityMechanismsCreated'],
        )
        distribution_of_family_planning_supplies = Activity.objects.create(
                name='Distribution of Family Planning Supplies',
                description='Provision of family planning supplies at health centers, community centers, schools, etc.',
                icon='family_planning',
                group=self.groups['Health'],
                output_metrics=['NumberOfCoupleYearsOfProtectionProvided'],
        )
        encouraging_women_to_deliver_at_health_institutions = Activity.objects.create(
                name='Encouraging women to deliver at health institutions',
                description='Provision and sensitization of obstetrics and gynecology '
                            'services at health institutions for expecting mothers.',
                icon='delivery',
                group=self.groups['Health'],
                output_metrics=['NumberOfWomenDeliveringWithASkilledAttendant'],
        )
        food_assistance = Activity.objects.create(
                name='Food Assistance',
                description='Provision of safe and nutritious food commodities; '
                            'or financing to support the centralised procurement '
                            'and distribution of food.',
                icon='food_assistance',
                group=self.groups['Health'],
                output_metrics=['EquivalentDollarValueOfItemsDistributed'],
        )
        generic_program = Activity.objects.create(
                name='Generic Program',
                description='General program to deliver services for the '
                            'benefit of people in need.',
                icon='cash',
                group=self.groups['Health'],
                output_metrics=['NumberOfUnits'],
        )
        legal_aid_case_management = Activity.objects.create(
                name='Legal Aid Case Management',
                description='Provision of legal case management services on '
                            'government documentation, access to social '
                            'services, illegal detention, asylum, etc.',
                icon='legal_aid',
                group=self.groups['Protection'],
                output_metrics=['NumberOfCasesManaged'],
        )
        mental_health_care_at_clinics = Activity.objects.create(
                name='Mental Health Care at Clinics',
                description='Provision of healthcare that addresses mental '
                            'health conditions and associated impaired functioning.',
                icon='primary_healthcare_services',
                group=self.groups['Health'],
                output_metrics=['NumberOfMentalHealthConsultationsProvided'],
        )
        non_food_item_distribution = Activity.objects.create(
                name='Non-Food-Item (NFI) Distribution',
                description='Distribution of non-edible items to beneficiaries '
                            'depending on the need and context, e.g. blankets, '
                            'cooking stoves, fuel, tents, mosquito nets, '
                            'clothes, buckets, soap, brushes, etc. ',
                icon='nonfood_distribution',
                group=self.groups['Health'],
                output_metrics=['EquivalentDollarValueOfItemsDistributed'],
        )
        parental_coaching_programs = Activity.objects.create(
                name='Parental Coaching Programs',
                description='Provision of parental coaching to strengthen the '
                            'capacities of caregivers to reduce violence in '
                            'the lives of children and support their healthy '
                            'development by introducing techniques parents can '
                            'use to communicate and problem solve effectively '
                            'with their children.',
                icon='child_friendly_spaces',
                group=self.groups['Community'],
                output_metrics=['NumberOfHouseholdsReachedWithCoaching'],
        )
        providing_business_grants = Activity.objects.create(
                name='Providing Business Grants',
                description='Provision of small business grants to entrepreneurs '
                            'to develop viable business plans; or business plan '
                            'competitions with cash awards.',
                icon='business_grants',
                group=self.groups['Economic'],
                output_metrics=['NumberOfPeopleReceivingABusinessGrant', 'ValueOfBusinessGrantAmount'],
        )
        provision_of_primary_healthcare_services = Activity.objects.create(
                name='Provision of Primary Healthcare Services',
                description='Provision of safe and effective primary healthcare '
                            'to prevent avoidable mortality, morbidity, '
                            'suffering and disability.',
                icon='primary_healthcare_services',
                group=self.groups['Health'],
                output_metrics=['NumberOfPrimaryHealthConsultationsProvided'],
        )
        provision_of_vaccines = Activity.objects.create(
                name='Provision of Vaccines',
                description='Procurement of vaccines for partners with '
                            'cold-chain storage and transport; or direct '
                            'delivery of vaccines to people in need.',
                icon='vaccines',
                group=self.groups['Health'],
                output_metrics=['NumberOfVaccineDosesDistributed', 'NumberOfPeopleReceivingVaccines'],
        )
        supporting_gbv_survivors_at_womens_centers = Activity.objects.create(
                name="Supporting GBV Survivors at Women's Centers",
                description='Provision of essential health and psychosocial '
                            'services to survivors of gender-based violence '
                            'at women\'s centers.',
                icon='gbv_survivors',
                group=self.groups['Health'],
                output_metrics=['NumberOfWomenServed'],
        )
        teacher_development_face_to_face_trainings = Activity.objects.create(
                name='Teacher Development: Face-to-Face Trainings',
                description='Provision of face-to-face training workshops '
                            'to teachers and school management on topics '
                            'such as pedagogy, classroom management, etc.',
                icon='teacher_development',
                group=self.groups['Education'],
                output_metrics=['NumberOfTeacherDaysOfTraining'],
        )
        teacher_development_ongoing_professional_support = Activity.objects.create(
                name='Teacher Development: Ongoing Professional Support',
                description='Provision of school-based facilitated forums '
                            'for teachers to meet regularly to revisit '
                            'content learned in a training workshop and '
                            'to gain ongoing learning opportunities '
                            'through peer discussions.',
                icon='teacher_development_ongoing',
                group=self.groups['Education'],
                output_metrics=['NumberOfTeacherYearsOfSupport'],
        )
        treatment_for_severe_acute_malnutrition = Activity.objects.create(
                name='Treatment for Severe Acute Malnutrition',
                description='Provision of treatment for severe acute '
                            'malnutrition and wasting among children.',
                icon='malnutrition',
                group=self.groups['Health'],
                output_metrics=['NumberOfChildrenTreated'],
        )
        unconditional_cash_transfer = Activity.objects.create(
                name='Unconditional Cash Transfer',
                description='Provision of cash to individuals, households, '
                            'or community recipients through electronic or '
                            'direct cash, or via paper or evouchers, without '
                            'conditions that must be met before receiving a '
                            'transfer or restrictions on what a transfer can '
                            'be spent on once received.',
                icon='cash',
                group=self.groups['Cash'],
                output_metrics=['ValueOfCashTransferred'],
        )
        self.activities = {
            'supporting_gbv_survivors_at_womens_centers': supporting_gbv_survivors_at_womens_centers,
            'unconditional_cash_transfer': unconditional_cash_transfer,
            'access_to_clean_water': access_to_clean_water,
            'access_to_latrines': access_to_latrines,
            'provision_of_vaccines': provision_of_vaccines,
            'treatment_for_severe_acute_malnutrition': treatment_for_severe_acute_malnutrition,
            'child_friendly_spaces': child_friendly_spaces,
            'agricultural_extension_support': agricultural_extension_support,
            'generic_program': generic_program,
            'legal_aid_case_management': legal_aid_case_management,
        }

    def add_cost_efficiency_strategies(self):
        columns = [
            'activity_name',
            'title',
            'efficiency_driver_description',
            'strategy_to_improve_description',
        ]
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'cost_efficiency_strategies.csv')
        with open(path) as csv_file:
            next(csv_file)  # Skip header.
            next(csv_file)  # Skip header.
            reader = csv.DictReader(csv_file, fieldnames=columns)
            for row in reader:
                activity_name = row.pop('activity_name')
                try:
                    activity = Activity.objects.get(name=activity_name)
                    row['efficiency_driver_description'] = '<p>{}</p>'.format(row['efficiency_driver_description'])
                    row['strategy_to_improve_description'] = '<p>{}</p>'.format(row['strategy_to_improve_description'])
                    strategy = CostEfficiencyStrategy.objects.create(**row)
                    strategy.activities.add(activity)
                    strategy.save()
                except Exception as e:
                    print(f'Activity Not Found: {activity_name}')

    def add_analyses(self):
        User = get_user_model()

        bangladesh_fund = Analysis.objects.create(
                title='DF119 WPE CM Jordan Ex-Post Analysis (April 2015)',
                analysis_type=self.analysis_types['Ex-post analysis'],
                activity=self.activities['supporting_gbv_survivors_at_womens_centers'],
                parameters={
                    'number_of_women_served': 2992,
                },
                description='Insight into the cost of operating individual WPE CM in Women\'s Centres in Jordan',
                start_date=datetime.date(2015, 5, 1),
                end_date=datetime.date(2016, 4, 30),
                country=self.countries['Jordan'],
                grants='DF119',
                owner=User.objects.get(name='Caitlin Tulloch'),
        )
        self._import_sample_transactions_to_analysis(
                bangladesh_fund,
                'transaction_max_3k_sorted.csv',
                only_grant_codes=['DF119'],
        )
        bangladesh_fund.create_cost_line_items_from_transactions()
        bangladesh_fund.auto_categorize_cost_line_items()
        bangladesh_fund.ensure_sector_category_objects()
        # bangladesh_fund.sector_categories.all().update(confirmed=True)

        jordan_fund = Analysis.objects.create(
                title='DF168 Cash Transfer Program for Syrian Refugees in Iraq - Ex-Post Analysis (Q3-Q4 2017)',
                analysis_type=self.analysis_types['Ex-post analysis'],
                activity=self.activities['unconditional_cash_transfer'],
                parameters={
                },
                description='Delivering Humanitarian Assistance and Building Resilience for Conflict-Affected Populations in Syria',
                start_date=datetime.date(2017, 7, 1),
                end_date=datetime.date(2017, 12, 31),
                country=self.countries['Iraq'],
                grants='DF168',
                owner=User.objects.get(name='Akmal Shah'),
        )
        self._import_sample_transactions_to_analysis(
                jordan_fund,
                'transaction_max_3k_sorted.csv',
                only_grant_codes=['DF168'],
        )
        jordan_fund.create_cost_line_items_from_transactions()
        jordan_fund.auto_categorize_cost_line_items()
        jordan_fund.ensure_sector_category_objects()
        # jordan_fund.sector_categories.all().update(confirmed=True)

        yemen_fund = Analysis.objects.create(
                title='USAID Liberia PACS Model-GT (2015)',
                analysis_type=self.analysis_types['Ex-post analysis'],
                activity=self.activities['generic_program'],
                parameters={
                    'number_of_units': 10000,
                },
                description='PACS Model-GT',
                start_date=datetime.date(2015, 2, 1),
                end_date=datetime.date(2016, 7, 27),
                country=self.countries['Liberia'],
                grants='GA298',
                owner=User.objects.get(name='Takhani Kromah'),
        )
        self._import_sample_transactions_to_analysis(
                yemen_fund,
                'transaction_max_3k_sorted.csv',
                only_grant_codes=['GA298'],
        )
        yemen_fund.create_cost_line_items_from_transactions()
        yemen_fund.auto_categorize_cost_line_items()
        yemen_fund.ensure_sector_category_objects()

        ydp_philippines = Analysis.objects.create(
                title='DFID CCI IRC Legal Aid Ex-Ante Analysis (September 2017)',
                analysis_type=self.analysis_types['Ex-ante analysis'],
                activity=self.activities['legal_aid_case_management'],
                parameters={
                    'number_of_cases_managed': 890,
                },
                description='CCI IRC legal aid analysis',
                start_date=datetime.date(2017, 9, 1),
                end_date=datetime.date(2018, 6, 30),
                country=self.countries['Iraq'],
                grants='Unknown',
                owner=User.objects.get(name='Akmal Shah'),
        )
        self._import_cost_line_items_from_file(ydp_philippines, 'DF186 Budget Upload.xlsx')
        ydp_philippines.auto_categorize_cost_line_items()
        ydp_philippines.ensure_sector_category_objects()
        # ydp_philippines.sector_categories.all().update(confirmed=True)

        cash_iraq = Analysis.objects.create(
                title='DFID CCI IRC Cash Transfer Program Ex-Ante Analysis (September 2017)',
                analysis_type=self.analysis_types['Ex-ante analysis'],
                activity=self.activities['unconditional_cash_transfer'],
                parameters={
                },
                description='CCI IRC Cash Transfer Program',
                start_date=datetime.date(2017, 9, 1),
                end_date=datetime.date(2018, 6, 30),
                country=self.countries['Iraq'],
                grants='Unknown',
                owner=User.objects.get(name='Akmal Shah'),
        )
        self._import_cost_line_items_from_file(cash_iraq, 'DF186 Budget Upload.xlsx')
        cash_iraq.auto_categorize_cost_line_items()
        cash_iraq.ensure_sector_category_objects()

    def _import_sample_transactions_to_analysis(self, analysis, filename, only_grant_codes=None):
        columns = [
            'date',
            'country_code',
            'grant_code',
            'budget_line_code',
            'account_code',
            'site_code',
            'sector_code',
            'transaction_code',
            'transaction_description',
            'currency_code',
            'budget_line_description',
            'amount_in_source_currency',
            'dummy_field_1',
            'dummy_field_2',
            'dummy_field_3',
            'dummy_field_4',
            'dummy_field_5',
        ]
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', filename)
        with open(path) as csv_file:
            bulk = BulkCreateManager()
            next(csv_file)  # Skip header.
            reader = csv.DictReader(csv_file, fieldnames=columns)
            for row in reader:
                if only_grant_codes and row['grant_code'] not in only_grant_codes:
                    continue

                # Add analysis.
                row['analysis'] = analysis

                # Remove extra whitespace.
                for field, value in row.items():
                    if isinstance(value, str):
                        row[field] = value.strip()
                    elif value is None:
                        row[field] = '' # Columns are non-nullable

                # Cast currency to Decimal, take absolute value until incoming transactions are no longer negative.
                row['amount_in_source_currency'] = abs(Decimal(row['amount_in_source_currency']))
                row['amount_in_instance_currency'] = row['amount_in_source_currency']

                bulk.add(Transaction(**row))
            bulk.done()
        analysis.source = Analysis.DATA_STORE_NAME
        analysis.save()

    def _import_cost_line_items_from_file(self, analysis, filename):
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', filename), 'rb') as f:
            load_cost_line_items_from_file(analysis, f)

    def add_account_code_descriptions(self):
        columns = [
            'account_code',
            'account_description',
        ]
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'account_code_descriptions.csv')
        with open(path) as csv_file:
            next(csv_file)  # Skip header.
            reader = csv.DictReader(csv_file, fieldnames=columns)
            for row in reader:
                AccountCodeDescription.objects.create(**row)

    def add_insight_comparison_data(self):
        columns = [
            'name',
            'country',
            'grants',
            'activity',
            'output_count',
            'cost_direct_only',
            'cost_all',
        ]
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'insight_comparison_data.csv')
        with open(path) as csv_file:
            next(csv_file)  # Skip first row
            reader = csv.DictReader(csv_file, fieldnames=columns)
            for row in reader:
                try:
                    row['country'] = Country.objects.get(name=row['country'])

                    try:
                        activity = Activity.objects.get(name=row['activity'])
                        row['activity'] = activity
                    except Exception as e:
                        print(f"Activity Not Found: {row['activity']}")
                        continue

                    output_count = row.pop('output_count')
                    output_metric = activity.output_metric_objects()[0]

                    # Skip insights that have more than one output metric
                    # parameter since the sample data only provides one.
                    if len(output_metric.parameters) > 1:
                        continue

                    parameter_key = list(output_metric.parameters)[0]
                    row['parameters'] = {
                        parameter_key: int(clean_csv_number(output_count)),
                    }

                    row['output_costs'] = {
                        output_metric.id: {
                            'direct_only': float(parse_csv_currency_to_decimal(row.pop('cost_direct_only'))),
                            'all': float(parse_csv_currency_to_decimal(row.pop('cost_all'))),
                        }
                    }
                except Exception as e:
                    print(e)
                    print(row)
                    continue

                InsightComparisonData.objects.create(**row)

    def add_assets(self):
        asset_captions = {
            'help-page-rte-image.png': "Ship of the imagination in a cosmic arena "
                                       "courage of our questions vastness is bearable "
                                       "only through love with pretty stories for "
                                       "which there's little good evidence "
                                       "inconspicuous motes of rock and gas"
        }
        asset_files = Path(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'assets'))
        images = asset_files / 'images'
        ext_types = get_available_image_extensions()
        for i in images.iterdir():
            if i.suffix.lstrip('.') in ext_types:
                img_asset = ImageAsset(title=i.name.replace(i.suffix, ''), caption=asset_captions.get(i.name, ''))
                f = open(str(i), 'rb')
                img_asset.image = File(f, name=i.name)
                img_asset.save()
                f.close()
                self.l(f": Imported {i.name} to image assets")

    def l(self, msg, level='NOTICE'):
        if hasattr(self.style, level):
            msg = getattr(self.style, level)(msg)
        self.stdout.write(msg)


def parse_csv_currency_to_decimal(value):
    if value:
        return Decimal(clean_csv_number(value))
    return value


def clean_csv_number(value):
    if value:
        value = value.replace('$', '')
        value = value.replace(',', '')
    return value

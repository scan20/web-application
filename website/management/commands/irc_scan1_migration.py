import csv
from decimal import Decimal
from itertools import groupby
import logging
import os
import re
from typing import List, Tuple
from django.core.management.base import BaseCommand, CommandError, CommandParser
from django.core.exceptions import ObjectDoesNotExist
import pyexcel as pe

from website.data_loading import COST_LINE_ITEM_IMPORT_HEADERS, _parse_upload_row_to_dict
from website.models import Activity, Analysis, AnalysisSectorCategoryGrant, AnalysisType, Category, CostLineItem, CostLineItemConfig, Country, Region, Sector
from website.models.activity import get_activity_parameter_mapping#, get_activity_output_metric_mapping
from website.workflow import Workflow
from .seed_data.irc import MAPPINGS

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Import IRC data from Dioptra data export'
    source_identifier = "irc_dioptra_migration"
    import_header_mapping = {
        0: "row_num",
        1: "analysis_id",
        2: "category_id",
        3: "weight",
        4: "include_in_value_transfers",
        5: "id",
        "grant_code": "t1",
        "budget_line_code": "t2",
        "account_code": "account_code",
        "site_code": "t6",
        "sector_code": "t3",
        "budget_line_description": "description",
        "total_cost": "total_usd"
    }

    @staticmethod
    def clean_cells(sheet) -> None:
        """Convert any empty cells (which have value '' when parsed by pyexcel) to None. Operation performed in place with no return value.

        :param sheet: Sheet to clean.
        :type sheet: pyexcel.Sheet
        """
        clean = lambda x: None if x == "" else x
        for row in sheet.row_range():
            for column in sheet.column_range():
                sheet.get_internal_array()[row][column] = clean(sheet.get_internal_array()[row][column])

    @staticmethod
    def track_operation(operation, tracked_item_num: int, **kwargs) -> int:
        result = operation(**kwargs)
        return result[tracked_item_num]
    
    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--dry-run',
            action='store_true',
            help='Do not commit the data to the database.'
        )

        parser.add_argument(
            '--limit',
            type=int,
            help='Specify maximum number of analyses to successfully process before quitting.'
        )

        parser.add_argument(
            '--reset',
            action='store_true',
            help='Undo any previous import that has been committed.'
        )

    def handle(self, *args, **options) -> None:
        if options['reset']:
            self.reset()
            return

        if options['limit']:
            self.limit = options["limit"]
        else:
            self.limit = None
        
        self.mappings = get_activity_parameter_mapping()
        self.load_ids_to_keep()
        self.load_book()
        self.parse_activity_types()
        self.parse_analysis_line_meta()
        self.parse_analysis_excluded_categores()
        self.parse_countries()
        self.parse_system_actual()
        self.parse_activity_mapping()
        self.parse_category_mapping()
        self.parse_sector_mapping()
        self.parse_analysis_grants()
        self.parse_analysis_related_grants()
        self.parse_categories()
        self.parse_analyses()
        self.parse_cost_line_items()

        #print("Importing {} analyses of {} parsed".format(len(self.analyses), len(self.ids_to_keep)))
        self.skips.sort(key=lambda x: x["id"])
        if len(self.skips) > 0:
            print("The following analyses could not be imported:")
        for s in self.skips:
            status = self.analysis_records[s["id"]]["status_id"]
            print("- Analysis {} (status {}) because {}".format(s["id"], status, s["reason"]))
        print("")

        invalid = self.not_valid()
        if invalid:
            msg = "No import was performed because of the following error(s):\n"
            msg = "\n".join([f"{i['name']}: {i['outcome']}, actual value = {i['value']}" for i in invalid])
            raise CommandError(msg)
        
        print(f"Preparing import of {len(self.analyses)} analyses...")
        if not options.get('dry_run', False):
            self.successful = {}
            (new_regions, new_countries) = self.commit_import()
            results = self.check_valid(new_regions, new_countries)
            print("")
            for i in results:
                if i["outcome"]:
                    print(f"{i['name']}")
                else:
                    print(f"{i['name']}. Expected value was {i['expected']}")
        else:
            print("No import performed because option --dry-run was passed.")

    def load_ids_to_keep(self) -> None:
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'IRC - Analyses that Contain Real Value_rev3.csv')
        ids = pe.get_sheet(file_name=path, start_row=9)
        self.ids_to_keep = [r[0] for r in ids.get_array() if not r[1].startswith("exclude this analysis")]

    def load_book(self) -> None:
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'build_content', 'IRC - Relational tables from dioptra 1.0_rev4.xlsx')
        self.book = pe.get_book(file_name=path)

    def parse_activity_types(self) -> None:
        activity_type_sheet = self.book.sheet_by_name("analysedtype")
        activity_type_sheet.name_columns_by_row(0)
        self.activity_types = { r["id"]: r["name"] for r in activity_type_sheet.records }

    def parse_analysis_line_meta(self) -> None:
        analysis_line_meta = self.book.sheet_by_name("analysislinemeta")
        analysis_line_meta.name_columns_by_row(0)
        Command.clean_cells(analysis_line_meta)
        self.analysis_line_meta = [{"row_num": ix + 1, **row} for ix, row in enumerate(analysis_line_meta.records) if row["analysis_id"] in self.ids_to_keep]
        header_names = [x["name"] for x in COST_LINE_ITEM_IMPORT_HEADERS]
        self.analysis_line_meta = [
            [row[value] for key, value in self.import_header_mapping.items() if key not in header_names] +
            [row[self.import_header_mapping[val]] for val in header_names if val in self.import_header_mapping]
            for row in self.analysis_line_meta
        ]
        self.analysis_line_meta.sort(key=lambda x: x[1])
        self.analysis_line_meta = {id: [r for r in g] for id, g in groupby(self.analysis_line_meta, lambda x: x[1])}

    def parse_analysis_excluded_categores(self) -> None:
        analysis_excluded_categories = self.book.sheet_by_name("analysisexcludedcategories")
        analysis_excluded_categories.name_columns_by_row(0)
        self.analysis_excluded_categories = [r for r in analysis_excluded_categories.records]

    def parse_countries(self) -> None:
        countries = self.book.sheet_by_name("country")
        countries.name_columns_by_row(0)
        Command.clean_cells(countries)
        self.countries_by_code = { r["code"]: r["name"] for r in countries.records }
        self.countries_by_id = { r["id"]: r["name"] for r in countries.records }

    def parse_system_actual(self) -> None:
        system_actual = self.book.sheet_by_name("systemactual")
        system_actual.name_columns_by_row(0)
        Command.clean_cells(system_actual)
        self.system_actual = [r for r in system_actual.records if r["analysis_id"] in self.ids_to_keep]

    def parse_categories(self) -> None:
        categories = self.book.sheet_by_name("categories")
        categories.name_columns_by_row(0)
        Command.clean_cells(categories)
        self.categories = [r for r in categories.records]

    def parse_activity_mapping(self) -> None:
        activity_mapping = {
            "Latrine-Building Program": "Access to Latrines",
            "Distribution of Family Planning Supplies": "Distribution of Family Planning Supplies",
            "Unconditional Cash Distribution": "Unconditional Cash Transfer",
            "Treatment for Severe Acute Malnutrition": "Treatment for Severe Acute Malnutrition",
            "Supporting GBV Survivors at Women's Centers": "Supporting GBV Survivors at Women's Centers",
            "Legal Assistance Case Management": "Legal Aid Case Management",
            "Case Management for At-Risk Children": "Case Management for At-Risk Children",
            "Teacher Development: Face-to-Face Trainings": "Teacher Development: Face-to-Face Trainings",
            "Generic IRC Program": "Generic Program",
            "Encouraging women to deliver at health institutions": "Encouraging women to deliver at health institutions",
            "Creating Social Accountability Mechanisms": "Creating Social Accountability Mechanisms",
            "Matching Grant": None,
            "Reception & Placement": None,
            "Parental Coaching Programs": "Parental Coaching Programs",
            "Mental Health Care at Clinics": "Mental Health Care at Clinics",
            "Business Skills Training": "Business Skills Training",
            "Providing Business Grants": "Providing Business Grants",
            "Teacher Development: Teacher Inquiry Groups": "Teacher Development: Ongoing Professional Support",
            "Teacher Development: One-on-One Coaching": "Teacher Development: Ongoing Professional Support",
            "Intensive Case Management": None,
            "Immigration Services": None,
            "Employment Programming": None,
            "Food & Farming Programs": None,
            "Youth Programs": None,
            "Education Programs (Youth Focused)": None,
            "Education Programs (non-Youth)": None,
            "Anti-Trafficking Programs": None
        }
        for k, v in activity_mapping.items():
            if v is None:
                continue
            activity_mapping[k] = Activity.objects.get(name=v)

        self.activity_mapping = activity_mapping

    def parse_category_mapping(self) -> None:
        mapping = [
            ['Supplies & Activities (ERD)', 'Materials & Activities', 'ERD'],
            ['Office Rent & Expenses (Health)', 'Office Expenses', 'Health'],
            ['Support Capital Assets', 'Assets & Equipment', 'Direct Shared Cost'],
            ['Travel (Governance)', 'Travel & Transport', 'Governance'],
            ['National Support Staff', 'National Staff', 'Direct Shared Cost'],
            ['Travel (ERD)', 'Travel & Transport', 'ERD'],
            ['M&E (ERD)', 'Materials & Activities', 'ERD'],
            ['Capital Assets (ERD)', 'Assets & Equipment', 'ERD'],
            ['Supplies & Activities (Health)', 'Materials & Activities', 'Health'],
            ['Sub-Grants & Sub-Contracts (Health)', 'Sub-Grants', 'Health'],
            ['National Staff (Health)', 'National Staff', 'Health'],
            ['International Staff (Health)', 'International Staff', 'Health'],
            ['National Staff (Protection)', 'National Staff', 'Protection'],
            ['Supplies & Activities (Education)', 'Materials & Activities', 'Education'],
            ['International Support Staff', 'International Staff', 'Direct Shared Cost'],
            ['Supplies & Activities (Governance)', 'Materials & Activities', 'Governance'],
            ['International Staff (Protection)', 'International Staff', 'Protection'],
            ['Travel (Protection)', 'Travel & Transport', 'Protection'],
            ['Office Rent & Expenses (Protection)', 'Office Expenses', 'Protection'],
            ['M&E (Education)', 'Materials & Activities', 'Education'],
            ['Supplies & Activities (Protection)', 'Materials & Activities', 'Protection'],
            ['National Staff (Governance)', 'National Staff', 'Governance'],
            ['Travel (Education)', 'Travel & Transport', 'Education'],
            ['International Staff (Education)', 'International Staff', 'Education'],
            ['Support Subgrants & Subcontracts', 'Sub-Grants', 'Direct Shared Cost'],
            ['M&E', 'Materials & Activities', 'Direct Shared Cost'],
            ['Office Rent & Expenses (Education)', 'Office Expenses', 'Education'],
            ['Sub-Grants & Sub-Contracts (Protection)', 'Sub-Grants', 'Protection'],
            ['Travel (Health)', 'Travel & Transport', 'Health'],
            ['Capital Assets (Education)', 'Assets & Equipment', 'Education'],
            ['Office Rent & Expenses (Governance)', 'Office Expenses', 'Governance'],
            ['Office Rent & Expenses (ERD)', 'Office Expenses', 'ERD'],
            ['Sub-Grants & Sub-Contracts (ERD)', 'Sub-Grants', 'ERD'],
            ['National Staff (ERD)', 'National Staff', 'ERD'],
            ['Cash Distribution Activities', 'Materials & Activities', 'ERD'],
            ['Capital Assets (Governance)', 'Assets & Equipment', 'Governance'],
            ['International Staff (Governance)', 'International Staff', 'Governance'],
            ['Capital Assets (Health)', 'Assets & Equipment', 'Health'],
            ['Sub-Grants & Sub-Contracts (Governance)', 'Sub-Grants', 'Governance'],
            ['Support Staff Travel', 'Travel & Transport', 'Direct Shared Cost'],
            ['National Staff (Education)', 'National Staff', 'Education'],
            ['M&E (Governance)', 'Materials & Activities', 'Governance'],
            ['Other', 'Office Expenses', 'Direct Shared Cost'],
            ['Support Office Rent & Expenses', 'Office Expenses', 'Direct Shared Cost'],
            ['Sub-Grants & Sub-Contracts (Education)', 'Sub-Grants', 'Education'],
            ['International Staff (ERD)', 'International Staff', 'ERD'],
            ['M&E (Protection)', 'Materials & Activities', 'Protection'],
            ['Capital Assets (Protection)', 'Assets & Equipment', 'Protection'],
            ['M&E (Health)', 'Materials & Activities', 'Health'],
            ['ICR Cost', 'ICR', 'ICR'],
            ['Office Support Costs', 'Office Expenses', 'Direct Shared Cost'],
        ]
        category_mapping = {}
        sector_mapping = {}
        for i in range(len(mapping)):
            if mapping[i][1] is None:
                category_mapping[mapping[i][0]] = None
                sector_mapping[mapping[i][0]] = None
            else:
                category_mapping[mapping[i][0]] = Category.objects.get(name=mapping[i][1])
                sector_name = mapping[i][2]
                sector_mapping[mapping[i][0]] = Sector.objects.get(name=sector_name)
        self.category_mapping = category_mapping
        self.sector_mapping_from_category = sector_mapping

    def parse_sector_mapping(self) -> None:
        mapping = {
            "AGRI": "ERD",
            "CYGE": "Protection",
            "DIST": "ERD",
            "EDAE": "Education",
            "EDBV": "Education",
            "EDCD": "Education",
            "EDGE": "Education",
            "EDJV": "Education",
            "EDPC": "Education",
            "EDPY": "Education",
            "EDRS": "Education",
            "EDSS": "Education",
            "EGNB": "Education",
            "EKID": "Protection",
            "EMSE": "Education",
            "EWAD": "ERD",
            "EWBN": "ERD",
            "EWCD": "ERD",
            "EWEE": "ERD",
            "EWFC": "ERD",
            "EWFL": "ERD",
            "EWFS": "ERD",
            "EWIA": "ERD",
            "EWME": "ERD",
            "EWMG": "ERD",
            "EWUA": "ERD",
            "EWYE": "ERD",
            "GOVR": "Governance",
            "GRFA": "Direct Shared Cost",
            "GSSA": "Direct Shared Cost",
            "GSUA": "Direct Shared Cost",
            "HCHS": "Health",
            "HEAL": "Health",
            "HECM": "Health",
            "HEED": "Health",
            "HEEH": "Health",
            "HEER": "Health",
            "HEHP": "Health",
            "HEHS": "Health",
            "HEMH": "Health",
            "HEMR": "Health",
            "HENU": "Health",
            "HESR": "Health",
            "HGBV": "Protection",
            "HINF": "Health",
            "HMTL": "Health",
            "HNUT": "Health",
            "HPCD": "Health",
            "HQUA": "Health",
            "HREP": "Health",
            "HTRN": "Health",
            "HTUB": "Protection",
            "LGDD": "Governance",
            "MEAL": "Direct Shared Cost",
            "MMUA": "ERD",
            "MYGE": "ERD",
            "OADF": "Direct Shared Cost",
            "OADH": "Direct Shared Cost",
            "OADM": "Direct Shared Cost",
            "ONPS": "Direct Shared Cost",
            "OPCA": "Governance",
            "OPSC": "Direct Shared Cost",
            "PWCD": "Governance",
            "PWCE": "Governance",
            "PWGV": "Governance",
            "PWIM": "Governance",
            "PWLD": "Governance",
            "PWVE": "Governance",
            "SAAS": "Protection",
            "SAAT": "Protection",
            "SACM": "Protection",
            "SACP": "Protection",
            "SAIM": "Protection",
            "SAOP": "Protection",
            "SARL": "Protection",
            "SARP": "Protection",
            "SAUC": "Protection",
            "SAVP": "Protection",
            "SAVS": "Protection",
            "SAWP": "Protection",
            "VPAP": "Protection",
            "VPRO": "Protection",
            "VPTR": "Protection",
            "WATR": "Health",
            "XHXB": "Health",
            "XOED": "Education",
            "XOGR": "Governance",
            "XOHU": "Health",
            "XRAE": "Direct Shared Cost",
            "XRAS": "Direct Shared Cost",
            "XREA": "Direct Shared Cost",
            "XRGL": "Direct Shared Cost",
            "XRSY": "Direct Shared Cost",
            "XRWA": "Direct Shared Cost",
            "ZEXC": "Direct Shared Cost",
            "ZGEN": "Direct Shared Cost",
            "ZRFA": "Direct Shared Cost",
            "ZZZZ": "Direct Shared Cost",
            # provided by client
            "DADO": "Direct Shared Cost",
            "DPOP": "Direct Shared Cost",
            "H011": "Health",
            "SADT": "Governance",
            "SCAP": "Direct Shared Cost",
            "STRD": "Direct Shared Cost",
            "TFOC": "Direct Shared Cost",
            "XHWB": "Direct Shared Cost",
            "XHWP": "Direct Shared Cost",
            "XHWQ": "Direct Shared Cost",
            "XHXG": "Direct Shared Cost",
            "XHXF": "Direct Shared Cost",
        }
        for k, v in mapping.items():
            mapping[k] = Sector.objects.get(name=v)
        self.sector_mapping = mapping

    def parse_analysis_grants(self) -> None:
        analysis_grant_sheet = self.book.sheet_by_name("analysisgrant")
        analysis_grant_sheet.name_columns_by_row(0)
        Command.clean_cells(analysis_grant_sheet)
        self.analysis_grants = [r for r in analysis_grant_sheet.records]

    def parse_analysis_related_grants(self) -> None:
        analysis_related_grant_sheet = self.book.sheet_by_name("analysis_related_grants")
        analysis_related_grant_sheet.name_columns_by_row(0)
        Command.clean_cells(analysis_related_grant_sheet)
        self.analysis_related_grants = [r for r in analysis_related_grant_sheet.records]

    def get_index_for_column(self, col):
        try:
            key = [key for key, value in self.import_header_mapping.items() if value == col][0]
        except IndexError:
            raise CommandError("No header mapping found for {}".format(col))
        skip = len([x for x in self.import_header_mapping if x not in [y["name"] for y in COST_LINE_ITEM_IMPORT_HEADERS]])
        order = [skip + ix for ix, val in enumerate(COST_LINE_ITEM_IMPORT_HEADERS) if val["name"] == key]
        if not order:
            order = [ix for ix, val in enumerate(self.import_header_mapping.keys()) if val == key]
        return order[0]

    def get_grants_for_analysis(self, analysis_id) -> str:
        grants = []
        related_grants = [r for r in self.analysis_related_grants if r["analysis_id"] == analysis_id]
        lines_for_analysis = [rr for r in related_grants for rr in self.analysis_grants if rr["id"] == r["analysisgrant_id"]]
        for r in lines_for_analysis:
            g = r["t1"]
            if g and g not in grants:
                grants.append(g)
        return ",".join(grants)

    def get_dates_for_analysis(self, analysis_id) -> Tuple:
        related_ids = [r for r in self.analysis_related_grants if r["analysis_id"] == analysis_id]
        analysis_grant_id = related_ids[0]["analysisgrant_id"]
        lines_for_analysis = [r for r in self.analysis_grants if r["id"] == analysis_grant_id]
        start_date = None
        end_date = None
        start_dates = []
        end_dates = []
        for r in lines_for_analysis:
            if r["start_date"]:
                start_dates.append(r["start_date"])
            if r["end_date"]:
                end_dates.append(r["end_date"])
        if len(start_dates) > 0:
            start_dates = list(set(start_dates))
            if len(start_dates) > 1:
                raise CommandError(f"Multiple start found")
            start_date = start_dates[0]
        if len(end_dates) > 0:
            end_dates = list(set(end_dates))
            if len(end_dates) > 1:
                raise CommandError(f"Multiple end dates found")
            end_date = end_dates[0]
        return start_date, end_date

    def add_new_regions(self, revert=False) -> int:
        total = 0
        if not revert:
            total += Command.track_operation(Region.objects.get_or_create, 1, name='North America', region_code='NA')
            total += Command.track_operation(Region.objects.get_or_create, 1, name='Europe', region_code='EU')
            return total
        elif revert:
            total += Command.track_operation(Region.objects.get(name='North America').delete, 0)
            total += Command.track_operation(Region.objects.get(name='Europe').delete, 0)
            return total
    
    def get_region(self, name) -> Region:
        try:
            return Region.objects.get(name=name)
        except Region.DoesNotExist:
            return Region.objects.get(name=name + " Region")

    def add_new_countries(self, revert=False) -> int:
        total = 0
        if not revert:
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Thailand', code='3THA', defaults={"region": self.get_region(name='Asia')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='New York', code='RIBUR', defaults={"region": self.get_region(name='North America')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Nigeria', code='2NGA', defaults={"region": self.get_region(name='West Africa')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Sierra Leone', code='2SLE', defaults={"region": self.get_region(name='West Africa')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name="Cote D'Ivoire", code='2CIV', defaults={"region": self.get_region(name='West Africa')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Syria', code='5SYR', defaults={"region": self.get_region(name='Middle East')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Greece', code='1GRG', defaults={"region": self.get_region(name='Europe')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Turkey', code='05TR', defaults={"region": self.get_region(name='Middle East')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Zimbabwe', code='2ZWE', defaults={"region": self.get_region(name='East Africa')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='Uganda', code='2UGA', defaults={"region": self.get_region(name='East Africa')})
            total += Command.track_operation(Country.objects.get_or_create, 1, name='South Sudan', code='2SSD', defaults={"region": self.get_region(name='East Africa')})
        elif revert:
            total += Command.track_operation(Country.objects.get(name='Thailand', region__name="Asia").delete, 0)
            total += Command.track_operation(Country.objects.get(name='New York', region__name="North America").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Nigeria', region__name="West Africa").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Sierra Leone', region__name="West Africa").delete, 0)
            total += Command.track_operation(Country.objects.get(name="Cote D'Ivoire", region__name="West Africa").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Syria', region__name="Middle East").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Greece', region__name="Europe").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Turkey', region__name="Middle East").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Zimbabwe', region__name="East Africa").delete, 0)
            total += Command.track_operation(Country.objects.get(name='Uganda', region__name="East Africa").delete, 0)
            total += Command.track_operation(Country.objects.get(name='South Sudan', region__name="East Africa").delete, 0)
        return total

    def get_country_for_analysis(self, analysis_id) -> str:
        country = None
        related_grants = [r for r in self.analysis_related_grants if r["analysis_id"] == analysis_id]
        lines_for_analysis = [rr for r in related_grants for rr in self.analysis_grants if rr["id"] == r["analysisgrant_id"]]
        for r in lines_for_analysis:
             value = (self.countries_by_id).get(r["t5_id"])
             if value is None:
                 continue
             if country is not None and value != country:
                 raise CommandError("Multiple countries used for this analysis: {}".format(analysis_id))
             country = value
        if country is None:
            return country
        try:
            country = Country.objects.get(name=country)
        except ObjectDoesNotExist:
            country = Country(name=country)
        return country

    def parse_analyses(self) -> None:
        analysis_sheet = self.book.sheet_by_name("analysis")
        analysis_sheet.name_columns_by_row(0)
        Command.clean_cells(analysis_sheet)
        records = [r for r in analysis_sheet.records if r["id"] in self.ids_to_keep]
        self.analysis_records = {r["id"]: r for r in records}
        analysis_type_ex_ante = AnalysisType.objects.get(title='Ex-ante analysis')
        analysis_type_ex_post = AnalysisType.objects.get(title='Ex-post analysis')

        analysis_type_mapping = {
            1: analysis_type_ex_ante,
            2: analysis_type_ex_post,
            3: analysis_type_ex_post,
            4: analysis_type_ex_ante
        }

        self.skips = []
        self.analyses = {}
        for r in records:
            id = r["id"]
            skip = False

            analysis_type_id = r["analysis_version_type_id"]
            if analysis_type_id is None:
                analysis_type_id = 1

            grants = self.get_grants_for_analysis(id)
            if len(grants) == 0:
                skip = True
                self.skips.append({"id": id, "reason": "no grants found"})

            start_date = r["analysis_start_date"]
            end_date = r["analysis_end_date"]
            if start_date is None or end_date is None:
                grant_start_date, grant_end_date = self.get_dates_for_analysis(id)
                if start_date is None:
                    start_date = grant_start_date
                if end_date is None:
                    end_date = grant_end_date

            if start_date is None or end_date is None:
                created_date = r["created_at"]
                if created_date is not None:
                    if start_date is None:
                        start_date = created_date
                    if end_date is None:
                        end_date = created_date
                
            if start_date is None:
                skip = True
                self.skips.append({"id": id, "reason": "start_date field is empty"})

            if end_date is None:
                skip = True
                self.skips.append({"id": id, "reason": "end_date field is empty"})

            country = self.get_country_for_analysis(id)
            if country is None:
                skip = True
                self.skips.append({"id": id, "reason": "country is empty"})
            
            activity = self.activity_mapping[self.activity_types[r["type_id"]]]
            mapping = self.mappings[activity.pk]
            # only have one value per analysis, so just fill in first parameter
            parameter_name = mapping[0]
            parameters = {
                parameter_name: r["numeric_value"]
            }
            if r["type_id"] == 2:
                parameters[mapping[1]] = 1

            if skip:
                continue

            values = {
                'source': self.source_identifier,
                'analysis_type': analysis_type_mapping[analysis_type_id],
                'activity': activity,
                'title': r["title (revised)"],
                'description': r["description"],
                'start_date':  start_date,
                'end_date': end_date,
                'country': country,
                'grants': grants,
                'parameters': parameters
            }

            analysis = Analysis(**values)
            self.analyses[id] = analysis
    
    def parse_cost_line_items(self) -> None:
        self.clis = {}
        self.cli_configs = {}

        analyses_to_remove: List[int] = []
        categories_missing: List[str] = []
        sectors_missing: List[str] = []
        self.cli_category_id_mapping = {}
        self.exclude_mapping_conflicts = {}
        for id, analysis in self.analyses.items():
            if self.limit is not None and len(self.clis) >= self.limit:
                analyses_to_remove.append(id)
                continue

            country_code = analysis.country.code
            lines_for_analysis = self.analysis_line_meta[id]
            error_list = []
            clis = []
            cli_configs = []
            for row in lines_for_analysis:
                d, errors = _parse_upload_row_to_dict(row[self.get_index_for_column("row_num")], row[self.get_index_for_column("t1"):])
                d.update({
                    "analysis": analysis,
                    "country_code": country_code
                })
                cli = CostLineItem(**d)

                category = None
                sector = None
                if row[self.get_index_for_column("category_id")] is not None:
                    categories = [c for c in self.categories if c["id"] == row[self.get_index_for_column("category_id")]]
                    if len(categories) != 1:
                        raise CommandError("{} categories for id {}".format(len(categories), row[self.get_index_for_column("category_id")]))

                    category_name = categories[0]["name"]
                    category = self.category_mapping.get(category_name)
                    if category is None:
                        errors.append("no mapping found for category {}".format(category_name))
                        if category_name not in categories_missing:
                            categories_missing.append(category_name)
                    else:
                        sector = self.sector_mapping_from_category.get(category_name)

                analysis_record = self.analysis_records[id]
                if sector is None:
                    if row[self.get_index_for_column("t3")] is not None:
                        sector_name = row[self.get_index_for_column("t3")]
                        sector = self.sector_mapping.get(sector_name)
                        if sector is None:
                            try:
                                sector = Sector.objects.get(name=row[self.get_index_for_column("t3")])
                            except ObjectDoesNotExist:
                                if analysis_record["status_id"] > 3:
                                    errors.append("no mapping found for sector {}".format(sector_name))
                                    if sector_name not in sectors_missing:
                                        sectors_missing.append(sector_name)
                    elif analysis_record["status_id"] > 3:
                        errors.append("sector is empty")

                if category is None and sector is not None:
                    for m in MAPPINGS:
                        if m[0] == str(cli.account_code) and m[3] == sector.name:
                            category_name = m[2]
                            category = Category.objects.get(name=category_name)
                            break
                if category is None and cli.account_code is not None and re.match(r'^[A-Z]', str(cli.account_code)):
                    category_name = cli.account_code
                    try:
                        category = Category.objects.get(name=category_name)
                    except ObjectDoesNotExist:
                        print("No category for account code {}".format(category_name))
                if category is None:
                    sector_name = sector.name if sector is not None else None
                    sector_code = row[self.get_index_for_column("t3")]
                    ac = str(cli.account_code)
                    category_name = None
                    for m in MAPPINGS:
                        if m[2] is None:
                            continue
                        if m[0] == ac and (m[1] == sector_name or m[3] == sector_code):
                            category_name = m[2]
                            break
                        elif m[0] == ac and ac is not None and m[1] is None and m[3] is None:
                            category_name = m[2]
                            break
                        elif m[3] == sector_name and sector_name is not None and m[0] is None:
                            category_name = m[2]
                            break
                        elif m[1] == sector_code and sector_code is not None and m[0] is None:
                            category_name = m[2]
                            break
                    if category_name is None:
                        print("Category empty: {}, {}".format(sector_name, ac))
                    else:
                        category = Category.objects.get(name=category_name)

                if category is not None:
                    key = (id, category.pk, sector.pk if sector is not None else None, cli.grant_code)
                    val = row[self.get_index_for_column("category_id")]
                    if val is not None:
                        if key in self.cli_category_id_mapping and val != self.cli_category_id_mapping[key]:
                            if key in self.exclude_mapping_conflicts:
                                self.exclude_mapping_conflicts[key].append(val)
                            else:
                                self.exclude_mapping_conflicts[key] = [self.cli_category_id_mapping[key], val]
                        else:
                            self.cli_category_id_mapping[key] = val

                if category is None and analysis_record["status_id"] > 3:
                    errors.append("category is empty")

                weight = row[self.get_index_for_column("weight")]
                if weight is not None:
                    weight = round(Decimal(str(weight)), 2)

                output_value = row[self.get_index_for_column("include_in_value_transfers")]
                if output_value == 1:
                    output_value = True
                else:
                    output_value = False
                
                cli_config = CostLineItemConfig(
                    cost_line_item=cli,
                    sector=sector,
                    category=category,
                    allocation=weight,
                    output_value=output_value,
                )

                if errors:
                    error_list.append(errors)
                    continue
                clis.append(cli)
                cli_configs.append(cli_config)

            error_mapping = {
                "column Budget line code: This field cannot be null.": "Budget line code (t2) is empty"
            }
            if error_list:
                error_types = [e.strip() for e in set([e.split(",")[1] if "," in e else e for ea in error_list for e in ea])]
                for i in range(len(error_types)):
                    if error_types[i] in error_mapping:
                        error_types[i] = error_mapping[error_types[i]]
                error_types = ["\t" + e for e in error_types]
                self.skips.append({"id": id, "reason": "the following errors were found with its CostLineItems: \n" + "\n".join(error_types)})
                analyses_to_remove.append(id)
            else:
                self.clis[id] = clis
                self.cli_configs[id] = cli_configs

        for a in analyses_to_remove:
            del self.analyses[a]
    
    def not_valid(self) -> List[str]:
        tests = [
            {
                "name": "CostLineItem count = CostLineItemConfig count",
                "value": "{} vs. {}".format(len(self.clis), len(self.cli_configs)),
                "outcome": len(self.clis) == len(self.cli_configs)
            },
        ]
        return [i for i in tests if not i["outcome"]]

    def check_valid(self, new_regions, new_countries) -> List[str]:
        new_analyses = self.successful.values()
        new_count = len(new_analyses)
        if new_count < len(self.successful):
            raise CommandError("Not all successful imports were found in database. Must be an error somewhere.")

        tests = [
            {
                "name": "Created {} new analyses".format(new_count),
                "expected": 280,
                "outcome": new_count == 280
            },
            {
                "name": "Created {} new region(s)".format(new_regions),
                "expected": 2,
                "outcome": new_regions == 2
            },
            {
                "name": "Created {} new countries".format(new_countries),
                "expected": 11,
                "outcome": new_countries == 11
            }
        ]
        return tests

    def commit_import(self) -> Tuple[int, int]:
        new_regions = self.add_new_regions()
        new_countries = self.add_new_countries()
        skips = []
        created_asc = 0
        for id, a in self.analyses.items():
            if a.country.pk is None:
                try:
                    a.country = Country.objects.get(name=a.country.name)
                except ObjectDoesNotExist as e:
                    logger.warn("Skipping analysis {} because country {} does not exist in database".format(id, a.country.name))
                    skips.append(id)
                    continue
            a.save()

            try:
                for cli in self.clis[id]:
                    # have to reassign to pass validation
                    cli.analysis = cli.analysis
                    cli.full_clean()
                    cli.save()
                for cli_config in self.cli_configs[id]:
                    # have to reassign to pass validation
                    cli_config.cost_line_item = cli_config.cost_line_item
                    cli_config.full_clean()
                    cli_config.save()

                created_ids = a.ensure_sector_category_objects()
                created_asc += len(created_ids)
                a.sector_categories.all().update(confirmed=True)

                for ascg in AnalysisSectorCategoryGrant.objects.filter(sector_category__analysis=a):
                    contributes_to_activity = True
                    key = (id, ascg.sector_category.category.id, ascg.sector_category.sector.id, ascg.grant)
                    zero_rows = None
                    if key in self.exclude_mapping_conflicts:
                        for category_id in set(self.exclude_mapping_conflicts[key]):
                            rows = [r for r in self.analysis_excluded_categories if r["analysis_id"] == id and r["category_id"] == category_id]
                            if zero_rows is None:
                                zero_rows = len(rows) == 0
                            if (len(rows) == 0) != zero_rows:
                                logger.warning("Unable to correctly map contribution exclusions for analysis {}: (Category, Sector, Grant) ({}, {}, {}) has values {}"
                                    .format(id, ascg.sector_category.category.name, ascg.sector_category.sector.name, ascg.grant, set(self.exclude_mapping_conflicts[key])))
                                zero_rows = True
                                break
                    elif key in self.cli_category_id_mapping:
                        category_id = self.cli_category_id_mapping[key]
                        rows = [r for r in self.analysis_excluded_categories if r["analysis_id"] == id and r["category_id"] == category_id]
                        zero_rows = len(rows) == 0

                    if zero_rows is False:
                        contributes_to_activity = False

                    ascg.contributes_to_activity = contributes_to_activity
                    ascg.save()
    
                a.calculate_output_costs()
                Workflow(a, "identify-output-value").active_step.update_parameter_values()

                self.successful[id] = a
            except Exception as e:
                logger.error("Error importing analysis {}. Rolling back import...".format(id))
                Analysis.objects.filter(source=self.source_identifier).delete()
                # don't delete because this can have unintended effects in production. we don't know what countries/regions already existed.
                # self.add_new_countries(revert=True)
                # self.add_new_regions(revert=True)
                raise e

        csv_lines = [
            [id, a.pk, a.title, Workflow(a, None).get_last_incomplete_or_last().name, a.get_first_cost_per_output_all()]
            for id, a in self.successful.items()
        ]

        try:
            os.mkdir("./temp")
        except FileExistsError:
            pass

        with open('./temp/migration-results.txt', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["old_id","new_id","title","step","cost_per_output"])
            writer.writerows(csv_lines)

        for id in skips:
            del self.analyses[id]
        
        return (new_regions, new_countries)
    
    def reset(self) -> None:
        # delete analyses
        analyses_to_delete = Analysis.objects.filter(source=self.source_identifier)
        ct = len(analyses_to_delete)
        analyses_to_delete.delete()
        print(f"{ct} analyses deleted")

        # delete any new countries
        ct = self.add_new_countries(revert=True)
        print(f"{ct} countries deleted")

        # delete any new regions
        ct = self.add_new_regions(revert=True)
        print(f"{ct} regions deleted")
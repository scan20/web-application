from django.core.management.base import BaseCommand
from website.models.cost_line_item import CostLineItemConfig
from website.models import Sector


class Command(BaseCommand):

    def handle(self, *args, **options):
        sector_ids = Sector.objects.all().values_list('id', flat=True)
        line_items = CostLineItemConfig.objects.all()

        for line_item in line_items:
            if line_item.sector_id not in sector_ids:
                # set line item sector id to null
                print(f'Sector id {line_item.sector_id} not found. Setting sector on line item {line_item.id} to null.')
                line_item.sector_id = None
                line_item.save()

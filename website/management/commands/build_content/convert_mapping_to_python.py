"""
A standalone script to simplify turning the user-editable spreadsheets into python objects for the build script.

Currently they live on Google Sheets and need to be downloaded as an xlsx sheet for this sheet to run.

This is not an entirely automated process.   Sectors in particular need some field updates.  Please refer to the existing data for examples.


"""
from pprint import pprint

import openpyxl
from openpyxl.worksheet import worksheet

wb = openpyxl.load_workbook(filename='Dioptra _ Categories, sectors and mappings for Save the Children MVP instance.xlsx', data_only=True)

category_sheet = wb['Categories']
sector_sheet = wb['Sectors']
mapping_sheet = wb['Category-Sector Mappings']

def categories_as_dicts(sheet: worksheet):
    categories = []
    # skip past the headers rows
    for row in sheet.iter_rows(min_row=3):
        name = row[0].value.strip().replace("\n", " ")
        description = row[1].value.strip().replace("\n", " ")
        categories.append({"name": name, "description": description})
    return categories


def sectors_as_dicts(sheet: worksheet):
    sectors = []
    # skip past the headers rows
    for row in sheet.iter_rows(min_row=3):
        name = row[0].value.strip().replace("\n", " ")
        type_ = row[1].value.strip().replace("\n", " ")
        sectors.append({"name": name, "type": type_, "direct_program_cost": True})
    return sectors

def mappings_as_list(sheet: worksheet):
    mappings = []
    # skip past the headers rows
    for row in sheet.iter_rows(min_row=4):
        account_code = str(int(row[3].value) if row[3].value else row[3].value).strip().replace("\n", " ")
        sector_code = str(row[5].value).strip().replace("\n", " ")
        category_name = str(row[8].value).strip().replace("\n", " ")
        sector_name = str(row[9].value).strip().replace("\n", " ")
        if account_code == 'None':
            account_code = None
        if sector_code == 'None':
            sector_code = None
        if category_name == 'None':
            category_name = None
        if sector_name == 'None':
            sector_name = None



        mappings.append([account_code, sector_code, category_name, sector_name])
    return mappings


category_output = categories_as_dicts(category_sheet)
sector_output = sectors_as_dicts(sector_sheet)
mapping_output = mappings_as_list(mapping_sheet)

pprint(category_output)
print()
print()
pprint(sector_output)
print()
print()
pprint(mapping_output)

print()
print()
print(f"Categories: {len(category_output)}")
print(f"Sectors: {len(sector_output)}")
print(f"Mappings: {len(mapping_output)}")
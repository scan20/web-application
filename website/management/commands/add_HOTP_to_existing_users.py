from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from website.email_2fa.models import HOTPEmailDevice

User = get_user_model()


class Command(BaseCommand):
    help = 'Add HOTP based OTPs for existing users.  This is a one-time script to avoid a rebuild for the end users.  Unlikely to be useful again.'

    def handle(self, *args, **options):
        for each_user in User.objects.all():
            HOTPEmailDevice.objects.create(user=each_user, confirmed=True)

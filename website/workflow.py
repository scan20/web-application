"""
Manages the flow and logic of individual steps in the analysis workflow.
"""
from collections import namedtuple
from decimal import Decimal

from django.db.models import Sum, F, Value
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _l

from website import betterdb, stopwatch
from website.data_loading import load_cost_line_items_from_file
from website.data_loading import load_transactions_from_data_store
from website.forms.fields import CalculatedFixedDecimalField
from website.models import Transaction
from website.models.activity import ValueOfBusinessGrantAmount, ValueOfCashTransferred, OUTPUT_METRICS_BY_ID
from website.models.core import Sector
from website.models.cost_line_item import CostLineItemConfig, CostLineItem
from website.utils import list_dedupe


class Step:
    name = None
    nav_title = None
    is_enabled = True

    def __init__(self, workflow):
        self.workflow = workflow
        self.analysis = workflow.analysis

    def clear_cached(self, property_name):
        if property_name in self.__dict__:
            del self.__dict__[property_name]

    @cached_property
    def is_complete(self):
        return False

    @cached_property
    def dependencies_met(self):
        return False

    def get_nav_title(self):
        return self.nav_title

    def get_href(self):
        return None

    def get_prev(self):
        return self.workflow.get_prev(self)

    def get_next(self):
        return self.workflow.get_next(self)

    def invalidate(self):
        pass


class MultiStep(Step):

    def get_last_incomplete_or_last(self):
        last_incomplete = self.get_last_incomplete()
        if last_incomplete:
            return last_incomplete
        try:
            return self.steps[-1]
        except IndexError:
            return None

    def get_last_incomplete(self):
        for step in self.steps:
            if not step.is_complete:
                return step
        return None

    @property
    def is_complete(self):
        if not self.dependencies_met:
            return False
        if self.analysis and getattr(self.analysis, 'pk', None):
            return all([substep.is_complete for substep in self.steps])
        return False


class SubStep(Step):

    def get_prev(self):
        try:
            # Get prev sub-step.
            step_index = self.parent.steps.index(self)
            if step_index - 1 < 0:
                return self.parent.get_prev()
            return self.parent.steps[step_index - 1]
        except (IndexError, ValueError):
            return self.parent.get_prev()

    def get_next(self):
        try:
            # Get prev sub-step.
            step_index = self.parent.steps.index(self)
            return self.parent.steps[step_index + 1]
        except (IndexError, ValueError):
            return self.parent.get_next()


###############################################################################


class Define(Step):
    """
    Both the Create and Update, Analysis isn't saved yet if it's create.
    """
    name = 'define'
    nav_title = _l('Define Analysis')

    @cached_property
    def is_complete(self):
        analysis_exists = getattr(self.analysis, 'pk', None) is not None
        if analysis_exists:
            if self.analysis.has_parameters():
                return True
        return False

    @cached_property
    def dependencies_met(self):
        return True

    def get_href(self):
        if getattr(self.analysis, 'pk', False):
            return reverse('analysis-define-update', kwargs={'pk': self.analysis.pk})
        else:
            return reverse('analysis-define-create')


class LoadData(Step):
    name = 'load-data'
    nav_title = _l('Load Data')

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('define').is_complete

    @cached_property
    def is_complete(self):
        if getattr(self.analysis, 'needs_transaction_resync', None):
            return False
        if self.analysis and getattr(self.analysis, 'pk', None):
            return self.analysis.cost_line_items.count() > 0
        return False

    def get_href(self):
        return reverse('analysis-load-data', kwargs={'pk': self.analysis.pk})

    @betterdb.transaction()
    def load_transactions_from_data_store(self, filter_by_country=False):
        succeeded, result = load_transactions_from_data_store(self.analysis, filter_by_country)
        if succeeded:
            self.analysis.create_cost_line_items_from_transactions(result['imported_transactions'])
            self.analysis.auto_categorize_cost_line_items()
            self.analysis.ensure_sector_category_objects()
        return succeeded, result

    @betterdb.transaction()
    def resync_transactions_from_data_store(self, filter_by_country=False):
        betterdb.delete(self.analysis.transactions.all())
        succeeded, result = load_transactions_from_data_store(self.analysis, filter_by_country)
        if succeeded:
            self.analysis.sync_cost_line_items(result['imported_transactions'])
            self.analysis.auto_categorize_cost_line_items()
            self.analysis.ensure_sector_category_objects()
        return succeeded, result

    @betterdb.transaction()
    def load_cost_line_items_from_file(self, f):
        succeeded, result = load_cost_line_items_from_file(self.analysis, f)
        if succeeded:
            self.analysis.auto_categorize_cost_line_items()
            self.analysis.ensure_sector_category_objects()
        return succeeded, result

    @stopwatch.trace()
    @betterdb.transaction()
    def invalidate(self):
        self.workflow.invalidate_step('insights')
        # Handle these 'cascades' manually, we don't want Django pulling this into Python.
        # Ideally the database would handle it though!
        Transaction.objects.filter(cloned_from__analysis=self.analysis).update(cloned_from=None)
        betterdb.delete(self.analysis.transactions.all())

        self.analysis.sector_categories.all().delete()

        CostLineItemConfig.objects.filter(cloned_from__cost_line_item__analysis=self.analysis).update(cloned_from=None)
        betterdb.delete(CostLineItemConfig.objects.filter(cost_line_item__analysis=self.analysis))

        self.analysis.cost_line_items.all().delete()
        self.analysis.source = None
        self.analysis.save()


class FixMissingData(Step):
    """
    Both the Create and Update, Analysis isn't saved yet if it's create.
    """
    name = 'fix-missing-data'
    nav_title = _l('Assign Sector & Category')

    @cached_property
    def is_complete(self):
        if not self.dependencies_met:
            return False
        analysis_exists = getattr(self.analysis, 'pk', None) is not None
        if analysis_exists:
            if self.analysis.has_uncategorized_cost_line_items():
                return False
            else:
                return True
        return False

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('load-data').is_complete

    def get_href(self):
        return reverse('analysis-fix-missing-data', kwargs={'pk': self.analysis.pk})


class Categorize(MultiStep):
    name = 'categorize'
    nav_title = _l('Confirm Categories')

    def __init__(self, workflow):
        super().__init__(workflow)
        self.steps = []
        if self.analysis and getattr(self.analysis, 'pk', None):
            for sector in self.analysis.get_sectors_used():
                self.steps.append(CategorizeSector(self, self.workflow, sector))

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('fix-missing-data').is_complete

    def get_href(self):
        return reverse('analysis-categorize', kwargs={'pk': self.analysis.pk})

    def get_step_by_sector(self, sector):
        for step in self.steps:
            if step.sector == sector:
                return step


class CategorizeSector(SubStep):
    name = 'categorize-sector'

    def __init__(self, parent, workflow, sector):
        super().__init__(workflow)
        self.parent = parent
        self.sector = sector

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('fix-missing-data').is_complete

    @cached_property
    def is_complete(self):

        if not self.dependencies_met:
            return False
        if self.analysis and getattr(self.analysis, 'pk', None):
            confirmed_sector_categories = self.analysis.sector_categories.filter(sector=self.sector).values_list('confirmed', flat=True)
            return len(confirmed_sector_categories) and all(confirmed_sector_categories)
        return False

    def get_nav_title(self):
        return self.sector.name

    def get_href(self):
        return reverse('analysis-categorize-sector', kwargs={'pk': self.analysis.pk, 'sector_pk': self.sector.pk})


OutputValueParam = namedtuple("OutputValueParam", "output_metric param_name")


class IdentifyOutputValue(MultiStep):
    name = 'identify-output-value'
    nav_title = _l('Identify Output Value')
    output_metrics_params = (
        OutputValueParam(OUTPUT_METRICS_BY_ID.get('ValueOfBusinessGrantAmount'), 'value_of_business_grant_amount'),
        OutputValueParam(OUTPUT_METRICS_BY_ID.get('ValueOfCashTransferred'), 'value_of_cash_transferred'),
    )

    def __init__(self, workflow):
        super().__init__(workflow)
        self.steps = []
        if self.analysis and getattr(self.analysis, 'pk', None):
            for sector in self.analysis.get_sectors_used():
                if not sector.type_obj().shared:
                    self.steps.append(AssignOutputValue(self, self.workflow, sector))

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('allocate').is_complete

    def get_href(self):
        return reverse('analysis-identify-output-value', kwargs={'pk': self.analysis.pk})

    @cached_property
    def is_complete(self):
        if not self.dependencies_met:
            return False
        if self.analysis and getattr(self.analysis, 'pk', None):
            line_items = CostLineItem.objects.filter(config__output_value=True, analysis_id=self.analysis.id)
            if line_items.count() > 0 and self.get_total() > 0:
                return True
        return False

    @cached_property
    def is_enabled(self):
        return getattr(self.analysis, 'pk', None) is not None and any(
            map(lambda x: x in self.output_metric_dict.keys(), self.analysis.activity.output_metrics)
        )

    @property
    def output_metric_dict(self):
        return {om.output_metric.id: om for om in self.output_metrics_params}

    def get_total(self):
        result = CostLineItem.objects.filter(
            analysis_id=self.analysis.id, config__output_value=True
        ).annotate(
                allocated_cost=F('total_cost') * (F('config__allocation') / Value(100))
        ).aggregate(total=Sum('allocated_cost'))
        return result.get('total')

    def update_parameter_values(self):
        output_metrics_ids = self.analysis.activity.output_metrics
        for output_metric_id in output_metrics_ids:
            for output_value in self.output_metrics_params:
                if output_metric_id == output_value.output_metric.id:
                    value = self.get_total()
                    if isinstance(value, Decimal):
                        value = float(value)
                    self.analysis.parameters[output_value.param_name] = value
        self.analysis.save()
        self.workflow.invalidate_step('insights')
        self.workflow.calculate_if_possible()

    def invalidate(self):
        self.clear_cached('dependencies_met')
        self.clear_cached('is_complete')
        CostLineItemConfig.objects.filter(cost_line_item__analysis=self.analysis).update(output_value=False)
        self.workflow.invalidate_step('insights')


class AssignOutputValue(SubStep):
    name = 'assign-output-value'

    def __init__(self, parent, workflow, sector):
        super().__init__(workflow)
        self.parent = parent
        self.sector = sector

    @cached_property
    def is_complete(self):
        return self.parent.is_complete

    def get_nav_title(self):
        return self.sector.name

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('categorize').is_complete

    def get_href(self):
        return reverse('analysis-assign-output-value', kwargs={'pk': self.analysis.pk, 'sector_pk': self.sector.pk})


class SetContribution(MultiStep):
    name = 'set-contribution'
    nav_title = _l('Set Contribution')

    def __init__(self, workflow):
        super().__init__(workflow)
        self.steps = []
        if self.analysis and getattr(self.analysis, 'pk', None):
            sector_ids_and_grants = list_dedupe(
                self.analysis.sector_category_grants.filter(
                    sector_category__sector__direct_program_cost=True,
                ).values_list('sector_category__sector_id', 'grant')
            )
            for sector_id, grant in sector_ids_and_grants:
                sector = Sector.objects.get(pk=sector_id)
                self.steps.append(SetContributionSectorGrant(self, self.workflow, sector, grant))

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('categorize').is_complete

    def get_href(self):
        return reverse('analysis-set-contribution', kwargs={'pk': self.analysis.pk})


class SetContributionSectorGrant(SubStep):
    name = 'set-contribution-sector-grant'

    def __init__(self, parent, workflow, sector, grant):
        super().__init__(workflow)
        self.parent = parent
        self.sector = sector
        self.grant = grant

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('categorize').is_complete

    @cached_property
    def is_complete(self):
        if self.analysis and getattr(self.analysis, 'pk', None):
            incomplete_count = self.analysis.sector_category_grants.filter(
                    sector_category__sector=self.sector,
                    grant=self.grant,
                    contributes_to_activity=None,
            ).count()
            return incomplete_count == 0
        return False

    def get_nav_title(self):
        if len(self.analysis.grants_list()) > 1:
            return f'{self.sector.name}: {self.grant}'
        return self.sector.name

    def get_href(self):
        return reverse('analysis-set-contribution-sector-grant', kwargs={
            'pk': self.analysis.pk,
            'sector_pk': self.sector.pk,
            'grant': self.grant,
        })


class Allocate(MultiStep):
    name = 'allocate'
    nav_title = _l('Allocate Costs')

    def __init__(self, workflow):
        super().__init__(workflow)
        self.steps = []
        if self.analysis and getattr(self.analysis, 'pk', None):
            sector_ids_and_grants = list_dedupe(
                self.analysis.sector_category_grants.filter(
                    contributes_to_activity=True
                ).values_list('sector_category__sector_id', 'grant')
            )
            for sector_id, grant in sector_ids_and_grants:
                sector = Sector.objects.get(pk=sector_id)
                self.steps.append(AllocateSectorGrant(self, self.workflow, sector, grant))

    @cached_property
    def dependencies_met(self):
        return self.workflow.get_step('set-contribution').is_complete

    def get_href(self):
        return reverse('analysis-allocate', kwargs={'pk': self.analysis.pk})

    def sectors_of_type_complete(self, sector_type):
        return all([
            step.is_complete
            for step in self.steps if step.sector.type == sector_type.id
        ])

    def sector_types_complete_through(self, sector_type):
        if not sector_type:
            return True
        for st in Sector.TYPES:
            if st == sector_type:
                return self.sectors_of_type_complete(st)
            if not self.sectors_of_type_complete(st):
                return False
        return False

    def invalidate(self):
        self.workflow.invalidate_step('insights')
        CostLineItemConfig.objects.filter(
                id__in=self.analysis.cost_line_items.values_list('id', flat=True)
        ).update(allocation=None)


class AllocateSectorGrant(SubStep):
    name = 'allocate-sector-grant'

    def __init__(self, parent, workflow, sector, grant):
        super().__init__(workflow)
        self.parent = parent
        self.sector = sector
        self.grant = grant

    @cached_property
    def dependencies_met(self):
        if not self.workflow.get_step('set-contribution').is_complete:
            return False

        # Require sectors of each type to be completed in order.
        previous_type = self.sector.get_previous_type()
        if previous_type and not self.parent.sector_types_complete_through(previous_type):
            return False

        return True

    @cached_property
    def is_complete(self):
        unallocated_count = self.analysis.contributing_cost_line_items.filter(
                grant_code=self.grant,
                config__sector_id=self.sector.id,
                config__allocation__isnull=True,
        ).count()
        return unallocated_count == 0

    def get_nav_title(self):
        if len(self.analysis.grants_list()) > 1:
            return f'{self.sector.name}: {self.grant}'
        return self.sector.name

    def get_href(self):
        return reverse('analysis-allocate-sector-grant', kwargs={
            'pk': self.analysis.pk,
            'sector_pk': self.sector.pk,
            'grant': self.grant,
        })


class Insights(Step):
    name = 'insights'
    nav_title = _l('View Insights')

    @cached_property
    def dependencies_met(self):
        return (self.workflow.get_step('identify-output-value').is_complete
                if self.workflow.get_step('identify-output-value').is_enabled
                else self.workflow.get_step('allocate').is_complete)

    @cached_property
    def is_complete(self):
        return self.dependencies_met

    def get_href(self):
        return reverse('analysis-insights', kwargs={'pk': self.analysis.pk})

    def invalidate(self):
        self.clear_cached('dependencies_met')
        self.clear_cached('is_complete')
        self.analysis.output_costs = {}

        self.analysis.save()

    def calculate_if_possible(self):
        if self.dependencies_met:
            self.analysis.calculate_output_costs()

    def calculations_done(self):
        output_costs_keys = self.analysis.output_costs.keys()
        for output_metric in self.analysis.activity.output_metric_objects():
            if output_metric.id not in output_costs_keys:
                return False
        return True


class Workflow:
    step_classes = [
        Define,
        LoadData,
        FixMissingData,
        Categorize,
        SetContribution,
        Allocate,
        IdentifyOutputValue,
        Insights,
    ]
    steps = None

    def __init__(self, analysis, active_step_name):
        self.analysis = analysis
        self.steps = [cls(self) for cls in self.step_classes]
        self.active_step = self.get_step(active_step_name)

    def get_step(self, step_name):
        for step in self.steps:
            if step.name == step_name:
                return step

    def get_prev(self, step):
        try:
            step_index = self.steps.index(step)
            if step_index - 1 < 0:
                return None
            prev_step = self.steps[step_index - 1]
            if isinstance(prev_step, MultiStep) and len(prev_step.steps):
                prev_sub_step = prev_step.steps[0]
                for sub_step in prev_step.steps:
                    prev_sub_step = sub_step
                    if not sub_step.is_complete:
                        prev_sub_step = sub_step
                        break
                prev_step = prev_sub_step
            return prev_step

        except (IndexError, ValueError):
            return None

    def get_next(self, step):
        try:
            step_index = self.steps.index(step)
            for step in self.steps[step_index + 1:]:
                if step.is_enabled:
                    return step
        except (IndexError, ValueError):
            return None

    def get_last_incomplete_or_last(self):
        last_incomplete = self.get_last_incomplete()
        if last_incomplete:
            return last_incomplete
        return self.steps[-1]

    def get_last_incomplete(self):
        for step in self.steps:
            if step.is_enabled and not step.is_complete:
                if isinstance(step, MultiStep):
                    last = step.get_last_incomplete_or_last()
                    if last:
                        return last
                return step
        return None

    def invalidate_step(self, step_name):
        for step in self.steps:
            if step.name == step_name:
                step.invalidate()

    def calculate_if_possible(self):
        self.get_step('insights').calculate_if_possible()

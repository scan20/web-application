# Dioptra / Web application service

![main.yaml badge](https://github.com/theirc-dioptra/dioptra-service-web/actions/workflows/main.yml/badge.svg)

Quickstart:

```
export DJANGO_SECRET_KEY=a_secret_key
export DATABASE_PASSWORD=a_password
docker-compose build
docker-compose run web python manage.py build -y
docker-compose up -d
# go to http://localhost:8000
# Login as dioptra_default+administrator@ombuweb.com, password 'password'
```

## Running using Docker

Define environment variables with the secrets required to run the application locally:

    export DJANGO_SECRET_KEY=a_secret_key
    export DATABASE_PASSWORD=a_password

Start all services:

    docker-compose up

If necessary, build the site content:

    docker-compose run web python manage.py build -y

Visit `http://localhost:8000`

Or, start each service individually:

    docker-compose up db
    docker-compose up web

The users created by the build command are defined in
 `website/management/commands/build.py`.
 
To run Django `manage.py` commands in the docker containers, use:

    docker-compose run web python manage.py ...

Rebuild the Docker images:

    docker-compose build

## Running with a local Python interpreter

We use `asdf` version manager with its Python plugin
for running etrm locally: [https://github.com/danhper/asdf-python](https://github.com/danhper/asdf-python).

We also use Docker for external services,
a `Makefile` with all our common commands,
and run our Python code (Django web, Celery worker) locally.

Once you have `asdf` and `asdf-python` installed, we may need to install the right version of Python:

    asdf install
    asdf reshim

Then install deps:

    make install

As of 9/2021, the dependency resolver in Pip 20.3 and higher causes extremely long install times,
so we require 20.2. Use `pip install --upgrade pip==20.2`.

If psycopg2 errors and you're on MacOS, you probably need to do this first:

    LDFLAGS=-L/usr/local/opt/openssl/lib pip install psycopg2

One last thing, we suggest `pgcli` for database access over `psql`:

    brew install pgcli

### External Services

tl;dr: `make up`

By default, we'll use the settings file `website/settings/local.py`,
which will run against Docker containers with external services,
as configured in `docker/docker-compose-local.yml`.

You can use a different file if you have different settings.
Use `export DJANGO_SETTINGS_MODULE=website.settings.whatever` if that's the case.
Or try out some of the other settings files.

Note that some things outside the application (like `make psql`)
assume the values configured in `local.py`, so you may need to change some things.

### Database Initialization

If you have an existing DB, you can import it:

```
BACKUP=/path/to/backup.sql.gz make restore-db
BACKUP=/path/to/transactions.sql make restore-transactions-db
```

Otherwise, we can create a local DB and initialize it
with a `system` superuser and imported sample data.

```
make create-db
make bootstrap-db
```

You can connect to the DB in either of these ways:

```
make dbshell # Uses manage.py dbshell
make psql # Opens pgcli directly
```

### Run the site

```
make run
```

Visit http://localhost:8000.  Log in with
`dioptra_default+administrator@ombuweb.com` and `password`.

### Validation

You can try connecting to the app, and running a test:

```
$ make shell
DJANGO_SETTINGS_MODULE=website.settings.local LOG_LEVEL=INFO ./manage.py shell
Python 3.7.11 (default, Jul 27 2021, 14:02:34)
[Clang 12.0.5 (clang-1205.0.22.11)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> quit()

$ make test
...
Ran 13 tests in 0.868s
OK
```

## Running the transaction pipeline and datastore

Go that service directory: `../../transaction-data-pipeline/` and follow the
steps in the README.

## OAuth Support

By default, Dioptra runs with regular Django authentication.
We have configurable OAuth providers through django-allauth.

For each integration we want to support,
we need to create an `allauth` social app
that contains the OAuth client and secret.

First, create a new OAuth app in your provider service (Okta, OneLogin, etc).
Record your client ID and secret.
Then run:

    python ./manage.py initialize_auth --provider=## --client-id=## --secret=##

You can also use the `--reset` flag to delete all existing `allauth` apps.

Then, when you run the app, you need to set the `AUTH_PROVIDERS` **environment
variable** to a comma-separated list of enabled auth providers. See `manage.py
initialize_auth -h` for the list of supported providers. Some providers require
additional configuration. These are the current known provider configurations:

`okta`:

- `OKTA_URL` (i.e. dev-123.okta.com)
- `OKTA_OAUTH2_PATH` (i.e. /oauth2/v1)

`onelogin`:

- `ONELOGIN_APP` (i.e. dioptra)


If an `AUTH_PROVIDERS` is set and requires additional config that is missing,
an error indicating the missing config is raised at load time.

## Running tests

Some initial pytest style tests have been added.   They require two databases, the normal Django database and an external transaction_store

Here is the quickstart for those:
    
        export DJANGO_SETTINGS_MODULE=website.settings.test
        docker-compose -f docker/docker-compose.test.yml up -d
        pytest

## Helpful commands

Connect to psql/pgcli:

    psql postgres://dioptra:$DATABASE_PASSWORD@127.0.0.1:5432/dioptra

Restore a transactions database dump:

    psql postgres://dioptra:$DATABASE_PASSWORD@127.0.0.1:5433/dioptra_transactions -f <filename>

## Benchmarks

There's a benchmarking script for running some routines that have traditionally been slow.
There are some options for running from fixtures, or you can run from live data.

```
$ docker-compose run web python manage.py benchmark import
$ docker-compose run web python manage.py benchmark import --debug-sql --analysis=6
```

LOCAL_DB_BASEURL=postgres://dioptra:dioptrapwd@localhost:12432/
LOCAL_DB_URL=$(LOCAL_DB_BASEURL)dioptra
DJ_LOG_LEVEL=$(or $(LOG_LEVEL), INFO)
TEST_LOG_LEVEL=$(or $(LOG_LEVEL), FATAL)
DJ_SETTINGS_MODULE=$(or $(DJANGO_SETTINGS_MODULE), website.settings.local)
ROLL_BACK=2

AWS_ACCESS=$(or $(AWS_ACCESS_KEY_ID), test)
AWS_SECRET=$(or $(AWS_SECRET_ACCESS_KEY), test)
AWS_ENDPT=$(AWS_ENDPOINT)
ifeq ($(AWS_ACCESS),test)
AWS_ENDPT=http://localhost:12533
endif

LOCALVARS=DJANGO_SETTINGS_MODULE=$(DJ_SETTINGS_MODULE) DJANGO_LOG_LEVEL=$(DJ_LOG_LEVEL) AWS_ACCESS_KEY_ID=$(AWS_ACCESS) AWS_SECRET_ACCESS_KEY=$(AWS_SECRET) AWS_ENDPOINT=$(AWS_ENDPT) $(XSTORE_ARGS)
TESTVARS=$(LOCALVARS) LOG_LEVEL=$(TEST_LOG_LEVEL) DJANGO_SETTINGS_MODULE=website.settings.test

up:
	docker-compose -p dioptra-web -f docker/docker-compose-local.yml up -d --build

stop:
	docker-compose -p dioptra-web -f docker/docker-compose-local.yml stop

install: _check-pip
	pip install -r requirements/development.txt

rebuild-db:
	$(LOCALVARS) ./manage.py build -y

psql:
	pgcli "$(LOCAL_DB_URL)"
psql-transactions:
	@echo "Run 'make psql' from transaction-data-pipeline to connect to the local transaction store."

dbshell:
	$(LOCALVARS) ./manage.py dbshell

shell:
	$(LOCALVARS) ./manage.py shell

notebook:
	$(LOCALVARS) ./manage.py shell_plus --notebook

create-db:
	psql "$(LOCAL_DB_BASEURL)postgres" -c "CREATE DATABASE dioptra;"
bootstrap-db:
	$(LOCALVARS) python ./manage.py build -y
changepass:
	$(LOCALVARS) python ./manage.py changepassword "system@ombuweb.com"

restore-db: env-BACKUP
	psql "$(LOCAL_DB_BASEURL)postgres" -c "DROP DATABASE IF EXISTS dioptra"
	psql "$(LOCAL_DB_BASEURL)postgres" -c "CREATE DATABASE dioptra;"
	gunzip -c "$(BACKUP)" | psql "$(LOCAL_DB_URL)"
restore-transactions-db: env-BACKUP
	psql "$(LOCAL_DB_URL)" -f "$(BACKUP)"

test:
	$(TESTVARS) python -Wa ./manage.py test --keepdb $(TESTS)
test-rebuild-db:
	$(TESTVARS) python -Wa ./manage.py test --noinput $(TESTS)

run:
	$(LOCALVARS) ./manage.py runserver

makemigrations:
	$(LOCALVARS) ./manage.py makemigrations
makemigrations-merge:
	$(LOCALVARS) ./manage.py makemigrations --merge

migrate:
	$(LOCALVARS) ./manage.py migrate

showmigrations:
	$(LOCALVARS) ./manage.py showmigrations

migrate-back:
	$(LOCALVARS) ./manage.py migrate website `$(LOCALVARS) ./manage.py showmigrations website | tail -n $(ROLL_BACK) | head -1 | cut -d " " -f 3`

fe-install:
	cd ./website/static/website && npm install && cd ../../..
fe-build:
	cd ./website/static/website && npm run sass && npm run js
fe-watch:
	cd ./website/static/website && npm start

env-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi

cmd-exists-%:
	@hash $(*) > /dev/null 2>&1 || \
		(echo "ERROR: '$(*)' must be installed and available on your PATH."; exit 1)

fmt:
	black .

fmt-check:
	black --diff --check .
	#npx standard ?
	#npx markdownlint ?

bandit: ## Run a full bandit check
	bandit -ll -r lib website -x website/static/website/node_modules/node-gyp

bandit-baseline: ## Run bandit against the baseline report
	bandit -ll -b bandit-baseline.json -r lib website

bandit-baseline-make: ## Make a baseline report
	bandit -ll -f json -o bandit-baseline.json -r lib website

_check-pip:
	@./bin/check-pip

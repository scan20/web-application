import io
import datetime
from django.apps import apps
from django.test import TestCase
from django.core import mail
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.conf import settings
from django.core.management import call_command
from django.core.exceptions import ImproperlyConfigured
from django.contrib.contenttypes.models import ContentType
from app_log.logger import log, create_entry, get_subscriptions_for_entry
from app_log.notifiers import SendEmailNotifier, Notifier
from app_log.models import AppLogEntry, Subscription, Email
from app_log.tests.models import TestObject1, TestObject2
from app_log.management.commands import app_log__send_emails


class AppLogEntryModelTest(TestCase):

    def setUp(self):
        self.object1 = TestObject1.objects.create(name='Object 1')
        self.object2 = TestObject2.objects.create(name='Object 2')
        self.user = get_user_model().objects.create(username='User 1')

    def test_add_log_entries(self):
        entry1 = create_entry(self.user, 'Created', self.object1)
        entry2 = create_entry(self.user, 'Created', self.object2)
        self.assertEqual(AppLogEntry.objects.all().count(), 2)
        self.assertNotEqual(entry1.obj, entry2.obj)

    def test_object_deleted_after_logging(self):
        obj = TestObject1.objects.create(name='To Delete')
        create_entry(self.user, 'Created', obj)
        obj.delete()
        entry = AppLogEntry.objects.last()
        self.assertIsNone(entry.obj)
        self.assertEqual(entry.content_type.model_class(), TestObject1)

    def test_user_deleted_after_logging(self):
        user = get_user_model().objects.create(username='User To Delete')
        create_entry(user, 'Created', self.object1)
        user.delete()
        entry = AppLogEntry.objects.last()
        self.assertIsNone(entry.actor_user)
        self.assertEqual(entry.actor_name, 'User To Delete')


class GetSubscriptionsTest(TestCase):

    def setUp(self):
        self.object1 = TestObject1.objects.create(name='Object 1')
        self.object2 = TestObject2.objects.create(name='Object 2')
        self.user = get_user_model().objects.create(username='User 1')

    def test_get_subscriptions_by_timestamp_after(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            timestamp_start=timezone.now() - datetime.timedelta(days=3),
        )

        # In timestamp range.
        entry = AppLogEntry(
            timestamp=timezone.now(),
            actor_name='some user',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

    def test_get_subscriptions_by_timestamp_before(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            timestamp_end=timezone.now(),
        )

        # In timestamp range.
        entry = AppLogEntry(
            timestamp=timezone.now() - datetime.timedelta(days=3),
            actor_name='some user',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

    def test_get_subscriptions_by_timestamp_range(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            timestamp_start=timezone.now() - datetime.timedelta(days=3),
            timestamp_end=timezone.now() + datetime.timedelta(days=3),
        )

        # In timestamp range.
        entry = AppLogEntry(
            timestamp=timezone.now(),
            actor_name='some user',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

        # Outside of timestamp range.
        entry = AppLogEntry(
            timestamp=timezone.now() + datetime.timedelta(days=10),
            actor_name='some user',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 0)

    def test_get_subscriptions_by_actor_name(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            actor_name='steve',
        )

        entry = AppLogEntry(
            actor_name='steve',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

        entry = AppLogEntry(
            actor_name='stephen',
            action='some action',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 0)

    def test_get_subscriptions_by_action(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            action='created',
        )

        entry = AppLogEntry(
            action='created',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

        entry = AppLogEntry(
            action='updated',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 0)

    def test_get_subscriptions_by_message_keywords(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            action='created',
            message_keywords='download',
        )

        entry = AppLogEntry(
            action='created',
            message='Steve Downloaded the Document',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)
        self.assertEqual(subscription.pk, subscriptions[0].pk)

        entry = AppLogEntry(
            action='created',
            message='Steve Uploaded the Document',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 0)

    def test_get_subscriptions_without_keywords_by_message_keywords(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            action='created',
        )

        entry = AppLogEntry(
            action='created',
            message='Steve Downloaded the Document',
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)

    def test_get_subscriptions_by_content_type(self):
        subscription = Subscription.objects.create(
            notifier='placeholder',
            action='created',
            content_type=ContentType.objects.get_for_model(self.object1)
        )

        entry = AppLogEntry(
            action='created',
            content_type=ContentType.objects.get_for_model(self.object1)
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 1)

        entry = AppLogEntry(
            action='created',
            content_type=ContentType.objects.get_for_model(self.object2)
        )
        subscriptions = get_subscriptions_for_entry(entry)
        self.assertEqual(len(subscriptions), 0)


class NotifierTest(TestCase):

    def setUp(self):
        self.object1 = TestObject1.objects.create(name='Object 1')
        self.user = get_user_model().objects.create(
                        username='User 1',
                        email='test@ombuweb.com',
                    )

    def test_notifier_creates_email(self):
        entry = AppLogEntry(
            actor_name='Test Actor Name',
            action='Updated',
            obj=self.object1,
            message='{} was updated.'.format(str(self.object1))
        )
        subscription = Subscription(
            owner=self.user,
            notifier='app_log.notifiers.SendEmailNotifier',
            notifier_config={},
        )
        notifier = SendEmailNotifier()
        notifier.notify(subscription, entry)

        email = Email.objects.last()
        self.assertEqual(Email.objects.all().count(), 1)
        self.assertTrue(entry.message in email.subject)
        self.assertEqual(self.user.email, email.to_address)

    def test_notifier_sends_emails(self):
        email = Email.objects.create(
            subject='Test Email',
            to_address='test@ombuweb.com',
            body='Test Body',
            body_html='<p>Test Body</p>',
        )

        notifier = SendEmailNotifier()
        results = notifier.send_emails()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(results['sent'], 1)
        self.assertTrue(email.subject in mail.outbox[0].subject)
        self.assertEqual(Email.objects.all().count(), 0) # Email was successfully deleted.


class SubscriptionSendEmailNotifierTest(TestCase):
    """
    Tests the whole flow put together.
    """
    def setUp(self):
        self.object1 = TestObject1.objects.create(name='Object 1')
        self.user = get_user_model().objects.create(
            username='steve',
            email='test@ombuweb.com',
        )

    def test_entire_flow(self):
        subscription = Subscription.objects.create(
            owner=self.user,
            notifier='app_log.notifiers.SendEmailNotifier',
            timestamp_start=timezone.now() - datetime.timedelta(days=3),
            timestamp_end=timezone.now() + datetime.timedelta(days=3),
            actor_name='steve',
            action='Updated',
            content_type=ContentType.objects.get_for_model(self.object1),
            message_keywords='was updated by',
        )

        log(
            actor=self.user,
            action='Updated',
            obj=self.object1,
            message=f'{str(self.object1)} was updated by {self.user.username}',
        )

        self.assertEqual(Email.objects.all().count(), 1)

        notifier = SendEmailNotifier()
        results = notifier.send_emails()

        self.assertEqual(results['sent'], 1)
        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue('was updated by' in mail.outbox[0].subject)
        self.assertEqual(Email.objects.all().count(), 0)


class TestSendEmailsManagementCommand(TestCase):
    """
    Tests the management command sending emails.
    """
    def setUp(self):
        self.object1 = TestObject1.objects.create(name='Object 1')
        self.user = get_user_model().objects.create(
            username='steve',
            email='test@ombuweb.com',
        )

    def test_notifier_sends_emails(self):
        Email.objects.create(
            subject='Test Email',
            to_address='test@ombuweb.com',
            body='Test Body',
            body_html='<p>Test Body</p>',
        )

        command = app_log__send_emails.Command()

        out = io.StringIO()
        call_command(command, stdout=out)

        out.seek(0)
        output = out.read()

        self.assertTrue('Emails sent: 1' in output)

        call_command(command, stdout=out)
        out.seek(0)
        output = out.read()
        self.assertTrue('No emails to send.' in output)


class MockNotifier(Notifier):
    pass

class TestNotifierLoadedFromSettings(TestCase):

    def test_notifiers_loaded_from_settings(self):
        # Temporarily write settings and reload the notifiers.
        settings.APP_LOG = {
            'notifiers': [
                'app_log.tests.tests.MockNotifier',
            ],
        }
        app_config = apps.get_app_config('app_log')
        app_config.load_notifiers()

        mock_notifier = app_config.get_notifier('app_log.tests.tests.MockNotifier')
        self.assertIsInstance(mock_notifier, MockNotifier)

        self.assertRaises(ImproperlyConfigured, app_config.get_notifier, 'app_log.notifiers.SendEmailNotifier')

        # Put settings back the way they were and reload notifiers.
        delattr(settings, 'APP_LOG')
        app_config.load_notifiers()

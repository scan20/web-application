from django.db import models


class TestObject1(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class TestObject2(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django.contrib.auth",
    "app_log",
    "app_log.tests", # Loads the models for the app_log tests.
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'app_log',
        'USER': 'app_log',
        'PASSWORD': 'pass',
        'HOST': 'localhost',
        'PORT': '54321',
    }
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [],
        },
    },
]


def log_noop(*args, **kwrgs):
    pass

try:
    from app_log.logger import log
except Exception:
    log = log_noop

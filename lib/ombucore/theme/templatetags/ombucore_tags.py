from django import template
from django.conf import settings

register = template.Library()

@register.filter
def group_rows(blocks, max_per_row=12):
    rows = []
    current_row = []
    for block in blocks:
        if not row_has_space_for(current_row, block.width, block.offset, max_per_row):
            rows.append(current_row)
            current_row = []
        current_row.append(block)
    rows.append(current_row)
    return rows

def row_has_space_for(row, width, offset, max_width=12):
    row_width = row_total_width(row)
    if row_width + width +offset > max_width:
        return False
    return True

def row_total_width(row):
    return sum([b.width + b.offset for b in row])

@register.filter
def get_class_name(obj):
    if inspect.isclass(obj):
        return obj.__name__.lower()
    else:
        return obj.__class__.__name__.lower()

@register.filter
def get_module(view):
    return 'ombucore.admin:' + view.__module__.split(".")[0] + '_'

@register.filter
def get_locale_name(locale):
    """
    Get the human readable name assciated with the locale. These live in website settings,
    not ombucore settings. So if you pass 'en-us' to this, it will return 'English (US)'.
    """
    return dict(settings.LANGUAGES)[locale]

@register.simple_tag
def is_localization_on():
    """
    A way to check if localization is turned on or not. Wrap Localization UI elements in a conditional that
    checks this tag. Invoke it like so:

    {% load ombucore_tags %}
    {% is_localization_on as localization_on %}
    {% if localization_on %}
      Fancy localization UI shiz
    {% endif %}
    """
    if hasattr(settings, 'LOCALIZATION'):
        return settings.LOCALIZATION
    else:
        return False

@register.simple_tag
def get_base_url():
    return settings.BASE_URL
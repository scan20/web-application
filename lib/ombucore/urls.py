from django.urls import include, path
from django.http import JsonResponse
from ombucore.imagewidget.views import ajax_file_preview
from ombucore.admin.views import FormView
from ombucore.basicauth import basicauth_exclude

urlpatterns = [
    path('', include('ombucore.admin.urls')),
    path('', include('ombucore.auth.urls')),
    path('', include('ombucore.assets.urls')),
    path('ajax-file-preview/', ajax_file_preview, name='ajax-file-preview'),
    path('taggit_autosuggest/', include('taggit_autosuggest.urls')),
    path('health-check', basicauth_exclude(lambda request: JsonResponse({"status": "ok"}))),
]

"""
Provides a form field that can render arbitrary HTML based on a render
function.

Render functions are passed (form, name, value, attrs=None).

Example:

def my_render_fn(form, name, value, attrs=None):
    return mark_safe('<p>Some HTML here</p>')

HtmlOutputField(render_fn=my_render_fn)

"""

from django.forms import Widget, Field
from django.forms.utils import flatatt
from django.utils.html import format_html
from django.utils.safestring import mark_safe

class HtmlOutputWidget(Widget):
    def render(self, *args, **kwargs):
        args = (self.form,) + args
        rendered = self.render_fn(*args, **kwargs)
        final_attrs = self.build_attrs(kwargs['attrs'])
        if not 'class' in final_attrs:
            final_attrs['class'] = ''
        final_attrs['class'] += ' html-output-widget'
        return format_html('<div {}>{}</div>',
                flatatt(final_attrs),
                rendered
            )

class HtmlOutputField(Field):
    widget = HtmlOutputWidget

    def __init__(self, *args, **kwargs):
        self.render_fn = kwargs.pop('render_fn')
        super(HtmlOutputField, self).__init__(*args, **kwargs)
        self.widget.render_fn = self.render_fn

    def get_bound_field(self, *args, **kwargs):
        bound_field = super(HtmlOutputField, self).get_bound_field(*args, **kwargs)
        bound_field.field.widget.form = bound_field.form
        return bound_field

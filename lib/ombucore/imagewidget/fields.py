from django.forms import ClearableFileInput, CheckboxInput
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from imagekit.cachefiles import ImageCacheFile
from imagekit.registry import generator_registry
from django.shortcuts import reverse
from django.utils.translation import gettext_lazy as _

class PreviewableImageInput(ClearableFileInput):
    template_name = 'django/forms/widgets/file.html'
    template_with_initial = """
        <span class="previewable-file-widget" data-mode="%(mode)s">
            <span class="mode-empty"><span class="drag-text">%(drag_text)s, %(or_text)s</span><label for="id_%(name)s" class="change">%(upload_text)s</label></span>
            <span class="mode-value">
                <span class="preview">%(preview)s</span>
                <label for="id_%(name)s" class="change">%(change_text)s</label> %(clear_template)s
            </span>
            %(input)s
            %(clear)s
        </span>
    """
    template_with_clear = '<a href="" class="remove" for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</a>'
    clear_checkbox_label = 'Remove'
    preview_generator = 'imagewidget:preview'

    def __init__(self, **kwargs):
        self.preview_generator = kwargs.pop('preview_generator', self.preview_generator)
        super(PreviewableImageInput, self).__init__(**kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        attrs.update({
            'data-ajax-file-preview-url': reverse('ajax-file-preview'),
            'data-preview-generator': self.preview_generator,
            'class': 'notrackchange',
        })
        substitutions = {
            'name': name,
            'clear': '',
            'clear_template': '',
            'clear_checkbox_label': _(self.clear_checkbox_label),
            'preview': '',
            'value': value,
            'mode': 'value' if value else 'empty',
            'input': super(ClearableFileInput, self).render(name, value, attrs, renderer=renderer),
            'drag_text': _("Drag an image here"),
            'or_text': _("or"),
            'upload_text': _("Upload from computer"),
            'change_text': _("Change"),
        }
        template = self.template_with_initial

        if self.is_initial(value):
            substitutions.update(self.get_template_substitution_values(value))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id, 'class': 'clear-checkbox notrackchange'})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)  # nosec

    def get_template_substitution_values(self, value):
        # values = super(PreviewableImageInput, self).get_template_substitution_values(value)
        generator = generator_registry.get(self.preview_generator, source=value)
        f = ImageCacheFile(generator)
        return {
            'preview': '<img src="%s">' % f.url,
        }

    class Media:
        js = ('js/ajax-file-preview.js',)
        css = {
            'all': ('css/ajax-file-preview.css',)
        }

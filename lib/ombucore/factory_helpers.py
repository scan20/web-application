import random
from random_words import RandomWords, RandomNicknames, LoremIpsum

def random_words_apply_blacklist(rw):
    blacklist = ['butt', 'breast', 'breasts', 'drunk', 'drunks', 'drunkeness'
        'drug', 'drugs', 'affair', 'affairs', 'sex', 'sexes', 'balls', 'butts',
        'vomit', 'bigamy']
    for l in rw.nouns:
        rw.nouns[l] = [w for w in rw.nouns[l] if w not in blacklist]
    return rw

rw = RandomWords()
random_words_apply_blacklist(rw)

rn = RandomNicknames()
li = LoremIpsum()

def get_title():
    return ' '.join(rw.random_words(count=random.randint(1, 4))).title()

def get_name():
    return ' '.join(rn.random_nicks(count=2))

def get_acronym():
    return rw.random_word()

def get_text(min_sentences=2, max_sentences=8):
    return li.get_sentences(random.randint(min_sentences ,max_sentences))

def get_paragraphs(min_p=2, max_p=5):
    num = random.randint(min_p, max_p)
    return ''.join(['<p>' + get_text() + '</p>' for i in range(num)])

def model_add_many_to_many(model, field_name, factories, min_num=1, max_num=4):
    attr = getattr(model, field_name)
    for i in range(0, random.randint(min_num, max_num)):
        factory = random.choice(factories)
        attr.add(factory())

def get_imageasset_embedded_tag(asset, **options):
    return get_asset_embed_tag(asset, **options)

def get_asset_embed_tag(asset, **options):
    from ombucore.admin.sites import site
    from ombucore.admin.templatetags.panels_extras import jsonattr
    obj_info = site.related_info_for(asset)
    asset_class_name = asset.__class__.__name__
    attr_name = ''
    preview_html = ''
    default_options = {
        'objInfo': obj_info,
    }
    if asset_class_name == 'ImageAsset':
        attr_name = 'data-ombuimage'
        preview_html = '<img src="{}" />'.format(obj_info['image_url'])
        default_options.update({
            'caption': '',
            'align': 'center',
        })
    elif asset_class_name == 'VideoAsset':
        attr_name = 'data-ombuvideo'
        preview_html = '<img src="{}" />'.format(obj_info['image_url'])
    elif asset_class_name == 'DocumentAsset':
        attr_name = 'data-ombudocument'
        preview_html = '<div class="ombudocument-title">{}</div><div class="ombudocument-type">{}</div>'.format(
            asset.title,
            asset.human_file_type(),
        )
        default_options.update({
            'align': 'left',
        })

    default_options.update(options)

    return '<div {}="{}">{}</div>'.format(
        attr_name,
        jsonattr(default_options),
        preview_html,
    )



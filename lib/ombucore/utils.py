from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


locales_by_code = dict(settings.LANGUAGES)

def get_locale_name(locale_code):
    return locales_by_code.get(locale_code, locale_code)

def get_default_locale_code():
    pass

def get_default_locale_code():
    if hasattr(settings, 'LOCALIZATION_DEFAULT'):
        return settings.LOCALIZATION_DEFAULT
    elif hasattr(settings, 'LANGUAGES'):
        return settings.LANGUAGES[0][0]
    else:
        raise ImproperlyConfigured("Please provide LANGUAGES and LOCATION_DEFAULT settings in settings/base.py.")

def extend_subclass(cls, subcls_name, subcls_options):
    """ Creates a derivative of `cls` so that an internal class can be extended """
    subcls_bases = (getattr(cls, subcls_name), object) if hasattr(cls, subcls_name) else (object,)
    subcls = type(str(subcls_name), subcls_bases, subcls_options)
    return type(str(cls.__name__), (cls,), {
        '__module__': cls.__module__,
        subcls_name: subcls,
    })

import os
from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError

@deconstructible
class FileExtensionValidator(object):
    whitelist = []

    def __init__(self, whitelist=None):
        if whitelist is not None:
            self.whitelist = [i.lower() for i in whitelist]

    def __call__(self, value):
        filename = value.name
        ext = os.path.splitext(filename)[1].split('.')[-1].lower()
        if ext not in self.whitelist:
            types = ', '.join(self.whitelist)
            raise ValidationError("Please upload a file of the type: %s" % types)

    def __eq__(self, other):
        return self.whitelist == other.whitelist

from django.forms import widgets
import django_filters
from imagekit.cachefiles import ImageCacheFile
from imagekit.registry import generator_registry
from taggit.models import Tag
from ombucore.admin.sites import site as admin_site
from ombucore.admin.modeladmin import ModelAdmin
from ombucore.admin.views import PreviewView, NestedReorderView
from ombucore.admin.filterset import FilterSet
from ombucore.assets.models import ImageAsset, VideoAsset, DocumentAsset, AssetFolder
from ombucore.assets.forms import ImageForm, VideoForm, DocumentForm, TreeNodeChoiceFilter, FolderForm


class ImageAssetFilterSet(FilterSet):
    search = django_filters.CharFilter(
        field_name='title',
        lookup_expr='icontains',
        help_text='',
    )
    folder = TreeNodeChoiceFilter(queryset=AssetFolder.objects.all())
    tags = django_filters.ModelChoiceFilter(queryset=Tag.objects.all().order_by('name'), widget=widgets.Select)
    order_by = django_filters.OrderingFilter(
                choices=(
                    ('-created', 'Created (newest first)'),
                    ('created', 'Created (oldest first)'),
                ),
                empty_label=None,
            )

    class Meta:
        model = ImageAsset
        fields = ['search', 'folder']


class ImagePreviewView(PreviewView):
    template_name = 'assets/panel-preview-image.html'


class ImageAssetAdmin(ModelAdmin):
    form_class = ImageForm
    filterset_class = ImageAssetFilterSet
    preview_view = ImagePreviewView
    list_display_grid = True
    list_display = (('render_preview', 'Preview'),)

    def modify_related_info(self, info, obj):
        generator = generator_registry.get('panels:widget:preview', source=obj.image)
        info['image_url'] = ImageCacheFile(generator).url
        return info


admin_site.register(ImageAsset, ImageAssetAdmin)


class VideoAssetFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='title',
                    lookup_expr='icontains',
                    help_text='',
                )
    provider = django_filters.ChoiceFilter(
                choices=VideoAsset.provider_filter_choices,
                help_text=''
            )
    folder = TreeNodeChoiceFilter(queryset=AssetFolder.objects.all())
    tags = django_filters.ModelChoiceFilter(queryset=Tag.objects.all().order_by('name'), widget=widgets.Select)
    order_by = django_filters.OrderingFilter(
                choices=(
                    ('-created', 'Created (newest first)'),
                    ('created', 'Created (oldest first)'),
                ),
                empty_label=None,
            )

    class Meta:
        model = VideoAsset
        fields = ['search', 'folder']


class VideoPreviewView(PreviewView):
    template_name = 'assets/panel-preview-video.html'


class VideoAssetAdmin(ModelAdmin):
    form_class = VideoForm
    filterset_class = VideoAssetFilterSet
    preview_view = VideoPreviewView
    list_display_grid = True
    list_display = (('render_changelist_preview', 'Preview'),)

    def modify_related_info(self, info, obj):
        info['image_url'] = obj.thumbnail_url
        return info

admin_site.register(VideoAsset, VideoAssetAdmin)


class DocumentAssetFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='title',
                    lookup_expr='icontains',
                    help_text='',
                )
    file_type = django_filters.ChoiceFilter(
                choices=DocumentAsset.file_type_filter_choices,
                help_text=''
            )
    folder = TreeNodeChoiceFilter(queryset=AssetFolder.objects.all())
    tags = django_filters.ModelChoiceFilter(queryset=Tag.objects.all().order_by('name'), widget=widgets.Select)
    order_by = django_filters.OrderingFilter(
                choices=(
                    ('-created', 'Created (newest first)'),
                    ('created', 'Created (oldest first)'),
                ),
                empty_label=None,
            )

    class Meta:
        model = DocumentAsset
        fields = ['search', 'folder']


class DocumentPreviewView(PreviewView):
    template_name = 'assets/panel-preview-document.html'


class DocumentAssetAdmin(ModelAdmin):
    form_class = DocumentForm
    filterset_class = DocumentAssetFilterSet
    preview_view = DocumentPreviewView
    list_display = (
        ('title', 'Title'),
        ('human_file_type', 'Type'),
    )


admin_site.register(DocumentAsset, DocumentAssetAdmin)


class AssetFolderAdmin(ModelAdmin):
    form_class = FolderForm
    changelist_view = NestedReorderView
    changelist_select_view = False


admin_site.register(AssetFolder, AssetFolderAdmin)

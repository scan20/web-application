import os
from django.conf import settings
from django.core.validators import get_available_image_extensions
from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from micawber import ProviderRegistry, Provider
from micawber.exceptions import ProviderNotFoundException, ProviderException
from imagekit.registry import generator_registry
from imagekit.cachefiles import ImageCacheFile

from ombucore.assets.renderers import render_label, ImageRenderer, VideoRenderer, DocumentRenderer
from .validators import FileExtensionValidator
from taggit_autosuggest.managers import TaggableManager
from polymorphic.models import PolymorphicModel
from mptt.models import MPTTModel, TreeForeignKey


class AssetFolder(MPTTModel):
    title = models.CharField(max_length=200)
    created = models.DateTimeField('created', editable=False, default=timezone.now)
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        verbose_name='Parent Folder',
        related_name='children',
        help_text='The parent folder to put this folder under.',
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Folder'


class Asset(PolymorphicModel):
    """
    Base polymorphic for all asset classes.
    """
    title = models.CharField(max_length=200)
    created = models.DateTimeField('created', editable=False, default=timezone.now)
    tags = TaggableManager(blank=True, help_text="Find tags by name or create new ones.")
    folder = TreeForeignKey(
        'assetfolder',
        null=True,
        blank=True,
        verbose_name='Asset Folder',
        related_name='folder',
        help_text='The folder to put this asset into.',
        on_delete=models.SET_NULL,
    )
    caption = models.CharField(max_length=255, blank=True)
    hide_caption = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def render_embedded(self, local_settings=None, render_fn=render_label):
        return render_fn(self, local_settings)

    class Meta:
        verbose_name = 'Asset'
        verbose_name_plural = 'Assets'
        ordering = ['-created']


class ImageAsset(Asset):
    image = models.ImageField(
        upload_to='img',
        width_field='width',
        height_field='height',
        help_text= getattr(settings, 'OMBUASSETS_IMAGE_HELP_TEXT', None),
        validators=[FileExtensionValidator(
            whitelist=getattr(settings, 'OMBUASSETS_IMAGE_WHITELIST', get_available_image_extensions()),
        )],
    )
    width = models.CharField(max_length=20, blank=True)
    height = models.CharField(max_length=20, blank=True)

    @property
    def url(self):
        return self.image.url

    def resized_image(self, generator_name='assets:preview_thumbnail'):
        generator = generator_registry.get(generator_name, source=self.image)
        return ImageCacheFile(generator)

    def render_embedded(self, local_settings=None, render_fn=ImageRenderer()):
        return render_fn(self, local_settings)

    def render_preview(self):
        url = self.resized_image().url
        title = self.title
        return format_html('<img src="{url}" title="{title}" />', url=url, title=title)
    render_preview.allow_tags = True
    render_preview.short_description = 'Image'

    @classmethod
    def folder_filter_choices(cls):
        return [('', 'All')] + cls.FOLDERS

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'

TRANSCRIPT_TYPES_WHITELIST = getattr(settings, 'TRANSCRIPT_TYPES_WHITELIST',   ('pdf',))


OMBUASSETS_VIDEO_PROVIDERS = [
    {
        'name': 'YouTube',
        'example_url': 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        'patterns': {
            'http://(\S*.)?youtu(\.be/|be\.com/watch)\S+': 'http://www.youtube.com/oembed',
            'https://(\S*.)?youtu(\.be/|be\.com/watch)\S+': 'http://www.youtube.com/oembed?scheme=https&',
        },
    },
    {
        'name': 'Vimeo',
        'example_url': 'https://vimeo.com/87329114',
        'patterns': {
            'http://vimeo.com/\S+': 'http://vimeo.com/api/oembed.json',
            'https://vimeo.com/\S+': 'http://vimeo.com/api/oembed.json',
        },
    },
]

video_provider_registry = ProviderRegistry()

for provider in getattr(settings, 'OMBUASSETS_VIDEO_PROVIDERS', OMBUASSETS_VIDEO_PROVIDERS):
    for regex, url in provider['patterns'].items():
        video_provider_registry.register(regex, Provider(url))

class VideoAsset(Asset):
    video_url = models.CharField('Video URL', max_length=255, blank=False)
    transcript = models.FileField('Transcript', validators=[FileExtensionValidator(TRANSCRIPT_TYPES_WHITELIST)], blank=True)
    provider = models.CharField(max_length=200, blank=True)
    thumbnail_url = models.CharField(max_length=255, blank=True)
    thumbnail_width = models.CharField(max_length=100, blank=True)
    thumbnail_height = models.CharField(max_length=100, blank=True)
    width = models.CharField(max_length=100, blank=True)
    height = models.CharField(max_length=100, blank=True)
    html = models.TextField(blank=True)

    def clean(self):
        if not self.video_url:
            return
        try:
            self.pull_video_data(self.video_url)
        except ProviderNotFoundException:
            raise ValidationError({'video_url': ['The video for the URL you entered could not be found.']})
        except ProviderException:
            raise ValidationError({'video_url': ['There was an error getting information about this video.']})

    def pull_video_data(self, video_url):
        response = video_provider_registry.request(video_url)

        if not self.title:
            self.title = response['title']
        self.provider = response['provider_name']
        self.thumbnail_url = response['thumbnail_url']
        self.thumbnail_width = response['thumbnail_width']
        self.thumbnail_height = response['thumbnail_height']
        self.width = response['width']
        self.height = response['height']
        self.html = response['html']
        return self

    def render_embedded(self, local_settings=None, render_fn=VideoRenderer()):
        return render_fn(self, local_settings)

    def render_preview(self):
        url = self.thumbnail_url
        title = self.title
        return format_html('<label>{title}</label><img src="{url}" title="{title}" />', url=url,title=title)
    render_preview.allow_tags = True

    def render_changelist_preview(self):
        url = self.thumbnail_url
        title = self.title
        return format_html('<img src="{url}" title="{title}" />', url=url,title=title)
    render_changelist_preview.allow_tags = True
    render_changelist_preview.short_description = 'Video'

    def aspect_box_html(self):
        return wrap_in_aspect_box(self.html, self.width, self.height)

    @classmethod
    def provider_filter_choices(cls):
        return [('', 'All')] + [(p, p) for p in cls.objects.order_by('provider').values_list('provider', flat=True).distinct()]

    @classmethod
    def video_url_help_text(cls):
        names = []
        example_urls = []
        for provider in getattr(settings, 'OMBUASSETS_VIDEO_PROVIDERS', OMBUASSETS_VIDEO_PROVIDERS):
            names.append(provider['name'])
            example_urls.append('"{0}"'.format(provider['example_url']))
        return "Supports {0}. E.g. {1}".format(', '.join(names), ', '.join(example_urls))

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'


@receiver(pre_save, sender=VideoAsset)
def video_pre_save(sender, instance, **kwargs):
    video = instance
    video.pull_video_data(video.video_url)


DOCUMENT_TYPES_WHITELIST = getattr(settings, 'DOCUMENT_TYPES_WHITELIST',
                                   ('ppt', 'pptx', 'pdf', 'doc', 'docx', 'xls', 'xlt', 'txt', 'ZIP', )
)

DOCUMENT_TYPE_NAME_MAP = getattr(
    settings, 'DOCUMENT_TYPE_NAME_MAP',
    {
        'ppt': 'Microsoft PowerPoint Document',
        'pptx': 'Microsoft PowerPoint Document',
        'pdf': 'PDF Document',
        'doc': 'Microsoft Word Document',
        'docx': 'Microsoft Word Document',
        'xls': 'Microsoft Excel Document',
        'xlsx': 'Microsoft Excel Document',
        'xlt': 'Microsoft Excel Document',
        'txt': 'Text Document',
        'zip': 'ZIP Archive',
    })


class DocumentAsset(Asset):
    document = models.FileField(upload_to='doc', validators=[FileExtensionValidator(DOCUMENT_TYPES_WHITELIST)])
    file_type = models.CharField(max_length=100)

    def clean(self):
        self.populate_file_type()

    def populate_file_type(self):
        filename = self.document.name
        ext = os.path.splitext(filename)[1].split('.')[-1].lower()
        self.file_type = ext

    def render_preview(self):
        return format_html('{title} ({file_type})', title=self.title, file_type=self.file_type)

    def render_embedded(self, local_settings=None, render_fn=DocumentRenderer()):
        return render_fn(self, local_settings)

    def human_file_type(self):
        return DOCUMENT_TYPE_NAME_MAP.get(self.file_type, self.file_type)

    @classmethod
    def file_type_filter_choices(cls):
        return [('', 'All')] + [
                (t, DOCUMENT_TYPE_NAME_MAP[t]) for t in
                DocumentAsset.objects.values_list('file_type', flat=True).distinct()
            ]

    @property
    def url(self):
        return self.document.url

    class Meta:
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'


@receiver(pre_save, sender=DocumentAsset)
def document_pre_save(sender, instance, **kwargs):
    document = instance
    document.populate_file_type()


def wrap_in_aspect_box(html, nativeWidth, nativeHeight):
    template = "<div class='aspect-ratio-box'><span class='aspect-prop' style='padding-top: {ratioPercentage}%;'></span>{html}</div>"
    ratioPercentage = (float(nativeHeight) / float(nativeWidth)) * 100
    return template.format(ratioPercentage=ratioPercentage, html=html)

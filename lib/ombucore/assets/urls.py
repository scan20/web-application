from django.urls import include, path

urlpatterns = [
    path('taggit_autosuggest/', include('taggit_autosuggest.urls')),
]

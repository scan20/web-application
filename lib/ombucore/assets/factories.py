import random
from factory import DjangoModelFactory, SubFactory
from factory.fuzzy import FuzzyAttribute
from factory.django import ImageField, FileField
from ombucore.factory_helpers import get_title
from ombucore.assets.models import ImageAsset, DocumentAsset, VideoAsset, AssetFolder, video_provider_registry

def get_video_url():
    videos = [
        'https://www.youtube.com/watch?v=-iiSVOXjpxQ',
        'https://www.youtube.com/watch?v=huDToAfTnRI',
        'https://www.youtube.com/watch?v=0fMJ_JwGc_g',
        'https://vimeo.com/87329114',
    ]
    url = random.choice(videos)
    if video_provider_registry.provider_for_url(url):
        return url
    return get_video_url()

class AssetFolderFactory(DjangoModelFactory):
    class Meta:
        model = AssetFolder

    title = FuzzyAttribute(get_title)

class ImageAssetFactory(DjangoModelFactory):
    class Meta:
        model = ImageAsset

    title = FuzzyAttribute(get_title)
    image = ImageField(color='rgb(72, 153, 222)')
    folder = SubFactory(AssetFolderFactory)
    caption = FuzzyAttribute(get_title)

class VideoAssetFactory(DjangoModelFactory):
    class Meta:
        model = VideoAsset

    video_url = FuzzyAttribute(get_video_url)
    folder = SubFactory(AssetFolderFactory)

class DocumentAssetFactory(DjangoModelFactory):
    class Meta:
        model = DocumentAsset

    title = FuzzyAttribute(get_title)
    document = FileField(filename='document.pdf')
    folder = SubFactory(AssetFolderFactory)

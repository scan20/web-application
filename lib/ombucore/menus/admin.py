from ombucore.admin.modeladmin import ModelAdmin
from ombucore.admin.views import NestedReorderView, LocalizableNestedReorderView
from ombucore.admin.views import AddView
from ombucore.menus.forms import MenuForm, LocalizableMenuForm
from ombucore.loc_vers.models import Locale


class MenuAdmin(ModelAdmin):
    form_class = MenuForm
    changelist_view = NestedReorderView
    changelist_select_view = False


class LocalizableMenuAddView(AddView):

    def get_initial(self):
        self.initial = super().get_initial()
        data_dict = {}
        # Pre-populate the add form with the locale value
        if self.request.GET.get('locale', None):
            locale = Locale.from_code(self.request.GET.get('locale'))
            data_dict['locale'] = locale

        if not self.initial:
            self.initial = {}
        self.initial.update(data_dict)
        return self.initial

class LocalizableMenuAdmin(MenuAdmin):
    add_view = LocalizableMenuAddView
    form_class = LocalizableMenuForm
    changelist_view = LocalizableNestedReorderView

Menus
=====

This app provides the building blocks to easily create menus. A menu is a 
collection of model instances in a tree structure.


## Installation

Add 'ombucore.menus' to `INSTALLED_APPS`.


## Model

The app includes a base `Menu` abstract model. This model inherits from
django-mptt's `MPTTModel` which provides the tree structure of the model
instances.

This model should be subclassed as needed to create menus.  For example:

    from ombucore.menus.models import Menu

    class HeaderMenu(Menu):
        class Meta:
            verbose_name = 'Header Menu Item'
            verbose_name_plural = 'Header Menu Items'

These menu items have a `url` field that uses AnyUrlField to provide linking to
external URLs as well as other models.  Only models registered with AnyUrlField
can be referenced.

    from any_urlfield.models import AnyUrlField

    AnyUrlField.register_model(Page)


## Forms

A base menu form class is included. There's nothing unique about it. If you do
subclass the `MenuForm` class, be sure to also subclass the `Meta` class in 
order to inherit any meta options like `fields` and `fieldsets`.

    class HeaderMenuForm(MenuForm):
        class Meta(MenuForm.Meta):
            model = HeaderMenu


## Admin

A `MenuAdmin` class is included to use with menu models. It is pre-configured
to use `admin.views.NestedReorderView` as the changelist view so that the menu
can easily be reordered through the admin UI.


## Output and Template Tags

Information about rendering a menu trees in templates can be found in the
[Django MPTT documentation](https://django-mptt.github.io/django-mptt/templates.html).
A simple template implementation is included in `theme/templates/_navigation.html`.

Two template filters are included to make rendering trees in a template easier.
The `load_model` filter simply loads a model class by its python path and
returns it. This can be used instead of a context preprocessor to inject the
menu into every page. For example, to render a simple `HeaderMenu`:

    {% with menu_model='website.models.HeaderMenu'|load_model %}
        <nav class="header">
          <ul>
            {% include '_navigation.html' with nodes=menu_model.objects.all %}
          </ul>
        </nav>
      {% endif %}
    {% endwith %}

The other template filter, `any_url_db_value`, is a helper to allow comparison
of AnyUrlField values to models without having to traverse the AnyUrlField
relationship which would cause a database call for each traversal. An example
of its usage can be seen in `theme/templates/_navigation.html` where it is used
to see if a model matches a menu item's referenced object:

    {% if page|any_url_db_value == node.url.to_db_value %} active {% endif %}

This is just a string comparison and doesn't require the menu to load its
referenced object from the database.

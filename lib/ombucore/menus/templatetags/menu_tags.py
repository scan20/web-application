from pydoc import locate
from django import template
from any_urlfield.models import AnyUrlValue

register = template.Library()

@register.filter(name='load_model')
def load_model(model_path):
    try:
        model_class = locate(model_path)
        model_class.do_not_call_in_templates = True
        return model_class
    except Exception:
        return None

@register.filter
def any_url_db_value(obj):
    return AnyUrlValue.from_model(obj).to_db_value()

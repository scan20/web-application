from django import forms
from django.conf import settings
from django.forms import ModelChoiceField
from django.utils.encoding import smart_text
from ombucore.admin.forms.base import ModelFormBase
from ombucore.menus.models import Menu

class MenuForm(ModelFormBase):
    class Meta:
        model = Menu
        fields = "__all__"
        fieldsets = (
            (
                'Basic', {
                    'fields': (
                        'locale', 'title', 'url',
                    )
                }
            ),
        )
        exclude = ['parent']

class LocalizableMenuForm(MenuForm):
    def __init__(self, *args, **kwargs):
        super(MenuForm, self).__init__(*args, **kwargs)
        self.fields['locale'].widget = forms.HiddenInput()

    class Meta(MenuForm.Meta):
        model = Menu
        fields = "__all__"
        fieldsets = (
            (
                'Basic', {
                    'fields': (
                        'locale', 'title', 'url',
                    )
                }
            ),
        )
        exclude = ['parent']

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from any_urlfield.models import AnyUrlField
from ombucore.loc_vers.models import Locale


class Menu(MPTTModel):
    title = models.CharField(max_length=255, unique=False)
    url = AnyUrlField('Link')
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        verbose_name='Parent Menu Item',
        related_name='children',
        help_text='The menu item to put this item under.',
        on_delete=models.SET_NULL,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return '%s' % (self.title)


class LocalizableMenu(Menu):
    locale = models.ForeignKey(Locale, blank=False, null=False, on_delete=models.CASCADE)

    class Meta:
        abstract = True

from django.conf.urls import url
from django.conf import settings
from django.http import Http404
from django.contrib.auth import get_permission_codename
from django.utils.decorators import classonlymethod
from django.views.generic import View, DetailView


class RoutingHandler(View):
    """
    Routing handler that wraps multiple views.

    Participant views must implment a `has_match()` class method that returns
    a boolean that determines if the view is used or other views are searched.
    """
    views = []

    def dispatch(self, request, *args, **kwargs):
        for view in self.views:
            if view.view_class.has_match(request, *args, **kwargs):
                return view(request, *args, **kwargs)
        raise Http404

    @classonlymethod
    def as_view(cls, **initkwargs):
        """
        Overwritten to add `urls` property to view.
        """
        view = super(RoutingHandler, cls).as_view(**initkwargs)
        view.urls = [
            url(r'^$', view, name='homepage'),
            url(r'^(?P<path>.*)/$', view, name='path'),
        ]
        return view


class RoutingSingleModelViewMixin(object):
    """
    Used to mix in `has_match()` handling into a DetailView.

    Also handles the homepage route.
    """
    model = None
    slug_field = 'path'
    slug_path_kwarg = 'path'
    extra_filters = {}

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = self.handle_homepage_in_kwargs(request, kwargs)
        return super(RoutingSingleModelViewMixin, self).dispatch(request, *args, **self.kwargs)

    def get_queryset(self):
        queryset = super(RoutingSingleModelViewMixin, self).get_queryset()
        if getattr(settings, 'LOCALIZATION', False):
            queryset = queryset.filter(locale=self.request.LANGUAGE_CODE)
        return queryset

    @classmethod
    def has_match(cls, request, *args, **kwargs):
        kwargs = cls.handle_homepage_in_kwargs(request, kwargs)
        obj_args = cls.get_has_match_kwargs(request, *args, **kwargs)
        obj_args.update(cls.extra_filters)
        try:
            return bool(cls.model.objects.filter(**obj_args).get())
        except:
            return False

    @classmethod
    def get_has_match_kwargs(cls, request, *args, **kwargs):
        obj_args = {}

        obj_args[cls.slug_field] = kwargs[cls.slug_path_kwarg]

        if getattr(settings, 'LOCALIZATION', False):
            obj_args['locale'] = request.LANGUAGE_CODE

            # Re-assign correct URL to homepage draft if localization is on
            if request.path == '/{}/draft/'.format(request.LANGUAGE_CODE):
                obj_args[cls.slug_field] = 'draft'

        return obj_args

    @classmethod
    def handle_homepage_in_kwargs(cls, request, kwargs):
        """
        The homepage route won't inject a 'path' key into kwargs, so we have
        to do it manually.
        """
        if not cls.slug_path_kwarg in kwargs:
            path = request.path
            if getattr(settings, 'LOCALIZATION', False):
                path = path.replace('/{}/'.format(request.LANGUAGE_CODE), '')
            if path.strip('/') == '':
                kwargs[cls.slug_path_kwarg] = ''

        return kwargs


class RoutingModelView(RoutingSingleModelViewMixin, DetailView):
    template_name = 'page.html'
    context_object_name = 'page'


class LocalizedVersionedModelView(DetailView):
    """
    Handles models that are both Localized and Versioned.

    - Looks up the localized model by its path
    - Checks if a version is specified in the query string
    - Loads the requested version and checks permissions on it
    """
    model = None
    slug_field = 'path'
    slug_path_kwarg = 'path'
    extra_filters = {}
    template_name = 'page.html'
    context_object_name = 'page'

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = self.handle_homepage_in_kwargs(request, **kwargs)
        return super().dispatch(request, *args, **self.kwargs)

    def get_object(self, queryset=None):
        version = self._get_object(self.request, **self.kwargs)
        if version and self.can_view(version):
            return version
        else:
            raise Http404()

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        localization = self.object.parent
        base_model = localization.parent
        kwargs['localization'] = localization
        kwargs['localization_links'] = base_model.get_localization_alternate_links(
            exclude_locale_code=self.request.LANGUAGE_CODE,
        )
        kwargs['locale_selector_links'] = base_model.get_language_selector_links()
        return kwargs

    def can_view(self, version):
        localization = version.parent
        if localization.get_published() == version:
            return True

        localization_change_permission = '{}.{}'.format(self.model._meta.app_label, get_permission_codename('change', self.model._meta))
        if self.request.user.has_perm(localization_change_permission):
            return True
        else:
            return False

    @classmethod
    def has_match(cls, request, *args, **kwargs):
        """
        Called by the router before routing to this view.
        """
        kwargs.update(cls.extra_filters)
        version = cls._get_object(request, **kwargs)
        if version:
            return True
        else:
            return False

    @classmethod
    def _get_object(cls, request, **kwargs):
        """
        Returns the requested version, published version or None.

        If a version is requested by number (e.g. version=2), that version is
        returned. Otherwise the published version is returned.
        """
        kwargs = cls.handle_homepage_in_kwargs(request, **kwargs)
        kwargs = cls.handle_locale_in_kwargs(request, **kwargs)
        localization = cls.model.objects.filter(**kwargs).first()
        if localization:
            if request.GET.get('version', None):
                # Specific version is requested.
                version_number = int(request.GET.get('version'))
                version = localization.get_version(version_number)
                return version
            else:
                # Published requested.
                version = localization.get_published()
                return version
        return None

    @classmethod
    def handle_locale_in_kwargs(cls, request, **kwargs):
        if getattr(settings, 'LOCALIZATION', False):
            kwargs['locales__code'] = request.LANGUAGE_CODE
        return kwargs

    @classmethod
    def handle_homepage_in_kwargs(cls, request, **kwargs):
        """
        The homepage route won't inject a 'path' key into kwargs, so we have
        to do it manually.
        """
        if not cls.slug_path_kwarg in kwargs:
            path = request.path
            if getattr(settings, 'LOCALIZATION', False):
                path = path.replace('/{}/'.format(request.LANGUAGE_CODE), '')
            if path.strip('/') == '':
                kwargs[cls.slug_path_kwarg] = ''
        return kwargs


class BasePageView(LocalizedVersionedModelView):
    pass

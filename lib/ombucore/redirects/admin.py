from django.forms import widgets
import django_filters
from ombucore.admin.modeladmin import ModelAdmin
from ombucore.admin.sites import site as admin_site
from ombucore.admin.filterset import FilterSet
from ombucore.redirects.models import Redirect
from ombucore.redirects.forms import RedirectForm

class RedirectFilterSet(FilterSet):
    search = django_filters.CharFilter(
                field_name='from_path',
                lookup_expr='icontains',
                help_text='',
                widget=widgets.TextInput(attrs={
                    'placeholder': 'Find a redirect...'
                })
            )

    enabled = django_filters.ChoiceFilter(
                choices=(
                    (True, 'Yes'),
                    (False, 'No'),
                ),
            )
    order_by = django_filters.OrderingFilter(
                choices=(
                    ('from_path', 'From path (A-Z)'),
                    ('-from_path', 'From path (Z-A)'),
                ),
                empty_label=None,
            )

    class Meta:
        model = Redirect
        fields = ['search',]

class RedirectAdmin(ModelAdmin):
    form_class = RedirectForm
    filterset_class = RedirectFilterSet
    list_display = (
        ('display_from_path', 'From'),
        ('to', 'To'),
        ('status_code', 'Status Code'),
        ('is_enabled', 'Enabled'),
    )

    def display_from_path(self, redirect):
        return '/{}'.format(redirect.from_path)

    def is_enabled(self, redirect):
        if redirect.enabled:
            return 'Yes'
        else:
            return 'No'

admin_site.register(Redirect, RedirectAdmin)

from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponseRedirectBase
from ombucore.redirects.models import Redirect

class ModelRedirectMiddleware(object):
    """
    Redirects based on redirect models in the database.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            redirect = Redirect.objects.get(from_path=request.path[1:-1], enabled=True)
            to = str(redirect.to)
            if to[0] == '/' and to[-1] != '/':
                to = to + '/'
            return HttpResponseRedirectBase(to, status=redirect.status_code)
        except ObjectDoesNotExist:
            pass

        return self.get_response(request)

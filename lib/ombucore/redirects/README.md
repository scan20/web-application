Redirects
=========

## Installation

Add `ombucore.redirects` to `INSTALLED_APPS`.

Add `ombucore.redirects.middleware.ModelRedirectMiddleware` to `MIDDLEWARE`.

Add permissions for the Redirect model.

- `add_redirect`
- `change_redirect`
- `delete_redirect`

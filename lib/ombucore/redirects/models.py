from django.db import models
from any_urlfield.models import AnyUrlField

class Redirect(models.Model):
    REDIRECT_STATUSES = (
        (300, '300 Multiple Choices'),
        (301, '301 Moved Permanently'),
        (302, '302 Found'),
        (303, '303 See Other'),
        (304, '304 Not Modified'),
        (305, '305 Use Proxy'),
        (307, '307 Temporary Redirect'),
    )

    from_path = models.CharField(
        'From',
        max_length=200,
        unique=False,
        help_text='Enter a path to redirect from.'
    )
    to = AnyUrlField(
        'To',
        help_text='The URL or page to redirect to.'
    )
    status_code = models.PositiveIntegerField(
        'Redirect Status',
        choices=REDIRECT_STATUSES,
        default=301,
        blank=False,
        null=False,
        help_text='You can find more information about HTTP redirect status codes at <a target="_blank" href="http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection">http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection</a>.'
    )
    enabled = models.BooleanField(
        'Enabled',
        default=True,
        help_text='If this box is checked, this redirect will be enabled.'
    )

    def clean(self):
        if self.from_path[-1] == '/':
            self.from_path = self.from_path[:-1]
        if len(str(self.from_path)) > 0 and self.from_path[0] == '/':
            self.from_path = self.from_path[1:]

    def __str__(self):
        return '{0}: /{1} =>  {2}'.format(self.status_code, self.from_path, str(self.to))

    class Meta:
        verbose_name = 'Redirect'
        verbose_name_plural = 'Redirects'

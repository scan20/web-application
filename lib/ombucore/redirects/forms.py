from ombucore.admin.forms.base import ModelFormBase
from ombucore.redirects.models import Redirect

class RedirectForm(ModelFormBase):
    class Meta:
        model = Redirect
        fields = ('from_path', 'to', 'status_code', 'enabled',)
        fieldsets = (
                        ('Basic', {
                            'fields': ('from_path', 'to', 'status_code', 'enabled',),
                            }
                        ),
                    )

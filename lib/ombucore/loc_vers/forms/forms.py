from django import forms
from django.urls import reverse
from django.utils.text import slugify
from django.db.models import QuerySet
from django.core.validators import ValidationError
from ombucore.admin.sites import site as admin_site
from ombucore.admin.forms.base import ModelFormBase, FormBase
from ombucore.admin.fields import UrlPathFormField
from ombucore.htmloutput_field import HtmlOutputField
from ombucore.loc_vers.models import Locale
from ombucore.admin.widgets import CheckboxSelectMultipleWithDisabledOptions
from ombucore.admin.buttons import LinkButton, SubmitButton, CancelButton
from ombucore.loc_vers.forms.fields import LocalesField


class LocVersBaseAddForm(ModelFormBase):
    """
    A form for adding a new "base" model that's localizable and versionable.
    """
    locale = forms.ModelChoiceField(
        label='Locale',
        queryset=Locale.objects.all(),
        required=True,
        help_text="Choose one or more locales for this page to serve at first. You will be able to add additional localizations later.",
    )

    class Meta:
        fields = ('name', 'locale')

    def save(self, commit=True):
        if self.user:
            self.instance.created_by = self.user
        instance = super().save(commit=commit)
        instance.create_localization(owner=self.user, **self.get_localization_instance_kwargs())
        return instance

    def get_localization_instance_kwargs(self):
        return {
            'locales': [self.data.get('locale')],
        }


class LocVersBaseModelWithPathAddForm(LocVersBaseAddForm):
    """
    A form for adding a new "base" model that's localizable and versionable.
    """
    path = UrlPathFormField(label='Path',
        max_length=200,
        required=False,
        help_text='The desired URL path for this page.  The path may include letters, numbers, hyphens, and forward slashes (e.g., ‘about/our-history’).'
    )

    class Meta(LocVersBaseAddForm.Meta):
        fields = ('name', 'path', 'locale')

    def clean(self):
        cleaned_data = super().clean()
        localization_cls = self._meta.model.children.rel.related_model
        path = cleaned_data.get('path')
        locale = cleaned_data.get('locale')
        if localization_cls.path_in_use_for_locales(path, [locale]):
            raise ValidationError({'path': "This path is already in use."})
        return cleaned_data

    def get_localization_instance_kwargs(self):
        kwargs = super().get_localization_instance_kwargs()
        kwargs.update({'path': self.cleaned_data.get('path')})
        return kwargs


class LocVersBaseModelWithPathCloneForm(FormBase):
    """
    A form for cloning a "base" model with optional configuration for which 
    localizations should also be cloned.
    """
    name = forms.CharField(required=True)
    localizations_label = HtmlOutputField(
        label="Localizations to clone",
        render_fn=lambda form, **kwargs: '',
        required=False,
    )

    def __init__(self, **kwargs):
        self.object = kwargs.pop('object')
        super().__init__(**kwargs)
        self.fields['name'].initial = "{} (duplicate)".format(self.object.name)
        self._add_fields_for_localizations()

    def _add_fields_for_localizations(self):
        for localization in self.object.localizations.all():
            field_name = self._localization_field_name(localization)
            path_field_name = self._localization_path_field_name(localization)
            label = ', '.join([locale.name for locale in localization.locales.all()])
            self.fields[field_name] = forms.BooleanField(label=label, required=False)

            path_required = field_name in self.data and self.data[field_name] == 'on'
            self.fields[path_field_name] = UrlPathFormField(
                label='Path for {}'.format(label),
                initial=localization.path,
                max_length=200,
                required=path_required,
                help_text='The desired URL path for this page.  The path may include letters, numbers, hyphens, and forward slashes (e.g., ‘about/our-history’).'
            )

    def clean(self):
        cleaned_data = self.cleaned_data
        localization_selected = False

        # Ensure the path isn't already used for a locale.
        for localization in self.object.localizations.all():
            field_name = self._localization_field_name(localization)
            path_field_name = self._localization_path_field_name(localization)
            if cleaned_data.get(field_name, False):
                localization_selected = True
                path = cleaned_data.get(path_field_name, None)
                locales = list(localization.locales.all())
                if path and localization.path_in_use_for_locales(path, locales):
                    raise ValidationError({path_field_name: "This path is already in use."})

        # Require at least on localization to be selected.
        if not localization_selected:
            first_localization = self.object.localizations.first()
            raise ValidationError({
                'localizations_label': "At least one localization must be selected.",
            })

        return cleaned_data

    def _localization_field_name(self, localization):
        return 'localization_{}'.format(localization.pk)

    def _localization_path_field_name(self, localization):
        return 'path_for_localization_{}'.format(localization.pk)

    def clone(self):
        cleaned_data = self.cleaned_data
        localization_data = []
        for localization in self.object.localizations.all():
            field_name = self._localization_field_name(localization)
            path_field_name = self._localization_path_field_name(localization)
            if cleaned_data.get(field_name, False):
                localization_data.append({
                    'pk': localization.pk,
                    'attrs': {
                        'path': cleaned_data.get(path_field_name),
                    },
                })

        return self.object.clone_with_localizations(
            attrs={
                'name': cleaned_data['name'],
            },
            localizations=localization_data,
        )

    class Media:
        js = (
            'loc_vers/js/loc-vers-base-model-clone-form.js',
        )


class LocVersLocalizationAddForm(ModelFormBase):
    """
    A form for adding a new "base" model that's localizable and versionable.
    """
    locales = LocalesField(
        label='Locale',
        queryset=Locale.objects.all(),
        required=True,
        help_text="Choose one or more locales for this page to serve at first. You will be able to add additional localizations later. If a locale is already being served by another localization of this content unit, its checkbox will be disabled.",
    )
    duplicate_existing = forms.BooleanField(
        label='Duplicate an existing localization',
        required=False,
    )
    source_localization = forms.ModelChoiceField(
        label="Localization to duplicate",
        required=False,
        widget=forms.RadioSelect,
        empty_label=None,
        queryset=QuerySet(), # No-op, the actual queryset is pulled from the paren object in __init__().
    )

    class Meta:
        fields = ('locales', 'parent')
        widgets = {
            'parent': forms.HiddenInput(),
        }

    class Media:
        js = (
            'loc_vers/js/loc-vers-localization-add-form.js',
        )

    def __init__(self, *args, **kwargs):
        self.disabled_locales = kwargs.pop('disabled_locales', [])
        parent = None
        if kwargs['initial'].get('parent'):
            parent = kwargs['initial'].get('parent')
            kwargs['initial'].update({'path': slugify(parent.name)})
        super().__init__(*args, **kwargs)
        if parent:
            if parent.children.all().count() > 0:
                # Set the localization-to-duplicate choices.
                self.fields['source_localization'].queryset = parent.children.all()
            else:
                self.fields['source_localization'].queryset = parent.children.all()
                self.fields['source_localization'].widget = forms.HiddenInput()
                self.fields['duplicate_existing'].widget = forms.HiddenInput()

        # Only require source_localization if a duplicate is requested.
        if 'data' in kwargs and 'duplicate_existing' in kwargs['data']:
            if kwargs['data']['duplicate_existing'] == 'on':
                self.fields['source_localization'].required = True

        choices = [
            (locale.pk, str(locale))
            for locale in self.fields['locales'].queryset.all()
        ]
        self.fields['locales'].set_choices(choices, self.disabled_locales)

    def save(self, commit=True):
        if self.cleaned_data['duplicate_existing']:
            source_localization = self.cleaned_data['source_localization']
            localization = source_localization.clone(
                attrs={
                    'locales': self.cleaned_data['locales'],
                },
                version_attrs={
                    'owner': self.user,
                },
            )
        else:
            localization = super().save(commit=commit)
            localization.create_draft(title=localization.parent.name, owner=self.user)
        return localization


class LocVersLocalizationWithPathAddForm(LocVersLocalizationAddForm):

    class Meta(LocVersLocalizationAddForm.Meta):
        fields = ('path', 'locales', 'parent')

    def clean(self):
        cleaned_data = super().clean()
        cls = self._meta.model
        path = cleaned_data.get('path')
        locales = cleaned_data.get('locales')
        if locales and cls.path_in_use_for_locales(path, locales):
            raise ValidationError({'path': "This path is already in use."})
        return cleaned_data


class LocVersLocalizationChangeUrlForm(ModelFormBase):

    class Meta:
        fields = ('path',)

    def clean(self):
        cleaned_data = super().clean()
        cls = self._meta.model
        path = cleaned_data.get('path')
        exclude_pk = self.instance.pk
        locales = self.instance.locales.all()
        if cls.path_in_use_for_locales(path, locales, exclude_pk=exclude_pk):
            raise ValidationError({'path': "This path is already in use."})
        return cleaned_data


class LocVersLocalizationChangeLocalesForm(ModelFormBase):
    locales = LocalesField(
        label='Locale',
        queryset=Locale.objects.all(),
        required=True,
        help_text="Choose one or more locales for this page to serve at first. You will be able to add additional localizations later. If a locale is already being served by another localization of this content unit, its checkbox will be disabled.",
    )

    class Meta:
        fields = ('locales',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        base_model = self.instance.parent
        instance_locales = self.instance.locales.all()
        disabled_options = []
        if base_model:
            for locale in base_model.get_locales():
                if locale not in instance_locales:
                    disabled_options.append(locale.pk)

        choices = [
            (locale.pk, str(locale))
            for locale in self.fields['locales'].queryset.all()
        ]
        self.fields['locales'].set_choices(choices, disabled_options)

        base_model_class = self.Meta.model.parent.field.related_model
        base_model_info_route = admin_site.url_for(base_model_class, 'info')
        self.buttons = [
            SubmitButton(text='Save changes'),
            CancelButton(),
            LinkButton(
                text='Manage other localizations',
                href=reverse(base_model_info_route, args=[self.instance.parent.pk]),
                panels_trigger=False,
                align='right',
            )
        ]


class LocVersVersionInfoForm(ModelFormBase):
    class Meta:
        fields = ('change_description',)

class LocVersVersionCommitForm(LocVersVersionInfoForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['change_description'].required = True


class LocVersVersionForm(ModelFormBase):

    def save(self, commit=True):
        self.instance.owner = self.user
        return super().save(commit=commit)

    class Meta:
        fields = (
            'title',
            'blocks',
            'seo_title',
            'seo_description',
            'robots_noindex',
            'robots_nofollow',
            'xmlsitemap_include',
            'xmlsitemap_changefreq',
            'xmlsitemap_priority',
        )
        fieldsets = (
            ('Basic', {
                'fields': ('title',),
            }),
            ('Blocks', {
                'fields': ('blocks',),
            }),
            ('SEO', {
                'fields': (
                    'seo_title',
                    'seo_description',
                    'robots_noindex',
                    'robots_nofollow',
                    'xmlsitemap_include',
                    'xmlsitemap_changefreq',
                    'xmlsitemap_priority',
                ),
            }),
        )

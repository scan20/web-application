from django import forms
from django.core.validators import ValidationError
from ombucore.admin.widgets import CheckboxSelectMultipleWithDisabledOptions
from ombucore.admin.widgets import SelectWithDisabledOptions


MULTIPLE = 'multiple'
CHOICE_MULTIPLE = (MULTIPLE, '+ Multiple')


class LocalesWidget(forms.widgets.MultiWidget):

    def __init__(self, attrs=None):
        widgets = (
            SelectWithDisabledOptions(),
            CheckboxSelectMultipleWithDisabledOptions(),
        )
        attrs = attrs or {}
        attrs['class'] = 'loc-vers-locales-widget'
        super().__init__(widgets, attrs)

    def decompress(self, value):
        """
        Takes the value from a parent field and deconstructs it into values for
        the individual widgets.

        Value is an array of locale pks. E.g. [1, 3]
        """
        if value:
            if len(value) > 1:
                return [MULTIPLE, value]
            if len(value) == 1:
                return [value[0], value]
            return value
        else:
            return [None, None]

    def get_context(self, name, value, attrs):
        """
        Break the value into the individual values for the sub-widgets.
        """
        value = self.decompress(value)
        context = super().get_context(name, value, attrs)
        return context

    def set_choices(self, choices, disabled_options):
        self.widgets[0].choices = choices + [CHOICE_MULTIPLE]
        self.widgets[0].disabled_options = disabled_options
        self.widgets[1].choices = choices
        self.widgets[1].disabled_options = disabled_options

    class Media:
        js = ('loc_vers/js/loc-vers-locales-widget.js',)
        css = {
            'screen': ('loc_vers/css/loc-vers-locales-widget.css',),
        }


class LocalesField(forms.ModelMultipleChoiceField):
    widget = LocalesWidget

    def set_choices(self, choices, disabled_options):
        self.widget.set_choices(choices, disabled_options)

    def clean(self, value):
        single_locale, multiple_locales = value

        if single_locale == MULTIPLE and not len(multiple_locales):
            raise ValidationError("Please select locales")

        locale_pks = []
        if single_locale == MULTIPLE:
            locale_pks = multiple_locales
        else:
            locale_pks = [single_locale]

        return locale_pks

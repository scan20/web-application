from django import template


register = template.Library()


@register.filter
def get_absolute_url_for_locale_code(version, locale_code):
    return version.get_absolute_url(locale_code=locale_code)

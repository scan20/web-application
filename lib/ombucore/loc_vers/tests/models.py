from django.db import models
from ombucore.admin.fields import CustomTreeForeignKey, ManyToManyField
from ombucore.models import abstract as abstract_models
from ombucore.models.mixins import BlockFieldsMixin, CloneableMixin
from ombucore.loc_vers.models import abstract as loc_vers_models


class InstantiatablePage(loc_vers_models.InstantiatableModel):
    pass

class InstantiatablePageInstance(loc_vers_models.InstantiatableModelInstance):
    title = models.CharField(max_length=255)
    parent = models.ForeignKey(InstantiatablePage, related_name='children', on_delete=models.CASCADE)

class LocalizablePage(loc_vers_models.LocalizableModel):
    pass

class LocalizablePageLocalization(loc_vers_models.LocalizableModelLocalization):
    title = models.CharField(max_length=255)
    parent = models.ForeignKey(LocalizablePage, related_name='children', on_delete=models.CASCADE)

class VersionablePage(loc_vers_models.VersionableModel):
    published_version = models.ForeignKey('VersionablePageVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

class VersionablePageVersion(loc_vers_models.VersionableModelVersion):
    parent = models.ForeignKey(VersionablePage, related_name='children', on_delete=models.CASCADE)

class LocVersPage(loc_vers_models.LocVersModel):
    pass

class LocVersPageLocalization(loc_vers_models.LocVersModelLocalization):
    parent = models.ForeignKey(LocVersPage, related_name='children', on_delete=models.CASCADE)
    published_version = models.ForeignKey('LocVersPageVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

class LocVersPageVersion(loc_vers_models.LocVersModelVersion):
    parent = models.ForeignKey(LocVersPageLocalization, related_name='children', on_delete=models.CASCADE)


###############################################################################
# Localizable, Versionable Model with blocks.
###############################################################################

class Block(abstract_models.BlockBase):
    pass

class RootBlock(Block):
    pass

class RichTextBlock(Block, abstract_models.RichTextBlock):
    class Meta(abstract_models.RichTextBlock.Meta):
        pass

class ContainerBlock(Block):
    @property
    def allowed_children(self):
        return [
            RichTextBlock,
            ContainerBlock,
        ]

class GallerySlide(CloneableMixin):
    title = models.CharField(max_length=100)

class GalleryBlock(Block):
    slides = ManyToManyField(GallerySlide, clone_on_clone=True, blank=True, related_name='slides')


class ModelWithBlocks(loc_vers_models.LocVersModel):
    pass

class ModelWithBlocksLocalization(loc_vers_models.LocVersModelLocalization):
    parent = models.ForeignKey(ModelWithBlocks, related_name='children', on_delete=models.CASCADE)
    published_version = models.ForeignKey('ModelWithBlocksVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

class ModelWithBlocksVersion(BlockFieldsMixin, loc_vers_models.LocVersModelVersion):
    parent = models.ForeignKey(ModelWithBlocksLocalization, related_name='children', on_delete=models.CASCADE)
    blocks = CustomTreeForeignKey(
                    RootBlock,
                    label="Blocks",
                    models=[RichTextBlock, ContainerBlock],
                    blank=True,
                    null=True,
                    related_name='%(app_label)s_%(class)s_parent',
                    on_delete=models.CASCADE,
                )

###############################################################################
# Localizable, Versionable Page.
###############################################################################


class Page(loc_vers_models.BasePage):
    pass

class PageLocalization(loc_vers_models.BasePageLocalization):
    parent = models.ForeignKey(Page, related_name='children', on_delete=models.CASCADE)
    published_version = models.ForeignKey('PageLocalization', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

class PageVersion(loc_vers_models.BasePageVersion):
    parent = models.ForeignKey(PageLocalization, related_name='children', on_delete=models.CASCADE)
    blocks = CustomTreeForeignKey(
                    RootBlock,
                    label="Blocks",
                    models=[RichTextBlock, ContainerBlock],
                    blank=True,
                    null=True,
                    related_name='%(app_label)s_%(class)s_parent',
                    on_delete=models.CASCADE,
                )

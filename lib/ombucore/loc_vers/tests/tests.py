from django.test import TestCase
from django.core.validators import ValidationError
from django.contrib.auth import get_user_model
from ombucore.loc_vers.models import Locale
from ombucore.loc_vers.tests.models import InstantiatablePage, InstantiatablePageInstance
from ombucore.loc_vers.tests.models import LocalizablePage, LocalizablePageLocalization
from ombucore.loc_vers.tests.models import VersionablePage, VersionablePageVersion
from ombucore.loc_vers.tests.models import LocVersPage, LocVersPageLocalization
from ombucore.loc_vers.tests.models import ModelWithBlocks, ModelWithBlocksLocalization
from ombucore.loc_vers.tests.models import ContainerBlock, RichTextBlock
from ombucore.loc_vers.tests.models import GalleryBlock, GallerySlide
from ombucore.loc_vers.tests.models import Page, PageLocalization, PageVersion

def setup_locales():
    locales = (
        ('English (US)', 'en-us'),
        ('English (UK)', 'en-gb'),
        ('Spanish (AR)', 'es-ar'),
        ('Spanish (MX)', 'es-mx'),
    )
    locale_models = {}
    for name, code in locales:
        locale = Locale.objects.create(name=name, code=code)
        locale_models[locale.code] = locale
    return locale_models


class InstantiatablePageTest(TestCase):

    def test_instantiatable_page(self):
        page = InstantiatablePage.objects.create()
        instance1 = InstantiatablePageInstance.objects.create(title='Instance 1', parent=page)
        instance2 = InstantiatablePageInstance.objects.create(title='Instance 2', parent=page)
        self.assertEqual(len(page.instances.all()), 2)
        self.assertEqual(instance2.title, page.get_instance(pk=instance2.pk).title)
        page.delete()
        self.assertEqual(len(InstantiatablePageInstance.objects.all()), 0)

    def test_clone_instantiatable_page(self):
        page = InstantiatablePage.objects.create()
        instance1 = InstantiatablePageInstance.objects.create(title='Instance 1', parent=page)
        instance2 = InstantiatablePageInstance.objects.create(title='Instance 2', parent=page)

        page_clone = page.clone()
        instance_clone1 = page_clone.get_instance(title='Instance 1')
        instance_clone2 = page_clone.get_instance(title='Instance 2')
        self.assertNotEqual(page.pk, page_clone.pk)
        self.assertNotEqual(instance_clone1.pk, instance1.pk)
        self.assertNotEqual(instance_clone2.pk, instance2.pk)


class LocalizablePageTest(TestCase):

    def setUp(self):
        self.locales = setup_locales()

    def test_add_locales_to_localizable_page(self):
        page = LocalizablePage.objects.create()

        english = LocalizablePageLocalization.objects.create(
                        title='English',
                        parent=page,
                    )
        english.locales.add(self.locales['en-us'])
        english.save()
        english = page.get_localization('en-us')
        self.assertEqual(english.locales.first().code, 'en-us')

        spanish = LocalizablePageLocalization.objects.create(
                        title='Spanish',
                        parent=page,
                    )
        spanish.locales.add(self.locales['es-ar'], self.locales['es-mx'])
        spanish.save()
        self.assertEqual(page.get_localization('es-ar'), page.get_localization('es-mx'))

    def test_clone_localizable_page(self):
        page = LocalizablePage.objects.create()
        english = LocalizablePageLocalization.objects.create(
                        title='English',
                        parent=page,
                    )
        english.locales.add(self.locales['en-us'])
        english.save()
        spanish = LocalizablePageLocalization.objects.create(
                        title='Spanish',
                        parent=page,
                    )
        spanish.locales.add(self.locales['es-ar'], self.locales['es-mx'])
        spanish.save()

        page_clone = page.clone()
        english_clone = page_clone.get_instance(title='English')
        spanish_clone = page_clone.get_instance(title='Spanish')
        self.assertNotEqual(page.pk, page_clone.pk)
        self.assertNotEqual(english.pk, english_clone.pk)
        self.assertNotEqual(spanish.pk, spanish_clone.pk)


class VersionablePageTest(TestCase):

    def setUp(self):
        User = get_user_model()
        self.user1 = User.objects.create(username='User 1')
        self.user2 = User.objects.create(username='User 2')

    def test_versionable_page_version_numbers_auto_increment(self):
        page = VersionablePage.objects.create(name='Versionable 1')
        version1 = VersionablePageVersion.objects.create(title='Version 1', parent=page, owner=self.user1)
        version2 = VersionablePageVersion.objects.create(title='Version 2', parent=page, owner=self.user1)
        self.assertEqual(version1.version, 1)
        self.assertEqual(version2.version, 2)

        page2 = VersionablePage.objects.create(name='Versionable 2')
        page2_version1 = VersionablePageVersion.objects.create(title='Version 1', parent=page2, owner=self.user1)
        self.assertEqual(page2_version1.version, 1)

        version3 = VersionablePageVersion.objects.create(title='Version 3', parent=page, owner=self.user1)
        self.assertEqual(version3.version, 3)

    def test_versionable_page_get_version(self):
        page = VersionablePage.objects.create(name='Versionable')
        version1 = VersionablePageVersion.objects.create(title='Version 1', parent=page, owner=self.user1)
        version2 = VersionablePageVersion.objects.create(title='Version 2', parent=page, owner=self.user1)
        self.assertEqual(page.get_version(2), version2)

    def test_versionable_page_published_version_must_be_committed(self):
        page = VersionablePage.objects.create(name='Versionable')
        version1 = VersionablePageVersion.objects.create(title='Version 1', parent=page, owner=self.user1)

        self.assertEqual(page.has_published(), False)
        page.published_version = version1
        self.assertRaises(ValidationError, page.full_clean) # Not committed yet.
        self.assertIsNone(version1.committed_at)

        version1.commit()

        page.full_clean()
        page.save()
        self.assertEqual(page.has_published(), True)
        self.assertEqual(page.get_published(), version1)
        self.assertIsNotNone(version1.committed_at)

    def test_versionable_page_published_must_be_child(self):
        page = VersionablePage.objects.create(name='Versionable 1')
        page2 = VersionablePage.objects.create(name='Versionable 2')
        page2_version1 = VersionablePageVersion.objects.create(title='Version 1', parent=page2, owner=self.user1)

        page.published_version = page2_version1
        self.assertRaises(ValidationError, page.full_clean) # Called by model forms.

    def test_only_one_draft_at_a_time(self):
        page = VersionablePage.objects.create(name='Versionable')

        # draft1 uncommitted.
        draft1 = VersionablePageVersion.objects.create(title='Draft 1', parent=page, owner=self.user1)
        draft1.full_clean()

        # draft2 uncommitted.
        draft2 = VersionablePageVersion(title='Draft 2', parent=page, owner=self.user1)

        # draft2 clean fails.
        self.assertRaises(ValidationError, draft2.full_clean)

        # commit draft1.
        draft1.full_clean()
        draft1.commit()

        # draft2 clean succeeds.
        draft2.full_clean()
        draft2.save()

    def test_committed_versions_cannot_be_saved(self):
        page = VersionablePage.objects.create(name='Versionable')
        version1 = VersionablePageVersion.objects.create(title='Draft 1', parent=page, committed=True, owner=self.user1)
        self.assertRaises(ValidationError, version1.full_clean)

    def test_clone_versionable_page(self):
        page = VersionablePage.objects.create(name='Versionable 1')
        version_1 = VersionablePageVersion.objects.create(
                        title='Version 1',
                        parent=page,
                        owner=self.user1,
                    )
        version_2 = VersionablePageVersion.objects.create(
                        title='Version 2',
                        parent=page,
                        owner=self.user1,
                    )
        self.assertEqual(list(page.versions.all()), [version_2, version_1])
        self.assertEqual(version_1.version, 1)
        self.assertEqual(version_2.version, 2)

        clone = page.clone()
        [clone_version_1] = list(clone.versions.all())
        self.assertNotEqual(page.pk, clone.pk)
        self.assertNotEqual(version_1.pk, clone_version_1.pk)
        self.assertEqual(clone_version_1.version, 1)

    def test_create_drafts(self):
        page = VersionablePage.objects.create(name='Versionable 1')
        version_1 = page.create_draft(owner=self.user1)
        version_1.commit()
        version_2 = page.create_draft(title='Draft 2', owner=self.user1)
        version_3 = page.create_draft_from(1, owner=self.user1)
        self.assertIsNotNone(version_1.committed_at)
        self.assertEqual(version_1.title, 'Versionable 1 Draft')
        self.assertEqual(version_2.title, 'Draft 2')
        self.assertEqual(version_2.version, 2)
        self.assertEqual(version_3.version, 3)
        self.assertEqual(version_3.title, 'Versionable 1 Draft')

    def test_can_change_version_owner_on_commit(self):
        page = VersionablePage.objects.create(name='Versionable 1')
        version_1 = page.create_draft(owner=self.user1)
        self.assertEqual(version_1.owner, self.user1)
        version_1.commit(commit_user=self.user2)
        self.assertEqual(version_1.owner, self.user2)


class LocalizableVersionablePageTest(TestCase):

    def setUp(self):
        self.locales = setup_locales()
        User = get_user_model()
        self.user1 = User.objects.create(username='User 1')
        self.user2 = User.objects.create(username='User 2')

    def test_create_page_and_localization(self):
        page = LocVersPage.objects.create(name='Test')
        english = LocVersPageLocalization.objects.create(parent=page)
        english.locales.add(self.locales['en-us'])
        english.save()
        english = page.get_localization('en-us')
        self.assertEqual(english.locales.first().code, 'en-us')
        english_draft_1 = english.create_draft(owner=self.user1)
        self.assertEqual(english_draft_1.parent.parent, page)
        self.assertEqual(english.get_version(1), english_draft_1)

        spanish = LocVersPageLocalization.objects.create(parent=page)
        spanish.locales.add(self.locales['es-mx'])
        spanish.save()
        spanish_draft_1 = spanish.create_draft(owner=self.user1)
        self.assertEqual(spanish.get_version(1), spanish_draft_1)

    def test_clone_localized_versioned_page(self):
        page = LocVersPage.objects.create(name='Test')
        english = LocVersPageLocalization.objects.create(parent=page)
        english.locales.add(self.locales['en-us'])
        english.save()
        english_draft_1 = english.create_draft(owner=self.user1)

        spanish = LocVersPageLocalization.objects.create(parent=page)
        spanish.locales.add(self.locales['es-mx'])
        spanish.save()
        spanish_draft_1 = spanish.create_draft(owner=self.user1)

        page_clone = page.clone()
        english_clone = page_clone.get_localization('en-us')
        english_clone_version = english_clone.get_latest()
        self.assertNotEqual(english_clone.pk, english.pk)
        self.assertNotEqual(english_clone.parent, english.parent)
        self.assertNotEqual(english_clone_version.pk, english_draft_1.pk)
        self.assertEqual(english_clone_version.version, english_draft_1.version)

    def test_clone_localization(self):
        page = LocVersPage.objects.create(name='Test')
        english_us = LocVersPageLocalization.objects.create(parent=page)
        english_us.locales.add(self.locales['en-us'])
        english_us.save()

        english_us_version_1 = english_us.create_draft(owner=self.user1)
        english_us_version_1.commit()

        english_us_version_2 = english_us.create_draft_from(english_us_version_1.version)
        english_us_version_2.change_description = "Change description"
        english_us_version_2.save()

        english_gb = english_us.clone(attrs={'locales': [self.locales['en-gb']]})
        english_gb_version = english_gb.get_latest()

        self.assertEqual(list(english_gb.locales.all()), [self.locales['en-gb']])
        self.assertNotEqual(len(english_us.children.all()), len(english_gb.children.all()))
        self.assertEqual(len(english_us.children.all()), 2)
        self.assertEqual(len(english_gb.children.all()), 1)
        self.assertEqual(english_gb_version.version, 1)
        self.assertEqual(english_gb_version.committed, True)
        self.assertTrue("Duplicated from" in english_gb_version.change_description)
        self.assertTrue(page.name in english_gb_version.change_description)
        self.assertTrue("v{}".format(english_us_version_2.version) in english_gb_version.change_description)

    def test_clone_page_with_localizations(self):
        page = LocVersPage.objects.create(name='Test')
        english = LocVersPageLocalization.objects.create(parent=page)
        english.locales.add(self.locales['en-us'])
        english.save()
        english_draft_1 = english.create_draft(owner=self.user1)

        spanish = LocVersPageLocalization.objects.create(parent=page)
        spanish.locales.add(self.locales['es-mx'])
        spanish.save()
        spanish_draft_1 = spanish.create_draft(owner=self.user1)

        page_clone = page.clone()
        english_clone = page_clone.get_localization('en-us')
        english_clone_version = english_clone.get_latest()
        self.assertNotEqual(english_clone.pk, english.pk)
        self.assertNotEqual(english_clone.parent, english.parent)
        self.assertNotEqual(english_clone_version.pk, english_draft_1.pk)
        self.assertEqual(english_clone_version.version, english_draft_1.version)
        self.assertNotEqual(english_clone.parent, english.parent)

    def test_clone_page_with_locale_settings(self):
        page = Page.objects.create(name='Test')
        english = PageLocalization.objects.create(parent=page, path='test')
        english.locales.add(self.locales['en-us'])
        english.save()
        english_draft_1 = english.create_draft(owner=self.user1)

        spanish = PageLocalization.objects.create(parent=page, path='test')
        spanish.locales.add(self.locales['es-mx'])
        spanish.save()
        spanish_draft_1 = spanish.create_draft(owner=self.user1)

        page_clone = page.clone_with_localizations(
            attrs={'name': 'Test (duplicate)'},
            localizations=[
                {
                    'pk': english.pk,
                    'attrs': {'path': 'test-duplicate'},
                },
            ]
        )
        english_clone = page_clone.get_localization('en-us')
        self.assertEqual(page_clone.localizations.all().count(), 1)
        self.assertNotEqual(english_clone.pk, english.pk)
        self.assertNotEqual(english_clone.parent, english.parent)
        self.assertEqual(english_clone.parent, page_clone)
        self.assertEqual(english_clone.path, 'test-duplicate')
        self.assertEqual(page_clone.name, 'Test (duplicate)')


class BlocksPageTest(TestCase):

    def setUp(self):
        locales = setup_locales()
        User = get_user_model()
        self.user1 = User.objects.create(username='User 1')
        self.user2 = User.objects.create(username='User 2')

        page = ModelWithBlocks.objects.create(name='Test')
        english = ModelWithBlocksLocalization.objects.create(parent=page)
        english.locales.add(locales['en-us'])
        english.save()
        english = page.get_localization('en-us')
        english_draft_1 = english.create_draft(owner=self.user1)

        self.page = page
        self.english = english
        self.english_draft_1 = english_draft_1

    def test_clone_version_with_blocks(self):
        rich_text_1 = RichTextBlock.objects.create(
            title='Rich Text 1',
            body="<p>Test rich text</p>",
            parent=self.english_draft_1.blocks,
        )
        container_1 = ContainerBlock.objects.create(
            title='Container 1',
            parent=self.english_draft_1.blocks,
        )
        rich_text_2 = RichTextBlock.objects.create(
            title='Rich Text 2',
            body="<p>Test rich text</p>",
            parent=container_1,
        )

        self.english_draft_1.commit()
        english_draft_2 = self.english_draft_1.clone()
        self.assertNotEqual(self.english_draft_1, english_draft_2)
        self.assertNotEqual(self.english_draft_1.blocks, english_draft_2.blocks)
        self.assertNotEqual(list(self.english_draft_1.blocks.get_descendants()), list(english_draft_2.blocks.get_descendants()))

        # Assert the nested blocks came through correctly.
        child_1, child_2 = english_draft_2.blocks.get_children()
        child_2_child = child_2.get_children()[0]
        self.assertIsInstance(child_1, RichTextBlock)
        self.assertIsInstance(child_2, ContainerBlock)
        self.assertIsInstance(child_2_child, RichTextBlock)

        different_fields = ['id', 'tree_id', 'parent']
        for field_name in different_fields:
            with self.subTest(field_name=field_name):
                self.assertNotEqual(getattr(child_1, field_name), getattr(rich_text_1, field_name))
                self.assertNotEqual(getattr(child_2, field_name), getattr(container_1, field_name))
                self.assertNotEqual(getattr(child_2_child, field_name), getattr(rich_text_2, field_name))

        same_fields = ['title', 'lft', 'rght', 'level']
        for field_name in same_fields:
            with self.subTest(field_name=field_name):
                self.assertEqual(getattr(child_1, field_name), getattr(rich_text_1, field_name))
                self.assertEqual(getattr(child_2, field_name), getattr(container_1, field_name))
                self.assertEqual(getattr(child_2_child, field_name), getattr(rich_text_2, field_name))


    def test_clone_gallery_block(self):
        """
        Tests that a gallery's slide m2m models are cloned along with the block.
        """
        block = GalleryBlock.objects.create(title='Gallery')
        block.slides.add(GallerySlide.objects.create(title='Slide 1'))
        block.slides.add(GallerySlide.objects.create(title='Slide 2'))
        block.slides.add(GallerySlide.objects.create(title='Slide 3'))
        clone = block.clone()
        block_slides = list(block.slides.all())
        clone_slides = list(clone.slides.all())
        self.assertNotEqual(block_slides, clone_slides)
        self.assertEqual(len(block_slides), len(clone_slides))
        self.assertNotEqual(block.slides.first().pk, clone.slides.first().pk)
        self.assertNotEqual(block.slides.last().pk, clone.slides.last().pk)


class PageLocalizationUniqueLocalePathTest(TestCase):

    def setUp(self):
        self.locales = setup_locales()
        User = get_user_model()
        self.user1 = User.objects.create(username='User 1')
        self.user2 = User.objects.create(username='User 2')
        self.page1 = Page.objects.create(name='Test 1')
        self.page2 = Page.objects.create(name='Test 2')

    def test_conflicting_localization_path(self):
        localization1 = PageLocalization.objects.create(parent=self.page1, path='test')
        localization1.locales.add(self.locales['en-us'])
        localization1.save()
        self.assertTrue(PageLocalization.path_in_use_for_locales('test', [self.locales['en-us']]))

    def test_conflicting_localization_path_multiple_locales(self):
        localization1 = PageLocalization.objects.create(parent=self.page1, path='test')
        localization1.locales.add(self.locales['en-us'], self.locales['en-gb'])
        localization1.save()
        self.assertTrue(PageLocalization.path_in_use_for_locales('test', [self.locales['en-us']]))

    def test_conflicting_localizations_path_multiple_locales_2(self):
        localization1 = PageLocalization.objects.create(parent=self.page1, path='test')
        localization1.locales.add(self.locales['en-us'])
        localization1.save()
        self.assertTrue(PageLocalization.path_in_use_for_locales('test', [self.locales['en-us'], self.locales['en-gb']]))

    def test_non_conflicting_localization_path(self):
        localization1 = PageLocalization.objects.create(parent=self.page1, path='test')
        localization1.locales.add(self.locales['en-us'])
        localization1.save()
        self.assertFalse(PageLocalization.path_in_use_for_locales('test', [self.locales['en-gb']]))

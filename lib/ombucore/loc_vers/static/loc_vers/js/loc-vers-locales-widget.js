$(function() {
    var $localesFormGroups = $('select.loc-vers-locales-widget').parents('.form-group')
    $localesFormGroups.each(function() {
        var formGroup = this;
        initializeLocalesWidget(formGroup);
    });

    function initializeLocalesWidget(formGroup) {
        var $formGroup = $(formGroup);
        var $select = $formGroup.find('select.loc-vers-locales-widget');
        var $checkboxes = $formGroup.find('ul.loc-vers-locales-widget');

        $select
            .on('change', function() {
                var showCheckboxes = $select.val() == 'multiple';
                $checkboxes.toggle(showCheckboxes);
            })
            .trigger('change');
    }

});

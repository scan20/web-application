$(function() {

    var $checkboxes = $('[name^="localization_"][type="checkbox"]');

    $checkboxes
        .on('change', function(e) {
            var $checkbox = $(e.currentTarget);
            var pathFieldName = 'path_for_' + $checkbox.attr('name');

            $('[name="' + pathFieldName + '"]')
                .parents('.form-group')
                .toggle($checkbox.is(':checked'));
        })
        .trigger('change');

});

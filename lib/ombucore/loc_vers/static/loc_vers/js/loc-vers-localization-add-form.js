$(function() {
    $('[name="duplicate_existing"]')
        .on('change', function(e) {
            var $duplicateExisting = $(e.currentTarget);
            $('[name="source_localization"]:first')
                .parents('.form-group')
                .toggle($duplicateExisting.is(':checked'));
        })
        .trigger('change');
});

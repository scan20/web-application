"""
Concrete models.
"""
from django.db import models


class Locale(models.Model):
    """
    A model definition of locales.

    This will probalby move somewhere else.
    """
    name = models.CharField(max_length=255)
    native_name = models.CharField(max_length=255)
    code = models.CharField(max_length=12)
    lang_code = models.CharField(max_length=12)

    def __str__(self):
        return "{} {}".format(self.code, self.name)

    @classmethod
    def from_code(cls, locale_code):
        return cls.objects.get(code=locale_code)

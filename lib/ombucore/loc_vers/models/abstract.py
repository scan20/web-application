"""
Abstract model classes.

These are good starting places for creating concrete models.
"""
from django.db import models
from django.urls import reverse
from django.utils import translation
from django.core.validators import ValidationError
from ombucore.admin.fields import CustomTreeForeignKey, UrlPathField
from ombucore.loc_vers.models.mixins import InstantiatableMixin, InstanceMixin
from ombucore.loc_vers.models.mixins import LocalizableMixin, LocalizationMixin
from ombucore.loc_vers.models.mixins import VersionableMixin, VersionMixin
from ombucore.loc_vers.models import Locale
from ombucore.models.mixins import SeoMixin, BlockFieldsMixin
from ombucore.models.abstract import RootBlock


###############################################################################
# Instantiatable - not to be used on its own.
###############################################################################

class InstantiatableModel(InstantiatableMixin, models.Model):
    name = models.CharField(max_length=255, help_text="This name is administrative in nature and is only visible inside the CMS")

    class Meta:
        abstract = True

class InstantiatableModelInstance(InstanceMixin, models.Model):
    """
    An instance of an instantiatable model.
    """
    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(InstantiatableModel, related_name='children', on_delete=models.CASCADE)

    class Meta:
        abstract = True


###############################################################################
# Localizable.
###############################################################################

class LocalizableModel(LocalizableMixin, models.Model):
    """
    A model that is localizable.

    This model only really has a name and ties different localizations
    together.
    """
    name = models.CharField(max_length=255, help_text="This name is administrative in nature and is only visible inside the CMS")

    class Meta:
        abstract = True

class LocalizableModelLocalization(LocalizationMixin, models.Model):
    """
    A localization of a model.

    ForeignKey references should point to this model.
    """
    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(LocalizableModel, related_name='children', on_delete=models.CASCADE)

    class Meta:
        abstract = True


###############################################################################
# Versionable.
###############################################################################

class VersionableModel(VersionableMixin, models.Model):
    """
    A page that can have many versions.
    """
    name = models.CharField(max_length=255, help_text="This name is administrative in nature and is only visible inside the CMS")

    # Put the correct foreign key relationship in the concrete model.
    published_version = models.ForeignKey('VersionableModelVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

class VersionableModelVersion(VersionMixin, models.Model):
    """
    A version of a page.

    The actual model content resides here.

    "Committed" versions cannot be edited. A new "draft" version can be created
    from any version. Any "committed" version can be published which will make
    it viewable on the site.
    """
    title = models.CharField(max_length=255)

    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(VersionableModel, related_name='children', on_delete=models.CASCADE)

    class Meta:
        abstract = True


###############################################################################
# Localizable and Versionable.
###############################################################################

class LocVersModel(LocalizableMixin, models.Model):
    name = models.CharField(max_length=255, help_text="This name is administrative in nature and is only visible inside the CMS")

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def create_localization(self, owner=None, **localization_kwargs):
        """
        Creates a new localization object with the given locale.

        A blank version is also created and has the user as the version owner.
        """
        localization_model_class = self.children.model
        locales = localization_kwargs.pop('locales')
        localization_kwargs.update({'parent': self})
        localization = localization_model_class.objects.create(**localization_kwargs)
        for locale in locales:
            localization.locales.add(locale)
        localization.save()
        localization.create_draft(title=self.name, owner=owner)
        return localization


class LocVersModelLocalization(LocalizationMixin, VersionableMixin, models.Model):
    """
    A localization of a page.

    ForeignKey references should point to this model.
    """
    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(LocVersModel, related_name='children', on_delete=models.CASCADE)

    # Put the correct foreign key relationship in the concrete model.
    published_version = models.ForeignKey('LocVersModelLocalizationVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

    @property
    def version_title(self):
        last = self.versions.last()
        if last:
            return self.versions.last().title
        return '- None -'

class LocVersModelVersion(VersionMixin, models.Model):
    title = models.CharField(max_length=255)

    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(LocVersModelLocalization, related_name='children', on_delete=models.CASCADE)

    class Meta:
        abstract = True


###############################################################################
# Page Base models.
###############################################################################

class BasePage(LocVersModel):

    def get_localization_alternate_links(self, exclude_locale_code=None):
        links = []
        for localization in self.children.all():
            if localization.has_published():
                for locale in localization.locales.all():
                    if exclude_locale_code and locale.code == exclude_locale_code:
                        continue
                    links.append({
                        'locale_name': locale.name,
                        'locale_code': locale.code,
                        'lang_code': locale.lang_code,
                        'href': localization.get_absolute_url(locale_code=locale.code),
                    })
        return links

    def get_language_selector_links(self):
        links = []
        for locale in Locale.objects.all():
            localization = self.children.filter(locales=locale).first()
            if localization and localization.has_published():
                links.append({
                    'locale_name': locale.name,
                    'locale_code': locale.code,
                    'lang_code': locale.lang_code,
                    'native_name': locale.native_name,
                    'href': localization.get_absolute_url(locale_code=locale.code),
                })
            else:
                with translation.override(locale.code):
                    links.append({
                        'locale_name': locale.name,
                        'locale_code': locale.code,
                        'lang_code': locale.lang_code,
                        'native_name': locale.native_name,
                        'href': reverse('homepage'),
                    })
        return links

    class Meta:
        abstract = True

class BasePageLocalization(LocVersModelLocalization):
    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(BasePage, related_name='children', on_delete=models.CASCADE)

    # Put the correct foreign key relationship in the concrete model.
    published_version = models.ForeignKey('BasePageVersion', related_name='+', blank=True, null=True, on_delete=models.SET_NULL)

    path = UrlPathField(
        'Path',
        max_length=200,
        blank=True,
        help_text='The desired URL path for this page.  The path may include letters, numbers, hyphens, and forward slashes (e.g., ‘about/our-history’).'
    )

    class Meta:
        abstract = True

    @classmethod
    def path_in_use_for_locales(cls, path, locales, exclude_pk=None):
        """
        Used for form validations.

        This couldn't be set as a model validation since the joined locales
        won't exist until the model is saved -- so there's no way to query
        which locales _will_ be saved after the base model.
        """
        queryset = cls.objects.all()

        if exclude_pk:
            queryset = queryset.exclude(pk=exclude_pk)

        queryset = queryset.filter(
                        path=path,
                        locales__in=locales,
                    )
        if queryset.count() > 0:
            return True
        return False


    def get_absolute_url(self, locale_code=None):
        """
        Returns a localized URL for the requested locale, using the first one
        if the locale_code isn't specified.
        """
        if not locale_code:
            locale_code = self.locales.first().code

        url = None
        with translation.override(locale_code):
            if self.path == '':
                url = reverse('homepage')
            else:
                url = reverse('path', kwargs={'path': self.path})
        return url

    def get_absolute_urls(self):
        """
        Returns an array of urls, one for each locale associated with this
        localization.
        """
        return [
            self.get_absolute_url(locale_code=locale.code)
            for locale in self.locales.all()
        ]


class BasePageVersion(BlockFieldsMixin, SeoMixin, LocVersModelVersion):
    # Put the correct foreign key relationship in the concrete model.
    parent = models.ForeignKey(LocVersModelLocalization, related_name='children', on_delete=models.CASCADE)

    # Override this in the concrete model with the acutal blocks that can be used.
    blocks = CustomTreeForeignKey(
                    RootBlock,
                    label="Blocks",
                    models=[],
                    related_name='%(app_label)s_%(class)s_parent',
                    on_delete=models.CASCADE,
                )

    class Meta:
        abstract = True

    def get_absolute_url(self, locale_code=None):
        parent = self.parent
        if not locale_code:
            locale_code = parent.locales.first().code

        url = parent.get_absolute_url(locale_code=locale_code)
        if parent.published_version_id != self.id:
            # Add the version as a query string.
            return "{}?version={}".format(url, self.version)
        return url

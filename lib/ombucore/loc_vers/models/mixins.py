"""
Model mixins to support Localization and Versioning.
"""
from django.db import models
from django.core import checks
from django.core.validators import ValidationError
from django.conf import settings
from django.utils import timezone
from ckeditor.fields import RichTextField
from ombucore.admin.fields import CustomTreeForeignKey
from ombucore.loc_vers.models import Locale
from ombucore.models.mixins import CloneableMixin


###############################################################################
# Base class and base class mixins.
###############################################################################

class InstantiatableMixin(CloneableMixin):
    """
    A base class for a model that can be instantiated.

    This base class is usually unused but supports the Localizable mixin.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

    @classmethod
    def check(cls, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(_check_model_cls_has_reverse_relation(cls, 'children'))
        return errors

    @property
    def instances(self):
        return self.children

    def get_instance(self, **kwargs):
        return self.children.get(**kwargs)

    def clone(self, **kwargs):
        duplicate = super().clone(**kwargs)
        duplicate = _clone_children_if_present(self, duplicate)
        return duplicate

    def _get_child_class(self):
        return self.children.model


class LocalizableMixin(CloneableMixin):
    """
    A base class for objects that are localizable.

    This class mostly provides convenience methods for querying localizations
    of this page.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

    @classmethod
    def check(cls, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(_check_model_cls_has_reverse_relation(cls, 'children'))
        return errors

    @property
    def localizations(self):
        return self.children

    def get_locales(self):
        """
        Returns the Locale models currently used by this models children.
        """
        return [
            locale for localization in self.children.all()
            for locale in localization.locales.all()
        ]

    def get_instance(self, **kwargs):
        return self.children.get(**kwargs)

    def get_localization(self, locale_code):
        return self.children.filter(locales__code=locale_code).first()

    def is_fully_localized(self):
        """
        Whether this object is localized for all locales.
        """
        return Locale.objects.all().count() == len(self.get_locales())

    def clone(self, **kwargs):
        duplicate = super().clone(**kwargs)
        duplicate = _clone_children_if_present(self, duplicate)
        return duplicate

    def clone_with_localizations(self, **kwargs):
        """
        Clones a base model and only the listed localizations.

        The `pk` is the only required value in the localizations dicts,
        everything else will be passed along as kwargs to the localizations
        `clone()` call.

            page.clone_with_localizations(
                attrs={
                    'name': 'Page (duplicate)',
                },
                localizations=[
                    {'pk': 2, 'attrs': {'path': 'my-page'}},
                ],
            )
        """
        localizations = kwargs.pop('localizations', {})
        duplicate = super().clone(**kwargs)

        for localization_info in localizations:
            pk = localization_info.pop('pk')
            if pk:
                localization = self.localizations.get(pk=pk)
                if not 'attrs' in localization_info:
                    localization_info['attrs'] = {}
                localization_info['attrs'].update({'parent': duplicate})
                localization_duplicate = localization.clone(**localization_info)

        return duplicate

    def _get_child_class(self):
        return self.children.model


class VersionableMixin(CloneableMixin):
    """
    A base class for objects that are versionable.

    This class mostly provides convenience methods for querying versions,
    publishing and runtime checks to ensure the model implementing the
    versions themselves are compatable with the versionable base class.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    @classmethod
    def check(cls, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(_check_model_cls_has_reverse_relation(cls, 'children'))
        errors.extend(_check_model_cls_has_field(cls, 'published_version', models.ForeignKey))
        return errors

    def clean(self):
        super().clean()
        if self.published_version and not self.published_version in self.versions.all():
            raise ValidationError("The object set to `published_version` is not a child of this object.")
        if self.published_version and not self.published_version.committed:
            raise ValidationError("A version must be committed before it can be published")

    @property
    def versions(self):
        return self.children.order_by('-version')

    def get_latest(self):
        return self.children.order_by('-version').first()

    def get_version(self, version):
        return self.children.filter(version=version).first()

    def has_published(self):
        return self.published_version_id is not None

    def get_published(self):
        return self.published_version

    def publish(self, version):
        self.published_version = version
        self.save()
        return self

    def unpublish(self):
        self.published_version = None
        self.save()
        return self

    def has_draft(self):
        return self.children.filter(committed=False).exists()

    def get_draft(self):
        return self.children.filter(committed=False).first()

    def create_draft(self, **draft_attrs):
        ChildClass = self._get_child_class()
        if _class_has_field(ChildClass, 'title') and not 'title' in draft_attrs:
            draft_attrs['title'] = self._get_new_draft_title()
        draft_attrs['parent'] = self
        draft = ChildClass(**draft_attrs)
        draft.full_clean()
        draft.save()
        return draft

    def _get_new_draft_title(self):
        """
        Attempts to generate a title for a new draft by looking up the chain
        for parents with a `title` or `name` field.
        """
        ChildClass = self._get_child_class()
        new_draft_title = None
        if hasattr(self, 'title'):
            new_draft_title = self.title
        elif hasattr(self, 'name'):
            new_draft_title = self.name
        elif hasattr(self, 'parent') and hasattr(self.parent, 'title'):
            new_draft_title = self.parent.title
        elif hasattr(self, 'parent') and hasattr(self.parent, 'name'):
            new_draft_title = self.parent.name

        if new_draft_title:
            return '{} Draft'.format(new_draft_title)
        return 'Draft'

    def create_draft_from(self, version_number, **draft_attrs):
        source_version = self.get_version(version_number)
        draft_attrs['version'] = None
        draft = source_version.clone(attrs=draft_attrs)
        return draft

    def clone(self, **kwargs):
        """
        Does not include a version history except for a committed version
        duplicated from the latest version with a commit message indicating the
        content unit, localization and version that it duplicates.
        """
        if not 'attrs' in kwargs:
            kwargs['attrs'] = {}

        locales = kwargs['attrs'].pop('locales', None)
        version_attrs = kwargs.pop('version_attrs', {})

        # Don't clone the locale references if locales was passed.
        if locales:
            if not 'exclude' in kwargs:
                kwargs['exclude'] = []
            kwargs['exclude'].append('locales')

        kwargs['attrs']['published_version'] = None

        duplicate = super().clone(**kwargs)

        # Then add the locales that were passed.
        if locales:
            duplicate.locales.add(*locales)

        tip_version = self.get_latest()

        version_attrs.update({
            'parent': duplicate,
            'committed': True,
            'change_description': "Duplicated from {} v{}".format(
                str(self),
                tip_version.version,
            ),
        })
        version_duplicate = tip_version.clone(attrs=version_attrs)
        return duplicate

    def _get_child_class(self):
        return self.children.model


###############################################################################
# Sub-class model mixins.
###############################################################################

class InstanceMixin(CloneableMixin):
    """
    A base class for an instance of an Instantiatable model.

    Can be a leaf node or a parent of versions.

    Page -> PageInstance
    Page -> PageInstance -> PageVersion

    Provides checks to ensure the structure of models is configured correctly.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    @classmethod
    def check(cls, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(_check_model_cls_has_field(cls, 'parent', models.ForeignKey))
        return errors


class LocalizationMixin(InstanceMixin):
    """
    A base class for localizations of a Localizable model.

    Can be a leaf node or a parent of versions.

    Page -> PageLocalization
    Page -> PageLocalization -> PageVersion

    Based on the same relationships as Instantiatable/Instance but includes
    many-to-many field to Locale.

    Unfortunately, there is no way for a Django model to validate many to many
    relationships during the model clean/validation process so this has to
    happen in a form. This means that we can't guarantee the same locale isn't
    applied to more than one localization.
    """
    locales = models.ManyToManyField(Locale)

    class Meta:
        abstract = True

    def __str__(self):
        parent_name = str(self.parent)
        locale_codes = ', '.join([locale.code for locale in self.locales.all()])
        return '{} ({})'.format(parent_name, locale_codes)


class VersionMixin(CloneableMixin):
    """
    A base class for versions of a Versionable model.

    Should always be a "leaf" node.

    Page -> PageVersion
    Page -> PageLocalization -> PageVersion

    Contains a 1-based version number relative to the parent Versionable model.

    "Committed" versions cannot be edited. A new "draft" version can be created
    from any version. Any "committed" version can be published which will make
    it viewable on the site.
    """
    version = models.IntegerField(blank=True, null=True)
    change_description = RichTextField(config_name='text_only', blank=True, null=True)
    committed = models.BooleanField(default=False)
    committed_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

    @classmethod
    def check(cls, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(_check_model_cls_has_field(cls, 'parent', models.ForeignKey))
        return errors

    def save(self, *args, **kwargs):
        self._set_version()
        if self.committed and not self.committed_at:
            self.committed_at = timezone.now()
        super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        self._clean_ensure_only_one_draft()
        self._clean_ensure_already_committed_version_cannot_be_saved()

    def publish(self):
        self.parent.publish(self)
        return self

    def unpublish(self):
        self.parent.unpublish()
        return self

    def is_published(self):
        return self.parent.published_version == self

    def is_draft(self):
        return not self.committed

    def commit(self, commit_user=None):
        self.committed = True
        if commit_user:
            self.owner = commit_user
        self.save()
        return self

    def clone(self, **kwargs):
        if not 'attrs' in kwargs:
            kwargs['attrs'] = {}

        kwargs['attrs']['version'] = None

        if not 'committed' in kwargs['attrs']:
            kwargs['attrs']['committed'] = False

        if not 'committed_at' in kwargs['attrs']:
            kwargs['attrs']['committed_at'] = None

        if not 'change_description' in kwargs['attrs']:
            kwargs['attrs']['change_description'] = None
        duplicate = super().clone(**kwargs)
        duplicate.save() # Triggers creation of root block nodes if they dont exist.
        return duplicate

    def _clean_ensure_only_one_draft(self):
        draft = self.parent.get_draft()
        if draft and draft.pk != self.pk:
            raise ValidationError("Only one draft can be present at a time.")

    def _clean_ensure_already_committed_version_cannot_be_saved(self):
        """ Prevent modifications to committed version by preventing a save. """
        if self.pk:
            already_committed = self.__class__.objects.values_list('committed', flat=True).get(pk=self.pk)
            if already_committed:
                raise ValidationError("Committed versions cannot be modified")

    def _set_version(self):
        if not self.version:
            last = self.parent.children.order_by('version').last()
            if last:
                self.version = last.version + 1
            else:
                self.version = 1


###############################################################################
# Check utility functions.
###############################################################################

def _check_model_cls_has_field(cls, field_name, field_type):
    for field in cls._meta.local_fields:
        if field.name == field_name and isinstance(field, field_type):
            return []
    return [
        checks.Error(
            "must have a {} field named '{}'".format(field_type.__name__, field_name),
            hint="Make sure the field is named correctly",
            obj=cls,
            id='ombucore.models.E001',
        )
    ]


def _check_model_cls_has_reverse_relation(cls, related_name):
    if not hasattr(cls, related_name):
        return[
            checks.Error(
                "child object must have a foreign key to this object with the related_name '{}'".format(related_name),
                hint="Make sure the 'related_name' is named correctly",
                obj=cls,
                id='ombucore.models.E002',
            )
        ]
    return []


###############################################################################
# Other utility functions.
###############################################################################

def _clone_children_if_present(model, duplicate, **child_clone_kwargs):
    if not hasattr(model, 'children'):
        return duplicate

    if not 'attrs' in child_clone_kwargs or not child_clone_kwargs['attrs']:
        child_clone_kwargs['attrs'] = {}
    child_clone_kwargs['attrs'].update({'parent': duplicate})

    for child in model.children.all():
        child_duplicate = child.clone(**child_clone_kwargs)
    return duplicate

def _class_has_field(cls, field_name):
    try:
        field = cls._meta.get_field(field_name)
    except models.FieldDoesNotExist:
        field = None
    return True if field else False

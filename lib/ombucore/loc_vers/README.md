Localization and Versioning
===========================

This app provides the building blocks to create localized and versioned models.
The main use case for this is for page models.


## Installation

Include `ombucore.loc_vers` in `INSTALLED_APPS`. At this time this is only
needed to register the `Locale` model provided in the app.


## Models

### Concrete Models

This app only provides one concrete model `Locale`. This model is used as
a foreign key or many-to-many reference for other models that are localized.

Currently, a localized site uses both the `LANGUAGES` setting and the `Locale`
model so the two should be kept in sync with the same locale codes.

### Mixins

The three levels of supporting localization and versioning are implemented in
a series of model mixins so that they can be used separately in the future,
they consist of:

- `InstantiatableMixin`: A mixin for a model that can have instances.
- `LocalizaableMixin`: Same as `InstantiatableMixin` but is intended for models
  that are localized. Includes helper methods for localizations.
- `VersionableMixin`: A mixin for a model that can have versions. Includes
  helper methods for creating, cloning and querying versions.
- `InstanceMixin`: A mixin for a model that is an instance.
- `LocalizationMixin`: Same as `InstanceMixin` but is intended for instances
  that are localized. Contains the reference to the `Locale` model.
- `VersionMixin`: A mixin for a model that is a version of another model.
  Includes version number, owner, committed, change_desription and helper
  methods for publishing/unpublishing, committing, and cloning.

### Abstract

These models are all abstract building blocks that concrete models are created
from by combining the model mixins. These include the various `parent` and
`child` relationships between the types of nodes.

The `LocVersModel`, `LocVersModelLocalization` and `LocVersModelVersion` are
the three pieces that make up the base of a localizable and versionable model.

Also included is an initial implementation of `BasePage`,
`BasePageLocalization` and `BasePageVersion` that has implementation details
that are involved with a page model, mostly having to do with URL handling and
routing.

The `LocVersModel*` and `BasePage*` abstract models are what localized and
versioned models should be based on in their concrete implementation.


## ModelAdmin

The rest of this app (views, forms, templates) supports the modeladmin for
ocalized and versioned models. These are based on the regular model admins but
have been augmented with additional urls/views to support the extra
functionality and UI needed for localized and versioned models.


## Tests

This app also includes tests that test the core localization and versioning
models and their various functions like cloning, publishing, etc.

These tests have been wired into the main ombucore `runtests.py` script.

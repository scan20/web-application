import re
from django.urls import reverse
from django.contrib import messages
from django.utils.html import format_html, strip_tags
from django.utils.safestring import mark_safe
from django.template.loader import get_template
from django.db.models import Q
from django.contrib.auth import get_permission_codename
from django.http import HttpResponseRedirect
from django.views.generic import DetailView
from django.utils.translation import gettext as _
import django_filters
from ombucore.admin.filterset import FilterSet
from ombucore.admin.actionlink import ActionLink
from ombucore.admin.views.mixins import PanelUIMixin
from ombucore.admin.views.base import AddView, ChangeView, ChangelistView
from ombucore.admin.views.base import FormView
from ombucore.loc_vers.forms import LocVersBaseAddForm, LocVersBaseModelWithPathCloneForm
from ombucore.loc_vers.forms import LocVersLocalizationAddForm
from ombucore.loc_vers.forms import LocVersVersionInfoForm, LocVersVersionCommitForm
from ombucore.admin import panel_commands as commands
from ombucore.admin.buttons import LinkButton, SubmitButton, CancelButton, BackLink
from ombucore.admin.buttons import ButtonGroup
from ombucore.admin.sites import site
from ombucore.utils import extend_subclass


###############################################################################
# Base model views
# Used for the "root" model in a localization/versioning hierarchy.
# E.g. Page.
###############################################################################

class LocVersBaseAddView(AddView):
    form_class = LocVersBaseAddForm
    supertitle = 'Create a new'
    buttons = [
        SubmitButton(text='Create'),
        CancelButton(),
    ]

    def get_success_commands(self):
        info = site.related_info_for(self.object)
        info_url = reverse(self.model_admin.url_for('info'), args=[self.object.pk])
        return [
            commands.NotifyOpener({
                'operation': 'saved',
                'info': info,
            }),
            commands.Redirect(info_url)
        ]


class LocVersBaseChangeView(ChangeView):
    buttons = [
        SubmitButton(text='Save'),
        CancelButton(),
    ]


class LocVersBaseInfoView(PanelUIMixin, DetailView):
    template_name = 'loc_vers/base-model-info.html'

    def get_supertitle(self):
        return self.model._meta.verbose_name

    def get_title(self):
        return self.object.name

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['base_model'] = self.object
        kwargs['localizations'] = list(self.object.localizations.all())

        add_localization_perm = self.localization_permission_codename('add')
        if self.request.user.has_perm(add_localization_perm):
            add_localization_urlname = self.model_admin.admin_site.url_for(self.get_localization_model(), 'add')
            kwargs['add_localization_url'] = '{path}?parent_id={base_model_id}'.format(
                path=reverse(add_localization_urlname),
                base_model_id=self.object.id,
            )

        kwargs['buttons'] = self.get_buttons()

        return kwargs

    def get_buttons(self):
        return [
            LinkButton(
                text='Rename',
                style='default',
                href=reverse(self.model_admin.url_for('change'), args=[self.object.pk]),
                align='right',
            ),
            ButtonGroup(
                LinkButton(
                    text='Clone',
                    href=reverse(self.model_admin.url_for('clone'), args=[self.object.pk]),
                    reload_on=None,
                ),
                LinkButton(
                    text='Delete',
                    style='danger',
                    href=reverse(self.model_admin.url_for('delete'), args=[self.object.pk]),
                    panels_trigger=False,
                    attrs={
                        'onclick': 'Panels.open($(this).attr("href")).then(Panels.current.resolve); return false;',
                    },
                ),
                text='Operations',
            )
        ]

    def get_localization_model(self):
        return self.model.children.rel.related_model

    def localization_permission_codename(self, permission_action):
        localization_opts = self.get_localization_model()._meta
        return '%s.%s' % (localization_opts.app_label, get_permission_codename(permission_action, localization_opts))

    def post(self, request, *args, **kwargs):
        method = request.POST.get('method', None)
        if method == 'create_draft_from_latest':
            localization_cls = self.get_localization_model()
            localization = localization_cls.objects.get(pk=request.POST.get('pk'))
            return self.create_draft_from_latest(localization)
        return super().post(request, *args, **kwargs)

    def create_draft_from_latest(self, localization):
        latest_version = localization.get_latest()
        draft = localization.create_draft_from(latest_version.version, owner=self.request.user)
        messages.success(self.request, 'Draft successfully created.')
        self.model_admin.log(
            actor=self.request.user,
            action='Created',
            obj=draft,
            message=f'Draft created for {draft.title}',
        )
        redirect_url = reverse(self.model_admin.admin_site.url_for(draft, 'change'), args=[draft.pk])
        return HttpResponseRedirect(redirect_url)


class LocVersBaseCloneView(FormView):
    title = 'Clone'
    form_class = LocVersBaseModelWithPathCloneForm
    success_message = '<strong>%(name)s</strong> cloned.'

    buttons = [
        SubmitButton(text='Clone'),
        CancelButton(),
    ]

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.object = self.get_object()

    def get_object(self):
        return self.model.objects.get(pk=self.kwargs['pk'])

    def get_supertitle(self):
        return "{} / {}".format(
            self.model._meta.verbose_name,
            self.object.name,
        )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['object'] = self.object
        return kwargs

    def form_valid(self, form):
        source_object = self.object
        duplicate = form.clone()
        self.object = duplicate
        self.model_admin.log(
            actor=self.request.user,
            action='Cloned',
            obj=duplicate,
            message=f'{self.object.name} cloned from {source_object.name}.',
        )
        return super().form_valid(form)

    def get_success_commands(self):
        change_url = reverse(self.model_admin.admin_site.url_for(self.object, 'change'), args=[self.object.pk])
        return [
            commands.CloseCurrentAndRedirectOpener(change_url),
        ]


###############################################################################
# Localization model.
# Used for the "localization" model in a localization/versioning hierarchy.
# E.g. PageLocalization.
###############################################################################

class LocVersLocalizationAddView(AddView):
    form_class = LocVersLocalizationAddForm
    supertitle = 'Create a new localization for'
    buttons = [
        SubmitButton(text='Create', disable_when_form_unchanged=False),
        BackLink(text='Cancel', style='cancel'),
    ]

    def get_title(self):
        base_model = self.get_base_model_instance()
        return base_model.name

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.GET.get('parent_id'):
            base_model = self.get_base_model_instance()
            if base_model:
                kwargs['initial']['parent'] = base_model
                kwargs['disabled_locales'] = [
                    locale.pk for locale in base_model.get_locales()
                ]
        return kwargs

    def get_success_commands(self):
        version = self.object.get_latest()
        version_change_url = reverse(self.model_admin.admin_site.url_for(version, 'change'), args=[version.pk])
        return [
            commands.NotifyOpener({
                'operation': 'saved',
                'info': site.related_info_for(self.object),
            }),
            commands.Redirect(version_change_url),
        ]

    def get_base_model_instance(self):
        base_model_class = self.get_base_model_class()
        if self.request.POST.get('parent'):
            return base_model_class.objects.get(pk=self.request.POST.get('parent'))
        elif self.request.GET.get('parent_id'):
            return base_model_class.objects.get(pk=self.request.GET.get('parent_id'))
        return None

    def get_base_model_class(self):
        return self.model.parent.field.related_model


class LocalizationObjectViewMixin():
    """
    Mixin to provide UI values for views that have a `self.model` that is an
    instance of a Localization.
    """

    def get_supertitle(self):
        base_model_class = self.get_base_model_class()
        base_model_instance = self.get_base_model_instance()
        base_model_route = self.model_admin.admin_site.url_for(base_model_class, 'info')
        base_model_url = reverse(base_model_route, args=[base_model_instance.pk])
        return format_html('{} / <a href="{}">{}</a> / {}',
            base_model_class._meta.verbose_name,
            base_model_url,
            base_model_instance.name,
            'Localization',
        )

    def get_title(self):
        locales = self.object.locales.all()
        return ', '.join([locale.name for locale in locales])

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['title'] = self.get_title()

        localization = self.object
        template = get_template('loc_vers/_localization-header-auxiliary.html')
        kwargs['panel_head_auxiliary'] = template.render({
            'localization': localization,
        })

        kwargs['tabs'] = self.model_admin.get_tabs_for(self.object)
        return kwargs

    def get_base_model_instance(self):
        return self.object.parent

    def get_base_model_class(self):
        return self.model.parent.field.related_model

    def get_version_model_class(self):
        return self.model.children.rel.related_model


class LocVersLocalizationChangeView(LocalizationObjectViewMixin, ChangeView):
    buttons = [
        SubmitButton(text='Save changes'),
        CancelButton(),
    ]


class VersionHistoryFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    method='search_filter',
                    help_text='',
                )

    class Meta:
        fields = ['search', 'owner']

    def search_filter(self, queryset, name, value):
        version_number = None
        digit_matches = re.findall('^(\d+)', value)
        version_string_matches = re.findall('^v(\d+)', value)
        if len(digit_matches):
            version_number = int(digit_matches[0])
        elif len(version_string_matches):
            version_number = int(version_string_matches[0])

        if version_number:
            return queryset.filter(
                Q(version=version_number) | Q(change_description__icontains=value)
            )
        else:
            return queryset.filter(change_description__icontains=value)

class LocVersLocalizationVersionHistoryView(LocalizationObjectViewMixin, ChangelistView):
    filterset_class = VersionHistoryFilterSet
    list_display = (
        ('display_version', 'Version'),
        ('display_change_description', 'Change Description'),
        ('display_owner', 'Owner'),
        ('display_date_committed', 'Date Committed'),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Inject the model into the filterset.
        self.filterset_class = extend_subclass(self.filterset_class, 'Meta', {
                                    'model': self.get_version_model_class(),
                                })

    def get(self, *args, **kwargs):
        self.localization_pk = kwargs.pop('pk')
        self.object = self.get_object()
        return super().get(*args, **kwargs)

    def get_object(self):
        return self.model.objects.get(pk=self.localization_pk)

    def get_queryset(self):
        version_class = self.get_version_model_class()
        return version_class.objects.filter(parent=self.object).order_by('-version')

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        del kwargs['tabs']
        del kwargs['panel_head_auxiliary']
        return kwargs

    def display_version(self, obj):
        if obj.committed:
            if self.object.published_version == obj:
                icon = get_template('svg/published.svg').render()
                return format_html('<span title="Published" class="gutter-icon">{icon}</span><span class="gutter-icon-label">v{version_number}</span>'.format(
                    icon=icon,
                    version_number=obj.version,
                ))
            else:
                return format_html('<span class="gutter-icon-label">v{}</span>'.format(obj.version))
        else:
            return 'Draft'

    def display_change_description(self, obj):
        if obj.change_description:
            return format_html(
                '<span class="filter-list-title-truncated inline-truncated-contents">{desc}</span>',
                desc=strip_tags(obj.change_description),
            )
        return None

    def display_owner(self, obj):
        if obj.owner:
            return str(obj.owner)
        return ''

    def display_date_committed(self, obj):
        if obj.committed:
            return obj.committed_at
        return mark_safe('<span class="none"><em>Not committed</em></span>')  # nosec

    def get_object_action_links(self, obj):
        if obj.committed:
            version_info_url = reverse(self.model_admin.admin_site.url_for(obj, 'info'), args=[obj.pk])
        else:
            version_info_url = reverse(self.model_admin.admin_site.url_for(obj, 'info_change'), args=[obj.pk])
        return [
            ActionLink(
                text='Info',
                href=version_info_url,
            ),
        ]




###############################################################################
# Version model.
# Used for the "version" model in a localization/versioning hierarchy.
# E.g. PageVersion.
###############################################################################

class LocVersVersionChangeView(ChangeView):

    def get_buttons(self):
        buttons = []
        if self.object.committed:
            # Committed version.
            if not self.object.parent.has_draft():
                buttons.append(SubmitButton(
                    text='Create a draft from this version',
                    align='right',
                    method='create_draft',
                    disable_when_form_unchanged=False,
                ))
            if self.object.is_published():
                buttons.append(SubmitButton(
                    text='Unpublish',
                    style='danger',
                    align='right',
                    method='unpublish_version',
                    disable_when_form_unchanged=False,
                    attrs={
                        'onclick': 'return confirm("' + _("Are you sure you want to unpublish this version?") + '");',
                    },
                ))
            else:
                buttons.append(SubmitButton(
                    text='Publish',
                    style='success',
                    align='right',
                    method='publish_version',
                    disable_when_form_unchanged=False,
                    attrs={
                        'onclick': 'return confirm("' + _("Are you sure you want to publish this version?") + '");',
                    },
                ))
            return buttons
        else:
            # Editable draft.
            buttons = [
                SubmitButton(text='Save changes'),
                CancelButton(),
                SubmitButton(
                    text='Save & commit draft',
                    method='save_and_commit',
                    align='right',
                    disable_when_form_unchanged=False,
                ),
            ]
            if self.object.parent.children.count() > 1:
                buttons.append(SubmitButton(
                    text='Discard draft',
                    style='danger',
                    align='right',
                    method='discard_draft',
                    disable_when_form_unchanged=False,
                    confirmation_text='Are you sure you want to discard this draft?',
                ))
            return buttons

    def get_supertitle(self):
        base_model_class = self.get_base_model_class()
        base_model_instance = self.get_base_model_instance()
        base_model_route = self.model_admin.admin_site.url_for(base_model_class, 'info')
        base_model_url = reverse(base_model_route, args=[base_model_instance.pk])
        return format_html('{} / <a href="{}">{}</a> / {}',
            base_model_class._meta.verbose_name,
            base_model_url,
            base_model_instance.name,
            'Localization',
        )

    def get_title(self):
        locales = self.get_localization_model_instance().locales.all()
        return ', '.join([locale.name for locale in locales])

    def get_subtitle(self):
        if self.object.committed:
            return 'v{}'.format(self.object.version)
        else:
            return 'Draft'

    def get_subtitle_auxiliary(self):
        version = self.object
        localization = version.parent
        template = get_template('loc_vers/_version-subtitle-auxiliary.html')
        return template.render({
            'version': version,
            'version_prev': localization.get_version(version.version - 1),
            'version_next': localization.get_version(version.version + 1),
            'localization': localization,
        })

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['title'] = self.get_title()
        localization = self.get_localization_model_instance()
        template = get_template('loc_vers/_localization-header-auxiliary.html')
        kwargs['panel_head_auxiliary'] = template.render({
            'localization': localization,
        })
        kwargs['tabs'] = self.model_admin.get_tabs_for(self.object)
        return kwargs

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs()
        if self.object.committed:
            kwargs['readonly'] = True
        return kwargs

    def get_success_commands(self):
        if self.request.POST.get('method', None) == 'save_and_commit':
            return [
                commands.Redirect(reverse(self.model_admin.url_for('commit'), args=[self.object.pk]))
            ]
        return []

    def get_localization_model_instance(self):
        return self.object.parent

    def get_localization_model_class(self):
        return self.model.parent.field.related_model

    def get_base_model_instance(self):
        return self.object.parent.parent

    def get_base_model_class(self):
        return self.get_localization_model_class().parent.field.related_model

    def create_draft(self):
        version = self.get_object()
        localization = version.parent
        draft = localization.create_draft_from(version.version, owner=self.request.user)
        messages.success(self.request, 'Draft successfully created.')
        self.model_admin.log(
            actor=self.request.user,
            action='Created',
            obj=draft,
            message=f'Draft created',
        )
        redirect_url = reverse(self.model_admin.url_for('change'), args=[draft.pk])
        return HttpResponseRedirect(redirect_url)

    def discard_draft(self):
        version = self.get_object()
        localization = version.parent
        version.delete()
        redirect_version = localization.get_latest()
        messages.success(self.request, 'Draft discarded.')
        self.model_admin.log(
            actor=self.request.user,
            action='Discarded',
            obj=version,
            message=f'Draft discarded for {self.get_title()}',
        )
        redirect_url = reverse(self.model_admin.url_for('change'), args=[redirect_version.pk])
        return HttpResponseRedirect(redirect_url)

    def publish_version(self):
        version = self.get_object()
        version.publish()
        messages.success(self.request, 'v{} published'.format(version.version))
        self.model_admin.log(
            actor=self.request.user,
            action='Published',
            obj=version,
            message=f'v{version.version} for {self.get_title()}.',
        )
        redirect_url = reverse(self.model_admin.url_for('change'), args=[version.pk])
        return HttpResponseRedirect(redirect_url)

    def unpublish_version(self):
        version = self.get_object()
        version.unpublish()
        messages.success(self.request, 'v{} unpublished'.format(version.version))
        self.model_admin.log(
            actor=self.request.user,
            action='Unpublished',
            obj=version,
            message=f'v{version.version} unpublished for {self.get_title()}.',
        )
        redirect_url = reverse(self.model_admin.url_for('change'), args=[version.pk])
        return HttpResponseRedirect(redirect_url)


class LocVersVersionInfoMixin():
    def get_supertitle(self):
        return 'Version Info'

    def get_title(self):
        return self.object.title


class LocVersVersionInfoView(LocVersVersionInfoMixin, ChangeView):
    form_class = LocVersVersionInfoForm


class LocVersVersionInfoChangeView(LocVersVersionInfoMixin, ChangeView):
    form_class = LocVersVersionInfoForm


class LocVersVersionCommitView(LocVersVersionInfoMixin, ChangeView):
    form_class = LocVersVersionCommitForm

    def get_buttons(self):
        return [
            SubmitButton(text='Commit', disable_when_form_unchanged=False),
            SubmitButton(
                text='Commit & Publish',
                style='success',
                attrs={'name': 'submit', 'value': 'commit_and_publish'},
                disable_when_form_unchanged=False,
            ),
            LinkButton(
                text='Cancel',
                href=reverse(self.model_admin.url_for('change'), args=[self.object.pk]),
                style='cancel',
                panels_trigger=False,
            ),
        ]

    def form_valid(self, form):
        self.object = form.save()
        self.object.committed = True
        self.object.save()
        title = self.object.title
        self.model_admin.log(
            actor=self.request.user,
            action='Committed',
            obj=self.object,
            message=f'v{self.object.version} committed for {title}.',
        )
        if self.request.POST.get('submit', None) == 'commit_and_publish':
            self.object.publish()
            messages.success(self.request, '<strong>{}</strong> was successfully committed and published.'.format(title))
            self.model_admin.log(
                actor=self.request.user,
                action='Published',
                obj=self.object,
                message=f'v{self.object.version} published for {title}.',
            )
        else:
            messages.success(self.request, '<strong>{}</strong> was successfully committed.'.format(title))
        redirect_url = reverse(self.model_admin.url_for('change'), args=[self.object.pk])
        return HttpResponseRedirect(redirect_url)


class LocVersVersionInfoView(LocVersVersionInfoMixin, PanelUIMixin, DetailView):
    template_name = 'loc_vers/version-info.html'

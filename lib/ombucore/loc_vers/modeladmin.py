import django_filters
from django.urls import reverse
from django.conf.urls import url
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.template.loader import get_template
from ombucore.utils import extend_subclass
from ombucore.admin.actionlink import ActionLink
from ombucore.admin.filterset import FilterSet
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.views import ChangelistSelectViewMixin
from ombucore.loc_vers.forms import LocVersLocalizationChangeUrlForm, LocVersLocalizationChangeLocalesForm
from ombucore.loc_vers.forms import LocVersVersionForm, LocVersBaseModelWithPathAddForm
from ombucore.loc_vers.forms import LocVersLocalizationWithPathAddForm
from ombucore.loc_vers import views


class LocVersBaseModelFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='name',
                    lookup_expr='icontains',
                    help_text='',
                )
    order_by = django_filters.OrderingFilter(
                choices=(
                    ('-updated_at', 'Updated (newest first)'),
                    ('updated_at', 'Updated (oldest first)'),
                    ('-created_at', 'Created (newest first)'),
                    ('created_at', 'Created (oldest first)'),
                ),
                empty_label=None,
                )

    class Meta:
        fields = ['search',]

class LocVersBaseModelAdmin(ModelAdmin):
    """
    ModelAdmin class used for the "Base" model in a localizable, versionable
    data model.
    """
    add_view = views.LocVersBaseAddView
    change_view = views.LocVersBaseChangeView
    change_form_config = {
        'fields': ('name',)
    }
    info_view = views.LocVersBaseInfoView
    clone_view = views.LocVersBaseCloneView
    filterset_class = LocVersBaseModelFilterSet
    list_display = [
        ('name', 'Name'),
        ('display_locales_published', 'Published Localizations'),
        ('display_locales_unpublished', 'Unpublished Localizations'),
        ('updated_at', 'Last Updated'),
    ]

    def get_changelist_object_action_links(self, obj):
        action_links = []
        info_url = self.url_for('info')
        if info_url:
            action_links.append(ActionLink(
                text='Open',
                href=reverse(info_url, args=[obj.pk])
            ))
        return action_links

    def display_locales_published(self, obj):
        return ', '.join(
            [
                locale.code
                for localization in obj.children.filter(published_version__isnull=False)
                for locale in localization.locales.all()
            ]
        )

    def display_locales_unpublished(self, obj):
        return ', '.join(
            [
                locale.code
                for localization in obj.children.filter(published_version__isnull=True)
                for locale in localization.locales.all()
            ]
        )

    def _initialize_views(self):
        super()._initialize_views()

        if self.info_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
            }
            self.info_view = type(str('%sInfoView' % self.model.__name__), (self.info_view,), opts)

        if self.clone_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
            }
            self.clone_view = type(str('%sCloneView' % self.model.__name__), (self.clone_view,), opts)

    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        urlpatterns = super().get_urls()

        if self.info_view:
            info_view_opts = {}
            info_view = self._wrap_view_with_permission(self.prepare_view(self.info_view, **info_view_opts), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/info/$', info_view, name='%s_%s_info' % info),
            )

        if self.clone_view:
            clone_view_opts = {}
            clone_view = self._wrap_view_with_permission(self.prepare_view(self.clone_view, **clone_view_opts), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/clone/$', clone_view, name='%s_%s_clone' % info),
            )

        return urlpatterns


class LocVersBaseModelWithPathAdmin(LocVersBaseModelAdmin):
    add_form_class = LocVersBaseModelWithPathAddForm


class LocVersLocalizationModelFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='title',
                    lookup_expr='icontains',
                    help_text='',
                )

    class Meta:
        fields = ['search',]


class LocVersLocalizationModelAdmin(ModelAdmin):
    """
    ModelAdmin class used for the "localization" model in
    a localizable/versionable data model.
    """
    add_view = views.LocVersLocalizationAddView
    change_view = False
    change_url_view = views.LocVersLocalizationChangeView
    change_url_form_class = LocVersLocalizationChangeUrlForm
    change_locales_view = views.LocVersLocalizationChangeView
    change_locales_form_class = LocVersLocalizationChangeLocalesForm
    version_history_view = views.LocVersLocalizationVersionHistoryView
    version_history_select_view = None
    filterset_class = LocVersLocalizationModelFilterSet
    list_display = [
        ('version_title', 'Title'),
        ('display_locales', 'Locales Served'),
        ('display_published', 'Published'),
        ('updated_at', 'Last Updated'),
    ]

    def display_locales(self, localization):
        return ', '.join(
            [
                locale.code for locale in localization.locales.all()
            ]
        )

    def display_published(self, localization):
        if localization.has_published():
            icon = get_template('svg/published.svg').render()
            version_number = localization.get_published().version
            return format_html('{icon} <span>v{version_number}</span>',
                        icon=icon,
                        version_number=version_number,
                    )
        else:
            return 'No'

    def get_changelist_action_links(self):
        # No "Create" link.
        return []

    def _initialize_form_classes(self):
        super()._initialize_form_classes()
        self.change_url_form_class = extend_subclass(self.change_url_form_class, 'Meta', {'model': self.model})
        self.change_locales_form_class = extend_subclass(self.change_locales_form_class, 'Meta', {'model': self.model})

    def _initialize_views(self):
        super()._initialize_views()

        if self.change_url_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
                'form_class': self.change_url_form_class,
            }
            self.change_url_view = type(str('%sChangeUrlView' % self.model.__name__), (self.change_url_view,), opts)

        if self.change_locales_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
                'form_class': self.change_locales_form_class,
            }
            self.change_locales_view = type(str('%sChangeLocalesView' % self.model.__name__), (self.change_locales_view,), opts)

        if self.version_history_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
            }
            self.version_history_view = type(str('%sVersionHistoryView' % self.model.__name__), (self.version_history_view,), opts)

        if self.version_history_select_view != False and self.version_history_view:
            self.version_history_select_view = type(str('%sVersionHistorySelectView' % self.model.__name__), (ChangelistSelectViewMixin, self.version_history_view,), {})

    def get_change_url_view(self):
        return self.change_url_view

    def get_change_locales_view(self):
        return self.change_locales_view

    def get_version_history_view(self):
        return self.version_history_view

    def get_version_history_select_view(self):
        return self.version_history_select_view

    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        urlpatterns = super().get_urls()

        change_locales_view = self.get_change_locales_view()
        if change_locales_view:
            change_locales_view_opts = {
                'supertitle': 'Localization',
                'success_redirect_urlname': 'change_locales',
            }
            change_locales_view = self._wrap_view_with_permission(self.prepare_view(change_locales_view, **change_locales_view_opts), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/change/locales/$', change_locales_view, name='%s_%s_change_locales' % info),
            )

        change_url_view = self.get_change_url_view()
        if change_url_view:
            change_url_view_opts = {
                'supertitle': 'URL',
                'success_redirect_urlname': 'change_url',
            }
            change_url_view = self._wrap_view_with_permission(self.prepare_view(change_url_view, **change_url_view_opts), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/change/url/$', change_url_view, name='%s_%s_change_url' % info),
            )

        version_history_view = self.get_version_history_view()
        if version_history_view:
            version_history_view = self._wrap_view_with_permission(self.prepare_view(version_history_view), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/version-history/$', version_history_view, name='%s_%s_version_history' % info),
            )

        version_history_select_view = self.get_version_history_select_view()
        if version_history_select_view:
            version_history_select_view = self._wrap_view_with_permission(self.prepare_view(version_history_select_view), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/version-history/select/$', version_history_select_view, name='%s_%s_version_history_select' % info),
            )

        return urlpatterns

    def get_change_action_links(self, localization):
        return []

    def get_tabs_for(self, localization):
        recent_version = localization.get_latest()
        return [
            {
                'title': 'Versioned Content',
                'url': '#',
                'url': reverse(self.admin_site.url_for(recent_version, 'change'), args=[recent_version.pk]),
                'icon': 'svg/edit.svg',
            },
            {
                'title': 'Locales Served',
                'url': reverse(self.url_for('change_locales'), args=[localization.pk]),
                'icon': 'svg/localization.svg',
            },
            {
                'title': 'URL',
                'url': reverse(self.url_for('change_url'), args=[localization.pk]),
                'icon': 'svg/url.svg',
            },
        ]


class LocVersLocalizationWithPathModelAdmin(LocVersLocalizationModelAdmin):
    add_form_class = LocVersLocalizationWithPathAddForm


class LocVersVersionModelAdmin(ModelAdmin):
    """
    ModelAdmin class used for the "version" model in a localizable/versionable
    data model.
    """
    form_class = LocVersVersionForm
    add_view = False
    changelist_view = False
    delete_view = False
    change_view = views.LocVersVersionChangeView
    info_view = views.LocVersVersionInfoView
    info_change_view = views.LocVersVersionInfoChangeView
    commit_view = views.LocVersVersionCommitView

    def get_change_action_links(self, version):
        # No "View" link.
        return []

    def get_info_view(self):
        return self.info_view

    def get_info_change_view(self):
        return self.info_change_view

    def get_commit_view(self):
        return self.commit_view

    def _initialize_views(self):
        super()._initialize_views()

        if self.info_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
            }
            self.info_view = type(str('%sInfoView' % self.model.__name__), (self.info_view,), opts)

        if self.info_change_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
                'form_class': extend_subclass(self.info_change_view.form_class, 'Meta', {'model': self.model})
            }
            self.info_change_view = type(str('%sInfoChangeView' % self.model.__name__), (self.info_change_view,), opts)

        if self.commit_view != False:
            opts = {
                '__module__': self.model.__module__,
                'model': self.model,
                'model_admin': self,
                'form_class': extend_subclass(self.commit_view.form_class, 'Meta', {'model': self.model})
            }
            self.commit_view = type(str('%sCommitView' % self.model.__name__), (self.commit_view,), opts)

    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        urlpatterns = super().get_urls()

        info_view = self.get_info_view()
        if info_view:
            info_view = self._wrap_view_with_permission(self.prepare_view(info_view), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/info/$', info_view, name='%s_%s_info' % info),
            )

        info_change_view = self.get_info_change_view()
        if info_change_view:
            info_change_view = self._wrap_view_with_permission(self.prepare_view(info_change_view), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/info/change/$', info_change_view, name='%s_%s_info_change' % info),
            )

        commit_view = self.get_commit_view()
        if commit_view:
            commit_view = self._wrap_view_with_permission(self.prepare_view(commit_view), 'change')
            urlpatterns.append(
                url(r'^(?P<pk>\d+)/commit/$', commit_view, name='%s_%s_commit' % info),
            )

        return urlpatterns


    def get_tabs_for(self, version):
        localization = version.parent
        return [
            {
                'title': 'Versioned Content',
                'url': reverse(self.url_for('change'), args=[version.pk]),
                'icon': 'svg/edit.svg',
            },
            {
                'title': 'Locales Served',
                'url': reverse(self.admin_site.url_for(localization, 'change_locales'), args=[localization.pk]),
                'icon': 'svg/localization.svg',
            },
            {
                'title': 'URL',
                'url': reverse(self.admin_site.url_for(localization, 'change_url'), args=[localization.pk]),
                'icon': 'svg/url.svg',
            },
        ]

    def admin_overlay_info_for(self, obj, user=None):
        version = obj
        localization = obj.parent
        base_model = localization.parent
        version_info = self.related_info_for(obj)

        overlay_info = super().admin_overlay_info_for(obj, user=user)

        base_model_route = self.admin_site.url_for(base_model, 'info')
        overlay_info['items'] = [
            ('This', version_info['verbose_name']),
            ('Name', format_html('<a href="{}" data-panels-trigger>{}</a>',
                            reverse(base_model_route, args=[base_model.pk]),
                            base_model.name,
                        )),

        ]

        locale_links = []
        for locale in base_model.get_locales():
            l = base_model.get_localization(locale.code)
            if l:
                if l == localization:
                    locale_links.append(format_html('<span title="{}">{}</span>',
                        locale.name,
                        locale.code,
                    ))
                else:
                    locale_links.append(format_html('<a href={} title="{}">{}</a>',
                        l.get_absolute_url(),
                        locale.name,
                        locale.code,
                    ))
        overlay_info['items'].append(('Locales', mark_safe(', '.join(locale_links))))  # nosec

        overlay_info['extra_markup'] = get_template('loc_vers/_admin-overlay--versions-info.html').render({
            'version_published': localization.get_published(),
            'version_latest': localization.get_latest(),
            'version_current': version,
            'version_prev': localization.get_version(version.version - 1),
            'version_next': localization.get_version(version.version + 1),
            'history_url': reverse(self.admin_site.url_for(localization, 'version_history_select'), args=[localization.pk]),
            'info_url': reverse(self.url_for('info'), args=[version.pk]) if version.committed else reverse(self.url_for('info_change'), args=[version.pk]),
            'localization': localization,
        })
        return overlay_info

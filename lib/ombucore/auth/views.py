from django.conf import settings
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.template.response import TemplateResponse
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import REDIRECT_FIELD_NAME, login, logout
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.contrib.auth.views import PasswordResetView as DjangoPasswordResetView
from django.contrib.auth.views import LoginView as DjangoLoginView
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.tokens import default_token_generator


class LoginView(DjangoLoginView):
    template_name = 'auth/login.html'
    redirect_authenticated_user = True

    def get_redirect_url(self):
        redirect_to = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, '')
        )
        url_is_safe = is_safe_url(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure(),
        )
        return redirect_to if url_is_safe else resolve_url(settings.LOGIN_REDIRECT_URL)


class PasswordResetView(DjangoPasswordResetView):
    email_template_name='auth/emails/password_reset_email.txt'
    template_name='registration/password_reset_form.html'
    html_email_template_name='auth/emails/password_reset_email.html'
    subject_template_name='auth/emails/password_reset_subject.txt'
    form_class=PasswordResetForm
    success_url=reverse_lazy('password_reset_done')
    extra_email_context={'base_url': settings.BASE_URL,}


@user_passes_test(lambda u: u.has_perm('auth.change_user'))
def admin_change_password(request, *args, **kwargs):
    """
    View endpoint for a site administrator to send a change password email for
    a given user.

    The user's `email` should be in request.GET.
    """
    form = PasswordResetForm(request.GET)
    if form.is_valid():
        opts = {
            'use_https': request.is_secure(),
            'token_generator': default_token_generator,
            'from_email': None,
            'email_template_name': 'auth/emails/password_reset_email.txt',
            'html_email_template_name': 'auth/emails/password_reset_email.html',
            'subject_template_name': 'auth/emails/password_reset_subject.txt',
            'request': request,
        }
        form.save(**opts)
        messages.success(request, "Password reset email sent!")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    messages.error(request, "There was an error sending a password reset email.")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

Auth
====

This Django app provides common log in/out, password reset and admin UI classes
for managing Users.

## Installation

Add `ombucore.auth` to `INSTALLED_APPS` and include the app's urls in your
project's `urls.py` file:

    urlpatterns = [
        ...
        url(r'^', include('ombucore.auth.urls')),
        ...
    ]

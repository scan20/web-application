from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.forms import widgets
import django_filters
from ombucore.admin.modeladmin import ModelAdmin
from ombucore.admin.filterset import FilterSet
from ombucore.auth.forms import UserCreationForm, UserChangeForm


class UserFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='username',
                    lookup_expr='icontains',
                    help_text='',
                )

    groups = django_filters.ModelMultipleChoiceFilter(
                label='Role',
                queryset=Group.objects.all(),
                widget=widgets.CheckboxSelectMultiple,
            )

    active = django_filters.ChoiceFilter(
                field_name='is_active',
                choices=(
                    ('1', 'Yes'),
                    ('0', 'No'),
                ),
                widget=widgets.RadioSelect,
            )

    class Meta:
        fields = ['search', 'groups', 'active']
        model = get_user_model()


class UserAdmin(ModelAdmin):
    filterset_class = UserFilterSet
    add_form_class = UserCreationForm
    change_form_class = UserChangeForm
    list_display = (
        ('display_full_name', 'Name'),
        ('username', 'Username'),
        ('display_role', 'Role'),
        ('last_login', 'Last Login'),
        ('is_active', 'Active'),
    )

    def display_full_name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name)

    def display_role(self, obj):
        try:
            return obj.groups.first().name
        except Exception:
            pass
        return None

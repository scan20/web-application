from django.apps import AppConfig
from django.utils.module_loading import autodiscover_modules

class AuthenticationConfig(AppConfig):

    name = 'ombucore.auth'
    label = 'ombucore_auth'
    verbose_name = 'Authentication'

    def ready(self):
        autodiscover_modules('auth')

import urllib
from django import forms
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth import forms as auth_forms
from django.contrib.auth.models import Group
from django.utils.safestring import mark_safe
from ombucore.htmloutput_field import HtmlOutputField


class UserGroupsFormMixin(object):

    def __init__(self, **kwargs):
        if 'groups' in kwargs['initial'] and len(kwargs['initial']['groups']):
            kwargs['initial']['groups'] = self.initial['groups'][0]
        else:
            self.initial['groups'] = None
        super(UserGroupsFormMixin, self).__init__(**kwargs)


def _password_reset_button(form, *args, **kwargs):
    link = f'<a class="btn btn-primary" href="{reverse("admin_change_password")}?email={urllib.parse.quote_plus(form.instance.email)}">Send Password Reset Email</a>'
    return mark_safe(link)  # nosec


class UserChangeForm(auth_forms.UserChangeForm):
    groups = forms.ModelChoiceField(label='Role', queryset=Group.objects.all().order_by('pk'))
    password = HtmlOutputField(
        render_fn=_password_reset_button,
        help_text="Raw passwords are not stored on this website so there is no way to see this user's existing password. Click this button to send the user an email containing  link they can use to reset their password."
    )

    def __init__(self, **kwargs):
        self.user = kwargs.pop('user', None)
        if 'instance' in kwargs:
            kwargs['initial']['groups'] = kwargs['instance'].groups.first()
        super(UserChangeForm, self).__init__(**kwargs)
        self.fields['password'].required = False
        self.fields['groups'].required = True
        self.fields['email'].required = True

    def clean_groups(self):
        group = self.cleaned_data['groups']
        return [group]

    class Meta:
        model = get_user_model()
        fields = (
            'username', 'first_name', 'last_name', 'email', 'groups', 'is_active', 'password'
        )
        fieldsets = (
            ('Basic', {'fields': ('first_name', 'last_name', 'username', 'email',)}),
            ('Status', {'fields': ('groups', 'is_active',)}),
            ('Privacy', {'fields': ('password',)}),
        )


class UserCreationForm(auth_forms.UserCreationForm):
    groups = forms.ModelChoiceField(label='Role', queryset=Group.objects.all().order_by('pk'))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        if 'groups' in kwargs['initial'] and len(kwargs['initial']['groups']):
            kwargs['initial']['groups'] = kwargs['initial']['groups'][0]
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['groups'].required = True

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return username
        raise forms.ValidationError("A user with that username already exists")

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            get_user_model().objects.get(email=email)
        except get_user_model().DoesNotExist:
            return email
        raise forms.ValidationError("A user with that email already exists")

    def clean_groups(self):
        group = self.cleaned_data['groups']
        return [group]

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=commit)
        # Get many-2-many fields (like groups) to save.
        if commit and hasattr(self, 'save_m2m'):
            self.save_m2m()
        return user

    class Meta:
        model = get_user_model()
        fields = ('groups', 'username', 'first_name', 'last_name', 'email',
                  'is_active', 'password1', 'password2')
        fieldsets = (
            ('Basic', {'fields': ('first_name', 'last_name', 'username', 'email',)}),
            ('Status', {'fields': ('groups', 'is_active',)}),

            ('Privacy', {'fields': ('password1', 'password2',)}),
        )

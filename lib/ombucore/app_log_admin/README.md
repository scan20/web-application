App Log OMBU Core Implementation
================================

Implements the [`app_log`](https://bitbucket.org/ombu/app_log) functionality
into the OMBU Core admin system.


Provides a ModelAdmin, forms and views to support viewing and filtering the log
as well as administering log subscriptions.

# Django==2.2 # Commented out here so the actual version can be set by the project.
psycopg2==2.7.5
django-filter==2.1.0
simplejson==3.13.2
Pillow==6.0.0
django-imagekit==4.0.2
beautifulsoup4==4.6.0
micawber==0.3.5
django-basicauth==0.5.2

# this has been forked due to bitbucket dropping support for mecurial
git+https://bitbucket.org/ombu/django-taggit-autosuggest-ombu.git@master#egg=django-taggit-autosuggest

django-mptt==0.9.0
django-ckeditor==5.6.1
django-polymorphic==2.0.2
django-polymorphic-tree==1.5
django-gm2m==0.5

# django-sortedm2m==1.5.0 # Commented out while we use the OMBU fork for Django 2.2 support
git+https://github.com/ombu/django-sortedm2m.git@master#egg=django-sortedm2m

django-any-urlfield==2.6.1
git+https://github.com/ombu/django_cloneable.git@master#egg=django_cloneable

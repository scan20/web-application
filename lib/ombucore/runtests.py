#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner

sys.path.append(os.path.abspath('..'))

if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'runtests_settings'
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests([
        "loc_vers.tests",
    ])
    sys.exit(bool(failures))

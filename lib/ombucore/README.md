OMBU Core
=========

This package provides all the OMBU Django apps and customizations.


## Installation

Install all the python dependencies with pip:

    $ pip install -r requirements/development.txt

Add the apps you'd like to use to `INSTALLED_APPS` and include the ombucore
urls in your project's `urls.py`.

    urlpatterns = [
        url(r'^', include('ombucore.urls')),
    ]


## Overview

### Base Settings

This package includes some default settings that configure Django to OBMU's
standards. These things include apps, middleware, CKEditor configuration, and
base permissions. To use these defaults, simiply import them into your settings
file and overwrite as needed:

    from ombucore.settings import *

    DATABASES['default']['NAME'] = 'ombudemo'
    DATABASES['default']['USER'] = 'root'
    DATABASES['default']['PASSWORD'] = 'meh'
    DATABASES['default']['HOST'] = 'localhost'


### BuildCommand base class

There's a management command at `ombucore.build.BuildCommand` that handles
dropping and creating the database as well as cleaning up assets so that
classes extending it can focus on the site-specific build functionality.


### Admin

Provides admin site functionality based on the Panels UI. See `admin/README.md`
for more.

### Auth

Provides common log in/out, password reset and user management views. See
`auth/README.md` for more.

### Pages

Provides a polymorphic Page model with publishing and SEO fields. These pages
contain polymorphic Block models as their content. See `pages/README.md` for
more.

### Menus

Provides an abstract menu model and admin classes to be used and instantiated
in other apps. See `menus/README.md` for more.

### Assets

Provides polymorphic Image, Video, Document, models and embedding. See
`assets/README.md` for more.

### Redirects

Provides a model, middleware, and an admin UI to manage site redirects.  See
`redirects/README.md` for more.

### Imagewidget

Provides an image upload widget that can immediatly show a preview. See
`imagewidget/README.md` for more.

### Theme

This app provides templates and static assets that will commonly be used or 
inherited from in the project app.

#### Project-Specific Theme Overrides for Panels

To override colors or fonts for specific projects, create a new `panels-inside.scss` 
file in your project that imports OMBU Core's `panels-inside-base` file. Add any 
desired variables from `_overrides.scss` and include that in `panels-inside.scss`.
Compile this file into `panels-inside.css`. Override `html-panel.html` in your project
and include your new `panels-inside.css` instead of the CSS file from OMBU Core.

### Actions

A way to create action items on the admin panel that are not supported by database models.


## Testing

Since this is a series of portable packages, the tests need to be run outside
of a Django application. A test runner has been set up to bootstrap Django and
run tests without `./manage.py`.

    $ ./runtests.py

from django.utils.translation import gettext_lazy as _

DEBUG = True

ALLOWED_HOSTS = []

UNAUTHENTICATED_USER = None

INSTALLED_APPS = [
    'ombucore.loc_vers',
    'ombucore.menus',
    'ombucore.assets',
    'ombucore.imagewidget',
    'ombucore.redirects',
    'ombucore.actions',
    'ombucore.admin',
    'ombucore.auth',
    'ombucore.theme',
    'ckeditor',
    'polymorphic_tree',
    'polymorphic',
    'mptt',
    'gm2m',
    'imagekit',
    'any_urlfield',
    'taggit_autosuggest',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ombucore.basicauth.BasicAuthMiddleware',
    'ombucore.redirects.middleware.ModelRedirectMiddleware',
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'website.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {}
}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

BASICAUTH_DISABLE = True

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

# To enable localization, set LOCALIZATION to True and add more tuples to LANGUAGES.
LOCALIZATION = False
USE_I18N = True
LOCALIZATION_PREFIX_DEFAULT = False
LANGUAGES = (
    ('en-us', _('English (US)')),
)
LANGUAGE_CODE = 'en-us'
LOCALIZATION_DEFAULT = 'en-us'

TIME_ZONE = 'America/Los_Angeles'

USE_I18N = True

USE_L10N = True

USE_TZ = True


CKEDITOR_CONFIGS = {
    'default': {
        'theme': 'default',
        'toolbar': [
            [
                'Bold', 'Italic', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'BulletedList', 'NumberedList', 'Link', 'Unlink', 'Anchor', 'Table', 'Ombublockquote',
                'HorizontalRule', 'Format', 'Styles'
            ],
            '/', # New line
            [
                'Source', 'RemoveFormat', 'Ombuimage', 'Ombuvideo', 'Ombudocument', 'Blockquote',
            ],
        ],
        'format_tags': 'p;h2;h3;h4;h5;h6;pre',
        'removeDialogTabs': 'link:upload;link:advanced',
        'removeButtons': 'BrowseServer',
        'forcePasteAsPlainText': True,
        'stylesSet': [
            {'name': 'Lead', 'element': 'p', 'attributes': {'class': 'lead'}},
            {'name': 'Button', 'element': 'a', 'attributes': {'class': 'btn'}},
            {'name': 'Call to action', 'element': 'a', 'attributes': {'class': 'cta'}},
            {'name': 'Blockquote-left', 'element': 'blockquote', 'attributes': {'class': 'pull-left'}},
            {'name': 'Blockquote-right', 'element': 'blockquote', 'attributes': {'class': 'pull-right'}},
        ],
        'extraPlugins': 'autogrow,ombuimage,ombuvideo,ombudocument',
        'autoGrow_minHeight': 200,
        'autoGrow_maxHeight': 300,
        'bodyClass': 'rte',
        'width': '100%',
        'disableNativeSpellChecker': False,
        'dialog_backgroundCoverColor': 'rgb(160, 160, 160)',
        'contentsCss': '/static/wysiwyg.css',
    },
    'text_only': {
        'theme': 'default',
        'toolbar': [
            ['Bold', 'Italic', 'BulletedList', 'NumberedList', 'Link',
            'Unlink', 'Format', 'RemoveFormat', 'Source'],
        ],
        'format_tags': 'p',
        'removeDialogTabs': 'link:upload;link:advanced',
        'removeButtons': 'BrowseServer',
        'forcePasteAsPlainText': True,
        'stylesSet': [],
        'extraPlugins': 'autogrow',
        'autoGrow_minHeight': 200,
        'autoGrow_maxHeight': 300,
        'bodyClass': 'rte',
        'width': '100%',
        'disableNativeSpellChecker': False,
        'dialog_backgroundCoverColor': 'rgb(160, 160, 160)',
        'contentsCss': '/static/wysiwyg.css',
    }
}

GROUP_PERMISSIONS = {
    'Administrator': [
        'add_assetfolder',
        'change_assetfolder',
        'delete_assetfolder',
        'add_imageasset',
        'change_imageasset',
        'delete_imageasset',
        'add_videoasset',
        'change_videoasset',
        'delete_videoasset',
        'add_documentasset',
        'change_documentasset',
        'delete_documentasset',
        'add_page',
        'change_page',
        'delete_page',
        'add_richtextblock',
        'change_richtextblock',
        'delete_richtextblock',
        'add_assetblock',
        'change_assetblock',
        'delete_assetblock',
        'add_redirect',
        'change_redirect',
        'delete_redirect',
        'access_admin_central',
        'send_test_email',
    ],
    'Content Editor': [
        'add_assetfolder',
        'change_assetfolder',
        'delete_assetfolder',
        'add_imageasset',
        'change_imageasset',
        'delete_imageasset',
        'add_videoasset',
        'change_videoasset',
        'delete_videoasset',
        'add_documentasset',
        'change_documentasset',
        'delete_documentasset',
        'add_page',
        'change_page',
        'delete_page',
        'add_richtextblock',
        'change_richtextblock',
        'delete_richtextblock',
        'add_assetblock',
        'change_assetblock',
        'delete_assetblock',
        'add_redirect',
        'change_redirect',
        'delete_redirect',
        'access_admin_central',
        'send_test_email',
    ],
}

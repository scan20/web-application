from decimal import Decimal
from django.db import models
from django_cloneable.models import CloneableMixin as DjangoCloneableMixin
from django_cloneable.models import ModelCloneHelper as DjangoCloneableModelCloneHelper
from gm2m.relations import GM2MRelation, GM2MRel
from taggit.managers import TaggableManager
from taggit.models import TaggedItem

from ombucore.admin.fields import CustomTreeForeignKey


###############################################################################
# Clone Blocks Mixin
###############################################################################

class BlockFieldsMixin():
    """
    Functionality common to models that have fields that are block trees.

    Specifically, this means that the field is an instance of
    CustomTreeForeignKey.

    Overrides the `save()` method to auto-create the root block node if it
    doesn't exist yet.

    Overrides the `clone()` method to also duplicate any blocks in the blocks
    fields (CustomTreeForeignKey) of the model.
    """
    def save(self, **kwargs):
        super().save(**kwargs)
        for field in self._block_fields:
            if not getattr(self, field.name):
                setattr(self, field.name, field.related_model.objects.create())

    def clone(self, **kwargs):
        duplicate = super().clone(**kwargs)
        duplicate = self._clone_blocks_to(duplicate)
        return duplicate

    def _clone_blocks_to(self, to_model):
        for field in self._block_fields:
            root_node = getattr(self, field.name)
            if root_node:
                root_node_copy = root_node.clone()
                setattr(to_model, field.name, root_node_copy)
        to_model.save()
        return to_model

    @property
    def _block_fields(self):
        return [
            field for field in self._meta.fields
            if isinstance(field, CustomTreeForeignKey)
        ]


###############################################################################
# Cloneable MPTT mixin.
###############################################################################

class ModelCloneHelper(DjangoCloneableModelCloneHelper):

    def _clone_copy_m2m(self, duplicate, exclude=None):
        """
        Overwritten to remove the faulty Django <= 1.11 / Django => 2 code
        handling.
        """
        exclude = exclude or []
        # copy.copy loses all ManyToMany relations.
        for field in self.instance._meta.many_to_many:
            # Skip this field.
            if field.name in exclude:
                continue
            # handle m2m using through
            remote_field = _get_remote_field(field)
            if remote_field.through and not remote_field.through._meta.auto_created:
                # through-model must be cloneable
                if hasattr(remote_field.through, 'clone'):
                    qs = remote_field.through._default_manager.filter(
                        **{field.m2m_field_name(): self.instance})
                    for m2m_obj in qs:
                        m2m_obj.clone(attrs={
                            field.m2m_field_name(): duplicate
                        })
                elif hasattr(field, 'm2m_field_name'):
                    qs = remote_field.through._default_manager.filter(
                        **{field.m2m_field_name(): self.instance})
                    for m2m_obj in qs:
                        # TODO: Allow switching to different helper?
                        m2m_clone_helper = ModelCloneHelper(m2m_obj)
                        m2m_clone_helper.clone(attrs={
                            field.m2m_field_name(): duplicate
                        })

            elif isinstance(field, TaggableManager):
                tags = getattr(self.instance, field.attname).all()
                for tag in tags:
                    TaggedItem.objects.create(content_object=duplicate, tag=tag)
            # <<< OMBU Modified
            elif isinstance(field, GM2MRelation):
                # Don't do anything for generic relations.
                pass
            # OMBU Modified >>>

            # normal m2m, this is easy
            else:
                objs = getattr(self.instance, field.attname).all()
                try:
                    # Django <= 1.11
                    setattr(duplicate, field.attname, objs)
                except TypeError:
                    # Django 2
                    getattr(duplicate, field.name).set(objs)

    def _clone_copy_reverse_m2m(self, duplicate, exclude=None):
        exclude = exclude or []
        qs = [
            f for f in self.instance._meta.get_fields(include_hidden=True)
            if f.many_to_many and f.auto_created
        ]
        for relation in qs:
            # handle m2m using through
            remote_field = _get_remote_field(relation.field)
            if (
                    remote_field.through and
                    not remote_field.through._meta.auto_created):
                # Skip this field.
                # TODO: Not sure if this is the right value to check for..
                if remote_field.related_name in exclude:
                    continue
                # through-model must be cloneable
                if hasattr(remote_field.through, 'clone'):
                    qs = remote_field.through._default_manager.filter(**{
                        relation.field.m2m_reverse_field_name(): self.instance
                    })
                    for m2m_obj in qs:
                        m2m_obj.clone(attrs={
                            relation.field.m2m_reverse_field_name(): duplicate
                        })
                else:
                    qs = remote_field.through._default_manager.filter(**{
                        relation.field.m2m_reverse_field_name(): self.instance
                    })
                    for m2m_obj in qs:
                        # TODO: Allow switching to different helper?
                        m2m_clone_helper = ModelCloneHelper(m2m_obj)
                        m2m_clone_helper.clone(attrs={
                            relation.field.m2m_reverse_field_name(): duplicate
                        })
            # <<< OMBU Modified
            elif isinstance(remote_field, GM2MRel):
                # Don't do anything for generic relations.
                pass
            # OMBU Modified >>>

            # normal m2m, this is easy
            else:
                # Skip this field.
                if remote_field.related_name in exclude:
                    continue
                objs_rel_manager = getattr(
                    self.instance,
                    remote_field.related_name)
                objs = objs_rel_manager.all()
                try:
                    # Django <= 1.11
                    setattr(duplicate, remote_field.related_name, objs)
                except TypeError:
                    # Django 2
                    getattr(duplicate, remote_field.related_name).set(objs)

class CloneableMixin(DjangoCloneableMixin):
    """
    Extended to auto-clone m2m fields if they are "add only" and not used as
    real m2m relationships.

    Supports the form widget to create child objects before the parent is saved.
    """
    CLONE_HELPER_CLASS = ModelCloneHelper

    class Meta:
        abstract = True

    def clone(self, attrs=None, commit=True, m2m_clone_reverse=True, exclude=None):
        """
        Default implementation clones the join models in m2m references,
        leaving the source objects as they were, as expected with m2m objects.

        We want to clone the m2m `slides` instead so new instances are created
        and associated with this object. This is accomplished in two parts:

        1. `slides` is excluded from the original `super().clone()` operation.
            This prevents the original copy of the join models during the clone.
        2. `slide.clone()` is called with the `m2m_clone_reverse=False` kwarg.
            Without this, the cloned slides will be added to BOTH the original
            and duplicate instances.
        """
        clone_on_clone_m2m = self._get_clone_on_clone_m2m_fields()

        exclude = exclude or []
        exclude.extend([field.name for field in clone_on_clone_m2m])
        duplicate = super().clone(attrs=attrs, commit=commit, m2m_clone_reverse=m2m_clone_reverse, exclude=exclude)

        for field in clone_on_clone_m2m:
            duplicate_m2ms = [
                m.clone(m2m_clone_reverse=False)
                for m in getattr(self, field.name).all()
            ]
            getattr(duplicate, field.name).set(duplicate_m2ms)

        return duplicate

    def _get_clone_on_clone_m2m_fields(self):
        fields = []
        for field in self._meta.many_to_many:
            if getattr(field, 'clone_on_clone', False):
                fields.append(field)
        return fields


class CloneableMPTTMixin():
    """
    Recursively clones all the children of a MPTT model.

    Should be used alongside CloneableMixin.

        class Block(MPTTModel, CloneableMPTTMixin, CloneableMixin):
            pass
    """
    def clone(self, **kwargs):
        self.refresh_from_db()
        # Exclude the MPTT fields so they get set fresh when `.move_to()` is
        # called in `_clone_children()`.
        kwargs['exclude'] = kwargs.get('exclude') or []
        kwargs['exclude'].extend(['lft','rght','tree_id','level', 'parent'])
        new_block = super().clone(**kwargs)
        self._clone_children(new_block, self)
        return new_block

    def _clone_children(self, new_parent, parent):
        for node in parent.get_children():
            new_child_node = node.clone()
            new_child_node.move_to(new_parent, 'last-child')
            new_parent.refresh_from_db()
            new_child_node.refresh_from_db()


def _get_remote_field(field):
    if hasattr(field, 'remote_field'):
        # Django 2
        return field.remote_field
    elif hasattr(field, 'rel'):
        # Django <= 1.11
        return field.rel
    return None

###############################################################################
# SEO mixin.
###############################################################################

class SeoMixin(models.Model):
    class Meta:
        abstract = True

    CHANGEFREQ_CHOICES = (
        ('always', 'Always'),
        ('hourly', 'Hourly'),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('yearly', 'Yearly'),
        ('never', 'Never'),
    )
    PRIORITY_CHOICES = (
        (Decimal("0.0"), '0.0'),
        (Decimal("0.1"), '0.1'),
        (Decimal("0.2"), '0.2'),
        (Decimal("0.3"), '0.3'),
        (Decimal("0.4"), '0.4'),
        (Decimal("0.5"), '0.5'),
        (Decimal("0.6"), '0.6'),
        (Decimal("0.7"), '0.7'),
        (Decimal("0.8"), '0.8'),
        (Decimal("0.9"), '0.9'),
        (Decimal("1.0"), '1.0'),
    )
    seo_title = models.CharField(
        'Title',
        max_length=255,
        help_text='A web page’s title element can dramatically influence its indexing & ranking by search engines.  You may provide a custom title up to 50 characters in length.  Otherwise, the page’s title will be used.  <a href="https://support.google.com/webmasters/answer/35624?hl=en#3" target="_blank">Authoring&nbsp;tips&nbsp;›</a>',
        blank=True,
        null=True)
    seo_description = models.CharField(
        'Description',
        max_length=255,
        blank=True,
        null=True,
        help_text='A web page’s meta description element can influence its indexing & ranking by search engines.  You may provide a custom description up to 160 characters in length.  Otherwise, the page’s summary will be used.  <a href="https://support.google.com/webmasters/answer/35624?hl=en#1" target="_blank">Authoring&nbsp;tips&nbsp;›</a>')
    robots_noindex = models.BooleanField('No Index', default=False)
    robots_nofollow = models.BooleanField('No Follow', default=False)
    xmlsitemap_include = models.BooleanField(
        'Include in Sitemap', default=True)
    xmlsitemap_changefreq = models.CharField(
        'Sitemap Change Frequency',
        max_length=100,
        choices=CHANGEFREQ_CHOICES,
        default='yearly',
        help_text='How frequently the page is likely to change. <a href="https://www.sitemaps.org/protocol.html#changefreqdef" target="_blank">More&nbsp;information&nbsp;›</a>',
    )
    xmlsitemap_priority = models.DecimalField(
        'Sitemap Priority',
        max_digits=2,
        decimal_places=1,
        choices=PRIORITY_CHOICES,
        default=0.5,
        help_text='The priority of this content relative to other content on your site. <a href="https://www.sitemaps.org/protocol.html#prioritydef" target="_blank">More&nbsp;information&nbsp;›</a>',
    )

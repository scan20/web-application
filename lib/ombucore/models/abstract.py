from django.db import models
from django.utils.translation import gettext_lazy as _
from polymorphic_tree.models import PolymorphicMPTTModel, PolymorphicTreeForeignKey
from ckeditor.fields import RichTextField
from ombucore.admin.fields import ForeignKey
from ombucore.models.mixins import CloneableMPTTMixin, CloneableMixin
from ombucore.assets.models import Asset, ImageAsset, VideoAsset, DocumentAsset


###############################################################################
# Blocks.
###############################################################################

class BlockBase(PolymorphicMPTTModel, CloneableMPTTMixin, CloneableMixin):
    template_name = 'blocks/block.html'
    WIDTH_CHOICES = (
        (1, '8%'),
        (2, '17%'),
        (3, '25%'),
        (4, '33%'),
        (5, '42%'),
        (6, '50%'),
        (7, '58%'),
        (8, '67%'),
        (9, '75%'),
        (10, '83%'),
        (11, '92%'),
        (12, '100%'),
    )
    OFFSET_CHOICES = (
        (0, '0'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10'),
        (11, '11'),
        (12, '12'),
    )

    title = models.CharField(max_length=100)
    parent = PolymorphicTreeForeignKey('self', blank=True, null=True, related_name='children', verbose_name=_('parent'), on_delete=models.CASCADE)
    hide_title = models.BooleanField(default=False)
    width = models.IntegerField(
        default=12,
        choices=WIDTH_CHOICES,
    )
    offset = models.IntegerField(
        default=0,
        choices=OFFSET_CHOICES,
    )

    @property
    def base_verbose_name(self):
        return 'Block'

    def get_class_name(self):
        return self.__class__.__name__

    @property
    def allowed_children(self):
        return []

    class Meta:
        abstract = True
        verbose_name = 'Block'
        verbose_name_plural = 'Blocks'


class RootBlock(BlockBase):
    class Meta:
        abstract = True
        verbose_name = 'Root Block'
        verbose_name_plural = 'Root Blocks'


class RichTextBlock(BlockBase):
    template_name = 'blocks/rich-text.html'
    body = RichTextField(blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name = 'Text'
        verbose_name_plural = 'Text'


class AssetBlock(BlockBase):
    template_name = 'blocks/asset.html'
    asset = ForeignKey(Asset, models=(ImageAsset, VideoAsset, DocumentAsset), on_delete=models.PROTECT)

    class Meta:
        abstract = True
        verbose_name = 'Asset'
        verbose_name_plural = 'Asset'

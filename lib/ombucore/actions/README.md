### Overview
The `actions` app allows us to place action items on the admin panel without creating models for them. For instance, if we want to allow users to send test emails, and we don't plan on saving those test emails, we don't need to create some `TestEmail` model. In fact, such functionality doesn't really fit into other parts of OMBUCore (`assets`, `pages`, `menu`, `redirects`, etc). So we created this `actions` app to encapsulate it.

### Adding a New Action
1. Create a form in `forms.py` which contains the fields, fieldsets, and methods needed for this action
2. Create a permission to complete the action in the `models.py` `ActionPermissions` class
3. Create a view for the action in `views.py`.
    - Be sure to subclass your view from our `ombucore.admin.views.FormView`
    - Point to the form class you just created using the `form_class` attribute
    - Also note that in the `form_valid()` method you can tell the app what to
      do once the form validates. For instance you can go ahead and send that
      email or show a success or error message.
4. Add the action in `admin.py` by subclassing from the `Action` model (which is located in `models.py`) and defining several attributes:
    - `url` - the url path of the action item (i.e. `'send-test-email'`)
    - `name` - the item title that will display on the admin panel (i.e. `'Test email'`)
    - `link_text` - the text of the link that will appear under the item title; user clicks on this link to go to the item's form panel (i.e. `'Send a test email'`)
    - `view` - the view class you just created in `views.py`. This is not a string, this is the actual class (i.e. `SendTestEmailView`)
    - `perm` - the permission you just made for this item in `models.py` `ActionPermissions` (i.e. `'actions.send_test_email'`)
5. Register the action in `admin.py` like so: `admin.site.register_action(SendTestEmail)`
6. Go to `ombucore/settings.py` and add the permission you created to GROUP_PERMISSIONS. To make it take effect, run the `build` and `manage_group_permissions` commands from ombudemo.

### How Does This Work?
`AdminSite` now has an `_action_registry` which the action was added to when you called `register_action()` in `ombucore/actions/admin.py`. When an action item is added to the registry, `AdminSite` can then use `get_urls` to add it to the site's URLs. In `ombucore/admin/sites.py` under the `AdminCentralView` class, there is a function called `get_action_group()` which scans the `_action_registry` of the `AdminSite`. It adds actions to the admin panel in this way. The entire registering and admin site process is designed to mirror the process for registering and showing models.


from django.db import models


class ActionPermissions(models.Model):
    """
    Empty model class that isn't managed so it won't create a database table.

    Used primarily to add arbitrary permissions that aren't associated with a 
    model.
    
    MAKE SURE you add your permission to ombucore/settings.py, too. Then run the
    build and configure_group_permissions commands to instantiate it.
    """
    class Meta:
        managed = False  # Don't create a database table
        permissions = (
            ('send_test_email', 'Send Test Email'),
        )


class Action(object):
    """
    Contains all the information we need to add an action to the admin.

    url: The action's URL --> 'some-url'
    view: The action's view --> MyActionView.as_view()
    name: The actions' name --> 'My Action'
    link_text: The text for the link under the action --> 'Complete my action'
    perm: The permission required to complete the action --> 'actions.complete_my_action'

    Note, the permission must be added to the ActionPermissionsModel and settings.py. Then
    you must run 'python manage.py build' and 'python manage.py configure_group_permissions'.
    """

    url = 'action-url' 
    view = None
    name = 'My Action'
    link_text = 'Complete my action'
    perm = 'actions.some_perm'

# -*- coding: utf-8 -*-

from django.shortcuts import render
from ombucore.admin.views import FormView
from ombucore.actions.forms import SendTestEmailForm
from django.contrib import messages

"""
Create a view for your form here. Be sure to import your form, subclass
FormView, and set the form_class to your form. This points to the template
'ombucore/admin/templates/panel-form.html'

form_valid tells the view what to do after the form is valid. This is where you
run your form methods and show any success or error messages.
"""


class SendTestEmailView(FormView):
    title = 'Test Email'
    supertitle = 'Send'
    form_class = SendTestEmailForm

    def form_valid(self, form):
        status, result = form.send_email()
        if not status:
            messages.error(self.request, 'There was an error sending the message: ' + str(result))
        else:
            messages.success(self.request, 'Message sent!')
        return super(SendTestEmailView, self).form_valid(form)

import smtplib
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django import forms
from ombucore.admin.buttons import SubmitButton, CancelButton

"""
Think of this as the functional basis for Actions. Since Actions don't have underlying models, the form must
provide all fields and methods to support the Action you want the user to take.

To use your form, import it into views.py, create a FormView for your action, and set that view's form_class to the
form you imported.
"""


class SendTestEmailForm(forms.Form):
    test_emails = forms.CharField(help_text='Comma-separated list of email addresses.')
    buttons = [
        SubmitButton(text='Send'),
        CancelButton(),
    ]

    class Meta:
        fieldsets = (
            ('Email', {
                'intro': 'Send a test email to the following address(es).',
                'fields': ('test_emails',)
            }),
        )

    def send_email(self):
        test_emails = self.cleaned_data['test_emails'].split(',')
        subject = 'Test email'
        text = render_to_string('emails/email-base.txt')
        html = render_to_string('emails/email-base.html')
        from_email = settings.DEFAULT_FROM_EMAIL
        try:
            return True, send_mail(
                    subject,
                    text,
                    from_email,
                    test_emails,
                    html_message=html
                )
        except smtplib.SMTPException as e:
            return False, str(e)

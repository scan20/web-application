from django.contrib import admin
from ombucore.admin.sites import site as admin_site
from ombucore.actions.models import Action
from ombucore.actions.views import SendTestEmailView

"""
Subclass "Action", fill in its attributes (look at the Action class in models.py
or read the actions README to find out more), and register it with the admin site
using "register_action()".

Registering in this way triggers the AdminSite class in ombucore/admin/sites.py
to correctly add this action's URL/view to the panel. It also triggers the
AdminCentralView (also in ombucore/admin/sites.py) to show this action item.

Item not showing up? Make sure you added its permission ("perm") to ActionPermissions
in models.py and to ombucore/settings.py and ran the build and configure_group_permissions
commands.
"""


# Sends a test email to one or several comma separated emails.
class SendTestEmail(Action):
    url = 'send-test-email'
    name = 'Test Email'
    link_text = 'Send a test email'
    view = SendTestEmailView
    perm = 'actions.send_test_email'


admin_site.register_action(SendTestEmail)
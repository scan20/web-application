# -*- coding: utf-8 -*-
from django.utils.deprecation import MiddlewareMixin
from basicauth.basicauthutils import validate_request
from basicauth.response import HttpResponseUnauthorized

class BasicAuthMiddleware(MiddlewareMixin):
    """
    Subclassed to allow view exclusion via a view decorator.

    Uses the functionality from the `django-basicauth` package but implements
    the validation check in `process_view` instead of `process_request` so
    that the view function is available to check against for exclusion.
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        if getattr(view_func, '_basicauth_exclude', False) == True:
            return None
        if not validate_request(request):
            return HttpResponseUnauthorized()
        return None

def basicauth_exclude(fn):
    """
    Decorator to exclude the decorated view from the BasicAuthMiddleware.
    """
    fn._basicauth_exclude = True
    return fn

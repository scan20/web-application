import django_filters
from ombucore.admin.modeladmin.base import ModelAdmin
from ombucore.admin.filterset import FilterSet

class SimplePageFilterSet(FilterSet):
    search = django_filters.CharFilter(
                    field_name='title',
                    lookup_expr='icontains',
                    help_text='',
                )
    published = django_filters.ChoiceFilter(
                    field_name='published',
                    choices=(
                        (None, '- Choose -'),
                        (1, 'Yes'),
                        (0, 'No'),
                    ),
                )

    class Meta:
        fields = ['search', 'published']


class SimplePageAdmin(ModelAdmin):
    filterset_class = PageFilterSet
    list_display = [
        ('title', 'Title'),
        ('last_modified', 'Last Updated'),
        ('published', 'Published'),
    ]

CKEDITOR.dialog.add('ombuvideo', function(editor) {

  var util = CKEDITOR.ombuutil;

  function update(dialog, objInfo) {
    var contentElement = dialog.getContentElement('main', 'video_id');
    var domElement = contentElement.getElement();
    if (objInfo && objInfo.id) {
      contentElement.setValue(objInfo);
      domElement.setHtml('<div class="preview"><img src="' + objInfo.image_url + '" /></div>');
      domElement.findOne('> *')
          .setStyle('max-width', '100%')
          .setStyle('max-height', '300px');
    }
    else {
      contentElement.setValue(null);
      domElement.setHtml("");
    }
  }

  function validateRequired(missing_msg) {
    return function() {
      if (!this.getValue()) {
        alert(missing_msg);
        return false;
      }
      return true;
    }
  }

  return {
    title: 'Video',
    minWidth: 300,
    minHeight: 'auto',
    contents: [
      {
        id: 'main',
        label: 'Video',
        elements: [
          {
            type: 'html',
            html: '<div></div>',
            id: 'video_id',
            setup: function(widget) {
              update(this.getDialog(), widget.data.objInfo);
            },
            commit: function(widget) {
              widget.setData('objInfo', this.getValue());
            },
            validate: validateRequired("Please select a video.")
          },
          {
            type: 'button',
            label: 'Select a Video',
            onClick: function() {
              var el = this;
              var dialog = el.getDialog();
              Panels.open('/panels/assets/videoasset/select/').then(function(data) {
                update(dialog, data.info);
              });
            }
          }
        ]
      }
    ],
    onLoad: function() {
      var dialog = this;

      // Add a class for targeting styles.
      dialog.parts.dialog.addClass('ombuvideo-dialog');
    }
  };
});

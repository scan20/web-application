SECRET_KEY = 'fake-key'
INSTALLED_APPS = [
    "ombucore.loc_vers.tests", # Loads the models for the loc_vers tests.
    "ombucore.loc_vers",
    "ombucore.assets",
    'django.contrib.auth',
    "django.contrib.contenttypes",
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'ombudemo',
        'USER': 'root',
        'PASSWORD': 'meh',
        'HOST': 'localhost',
    }
}

#! /bin/sh

# Ensure `www-data` owns any directories in media, so the uwsgi process can write
# into them
find media -type d -exec chown www-data {} \;

# Start uWSGI
uwsgi --http-auto-chunked --http-keepalive --static-map \
     /static=/var/www/static --static-map /media=/var/www/media \
     --static-gzip-ext css --static-gzip-ext js \
     --mimefile /etc/mime.types
